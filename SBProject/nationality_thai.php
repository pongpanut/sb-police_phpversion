<?php namespace SB\index;	
if(!isset($_SESSION)) { 
   session_start(); 
}
use SB\Controller\Controller;
include  'Controller/Controller.php';
$Controller = new Controller();
$page_type_id =$_SESSION['pagetype'];
?>
<!DOCTYPE html>
<html>
<head>
<!-- <meta charset="UTF-8"> -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<title>SBPolice</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/submenu.css" type="text/css" media="screen">

<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/form.js"></script>


</head>
	<body class="background">
	<?php include '../SBProject/section_header.php'; 
	include '../SBProject/bg.php';
	      $Controller->getImage($page_type_id);?>

	<!-- 	content Body -->
		<div class="container content">
		
		<div class="advertise">
			<div class="row">
				<div class="col-md-12 ">
					<div class="box" style="background-color: rgba(0, 0, 0, 0.16)">
					<div class="h1">การขอแปลงสัญชาติไทย</div>
					<div class="h2">คำแนะนำและเอกสารต่างๆที่เกี่ยวข้อง</div>
						<ul class="list-1" >
							<li class="unwrapclass"> 1. คำแนะนำในการยื่นคำขอแปลงสัญชาติเป็นไทย &nbsp;&nbsp;&nbsp; <a href="../SBProject/pdf/nationality/1_06062553.pdf"><span class="glyphicon glyphicon-download"style="color:#5cb85c"></span></a></li>
							<li class="unwrapclass"> 2. แนวทางประกอบการใช้ดุลพินิจของรัฐมนตรีว่าการกระทรวงมหาดไทย &nbsp;&nbsp;&nbsp; <a href="../SBProject/pdf/nationality/2_06062553.pdf"><span class="glyphicon glyphicon-download"style="color:#5cb85c"></span></a></li>
							<li class="unwrapclass"> 3. ผนวก ก .เกณฑ์การให้คะแนน กรณีการขอแปลงสัญชาติเป็นไทย &nbsp;&nbsp;&nbsp; <a href="../SBProject/pdf/nationality/2.1_06062553.pdf"><span class="glyphicon glyphicon-download"style="color:#5cb85c"></span></a></li>
							<li class="unwrapclass"> 4. คำขอแปลงสัญชาติเป็นไทย (แบบ ป.ช.๑) &nbsp;&nbsp;&nbsp;<a href="../SBProject/pdf/nationality/3_06062553.pdf"><span class="glyphicon glyphicon-download"style="color:#5cb85c"></span></a></li>
							<li class="unwrapclass"> 5. แบบฟอร์มข้อมูลประวัติภรรยาสัญชาติไทยของผู้ยื่นคำขอแปลงสัญชาติเป็นไทย &nbsp;&nbsp;&nbsp;<a href="../SBProject/pdf/nationality/4_06062553.pdf"><span class="glyphicon glyphicon-download"style="color:#5cb85c"></span></a></li>
							<li class="unwrapclass"> 6. พยานคน (รับรองความประพฤติและหลักทรัพย์)&nbsp;&nbsp;&nbsp;<a href="../SBProject/pdf/nationality/5_06062553.pdf"><span class="glyphicon glyphicon-download"style="color:#5cb85c"></span></a></li>
							<li class="unwrapclass"> 7. พระราชบัญญัติ สัญชาติ พ.ศ. ๒๕๐๘(ฉบับแก้ไขเพิ่มเติม)&nbsp;&nbsp;&nbsp;<a href="../SBProject/pdf/nationality/6_06062553.pdf"><span class="glyphicon glyphicon-download"style="color:#5cb85c"></span></a></li>
							<li class="unwrapclass"> 8. ผังขั้นตอนการขอแปลงสัญชาติไทย &nbsp;&nbsp;&nbsp;<a href="../SBProject/pdf/nationality/7_06062553.pdf"><span class="glyphicon glyphicon-download"style="color:#5cb85c"></span></a></li>
						</ul>
			          </div>
				</div>
			</div>
		</div>
		<?php include '../SBProject/section_footer.php'; ?>
		</div>
	</body>
</html>