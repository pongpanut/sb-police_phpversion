<?php  namespace SB\conn\dao;
interface headerDao{
	
	public function insertHeader($headerBOM);
	public function updateHeader($headerBOM);
	public function loadHeader($page_type_id);

}
?>