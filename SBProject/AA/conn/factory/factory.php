<?php  namespace SB\conn\factory;

	use SB\conn\dao\menuDao;
	use SB\conn\dao\headerDao;
	use SB\conn\dao\pageTypeDao;
	use SB\conn\daoImpl\menuDaoImpl;	
	use SB\conn\daoImpl\staticpageDaoImpl2;	;
	use SB\conn\daoImpl\headerDaoImpl;	
	use SB\conn\daoImpl\pageTypeDaoImpl;
	use SB\conn\daoImpl\staticPageDaoImpl;
	use SB\conn\daoImpl\newsDaoImpl;
	use SB\conn\daoImpl\missionDaoImpl;
	use SB\conn\daoImpl\imageDaoImpl;
	use SB\conn\daoImpl\userDaoImpl;
	use SB\conn\daoImpl\bgDaoImpl;
	use SB\conn\daoImpl\videoDaoImpl;
	use SB\conn\daoImpl\departmentDaoImpl;
	use SB\conn\daoImpl\docDaoImpl;
	use SB\conn\daoImpl\othernewsDaoImpl;
	use SB\conn\daoImpl\subscriptDaoImpl;
	use SB\conn\daoImpl\contactDaoImpl;
	use SB\conn\daoImpl\constantDaoImpl;
	use SB\conn\daoImpl\tabContentDaoImpl;
	use SB\conn\daoImpl\tabModalDaoImpl;
	use SB\conn\daoImpl\organizationDaoImpl;
	use SB\conn\daoImpl\tabDaoImpl;
	use SB\conn\daoImpl\calendarDaoImpl;
	use SB\conn\daoImpl\pollDaoImpl;
	use SB\conn\daoImpl\missionImgDaoImpl;
	use SB\conn\daoImpl\contactInfoDaoImpl;
	
	
	
	include '../conn/dao/menuDao.php';
	include '../conn/dao/headerDao.php';
	include '../conn/dao/pageTypeDao.php';
	
	
	include '../conn/daoImpl/menuDaoImpl.php';
	include '../conn/daoImpl/staticpageDaoImpl2.php';
	include '../conn/daoImpl/headerDaoImpl.php';
	include '../conn/daoImpl/pageTypeDaoImpl.php';
	include '../conn/daoImpl/staticPageDaoImpl.php';
	include '../conn/daoImpl/newsDaoImpl.php';
	include '../conn/daoImpl/missionDaoImpl.php';
	include '../conn/daoImpl/imageDaoImpl.php';
	include '../conn/daoImpl/userDaoImpl.php';
	include '../conn/daoImpl/bgDaoImpl.php';
	include '../conn/daoImpl/videoDaoImpl.php';
	include '../conn/daoImpl/departmentDaoImpl.php';
	include '../conn/daoImpl/docDaoImpl.php';
	include '../conn/daoImpl/othernewsDaoImpl.php';
	include '../conn/daoImpl/subscriptDaoImpl.php';
	include '../conn/daoImpl/contactDaoImpl.php';
	include '../conn/daoImpl/constantDaoImpl.php';
	include '../conn/daoImpl/tabContentDaoImpl.php';
	include '../conn/daoImpl/tabModalDaoImpl.php';
	include '../conn/daoImpl/organizationDaoImpl.php';
	include '../conn/daoImpl/tabDaoImpl.php';
	include '../conn/daoImpl/calendarDaoImpl.php';
	include '../conn/daoImpl/pollDaoImpl.php';
	include '../conn/daoImpl/missionImgDaoImpl.php';
	include '../conn/daoImpl/contactInfoDaoImpl.php';
	
	
	class App_DaoFactory{
		private static $_instance;

		public function __construct()
		{
		}
		
		/**
		 * Set the factory instance
		 * @param App_DaoFactory $f
		 */
		public static function setFactory(App_DaoFactory $f)
		{
			self::$_instance = $f;
		}
		
		/**
		 * Get a factory instance.
		 * @return App_DaoFactory
		 */
		public static function getFactory()
		{
			if(!self::$_instance)
				self::$_instance = new self;
		
				return self::$_instance;
		}
	
		public  function  getMenu(){
			return new menuDaoImpl();
		}

		public  function  getStaticPage2(){
			return new staticpageDaoImpl2();
		}
		public  function  getheaderDao(){
			return new headerDaoImpl();
		}
		public  function  getPageTypeDao(){
			return new pageTypeDaoImpl();
		}
		public  function  getStaticPage(){
			return new staticPageDaoImpl();
		}
		public  function  getNewsDao(){
			return new newsDaoImpl();
		}
		public  function  getMissionDao(){
			return new missionDaoImpl();
		}
		public  function  getImageDao(){
			return new imageDaoImpl();
		}
		public  function  getUserDao(){
			return new userDaoImpl();
		}
		public  function  getBgDao(){
			return new bgDaoImpl();
		}
		
		public  function  getVideoDao(){
			return new videoDaoImpl();
		}
		public  function  getDepartmentDao(){
			return new departmentDaoImpl();
		}
		
		public  function  getDocDao(){
			return new docDaoImpl();
		}
		public  function  getOtherNewsDao(){
			return new othernewsDaoImpl();
		}
		public  function  getSubscriptDao(){
			return new subscriptDaoImpl();
		}
		
		public  function  getContactDao(){
			return new contactDaoImpl();
		}
		
		public  function  getConstantDao(){
			return new constantDaoImpl();
		}
		
		public  function  getTabCostentDao(){
			return new tabContentDaoImpl();
		}
		
		public  function  getTabModalDao(){
			return new tabModalDaoImpl();
		}
		public  function  getOrgDao(){
			return new organizationDaoImpl();
		}
		public  function  getTabDao(){
			return new tabDaoImpl();
		}
		
		public  function  getCalendarDao(){
			return new calendarDaoImpl();
		}
		
		public  function  getPollDao(){
			return new pollDaoImpl();
		}
		public  function  getContactInfoDao(){
			return new contactInfoDaoImpl();
		}
		public  function  getMissionImgDao(){
			return new missionImgDaoImpl();
		}
		
		
		
	}
?>