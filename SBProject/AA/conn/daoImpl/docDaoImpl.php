<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use SB\model\docM;
	  include_once '../conn/connection.php';
	  include '../model/docM.php';
class docDaoImpl{
	
	
	public function InsertDoc($docM){
		
		$id =$docM->getDocId();
		$doc_header =$docM->getDocHeader();
		$doc_header_en =$docM->getDocHeaderEN();
		$doc_detail =$docM->getDocDetail();
		$doc_upload =$docM->getDocUpload();
		$page_type = $docM->getPageTypeId();
		
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="INSERT INTO document ( doc_header,doc_header_en, doc_detail, doc_upload, pagetype) VALUES ('$doc_header','$doc_header_en' ,'$doc_detail', '$doc_upload', $page_type)";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function UpdateDoc($docM){
		
		$id =$docM->getDocId();
		$doc_header =$docM->getDocHeader();
		$doc_header_en =$docM->getDocHeaderEN();
		$doc_detail =$docM->getDocDetail();
		$doc_upload =$docM->getDocUpload();
		$page_type = $docM->getPageTypeId();

	
		if($docM->getDocUpload()!=""){
			$doc_upload =$docM->getDocUpload();
		}
	
		
		
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="UPDATE document SET  doc_header = '$doc_header',doc_header_en = '$doc_header_en', doc_detail = '$doc_detail',";
		if($doc_upload !=""){
		$sql.="doc_upload = '$doc_upload',";
		}
		$sql.="pagetype = $page_type WHERE doc_id = $id";

		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
public function loadDoc($page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		
		$data =  array();
		$sql="SELECT doc_id, doc_header,doc_header_en, doc_detail, doc_upload, pagetype FROM document where  pagetype = $page_type_id order by doc_id desc";
		$result = mysqli_query($db, $sql);
		
		if ($result !=null && mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$docM = new docM();
				$docM->setDocId($row['doc_id']);
				$docM->setDocHeader($row['doc_header']);
				$docM->setDocHeaderEN($row['doc_header_en']);
				$docM->setDocDetail($row['doc_detail']);
				$docM->setDocUpload($row['doc_upload']);
				$docM->setPageTypeId($row['pagetype']);
				array_push($data,$docM);
			}
			
		
		} else {
			//echo "0 results";
		}
		
		mysqli_close($db);
		return $data;
	}
	
	public function deleteDoc($id){
		$idDel=$id;
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="DELETE FROM document WHERE doc_id = $idDel";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function getdocById($id){
		$conn =new  connection();
		$db = $conn->getConnection();
	
		
		$sql="SELECT doc_id, doc_header,doc_header_en ,doc_detail, doc_upload, pagetype FROM document where  doc_id = $id order by doc_id desc";
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$docM = new docM();
				$docM->setDocId($row['doc_id']);
				$docM->setDocHeader($row['doc_header']);
				$docM->setDocHeaderEN($row['doc_header_en']);
				$docM->setDocDetail($row['doc_detail']);
				$docM->setDocUpload($row['doc_upload']);
				$docM->setPageTypeId($row['pagetype']);
			
			}
				
	
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $docM;
	}
	

	
}

?>
