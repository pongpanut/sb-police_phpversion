<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\staticpageM2;

	  include_once '../conn/connection.php';
      include '../model/staticpageM2.php';


class staticpageDaoImpl2{
	
	public function loadStaticpage($page_type){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT * FROM staticpage where page_type_id = '$page_type' order by id desc ";
		$result = mysqli_query($db, $sql);
		
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$staticpageM = new staticpageM2();	
				$staticpageM->setStaticpageId($row['id']);
				$staticpageM->setStaticpageText($row['text']);
				$staticpageM->setStaticpageLink($row['link']);
				$staticpageM->setStaticpageActive($row['is_active']);
				$staticpageM->setStaticPageTypeId($row['page_type_id']);
				$staticpageM->setStaticHtml($row['html']);
				array_push($data,$staticpageM);
			}

		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	
	public function getStaticID($Statictext,$page_type){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT * FROM staticpage where page_type_id = '$page_type' order by id desc  ";
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$staticpageM = new staticpageM2();
				$staticpageM->setStaticpageId($row['id']);
				$staticpageM->setStaticpageText($row['text']);
				$staticpageM->setStaticpageLink($row['link']);
				$staticpageM->setStaticpageActive($row['is_active']);
				$staticpageM->setStaticPageTypeId($row['page_type_id']);
				$staticpageM->setStaticHtml($row['html']);
				array_push($data,$staticpageM);
			}
	
		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	
	
	public function getStaticpageByID($staticId,$page_type){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT * FROM staticpage where page_type_id = '$page_type' and id= $staticId order by id desc  ";
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$staticpageM = new staticpageM2();
				$staticpageM->setStaticpageId($row['id']);
				$staticpageM->setStaticpageText($row['text']);
				$staticpageM->setStaticpageLink($row['link']);
				$staticpageM->setStaticpageActive($row['is_active']);
				$staticpageM->setStaticPageTypeId($row['page_type_id']);
				$staticpageM->setStaticHtml($row['html']);
				array_push($data,$staticpageM);
			}
	
		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	
	
	public function InsertStaticpage($staticpageM2){
		
		$id=$staticpageM2->getStaticpageId();
		$text = $staticpageM2->getStaticpageText();
		$staticLink=$staticpageM2->getStaticpageLink();
		$active =$staticpageM2->getStaticpageActive();
		$html =$staticpageM2->getStaticHtml();
		$page_type_id =$staticpageM2->getStaticPageTypeId();
		

		
		
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql=" INSERT INTO staticpage (text, link, is_active, page_type_id, html) VALUES ('$text', '$staticLink', $active, $page_type_id, '$html') ";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	public function updateStaticpage($staticpageM2){
		$id=$staticpageM2->getStaticpageId();
		$text = $staticpageM2->getStaticpageText();
		$link = $staticpageM2->getStaticpageLink();
		$active =$staticpageM2->getStaticpageActive();
		$html =$staticpageM2->getStaticHtml();
		$page_type_id =$staticpageM2->getStaticPageTypeId();

		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="UPDATE staticpage SET text = '$text',  is_active =$active , page_type_id = $page_type_id, html = '$html' WHERE id = $id ";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	
	public function deleteStaticpage($id){
		$id=$id;
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="DELETE FROM staticpage WHERE id =$id";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function getSeqStatic(){
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = 0;
		$sql="SELECT `AUTO_INCREMENT`	FROM  INFORMATION_SCHEMA.TABLES	WHERE TABLE_SCHEMA = 'specialbranch'	AND   TABLE_NAME   = 'staticpage'";// aaaaa
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$data= $row['AUTO_INCREMENT'];
			}
		} else {
			//echo "0 results";
		}
		echo $data;
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	
	
	
}

?>
