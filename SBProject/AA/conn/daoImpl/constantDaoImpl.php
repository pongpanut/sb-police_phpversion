<?php   namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\constantM;
include_once '../conn/connection.php';
include_once '../model/constantM.php';

class constantDaoImpl{
	
	public function loadconstantByType($cons_type){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql=" SELECT cons_id, cons_name, cons_value, cons_type, cons_flag FROM sb_constant where cons_type='$cons_type' and cons_flag='Y' order by cons_id desc";
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$constantM = new constantM();
				$constantM->setConsId($row['cons_id']);
				$constantM->setConsName($row['cons_name']);
				$constantM->setConsValue($row['cons_value']);
				$constantM->setConsType($row['cons_type']);
				$constantM->setConsFlag($row['cons_flag']);
				array_push($data,$constantM);
			}
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $data;
	}
	
	
	public function loadconstant($cons_type,$cons_value){
		$conn =new  connection();
		$db = $conn->getConnection();
		
		$sql=" SELECT cons_id, cons_name, cons_value, cons_type, cons_flag FROM sb_constant where cons_type='$cons_type' and cons_flag='Y' and cons_value= '$cons_value' order by cons_id desc";
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$constantM = new constantM();
				$constantM->setConsId($row['cons_id']);
				$constantM->setConsName($row['cons_name']);
				$constantM->setConsValue($row['cons_value']);
				$constantM->setConsType($row['cons_type']);
				$constantM->setConsFlag($row['cons_flag']);
			
			}
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $constantM;
	}
	
	

}

?>
