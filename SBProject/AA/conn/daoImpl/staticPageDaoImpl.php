<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
	  include_once '../conn/connection.php';



class staticPageDaoImpl{
	
	
	public function InsertStaticPage($staticPageM){
	
		
		$text =$staticPageM->getText();
		$link=	$staticPageM->getLink();
		$isactive = $staticPageM->getIsActive();
		$pageTypeId=$staticPageM->getHeaderPageTypeId();
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="INSERT INTO staticpage (text, link, is_active, page_type_id) VALUES ( '$text', '$link', $isactive, $pageTypeId);";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function UpdateStaticPage($staticPageM){
	
		$id =$staticPageM->getId();
		$text =$staticPageM->getText();
		$link=	$staticPageM->getLink();
		$isactive = $staticPageM->getIsActive();
		$pageTypeId=$staticPageM->getHeaderPageTypeId();
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="UPDATE staticpage SET  text = '$text', link = '$link', is_active = $isactive, page_type_id = $pageTypeId WHERE id ='$id'";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	public function getStaticPage($page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT id, text, link, is_active, page_type_id FROM staticpage where page_type_id='$page_type_id' order by id desc";
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$staticPageM = new staticPageM();
				$staticPageM->setId($row['id']);
				$staticPageM->setText($row['text']);
				$staticPageM->setLink($row['link']);
				$staticPageM->setIsActive($row['is_active']);
				$staticPageM->setPageTypeId($row['page_type_id']);
				array_push($data,$staticPageM);
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $data;
	}

	
}

?>
