<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\departmentBOM;
use  SB\model\departmentLiBOM;
use  SB\model\departmentSubBOM;

include_once '../conn/connection.php';
include_once '../model/departmentBOM.php';
include_once '../model/departmentLiBOM.php';
include_once '../model/departmentSubBOM.php';


class departmentDaoImpl{
	
	public function loadDepartment($page_type){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT d.dept_id, d.dept_text, d.dept_link, d.page_type_id FROM department as d WHERE d.page_type_id = '$page_type' order by dept_id desc";
		$result = mysqli_query($db, $sql);
		
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$departmentM = new departmentBOM();	
				$departmentM->setDeptId($row['dept_id']);
				$departmentM->setDeptText($row['dept_text']);
				$departmentM->setDeptLink($row['dept_link']);
				$departmentM->setPageTypeId($row['page_type_id']);
				array_push($data,$departmentM);
			}

		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	
	public function loadSubDepartmentList($id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT dept_sub_id, dept_sub_text, dept_sub_link, dept_li_id, page_type_id, seq FROM department_sub WHERE dept_li_id = '$id' order by dept_sub_id desc";
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$departmentSubBOM = new departmentSubBOM();
				$departmentSubBOM->setDeptSubId($row['dept_sub_id']);
				$departmentSubBOM->setDeptSubText($row['dept_sub_text']);
				$departmentSubBOM->setDeptSubLink($row['dept_sub_link']);
				$departmentSubBOM->setDeptId($row['dept_li_id']);
				$departmentSubBOM->setPageTypeId($row['page_type_id']);
				$departmentSubBOM->setSeq($row['seq']);
				array_push($data,$departmentSubBOM);
			}
	
		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	
	public function loadDepartmentList($page_type){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql=" SELECT  d.dept_li_id, d.dept_li_text, d.dept_li_link, d.page_type_id, d.seq FROM department_list as d WHERE d.page_type_id = '$page_type' order by dept_li_id desc";
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$departmentLiBOM = new departmentLiBOM();
				$departmentLiBOM->setDeptListId($row['dept_li_id']);
				$departmentLiBOM->setDeptListText($row['dept_li_text']);
				$departmentLiBOM->setDeptListLink($row['dept_li_link']);
				$departmentLiBOM->setPageTypeId($row['page_type_id']);
				$departmentLiBOM->setSeq($row['seq']);
				array_push($data,$departmentLiBOM);
			}
	
		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	
	public function insertDepartmentList($departmentLiBOM){
		$Text = $departmentLiBOM->getDeptListText();
		$Link = $departmentLiBOM->getDeptListLink();
		$PageType = $departmentLiBOM->getPageTypeId();
		$Seq = $departmentLiBOM->getSeq();

		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql = "INSERT INTO department_list(dept_li_id, dept_li_text, dept_li_link, page_type_id, seq) VALUES ('','$Text','$Link','$PageType','$Seq')";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function deletedepartmentSub($departmentSubId){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
	
		$sql = "DELETE FROM department_sub WHERE dept_sub_id = '$departmentSubId'";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function insertDepartmentSub($departmentSubBOM){
		$Text = $departmentSubBOM->getDeptSubText();
		$Link = $departmentSubBOM->getDepSubLink();
		$DeptId = $departmentSubBOM->getDeptId();
		$PageType = $departmentSubBOM->getPageTypeId();
		$Seq = $departmentSubBOM->getSeq();
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql = "INSERT INTO department_sub(dept_sub_id, dept_sub_text, dept_sub_link, dept_li_id, page_type_id, seq) VALUES ('','$Text','$Link','$DeptId','$PageType','$Seq')";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	
	public function loadLatestDepartmentIndex($page_type){
		$conn =new  connection();
		$db = $conn->getConnection();
		$sql="SELECT cur_index FROM index_dept ";
		echo $sql;
		$result = mysqli_query($db, $sql);
		$data = 0;
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$data = $row['cur_index'];
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $data;
	}
	
	public function loadLatestDepartment(){
		$conn =new  connection();
		$db = $conn->getConnection();
		$sql="SELECT cur_index FROM index_dept";
		echo $sql;
		$result = mysqli_query($db, $sql);
		$data = 0;
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$data = $row['cur_index'];
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $data;
	}

	public function updateDept($deptMInput){
		$Id = $deptMInput->getDeptId();
		$Text = $deptMInput->getDeptText();
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="UPDATE department SET dept_text = '$Text' WHERE dept_id='$Id'  ";
		echo $sql;
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	

	public function updateDeptSub($departmentSubBOM){
		$id = $departmentSubBOM->getDeptSubId();
		$text = $departmentSubBOM->getDeptSubText();
		$link = $departmentSubBOM->getDepSubLink();
		$seq = $departmentSubBOM->getSeq();
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="UPDATE department_sub SET dept_sub_text= '$text',dept_sub_link='$link',seq='$seq' WHERE dept_sub_id ='$id'";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function updateDeptList($deptLiMInput){
		$id = $deptLiMInput->getDeptListId();
		$text = $deptLiMInput->getDeptListText();
		$link = $deptLiMInput->getDeptListLink();
		$seq = $deptLiMInput->getSeq();
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="UPDATE department_list SET dept_li_text='$text',dept_li_link= '$link', seq='$seq' WHERE dept_li_id = '$id'";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function deleteSql($sql){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
	
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function insertDepartment($sql){

		$conn =new  connection();
		$db = $conn->getConnection();
		$data = '';

		if (mysqli_query($db, $sql)) {
			$data= $db->insert_id;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function getMaxId($column,$table){
	
			$conn =new  connection();
		$db = $conn->getConnection();
		$sql="SELECT MAX($column) as max_column FROM $table";
		$result = mysqli_query($db, $sql);
		$data = 0;
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$data = $row['max_column'];
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $data;
	}
	
	public function InsertnewAdmin($adminuser,$pass, $newpagetype,$currid){
	
		
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="INSERT INTO admin(admin_id, admin_user, admin_password, admin_privillege, page_type_id) VALUES ('','$adminuser','$pass','$newpagetype','$currid')";
		
		if (mysqli_query($db, $sql)) {
			$data= true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function UpdateLatestDepartmentIndex($newIndex){

		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="UPDATE index_dept SET cur_index = '$newIndex'   ";
		echo $sql;
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
		
		mysqli_close($db);
		return $data;
	}
}

?>
