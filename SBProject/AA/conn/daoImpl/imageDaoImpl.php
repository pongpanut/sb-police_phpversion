<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use SB\model\imageM;
	  include_once '../conn/connection.php';
	  include '../model/imageM.php';
class imageDaoImpl{
	
	
	public function InsertImage($imageM){
		
		$id =$imageM->getImgid();
		$imageDesc =$imageM->getImgdesc();
		$imageName =$imageM->getImgname();
		$imagePath =$imageM->getImgpath();
		$imageType = $imageM->getImgtype();
		$imageHeader = $imageM->getImgheader();
		$ref_id = $imageM->getRefId();
		$seq = $imageM->getSeq();
		$pageType =$imageM->getPageTypeId();
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		if($ref_id !=0){
			$sql="INSERT INTO image (img_desc, img_name, img_path, img_type, img_header, page_type_id ,ref_id ) VALUES ( '$imageDesc', '$imageName', '$imagePath', '$imageType', '$imageHeader', $pageType,$ref_id)";
		}else{
			$sql="INSERT INTO image (img_desc, img_name, img_path, img_type, img_header, page_type_id ) VALUES ( '$imageDesc', '$imageName', '$imagePath', '$imageType', '$imageHeader', $pageType)";
		}
	
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function UpdateImage($imageM){
		$id =$imageM->getImgid();
		$imageDesc =$imageM->getImgdesc();
		$imageName =$imageM->getImgname();
		$imagePath =$imageM->getImgpath();
		$imageType = $imageM->getImgtype();
		$imageHeader = $imageM->getImgheader();

		$ref_id = 0;
		$seq = 0;
		if($imageM->getRefId()!=""){
			$ref_id = $imageM->getRefId();
		}
		if($imageM->getSeq()!=""){
			$seq = $imageM->getSeq();
		}
	
		$pageType =$imageM->getPageTypeId();
		
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="UPDATE image SET  img_desc = '$imageDesc', img_name = '$imageName',";
		if($imagePath!=""){
			$sql.= "img_path = '$imagePath',";
		}
		$sql.="img_type = '$imageType', img_header = '$imageHeader', ref_id = $ref_id, page_type_id = $pageType, seq = $seq WHERE img_id = $id";

		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function loadImg($page_type_id,$type){
		$conn =new  connection();
		$db = $conn->getConnection();
		
		$data =  array();
		$sql="SELECT img_id, img_desc, img_name, img_path ,img_type ,img_header ,seq FROM image where page_type_id =' $page_type_id' and img_type in($type)  order by img_id desc ";
		$result = mysqli_query($db, $sql);
		
		
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$imageM = new imageM();
				$imageM->setImgid($row['img_id']);
				$imageM->setImgdesc($row['img_desc']);
				$imageM->setImgname($row['img_name']);
				$imageM->setImgpath($row['img_path']);
				$imageM->setImgtype($row['img_type']);
				$imageM->setImgheader($row['img_header']);
				$imageM->setSeq($row['seq']);
				array_push($data,$imageM);
			}
			
		
		} else {
			//echo "0 results";
		}
		
		mysqli_close($db);
		return $data;
	}
	
	public function loadImgOrderBySeq($page_type_id,$type){
		$conn =new  connection();
		$db = $conn->getConnection();
	
		$data =  array();
		$sql="SELECT img_id, img_desc, img_name, img_path ,img_type ,img_header ,seq FROM image where page_type_id =' $page_type_id' and img_type in($type) order by seq desc ";
		$result = mysqli_query($db, $sql);
		
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$imageM = new imageM();
				$imageM->setImgid($row['img_id']);
				$imageM->setImgdesc($row['img_desc']);
				$imageM->setImgname($row['img_name']);
				$imageM->setImgpath($row['img_path']);
				$imageM->setImgtype($row['img_type']);
				$imageM->setImgheader($row['img_header']);
				$imageM->setSeq($row['seq']);
				array_push($data,$imageM);
			}
				
	
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function deleteImage($id){
		$idDel=$id;
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="DELETE FROM image WHERE img_id = $idDel";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function loadImgById($id){
		$conn =new  connection();
		$db = $conn->getConnection();
	
		
		$sql="SELECT img_id, img_desc, img_name, img_path ,img_type ,img_header ,seq FROM image where img_id=$id  order by img_id desc";
		$result = mysqli_query($db, $sql);
		$imageM = new imageM();
		
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$imageM->setImgid($row['img_id']);
				$imageM->setImgdesc($row['img_desc']);
				$imageM->setImgname($row['img_name']);
				$imageM->setImgpath($row['img_path']);
				$imageM->setImgtype($row['img_type']);
				$imageM->setImgheader($row['img_header']);
				$imageM->setSeq($row['seq']);
				
			}
				
	
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $imageM;
	}
	
	public function getSeqImage(){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = 0;
		$sql="SELECT `AUTO_INCREMENT`	FROM  INFORMATION_SCHEMA.TABLES	WHERE TABLE_SCHEMA = 'specialbranch'	AND   TABLE_NAME   = 'image'";// aaaaa
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$data= $row['AUTO_INCREMENT'];
			}
		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	
	public function loadImgByRefId($Refid){
		$conn =new  connection();
		$db = $conn->getConnection();
	
		$data =  array();
		$sql="SELECT img_id, img_desc, img_name, img_path ,img_type ,img_header ,seq FROM image where ref_id =$Refid order by img_id desc";
		$result = mysqli_query($db, $sql);
		
		if($result!=null){
			if (mysqli_num_rows($result) > 0) {
				while($row = mysqli_fetch_assoc($result)) {
					$imageM = new imageM();
					$imageM->setImgid($row['img_id']);
					$imageM->setImgdesc($row['img_desc']);
					$imageM->setImgname($row['img_name']);
					$imageM->setImgpath($row['img_path']);
					$imageM->setImgtype($row['img_type']);
					$imageM->setImgheader($row['img_header']);
					$imageM->setSeq($row['seq']);
					array_push($data,$imageM);
				}
		
		
			} else {
				//echo "0 results";
			}
		}
	
		mysqli_close($db);
		return $data;
	}

	
}

?>
