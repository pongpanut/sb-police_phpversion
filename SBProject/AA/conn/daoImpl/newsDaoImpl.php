<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use SB\model\newsM;
	  include_once '../conn/connection.php';
	  include '../model/newsM.php';
class newsDaoImpl{
	
	
	public function InsertNews($newsM){
	
		
		$id =$newsM->getNewsId();
		$code =$newsM->getNewsCode();
		$header =$newsM->getNewsHeader();
		$detail =$newsM->getNewsDetail();
		$pdf = $newsM->getNewsPdf();
		$pageType =$newsM->getPageTypeId();
		$isHighlight = $newsM->getIsHighLight();
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="INSERT INTO news ( news_code, news_header, news_detail, pdf_path, page_type_id, rec_date, is_highlight) VALUES ( '$code', '$header', '$detail', '$pdf', $pageType, now(), $isHighlight)";
		echo $sql;
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			//echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function UpdateNews($newsM){
		$id =$newsM->getNewsId();
		$code =$newsM->getNewsCode();
		$header =$newsM->getNewsHeader();
		$detail =$newsM->getNewsDetail();
		$pdf = $newsM->getNewsPdf();
		$pageType =$newsM->getPageTypeId();
		$isHighlight = $newsM->getIsHighLight();
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="UPDATE news SET  news_code = '$code', news_header = '$header', news_detail = '$detail', rec_date = now(), is_highlight = $isHighlight, ";
		if($pdf !=""){
			$sql.="pdf_path = '$pdf',";
		}
		$sql.="page_type_id = $pageType  WHERE news_id = $id ";
		echo $sql;
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			//echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	public function getNews($page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT news_id, news_code, news_header, news_detail, pdf_path, page_type_id, is_highlight FROM news where page_type_id=$page_type_id order by  news_id  desc";
		$result = mysqli_query($db, $sql);
		if (!empty($result) && mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$newsM = new newsM();
				$newsM->setNewsId($row['news_id']);
				$newsM->setNewsCode($row['news_code']);
				$newsM->setNewsHeader($row['news_header']);
				$newsM->setNewsDetail($row['news_detail']);
				$newsM->setNewsPdf($row['pdf_path']);
				$newsM->setPageTypeId($row['page_type_id']);
				$newsM->setIsHighLight($row['is_highlight']);
				array_push($data,$newsM);
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $data;
	}
	
	public function deleteNews($id){
		$id=$id;
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="DELETE FROM news WHERE news_id =$id";
		echo $sql;
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			//echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function getNewsById($id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$sql="SELECT news_id, news_code, news_header, news_detail, pdf_path, page_type_id, is_highlight FROM news where news_id=$id order by news_id desc";
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$newsM = new newsM();
				$newsM->setNewsId($row['news_id']);
				$newsM->setNewsCode($row['news_code']);
				$newsM->setNewsHeader($row['news_header']);
				$newsM->setNewsDetail($row['news_detail']);
				$newsM->setNewsPdf($row['pdf_path']);
				$newsM->setPageTypeId($row['page_type_id']);
				$newsM->setIsHighLight($row['is_highlight']);
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $newsM;
	}

	
}

?>
