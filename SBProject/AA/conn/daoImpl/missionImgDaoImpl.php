<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use SB\model\missionImgM;
	  include_once '../conn/connection.php';
	  include '../model/missionImgM.php';
class missionImgDaoImpl{
	
	
	public function InsertMissionImg($missionImgM){
	
		
		
		$paht=	$missionImgM->getImagePaht();
		$pageType=	$missionImgM->getPageTypeId();
		$refId= $missionImgM->getRefId();
		
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql=" INSERT INTO mission_image ( image_paht, page_type_id, ref_id, rec_date) VALUES ( '$paht', $pageType, $refId, now())  ";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			//echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function UpdateMissionImg($missionImgM){
		
		$id=	$missionImgM->getId();
		$paht=	$missionImgM->getImagePaht();
		$pageType=	$missionImgM->getPageTypeId();
		$refId= $missionImgM->getRefId();
		
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql=" UPDATE mission_image SET  ";	
		if(!empty($paht)){
			$sql.=" image_paht = '$paht', ";
		}
		$sql.=" page_type_id = $pageType, ref_id = $refId, rec_date = now()  WHERE id = $id ";

		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			//echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	public function getMissionImg($ref_id,$page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql=" SELECT id, image_paht, page_type_id, ref_id, rec_date FROM mission_image where page_type_id = $page_type_id and ref_id=$ref_id order by id desc";
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$missionImgM = new missionImgM();
				$missionImgM->setId($row['id']);
				$missionImgM->setImagePaht($row['image_paht']);
				$missionImgM->setPageTypeId($row['page_type_id']);
				$missionImgM->setRefId($row['ref_id']);
				$missionImgM->setRecDate($row['rec_date']);
				array_push($data,$missionImgM);
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $data;
	}
	
	public function deleteMissionImg($id){
		$id=$id;
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="DELETE FROM mission_image WHERE id =$id";
		echo $sql;
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			//echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function getMissionImgByid($id){
		$conn =new  connection();
		$db = $conn->getConnection();
	
		$sql=" SELECT id, image_paht, page_type_id, ref_id, rec_date FROM mission_image where id = $id order by id desc";
		$result = mysqli_query($db, $sql);
		$missionImgM = new missionImgM();
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				
				$missionImgM->setId($row['id']);
				$missionImgM->setImagePaht($row['image_paht']);
				$missionImgM->setPageTypeId($row['page_type_id']);
				$missionImgM->setRefId($row['ref_id']);
				$missionImgM->setRecDate($row['rec_date']);
				
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $missionImgM;
	}
	
	
}

?>
