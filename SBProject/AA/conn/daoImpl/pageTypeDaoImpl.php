<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\pageTypeM;
use  SB\conn\dao\pageTypeDao;
	  include_once '../conn/connection.php';

class pageTypeDaoImpl implements pageTypeDao{	
	
	public function getPageTypeByUserID($userId){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT page_type_id, page_type_name FROM page_type  where page_type_id=$userId  order by page_type_id desc";
		$result = mysqli_query($db, $sql);
		
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$pageType = new pageTypeM();	
				$pageType->setPageTypeId($row['page_type_id']);
				$pageType->setPageTypeName($row['page_type_name']);
				array_push($data,$pageType);
			}
		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	public function getPageTypeMappging($userId){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT page_type_text FROM pagetype_mapping where page_type_id=$userId order by page_type_id desc ";
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			//echo  mysqli_fetch_assoc($result);
			while($row = mysqli_fetch_assoc($result)) {
				array_push($data,$row['page_type_text']);
			}
		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	
	public function loadpagetypemappingText($Id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$sql=" SELECT page_type_text as pagetypetext FROM  pagetype_mapping WHERE page_type_id ='$Id' ";
		//echo $sql;
		$result = mysqli_query($db, $sql);
		$data = '';
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$data= $row['pagetypetext'];
					
			}
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $data;
	}
	
}

?>
