<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use SB\model\videoM;
	  include_once '../conn/connection.php';
	  include '../model/videoM.php';
class VideoDaoImpl{
	
	
	public function InsertVideo($videoM){
		
		$id =$videoM->getVideoId();
		$videoPath =$videoM->getVideoPath();
		$videoName =$videoM->getVideoName();
		$pageType = $videoM->getPageTypeId();
		$status = $videoM->getStatus();
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="INSERT INTO video ( video_name, video_path, status, page_type_id) VALUES ( '$videoName', '$videoPath', '$status', $pageType)";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function UpdateVideo($videoM){
		$id =$videoM->getVideoId();
		$videoPath =$videoM->getVideoPath();
		$videoName =$videoM->getVideoName();
		$pageType = $videoM->getPageTypeId();
		$status = $videoM->getStatus();

		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="UPDATE video SET  video_name = '$videoName', ";
		if($videoPath!=""){
			$sql.="video_path = '$videoPath',";
		}
		$sql.=" status = '$status', page_type_id = $pageType WHERE video_id = $id ";
		
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
public function loadVideo($page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		
		$data =  array();
		$sql="SELECT video_id, video_name, video_path, status, page_type_id FROM video  where page_type_id=$page_type_id  order by video_id desc" ;
		$result = mysqli_query($db, $sql);
		
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$videoM = new videoM();
				$videoM->setVideoId($row['video_id']);
				$videoM->setVideoPath($row['video_path']);
				$videoM->setPageTypeId($row['page_type_id']);
				$videoM->setVideoName($row['video_name']);
				$videoM->setStatus($row['status']);
				array_push($data,$videoM);
			}
			
		
		} else {
			//echo "0 results";
		}
		
		mysqli_close($db);
		return $data;
	}
	
	public function deleteVideo($id){
		$idDel=$id;
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="DELETE FROM video WHERE video_id = $idDel";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function loadVideoById($id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$sql="SELECT video_id, video_name, video_path, status, page_type_id FROM video  where video_id=$id " ;
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$videoM = new videoM();
				$videoM->setVideoId($row['video_id']);
				$videoM->setVideoPath($row['video_path']);
				$videoM->setPageTypeId($row['page_type_id']);
				$videoM->setVideoName($row['video_name']);
				$videoM->setStatus($row['status']);
			}

		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $videoM;
	}

	
}

?>
