<?php   namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\tab_contentM;
include_once '../conn/connection.php';
include_once '../model/tab_contentM.php';

class tabContentDaoImpl{
	
	public function loadTabContent($page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql=" SELECT tab_content_id, tab_content_header, tab_content_detail, tab_content_image_id, tab_id, page_type_id FROM tab_content where page_type_id= $page_type_id order by tab_content_id desc ";
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$tab_contentM = new tab_contentM();
				$tab_contentM->setTabContentId($row['tab_content_id']);
				$tab_contentM->setTabContentHeader($row['tab_content_header']);
				$tab_contentM->setTabContentDetail($row['tab_content_detail']);
				$tab_contentM->setTabContentImageId($row['tab_content_image_id']);
				$tab_contentM->setTabId($row['tab_id']);
				$tab_contentM->setPageTypeId($row['page_type_id']);
				array_push($data,$tab_contentM);
			}
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $data;
	}
	
	
	public function InsertTabContent($tab_contentM){
		$TabId=0;
		$TabContentHeader=	$tab_contentM->getTabContentHeader();
		$TabContentDetail=	$tab_contentM->getTabContentDetail();
		$TabContentImageId=	$tab_contentM->getTabContentImageId();
		if($tab_contentM->getTabId() !=""){
			$TabId=	$tab_contentM->getTabId();
		}
		
		$PageTypeId=	$tab_contentM->getPageTypeId();

		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql=" INSERT INTO tab_content ( tab_content_header, tab_content_detail, tab_content_image_id, tab_id, page_type_id) VALUES ( '$TabContentHeader', '$TabContentDetail', $TabContentImageId, $TabId, $PageTypeId)";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	public function updateTabContent($tab_contentM){
		
		$TabContentid=$tab_contentM->getTabContentId();
		$TabContentHeader=	$tab_contentM->getTabContentHeader();
		$TabContentDetail=	$tab_contentM->getTabContentDetail();
		$TabContentImageId=	$tab_contentM->getTabContentImageId();
		$TabId=	$tab_contentM->getTabId();
		$PageTypeId=	$tab_contentM->getPageTypeId();
		
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql=" UPDATE tab_content SET  tab_content_header = '$TabContentHeader', tab_content_detail = '$TabContentDetail',";
		if($TabContentImageId !=0){
			$sql.="tab_content_image_id = $TabContentImageId ";
		}
		$sql.=", tab_id = $TabId, page_type_id = $PageTypeId WHERE tab_content_id = $TabContentid ";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	
	public function deleteTabContent($id){
		$id=$id;
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="DELETE FROM tab_content WHERE tab_content_id =$id";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function loadTabContentByTabId($page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql=" SELECT tab_content_id, tab_content_header, tab_content_detail, tab_content_image_id, tab_id, page_type_id FROM tab_content where page_type_id= $page_type_id order by tab_content_id desc ";
		$result = mysqli_query($db, $sql);
	
		if ($result !=null && mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$tab_contentM = new tab_contentM();
				$tab_contentM->setTabContentId($row['tab_content_id']);
				$tab_contentM->setTabContentHeader($row['tab_content_header']);
				$tab_contentM->setTabContentDetail($row['tab_content_detail']);
				$tab_contentM->setTabContentImageId($row['tab_content_image_id']);
				$tab_contentM->setTabId($row['tab_id']);
				$tab_contentM->setPageTypeId($row['page_type_id']);
				array_push($data,$tab_contentM);
			}
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function loadTabContentById($page_type_id , $id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$sql=" SELECT tab_content_id, tab_content_header, tab_content_detail, tab_content_image_id, tab_id, page_type_id FROM tab_content where page_type_id= $page_type_id  and tab_content_id= $id order by tab_content_id desc";
		$result = mysqli_query($db, $sql);
		$tab_contentM = new tab_contentM();
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				
				$tab_contentM->setTabContentId($row['tab_content_id']);
				$tab_contentM->setTabContentHeader($row['tab_content_header']);
				$tab_contentM->setTabContentDetail($row['tab_content_detail']);
				$tab_contentM->setTabContentImageId($row['tab_content_image_id']);
				$tab_contentM->setTabId($row['tab_id']);
				$tab_contentM->setPageTypeId($row['page_type_id']);
				
			}
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $tab_contentM;
	}
	
	public function getSeqTabContent(){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = 0;
		$sql="SELECT `AUTO_INCREMENT`	FROM  INFORMATION_SCHEMA.TABLES	WHERE TABLE_SCHEMA = 'specialbranch'	AND   TABLE_NAME   = 'tab_content'";// aaaaa
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$data= $row['AUTO_INCREMENT'];
			}
		} else {
			//echo "0 results";
		}
		echo $data;
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	

}

?>
