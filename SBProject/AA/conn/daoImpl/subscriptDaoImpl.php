<?php   namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\subscriptBOM;
 include_once '../conn/connection.php';
include_once '../model/subscriptBOM.php';

class subscriptDaoImpl{
	
	public function loadAllSubscripter(){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		
		$sql="SELECT * FROM subscript_list order by subscript_id desc";
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$subscriptBOM = new subscriptBOM();
				$subscriptBOM->setSubscriptId($row['subscript_id']);
				$subscriptBOM->setSubscriptEmail($row['subscript_email']);
				array_push($data,$subscriptBOM);
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $data;
	}
	
	public function deleteSubscripter($id){
		$id=$id;
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="DELETE FROM subscript_list WHERE subscript_id=$id";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}


}

?>
