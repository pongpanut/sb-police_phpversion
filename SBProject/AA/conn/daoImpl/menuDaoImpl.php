<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\menuM;
use  SB\model\submenuBOM;
use  SB\model\submenulistBOM;
use  SB\conn\dao\menuDao;
	  include_once '../conn/connection.php';
	  include_once '../model/menuM.php';
	  include_once '../model/submenuBOM.php';
	  include_once '../model/submenulistBOM.php';

class menuDaoImpl implements menuDao{	
	
	/*
	 * Menu section
	 */
	public function loadMenu($page_type){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT DISTINCT m.menu_id, m.menu_text,m.menu_page_type,s.link,m.has_submenu, m.seq, m.is_department FROM menu as m LEFT join staticpage as s  ON  m.menu_link = s.id WHERE m.menu_page_type = '$page_type' ORDER BY  m.seq desc";
		$result = mysqli_query($db, $sql);
		
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$menuM = new menuM();	
				$menuM->setMenuId($row['menu_id']);
				$menuM->setMenuText($row['menu_text']);
				$menuM->setMenuPageType($row['menu_page_type']);
				$menuM->setMenuLink($row['link']);
				$menuM->setMenuHasSubmenu($row['has_submenu']);
				$menuM->setMenuSeq($row['seq']);
				$menuM->setMenuIsDepartment($row['is_department']);
				array_push($data,$menuM);
			}

		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	
	public function loadMenuNoDept($page_type){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT DISTINCT m.menu_id, m.menu_text,m.menu_page_type,s.link,m.has_submenu, m.seq, m.is_department FROM menu as m LEFT join staticpage as s  ON  m.menu_link = s.id WHERE m.menu_page_type = '$page_type' AND m.is_department != 1 ORDER BY  m.seq desc";
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$menuM = new menuM();
				$menuM->setMenuId($row['menu_id']);
				$menuM->setMenuText($row['menu_text']);
				$menuM->setMenuPageType($row['menu_page_type']);
				$menuM->setMenuLink($row['link']);
				$menuM->setMenuHasSubmenu($row['has_submenu']);
				$menuM->setMenuSeq($row['seq']);
				$menuM->setMenuIsDepartment($row['is_department']);
				array_push($data,$menuM);
			}
	
		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	
	public function updateMenu($menuMInput){
		$Id = $menuMInput->getMenuId();
		$Text = $menuMInput->getMenuText();
		$PageType = $menuMInput->getMenuPageType();
		$Link = $menuMInput->getMenuLink();
		$HasSubmenu = $menuMInput->getMenuHasSubmenu();
		$Seq = $menuMInput->getMenuSeq();
		
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="UPDATE menu SET menu_text = '$Text' ,menu_page_type='$PageType',menu_link='$Link',has_submenu='$HasSubmenu',seq='$Seq' WHERE menu_id='$Id'  ";
		echo "<script type='text/javascript'>alert('$sql');</script>";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	
	
	public function deleteMenu($menuMInput){
		$Id = $menuMInput->getMenuId();
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		
		$sql="DELETE FROM menu WHERE menu_id='$Id'  ";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function insertMenu($menuMInput){
		$Text = $menuMInput->getMenuText();
		$PageType = $menuMInput->getMenuPageType();
		$Link = $menuMInput->getMenuLink();
		$HasSubmenu = $menuMInput->getMenuHasSubmenu();
		$Seq = $menuMInput->getMenuSeq();
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql=" INSERT INTO menu(menu_id, menu_text, menu_page_type, menu_link, has_submenu, seq) VALUES ('','$Text','$PageType','$Link','$HasSubmenu','$Seq')";
		echo "<script type='text/javascript'>alert('$sql');</script>";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	/*
	 * SubMenu section
	 */
	public function loadSubMenu($id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$newquery = "SELECT DISTINCT m.submenu_id,m.submenu_text,s.link,m.submenu_has_menulist,m.menu_id,m.page_type_id,m.seq FROM submenu as m  LEFT join staticpage as s  ON  m.submenu_link = s.id  where m.menu_id= '$id' ORDER BY  m.seq desc";
		$resultSub = mysqli_query($db, $newquery);
		if (mysqli_num_rows($resultSub) > 0) {
			while($row = mysqli_fetch_assoc($resultSub)) {
				$submenuM = new submenuBOM();	
				$submenuM->setSubMenuId($row['submenu_id']);
				$submenuM->setSubMenuText($row['submenu_text']);				
				$submenuM->setSubMenuLink($row['link']);
				$submenuM->setSubmenuHasMenuList($row['submenu_has_menulist']);
				$submenuM->setMenuId($row['menu_id']);
				$submenuM->setSubMenuSeq($row['seq']);
				array_push($data,$submenuM);
			}

		} else {
			//echo "0 results";
		} 
		mysqli_close($db);
		return $data;
	}
	
	public function getChildSubmenuID($menuID){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT DISTINCT m.submenu_id FROM submenu as m where m.menu_id='$menuID'" ;
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$submenuM = new submenuBOM();	
				$submenuM->setSubMenuId($row['submenu_id']);
				array_push($data,$submenuM);
			}
		} 
		
		mysqli_close($db);
		return $data;
	}
	
	public function loadSubMenuByPageTypeId($id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$newquery = "SELECT DISTINCT m.submenu_id,m.submenu_text,s.link,m.submenu_has_menulist,m.menu_id,m.page_type_id,m.seq FROM submenu as m  LEFT join staticpage as s  ON  m.submenu_link = s.id  where m.page_type_id= '$id' ORDER BY  m.menu_id desc";
	
		$resultSub = mysqli_query($db, $newquery);
		if (mysqli_num_rows($resultSub) > 0) {
			while($row = mysqli_fetch_assoc($resultSub)) {
				$submenuM = new submenuBOM();
				$submenuM->setSubMenuId($row['submenu_id']);
				$submenuM->setSubMenuText($row['submenu_text']);
				$submenuM->setSubMenuLink($row['link']);
				$submenuM->setSubmenuHasMenuList($row['submenu_has_menulist']);
				$submenuM->setMenuId($row['menu_id']);
				$submenuM->setSubMenuSeq($row['seq']);
				array_push($data,$submenuM);
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $data;
	}
		
	public function updateSubMenu($submenuMInput){
		$Id = $submenuMInput->getSubMenuId();
		$Text = $submenuMInput->getSubMenuText();
		$PageType = $submenuMInput->getPageTypeId();
		$Link = $submenuMInput->getSubMenuLink();
		$HasSubmenu = $submenuMInput->getSubmenuHasMenuList();
		$Seq = $submenuMInput->getSubMenuSeq();
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		
		$sql="UPDATE submenu SET submenu_text = '$Text' ,submenu_link='$Link' ,page_type_id='$PageType',submenu_has_menulist='$HasSubmenu',seq='$Seq' WHERE submenu_id='$Id'  ";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function insertSubMenu($submenuMInput){
		$Id = $submenuMInput->getMenuId();
		$Text = $submenuMInput->getSubMenuText();
		$PageType = $submenuMInput->getPageTypeId();
		$Link = $submenuMInput->getSubMenuLink();
		$HasSubmenu = $submenuMInput->getSubmenuHasMenuList();
		$Seq = $submenuMInput->getSubMenuSeq();
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql=" INSERT INTO submenu(submenu_id, submenu_text, submenu_link, submenu_has_menulist, menu_id, page_type_id ,seq) VALUES ('','$Text','$Link','$HasSubmenu','$Id','$PageType','$Seq')";
		echo $sql;
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function deleteSubMenu($subMenuMInput){
		$Id = $subMenuMInput->getSubMenuId();
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
	
		$sql="DELETE FROM submenu WHERE submenu_id='$Id'";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}

	/*
	 * Sub menu list section
	 */
	
	public function loadSubMenuList($id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$newquery = "SELECT DISTINCT m.submenu_list_id,m.submenu_list_text,s.link,m.submenu_id,m.page_type_id, m.seq FROM submenu_list as m LEFT join staticpage as s ON m.submenu_list_link = s.id where m.submenu_id= '$id' ORDER BY  m.seq desc";
		$resultSub = mysqli_query($db, $newquery);
		if (mysqli_num_rows($resultSub) > 0) {
			while($row = mysqli_fetch_assoc($resultSub)) {
				$submenuListM = new submenulistBOM();	
				$submenuListM->setSubmenuListId($row['submenu_list_id']);
				$submenuListM->setSubmenuListText($row['submenu_list_text']);				
				$submenuListM->setSubmenuListLink($row['link']);
				$submenuListM->setSubmenuId($row['submenu_id']);
				$submenuListM->setSubmenuListSeq($row['seq']);
				array_push($data,$submenuListM);
			}

		} else {
			//echo "0 results";
		} 
		mysqli_close($db);
		return $data;
	}
	
	public function getChildSubmenuListID($submenuID){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT DISTINCT m.submenu_list_id FROM submenu_list as m  where m.submenu_id='$submenuID'" ;
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$submenuListM = new submenulistBOM();	
				$submenuListM->setSubmenuListId($row['submenu_list_id']);
				array_push($data,$submenuListM);
			}
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function updateSubMenuList($submenuListMInput){
		$Id = $submenuListMInput->getSubmenuListId();
		$Text = $submenuListMInput->getSubmenuListText();
		$PageType = $submenuListMInput->getPageTypeId();
		$Link = $submenuListMInput->getSubmenuListLink();
		$Seq = $submenuListMInput->getSubmenuListSeq();
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
	
		$sql="UPDATE submenu_list SET submenu_list_text ='$Text' ,submenu_list_link='$Link' ,page_type_id='$PageType' ,seq='$Seq' WHERE submenu_list_id='$Id'  ";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function insertSubMenuList($submenulistMInput){
		$Id = $submenulistMInput->getSubmenuId();
		$Text = $submenulistMInput->getSubmenuListText();
		$PageType = $submenulistMInput->getPageTypeId();
		$Link = $submenulistMInput->getSubmenuListLink();
		$Seq = $submenulistMInput->getSubmenuListSeq();
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;

		$sql=" INSERT INTO submenu_list(submenu_list_id, submenu_list_text, submenu_list_link, submenu_id, page_type_id ,seq) VALUES ('','$Text','$Link','$Id','$PageType','$Seq')";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function deleteSubMenuList($subMenuListMInput){
		$Id = $subMenuListMInput->getSubmenuListId();
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		
		$sql="DELETE FROM submenu_list WHERE submenu_list_id='$Id'";
		echo $sql;
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
}

?>
