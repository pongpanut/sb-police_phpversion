<?php   namespace SB\conn\daoImpl;
@session_start();
use  SB\conn\connection;
use  SB\model\userM;
 include_once '../conn/connection.php';
include_once '../model/userM.php';

class userDaoImpl{
	
	public function loadUser($user){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql=" SELECT m.* FROM admin as m  WHERE m.admin_user like '%$user%'";
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$userM = new userM();
				$userM->setAdminId($row['admin_id']);
				$userM->setAdminUser($row['admin_user']);
				$userM->setAdminPassword($row['admin_password']);
				$userM->setAdminPrivillege($row['admin_privillege']);
				array_push($data,$userM);
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $data;
	}
	
	public function loadAllUserNoSuperuser(){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		//$sql=" SELECT m.* FROM admin as m WHERE m.admin_user not like '%superuser%'";
		$sql="SELECT m.* , p.page_type_text as pagetext FROM admin as m LEFT JOIN pagetype_mapping as p ON p.page_type_id = m.page_type_id WHERE m.admin_user  not like '%superuser%' order by admin_id desc";
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$userM = new userM();
				$userM->setAdminId($row['admin_id']);
				$userM->setAdminUser($row['admin_user']);
				$userM->setAdminPassword($row['admin_password']);
				$userM->setAdminPrivillege($row['admin_privillege']);
				$userM->setPageTypeText($row['pagetext']);
				array_push($data,$userM);
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $data;
	}
	
	public function loadAllUserWithtext(){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT DISTINCT(m.`admin_user`) , p.page_type_text FROM admin as m LEFT JOIN pagetype_mapping as p ON p.page_type_id = m.page_type_id WHERE m.admin_user  not like '%superuser%'";
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$userM = new userM();
				$userM->setAdminId($row['admin_id']);
				$userM->setAdminUser($row['admin_user']);
				$userM->setAdminPassword($row['admin_password']);
				$userM->setAdminPrivillege($row['admin_privillege']);
				array_push($data,$userM);
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $data;
	}
	
	public function updateAdminpass($user, $newpass){

		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="UPDATE admin SET admin_password = '$newpass' WHERE admin_user = '$user'";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function updateAdminpassById($id, $newpass){
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="UPDATE admin SET admin_password = '$newpass' WHERE admin_id = '$id'";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	

}

?>
