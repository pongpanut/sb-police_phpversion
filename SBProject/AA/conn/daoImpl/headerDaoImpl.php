<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\conn\dao\headerDao;
use  SB\model\headerBOM;
	  include_once '../conn/connection.php';

	  include '../model/headerBOM.php';

class headerDaoImpl implements headerDao{
	
	
	public function insertHeader($headerBOM){
	
		$logo = $headerBOM->getHeaderLogo() ;
		$text =$headerBOM->getHeaderText();
		$title=	$headerBOM->getHeaderTitle();
		$pageTypeId=$headerBOM->getHeaderPageTypeId();
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="INSERT INTO header ( header_logo, header_text, header_title, header_page_type_id) VALUES ('$logo', '$text', '$title', $pageTypeId)";
		//echo $sql;
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function updateHeader($headerBOM){
	
		$logo = $headerBOM->getHeaderLogo() ;
		$text =$headerBOM->getHeaderText();
		$title=	$headerBOM->getHeaderTitle();
		$pageTypeId=$headerBOM->getHeaderPageTypeId();
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="UPDATE header SET ";
		if($logo!="" && $logo!=null){
			$sql.="header_logo = '$logo',";
		}
		$sql.=" header_text = '$text', header_title = '$title' WHERE header_page_type_id = $pageTypeId ";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function loadHeader($page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$sql=" SELECT m.* FROM header as m  WHERE m.header_page_type_id ='$page_type_id' order by header_id desc";
		$result = mysqli_query($db, $sql);
		$headerM = new headerBOM();
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$headerM->setHeaderId($row['header_id']);
				$headerM->setHeaderLogo($row['header_logo']);
				$headerM->setHeaderText($row['header_text']);
				$headerM->setHeaderTitle($row['header_title']);
				$headerM->setHeaderPageTypeId($row['header_page_type_id']);
				
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $headerM;
	}
	
	public function IsExistHeader($page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$sql=" SELECT count(*) as countrow FROM header as m  WHERE m.header_page_type_id='$page_type_id'";
		$result = mysqli_query($db, $sql);
		$resultrow = 0;
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$resultrow=$row['countrow'];
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $resultrow;
	}
	
	
}

?>
