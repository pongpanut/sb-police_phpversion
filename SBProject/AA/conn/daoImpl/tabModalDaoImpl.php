<?php   namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\tab_modalM;
include_once '../conn/connection.php';
include_once '../model/tab_modalM.php';

class tabModalDaoImpl{
	
	public function loadTabModal($page_type_id,$tab_content_id){
		$conn =new  connection();
		$db = $conn->getConnection();
	
		$sql=" SELECT modal_id, modal_header, modal_content, tab_content_id, page_type_id ,rec_date FROM tab_modal  where page_type_id= $page_type_id and tab_content_id=$tab_content_id order by modal_id desc";
		$result = mysqli_query($db, $sql);
		$tab_modalM = new tab_modalM();
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				
				$tab_modalM->setTabModalId($row['modal_id']);
				$tab_modalM->setTabModalHeader($row['modal_header']);
				$tab_modalM->setTabModalContent($row['tab_content_id']);
				$tab_modalM->setTabContentId($row['tab_content_id']);
				$tab_modalM->setPageTypeId($row['tab_content_id']);
				$tab_modalM->setRecDate($row['rec_date']);
				
			}
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $tab_modalM;
	}
	
	
	public function InsertTabModal($tab_modalM){
		
	$tabModalHeader=$tab_modalM->getTabModalHeader();
	$tabModalContent=$tab_modalM->getTabModalContent();
	$tabContentId=$tab_modalM->getTabContentId();
	$pageTypeId=$tab_modalM->getPageTypeId();

		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql=" INSERT INTO tab_modal ( modal_header, modal_content, tab_content_id, page_type_id , rec_date) VALUES ( '$tabModalHeader', '$tabModalContent', $tabContentId, $pageTypeId,now()) ";
		
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	public function updateTabModal($tab_modalM){
		
		$id=$tab_modalM->getTabModalId();
		$tabModalHeader=$tab_modalM->getTabModalHeader();
		$tabModalContent=$tab_modalM->getTabModalContent();
		$tabContentId=$tab_modalM->getTabContentId();
		$pageTypeId=$tab_modalM->getPageTypeId();
		
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		if($id!=""){
			$sql=" UPDATE tab_modal SET  modal_header = '$tabModalHeader', modal_content = '$tabModalContent', tab_content_id =$tabContentId, page_type_id = $pageTypeId , rec_date=now() WHERE tab_content_id =$tabContentId ";
		}else{
			$sql=" UPDATE tab_modal SET  modal_header = '$tabModalHeader', modal_content = '$tabModalContent', tab_content_id =$tabContentId, page_type_id = $pageTypeId , rec_date=now() WHERE modal_id =$id ";
		}
		
		
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	
	public function deleteTabModal($id){
		$id=$id;
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="DELETE FROM tab_modal WHERE modal_id =$id";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function getSeqModal(){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = 0;
		$sql="SELECT `AUTO_INCREMENT`	FROM  INFORMATION_SCHEMA.TABLES	WHERE TABLE_SCHEMA = 'specialbranch'	AND   TABLE_NAME   = 'tab_modal'"; // aaaaa
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$data= $row['AUTO_INCREMENT'];
			}
		} else {
			//echo "0 results";
		}
		echo $data;
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}

}

?>
