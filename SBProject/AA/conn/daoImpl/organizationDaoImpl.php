<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\organizationBOM;

include_once '../conn/connection.php';
include_once '../model/organizationBOM.php';


class organizationDaoImpl{
	
	public function loadOrgarnization($page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT o.org_id, o.org_pos, o.org_name, o.org_img, o.org_currow, o.org_pagetype FROM organization as o where o.org_pagetype = '$page_type_id' order by o.org_id desc";
	
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$organizationBOM = new organizationBOM();
				$organizationBOM->setOrgId($row['org_id']);
				$organizationBOM->setOrgPos($row['org_pos']);
				$organizationBOM->setOrgName($row['org_name']);
				$organizationBOM->setOrgImg($row['org_img']);
				$organizationBOM->setOrgCurrow($row['org_currow']);
				$organizationBOM->setpagetype($row['org_pagetype']);
				array_push($data,$organizationBOM);
			}
	
		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	
	public function insertOrganization($organizationBOM){
	    $org_pos = $organizationBOM->getOrgPos();
		$org_name = $organizationBOM->getOrgName();
		$org_img = $organizationBOM->getOrgImg();
		$org_currow = $organizationBOM->getOrgCurrow();
		$org_pagetype = $organizationBOM->getpagetype();

		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql = "INSERT INTO organization(org_id, org_pos, org_name, org_img, org_currow, org_pagetype) VALUES ('','$org_pos','$org_name','$org_img','$org_currow','$org_pagetype')";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	


	
	public function deleteOrganizationByPageType($pagetype){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
	
		$sql="DELETE FROM organization WHERE org_pagetype='$pagetype'  ";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
		
	}
	
	public function UpdateOrg($organizationBOM){
		$id =$organizationBOM->getOrgId();
		$org_pos = $organizationBOM->getOrgPos();
		$org_name = $organizationBOM->getOrgName();
		$org_img = $organizationBOM->getOrgImg();
		$org_currow = $organizationBOM->getOrgCurrow();
		$org_pagetype = $organizationBOM->getpagetype();
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql=" UPDATE organization SET  org_pos = '$org_pos', org_name = '$org_name', ";
		if(!empty($org_img)){
			$sql.=	" org_img = '$org_img',";
		}		
		$sql.=  "org_pagetype = $org_pagetype WHERE org_id = $id ";

		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
}

?>
