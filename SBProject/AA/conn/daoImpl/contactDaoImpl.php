<?php   namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\contactBOM;
include_once '../conn/connection.php';
include_once '../model/contactBOM.php';

class contactDaoImpl{
	
	public function loadContact($contact_page_type_id){
		$conn =new  connection();
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT id, name, email, phone, message, page_type_id , answer FROM contact where page_type_id ='$contact_page_type_id' order by id desc";
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$contactBOM = new contactBOM();
				$contactBOM->setContactId($row['id']);
				$contactBOM->setName($row['name']);
				$contactBOM->setEmail($row['email']);
				$contactBOM->setPhone($row['phone']);
				$contactBOM->setMessage($row['message']);
				$contactBOM->setPageTypeId($row['page_type_id']);
				$contactBOM->setAns($row['answer']);
				array_push($data,$contactBOM);
			}
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function deleteContact($id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		
		$sql="DELETE FROM contact WHERE id='$id'";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
		
		mysqli_close($db);
		return $data;
	}
	
	public function UpdateContact($contactBOM){
			$id =	$contactBOM->getContactId();
			$pageTypeId =	$contactBOM->getPageTypeId();
			$ans =	$contactBOM->getAns();

	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="UPDATE contact SET   answer = '$ans' WHERE id = $id and page_type_id = $pageTypeId";
		
	
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	

}

?>
