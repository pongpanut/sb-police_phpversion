<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use SB\model\missionM;
	  include_once '../conn/connection.php';
	  include '../model/missionM.php';


class missionDaoImpl{
	
	
	public function InsertMission($missionM){
	
		 $mission_id = $missionM->getMissionId();
		 $mission_text = $missionM->getMissionText();
		 $mission_header = $missionM->getMissionHeader();
		 $mission_image = $missionM->getMissionImage();
		 $mission_desc = $missionM->getMissionDesc();
		 $mission_type = $missionM->getMissionType();
		 $show_flag = $missionM->getShowFlag();
		 $page_type_id = $missionM->getPageTypeId();
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="INSERT INTO mission ( mission_text, mission_header, mission_image, mission_desc, mission_type, show_flag, page_type_id,rec_date) VALUES ( '$mission_text', '$mission_header', '$mission_image', '$mission_desc', '$mission_type', '$show_flag', $page_type_id,now())";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function UpdateMission($missionM){
		$mission_id = $missionM->getMissionId();
		$mission_text = $missionM->getMissionText();
		$mission_header = $missionM->getMissionHeader();
		$mission_image = $missionM->getMissionImage();
		$mission_desc = $missionM->getMissionDesc();
		$mission_type = $missionM->getMissionType();
		$show_flag = $missionM->getShowFlag();
		$page_type_id = $missionM->getPageTypeId();
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql=" UPDATE ".
    		 " mission ".
				" SET ".
				    " mission_text = '$mission_text', ".
				    " mission_header = '$mission_header', ".
				    " mission_image = '$mission_image', ".
				    " mission_desc = '$mission_desc', ".
				    " mission_type = '$mission_type', ".
				    " show_flag = '$show_flag', ".
				    " page_type_id = $page_type_id ,".
				    "rec_date =now()".
				" WHERE".
				    " mission_id = $mission_id";

		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	public function getMissions($page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT mission_id, mission_text, mission_header, mission_image, mission_desc, mission_type, show_flag, page_type_id,rec_date FROM mission where page_type_id= $page_type_id order by mission_id desc";
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$missionM = new missionM();
				$missionM->setMissionId($row['mission_id']);
				$missionM->setMissionText($row['mission_text']);
				$missionM->setMissionHeader($row['mission_header']);
				$missionM->setMissionImage($row['mission_image']);
				$missionM->setMissionDesc($row['mission_desc']);
				$missionM->setMissionType($row['mission_type']);
				$missionM->setShowFlag($row['show_flag']);
				$missionM->setPageTypeId($row['page_type_id']);
				$missionM->setPageTypeId($row['rec_date']);
				array_push($data,$missionM);
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $data;
	}
	
	public function deleteMission($id){
		$id=$id;
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
	
		$sql="DELETE FROM mission WHERE mission_id = $id";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function getMissionById($page_type_id,$id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$missionM = new missionM();
		$sql="SELECT mission_id, mission_text, mission_header, mission_image, mission_desc, mission_type, show_flag, page_type_id,rec_date FROM mission where page_type_id= $page_type_id and mission_id =$id order by mission_id desc";
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				
				$missionM->setMissionId($row['mission_id']);
				$missionM->setMissionText($row['mission_text']);
				$missionM->setMissionHeader($row['mission_header']);
				$missionM->setMissionImage($row['mission_image']);
				$missionM->setMissionDesc($row['mission_desc']);
				$missionM->setMissionType($row['mission_type']);
				$missionM->setShowFlag($row['show_flag']);
				$missionM->setPageTypeId($row['page_type_id']);
				$missionM->setPageTypeId($row['rec_date']);
			
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $missionM;
	}

	
}

?>
