$(document).ready(function() {  
	 $('#load_screen').hide();
    $(document).on("click", "#modifymodal", function(e){
    	e.preventDefault();
    	var formValue = $(this).val();
    	var msg= validateForm(formValue);
       	if(msg !=""){
       		alert(msg);
    		return false;
    	}
    	
    	var id  = $('#'+formValue).attr('value');
    	var menuText = $('#menutextModal'+id).val();
    	var dropdown = $('#heardmodal'+id).val();
    	var seq = $('#seqModal'+id).val();
    	var pageType =1;
    	var hasSubmenu = 1;
    	if(dropdown > 0){
    		hasSubmenu = 0;
    	}
    	
	    jQuery.ajax({
	        type: "POST",
	        url: 'AA_updateform.php',
	        data: {id: id, menuText:menuText , dropdown:dropdown, seq:seq ,pageType:pageType,hasSubmenu:hasSubmenu}, 
	         success:function(data) {
	        	 //alert(data);
	        	 if(data!=""){
	        		 alert(data)
	        	 }
	        	 
	         }
	  
	    });
  	});
    
    $(document).on("click", "#delmodal", function(e){
    	e.preventDefault();
    	var formValue = $(this).val();
    	var id  = $('#'+formValue).attr('value');
    	var menuText = $('#menutextModal'+id).val();
    	var dropdown = $('#heardmodal'+id).val();
    	var seq = $('#seqModal'+id).val();
  	});
    
    $(document).on("click", "#buttonRemove", function(e){
    	e.preventDefault();
    	var id = $(this).val();
    	
    	bootbox.confirm("Are you sure?", function(result) {
    		  if(result){
    			  jQuery.ajax({
    			        type: "POST",
    			        url: 'AA_deletemenuform.php',
    			        data: {id:id }, 
    			         success:function(data) {
    			        	 location.reload();
    			         }
    			    });
    		  }
    		  else{
    			  return;
    		  }
    	}); 
  	});
    
    $(document).on("click", "#deleteContactInbox", function(e){
    	e.preventDefault();
    	var id = $(this).val();
    	
    	bootbox.confirm("Are you sure?", function(result) {
    		  if(result){
    			  jQuery.ajax({
    			        type: "POST",
    			        url: 'AA_deleteContact.php',
    			        data: {id:id }, 
    			         success:function(data) {
    			        	 alert(data);
    			        	 location.reload();
    			         }
    			    });
    		  }
    		  else{
    			  return;
    		  }
    	}); 
  	});
    
    $("#updateAns").click(function() {

    	var id =$(this).val();
    	var msg= validateForm("contactDetail"+id);
		var pagetype=$("#pageType").val();
		var ans = $("#ans"+id).val();
    	var mode ="edit";
    	
       	if(msg !=""){
       		alert(msg);
    		return false;
    	}
    	jQuery.ajax({
	        type: "POST",
	        url: 'AA_deleteContact.php',
	        data:{id:id,pagetype:pagetype,ans:ans,mode:mode}, 
	         success:function(data) {
	        	//console.log(data);
	        	 location.reload();
	         }
	    });
    });
    
    
    
    
    
    $("#insertButton").click(function() {
    	var menuText = $('#insertMenuText').val();
    	var dropdown = $('#insertLink').val();
    	var seq = $('#insertSeq').val();
    	var msg= validateForm("departmentmanagementForm");
       	if(msg !=""){
       		alert(msg);
    		return false;
    	}
    	var pageType = 1;
    	var hasSubmenu = 1;
    	if(dropdown > 0){
    		hasSubmenu = 0;
    	}
    	
    	
    	jQuery.ajax({
	        type: "POST",
	        url: 'AA_insertmenuform.php',
	        data: {menuText:menuText , dropdown:dropdown, seq:seq ,pageType:pageType,hasSubmenu:hasSubmenu}, 
	         success:function(data) {
	        	 location.reload();
	         }
	    });
    });
    
    $("#insertDepartmentListButton").click(function() {
    	var menuText = $('#insertDepartmentListText').val();
    	var dropdown = $('#insertDepartmentListLink').val();
    	var seq = $('#insertDepartmentListSeq').val();
    	var pagetype = $('#pagetype').val();
    	var msg= validateForm("departmentmanagementlistForm");
       	if(msg !=""){
       		alert(msg);
    		return false;
    	}   	
    	
    	jQuery.ajax({
	        type: "POST",
	        url: 'AA_insertdepartlist.php',
	        data: {menuText:menuText , dropdown:dropdown, seq:seq ,pagetype:pagetype}, 
	         success:function(data) {
	        	 location.reload();
	         }
	    });
    });

   $(document).on("click", "#submitStatic", function(e){
    	e.preventDefault();
     	
    	var formValue = $(this).val();
    	var msg= validateForm(formValue);
       	if(msg !=""){
       		alert(msg);
    		return false;
    	}
    	var id  = $('#'+formValue).attr('value');
    	var staticText = $('#staticText'+id).val();
    	var staticLink = $('#staticLink'+id).val();
    	var staticActive = $('#staticActive'+id+' option:selected').val();
    	var elm1=tinyMCE.get('elm1'+id).getContent();
    	var replaced = elm1.replace('\n', '');
    	var pageType =$("#page_type_id").val();
    	var mode="add";
    	jQuery.ajax({
	        type: "POST",
	        url: 'AA_StaticForm.php',
	        data: {id: id, staticText:staticText , staticLink:staticLink,staticActive:staticActive ,elm1:replaced,pageType:pageType,mode:mode}, 
	         success:function(data) {
	        	//console.log(data);
	        	location.reload();
	         }
	    });
    
  	});
   $(document).on("click", "#submitStaticAdd", function(e){
   	e.preventDefault();
   	var formValue = $(this).val();
   	var msg= validateForm(formValue);
   	if(msg !=""){
   		alert(msg);
		return false;
	}
   	var id  = $('#'+formValue).attr('value');
   	var staticText = $('#staticTextAdd').val();
   	var staticActive = $('#staticActiveAdd option:selected').val();
   	var elm1=	tinyMCE.get('elm1Add').getContent();
   	var pageType =$("#page_type_id").val();
	var mode="add";
   	jQuery.ajax({
	        type: "POST",
	        url: 'AA_StaticForm.php',
	        data: {id: id, staticText:staticText ,staticActive:staticActive ,elm1:elm1,pageType:pageType,mode:mode}, 
	         success:function(data) {
	        	//console.log(data);
	        	location.reload();
	         }
	    });
   
 	});
   
	   $(document).on("click", "#StaticButtonRemove", function(e){
		   var mode="delete";
		   if (confirm("ลบข็อมูล")) {
			   var id  =$(this).val();
			   	var mode="delete";
			   	jQuery.ajax({
				        type: "POST",
				        url: 'AA_StaticForm.php',
				        data: {id: id,mode:mode}, 
				         success:function(data) {
				        	location.reload();
				         }
				    });
		   	}
		   return false;
		});

   /*
    *	Below section for submenu management 
    */
   $('#menuList').change( function() {
	   		  if ( $(this).val() != '0' && $(this).val() !="")
		      {
	   			  $("#inputForm2").show();
		      }
		      else
		      {
		    	  $("#inputForm2").hide();
		      }
	   
		   jQuery.ajax({ // create an AJAX call...
	           type: "POST", 
	           url: 'AA_submenumanagementTableApi.php',  
	           data: {dropdown: $(this).val()}, 
	           success: function(response) { 
	        	//   alert(response);
	               $('#output').html(response); 
	           },
	           error: function (xhr, ajaxOptions, thrownError) {
	               alert(xhr.status);
	               alert(thrownError);
	             }
	       });
   });
   

  
   
   $(document).on("click", "#modifysubmenumodal", function(e){
   	e.preventDefault();
   	var formValue = $(this).val();
   	var msg= validateForm(formValue);
   	if(msg !=""){
   		alert(msg);
		return false;
	}
   	
   	var id  = $('#'+formValue).attr('value');
   	var menuText = $('#submenutextModal'+id).val();
   	var dropdown = $('#submenuOptionmodal'+id).val();
   	var seq = $('#submenuseqModal'+id).val();
   	var pageType =1;
   	var hasSubmenu = 1;
   	if(dropdown > 0){
   		hasSubmenu = 0;
   	}
 
	    jQuery.ajax({
	        type: "POST",
	        url: 'AA_submenumanageUpdateSub.php',
	        data: {id: id, menuText:menuText , dropdown:dropdown, seq:seq ,pageType:pageType,hasSubmenu:hasSubmenu}, 
	         success:function(data) {
	        	 location.reload();
	         },
	        error: function (xhr, ajaxOptions, thrownError) {
	               alert(xhr.status);
	               alert(thrownError);
	             }
	    });
 	});
   
   $("#insertSubmenuButton").click(function() {
   	var menuText = $('#insertSubMenuText').val();
   	var dropdown = $('#AddlinkSubMenuValue').val();
   	var seq = $('#insertSubMenuSeq').val();
   	var menuId = $('#menuList').val();
   	var pageType = 1;
   	var hasSubmenu = 1;
   	
   	var msg= validateForm("submenuForm");
   	if(msg !=""){
   		alert(msg);
		return false;
	}
   	if(dropdown > 0){
   		hasSubmenu = 0;
   	}
   	
   	
   	jQuery.ajax({
	        type: "POST",
	        url: 'AA_submenumanageInsertSub.php',
	        data: {menuText:menuText , dropdown:dropdown, seq:seq ,menuId:menuId,pageType:pageType,hasSubmenu:hasSubmenu}, 
	         success:function(data) {
	        	 location.reload();
	         }
	    });
	     
   });
   
   $("#insertDepartmentSubButton").click(function() {
	   	var text = $('#DepartmentSubText').val();
	   	var dropdown = $('#AddDepartmentSub').val();
	   	var seq = $('#DepartmentSubSeq').val();
	   	var pageType = $('#pagetype').val();
	   	var departmentId = $('#departmentList').val()

	   	
	   	var msg= validateForm("departmentsubForm");
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}

	   	jQuery.ajax({
		        type: "POST",
		        url: 'AA_departmentsubInsert.php',
		        data: {text:text , dropdown:dropdown, seq:seq ,departmentId:departmentId,pageType:pageType}, 
		         success:function(data) {
		        	 alert(data);
		        	 location.reload();
		         }
		    });
		     
	   });
   
   
   
   $(document).on("click", "#buttonRemoveSubmenu", function(e){
   	e.preventDefault();
   	var id = $(this).val();
   	
   	bootbox.confirm("Are you sure?", function(result) {
   		  if(result){
   			  jQuery.ajax({
   			        type: "POST",
   			        url: 'AA_submenumanagedeleteSub.php',
   			        data: {id:id }, 
   			         success:function(data) {
   			        	 location.reload();
   			         }
   			    });
   		  }
   		  else{
   			  return;
   		  }
   	}); 
 	});

   /*End of Submenu management*/
   
   /*
    * Below section for Sub menu list section
    */
   $('#submenuList').change( function() {
		  if ( $(this).val() != '0' && $(this).val() !="")
	      {
			  $("#inputFormSub").show();
	      }
	      else
	      {
	    	  $("#inputFormSub").hide();
	      }

	   jQuery.ajax({ // create an AJAX call...
        type: "POST", 
        url: 'AA_submenuListTableApi.php',  
        data: {dropdown: $(this).val()}, 
        success: function(response) { 
     	//   alert(response);
            $('#output').html(response); 
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
          }
	   });
   });
   
   $(document).on("click", "#modifysubmenulistmodal", function(e){
	   	e.preventDefault();
	   	var formValue = $(this).val();
	   	var id  = $('#'+formValue).attr('value');
	   	var menuText = $('#submenulisttextModal'+id).val();
	   	var dropdown = $('#submenulstOptionmodal'+id).val();
	   	var seq = $('#submenulistseqModal'+id).val();
	   	var pageType =1;
	   	var msg= validateForm(formValue);
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
		
	   	
		    jQuery.ajax({
		        type: "POST",
		        url: 'AA_submenuListUpdate.php',
		        data: {id: id, menuText:menuText , dropdown:dropdown, seq:seq ,pageType:pageType}, 
		         success:function(data) {
		        	 //alert(data);
		        	 location.reload();
		         },
		        error: function (xhr, ajaxOptions, thrownError) {
		               alert(xhr.status);
		               alert(thrownError);
		             }
		    });
		    
	 	});
   
   $(document).on("click", "#modifydepartmentsubmodal", function(e){
	   	e.preventDefault();
	   	var formValue = $(this).val();
	   	var id  = $('#'+formValue).attr('value');
	   	var text = $('#departmentsubtextModal'+id).val();
	   	var dropdown = $('#departmentsubOptionmodal'+id).val();
	   	var seq = $('#departmentsubseqModal'+id).val();
	   	var pageType =1;
	   	var msg= validateForm(formValue);
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
		
	   	
		    jQuery.ajax({
		        type: "POST",
		        url: 'AA_departmentSubUpdate.php',
		        data: {id: id, text:text , dropdown:dropdown, seq:seq }, 
		         success:function(data) {
		        	 location.reload();
		         },
		        error: function (xhr, ajaxOptions, thrownError) {
		               alert(xhr.status);
		               alert(thrownError);
		             }
		    });
		    
	 	});
   
   $("#insertSubmenuListButton").click(function() {
	   	var menuText = $('#insertSubMenuListText').val();
	   	var dropdown = $('#AddlinkSubMenuListValue').val();
	   	var seq = $('#insertSubMenuListSeq').val();
	   	var menuId = $('#submenuList').val();
	   	var pageType = 1;   	   	
	   	var msg= validateForm("submenuList");
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
	   	jQuery.ajax({
		        type: "POST",
		        url: 'AA_submenuListInsert.php',
		        data: {menuText:menuText , dropdown:dropdown, seq:seq ,menuId:menuId,pageType:pageType }, 
		         success:function(data) {
		        	 //alert(data);
		        	 location.reload();
		         }
		    });
		     
	   });
   
   $(document).on("click", "#buttonRemoveSubmenuList", function(e){
	   	e.preventDefault();
	   	var id = $(this).val();
	   	
	   	bootbox.confirm("Are you sure?", function(result) {
	   		  if(result){
	   			  jQuery.ajax({
	   			        type: "POST",
	   			        url: 'AA_submenuListDelete.php',
	   			        data: {id:id }, 
	   			         success:function(data) {
	   			        	 //alert(data);
	   			        	 location.reload();
	   			         }
	   			    });
	   		  }
	   		  else{
	   			  return;
	   		  }
	   	}); 
	 });
   
   $(document).on("click", "#buttonRemoveDepartmentsub", function(e){
	   	e.preventDefault();
	   	var id = $(this).val();
	   	
	   	bootbox.confirm("Are you sure?", function(result) {
	   		  if(result){
	   			  jQuery.ajax({
	   			        type: "POST",
	   			        url: 'AA_departmentsubDelete.php',
	   			        data: {id:id }, 
	   			         success:function(data) {
	   			        	 alert(data);
	   			        	 location.reload();
	   			         }
	   			    });
	   		  }
	   		  else{
	   			  return;
	   		  }
	   	}); 
	 });
   
   $(document).on("click", "#submitMissionEdit", function(e){
   	e.preventDefault();
   	var formValue = $(this).val();
	var msg= validateForm(formValue);
   	if(msg !=""){
   		alert(msg);
		return false;
	}
   	var id  = $('#'+formValue).attr('value');
   	var missionHeader = $('#missionHeader'+id).val();
   	var missionDesc = $('#missionDesc'+id).val();
   	var missionType = $('#missionType'+id+' option:selected').val();
 	var showFlag = $('#showFlag'+id+' option:selected').val();
   	var elm1=tinyMCE.get('elm1'+id).getContent();
   	var replaced = elm1.replace('\n', '');
   	var pageType =$('#PageTypeId').val();
	var missionImage = $('#missionImage'+id+' option:selected').val();
   	var mode="add";
   	jQuery.ajax({
	        type: "POST",
	        url: 'AA_missionForm.php',
	        data: {id: id, missionHeader:missionHeader , missionType:missionType ,showFlag:showFlag,elm1:replaced,mode:mode,pageType:pageType,missionImage:missionImage, missionDesc, missionDesc}, 
	         success:function(data) {
	       // console.log(data);
	        	location.reload();
	         }
	    });
   
 	});
   $(document).on("click", "#submitMission", function(e){
	   	e.preventDefault();
	   	var id  ="";
	   	var msg= validateForm("missionFormAdd");
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
	   	var missionHeader = $('#missionHeaderAdd').val();
	   	var missionDesc = $('#missionDescAdd').val();
	   	var missionType = $('#missionTypeAdd option:selected').val();
	 	var showFlag = $('#showFlagAdd option:selected').val();
	   	var elm1=tinyMCE.get('elm1Add').getContent();
	   	var replaced = elm1.replace(/\n/g, '');
	   	var pageType =$('#PageTypeId').val();
	   	var missionImage = $('#missionImageAdd option:selected').val();
	   	var mode="add";
	   	jQuery.ajax({
		        type: "POST",
		        url: 'AA_missionForm.php',
		        data: {id: id, missionHeader:missionHeader , missionType:missionType ,showFlag:showFlag,elm1:replaced,mode:mode,pageType:pageType,missionImage:missionImage,missionDesc:missionDesc}, 
		         success:function(data) {
		       // console.log(data);
		        	location.reload();
		         }
		    });
	   
	 	});
   
   $(document).on("click", "#buttonRemoveMission", function(e){	
	    var mode="delete";
		   if (confirm("ลบข็อมูล")) {
			   var id  =$(this).val();
			   	var mode="delete";
			   	jQuery.ajax({
				        type: "POST",
				        url: 'AA_missionForm.php',
				        data: {id: id,mode:mode}, 
				         success:function(data) {
				        	location.reload();
				         }
				    });
		   	}
		   return false;
	   
	 	});
   $(document).on("click", "#buttonRemoveNews", function(e){	
	    var mode="delete";
	 	var id = $(this).val();
		   if (confirm("ลบข้อมูล")) {	
			   var mode="delete";	  
			   	jQuery.ajax({
				        type: "POST",
				        url: 'AA_newsForm.php',
				        data: {id: id,mode:mode}, 
				         success:function(data) {
				        	// console.log(data);
				        	location.reload();
				         }
				    });
		   	}
		   return false;
	   
	 	});
   

   /*
    * 	Subscript section
    */
   
   
   $(document).on("click", "#deleteSubscript", function(e){
	   	e.preventDefault();
	   	var id = $(this).val();
	   	
	   	bootbox.confirm("Are you sure?", function(result) {
	   		  if(result){
	   			  jQuery.ajax({
	   			        type: "POST",
	   			        url: 'AA_subscriptDelete.php',
	   			        data: {id:id }, 
	   			         success:function(data) {
	   			        	// alert(data);
	   			        	 location.reload();
	   			         }
	   			    });
	   		  }
	   		  else{
	   			  return;
	   		  }
	   	}); 
	 }); 
   
   $("#SendEmail").on('submit',(function(e) {
	   e.preventDefault();
	   var msg= validateForm("SendEmail");
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
	   var subject  = $('#emailHeaderAdd').val();
	   var elm1=tinyMCE.get('elm1Add').getContent();
	   var body = elm1.replace(/(?:\r\n|\r|\n)/g, '<br />');

		   $.ajax({
		   url: "AA_sendEmail.php", 
		   type: "POST",            
		   data: {subject: subject,body:body}, 
		   success: function(data)
			   	{
				   if(data != ''){
					 alert(data);
				   }
				   else{
					   location.reload();
				   }
			   	}
		   });
		   
   }));
   
   
   /*
    * 	Change password section
    */
   
   $("#Changpassbutton").click(function() {
	   	var username = $('#hiddenuser').val();
	   	var currentpass = $('#currpass').val();
	   	var newpass = $('#newpass').val();
	   	var connewpass = $('#confirmnewpass').val();
	   	
	   	if(newpass != connewpass){
	   		alert("รหัสผ่านใหม่ที่ตั้งและรหัสผ่านในช่องยืนยันรหัสผ่านไม่ตรงกัน");
	   		return;
	   	}
	   	
	    jQuery.ajax({
	        type: "POST",
	        url: 'AA_passwordchangeCheckLogin.php',
	        data: {user: username, pass: currentpass, newpass: newpass }, 
	         success:function(data) {
	        	 if(data == '1'){
		        	 alert("ตั้งรหัสผ่านใหม่สำเร็จ");
		        	 location.reload();
	        	 }
	        	 else if(data == '2'){
	        		 alert("ท่านกรอกรหัสผ่านปัจจุบันผิดพลาด");
		        	 location.reload();
	        	 }
	        	 else{
	        		 alert($data);
	        	 }
	         },
	         error: function(ts) { console.log(ts.responseText) }
	    });
	   
	   });
   
   /*
    * department sub section
    */
   $('#departmentList').change( function() {
		  if ( $(this).val() != '0' && $(this).val() !="")
	      {
			  $("#inputForm2").show();
	      }
	      else
	      {
	    	  $("#inputForm2").hide();
	      }

	   jQuery.ajax({ // create an AJAX call...
     type: "POST", 
     url: 'AA_subdepartmentTableApi.php',  
     data: {dropdown: $(this).val()}, 
     success: function(response) { 
         $('#output').html(response); 
     },
     error: function (xhr, ajaxOptions, thrownError) {
         alert(xhr.status);
         alert(thrownError);
       }
 });
});
   
   /*
    * End department sub section
    */
   
  /* RemoveImage*/
   
   $(document).on("click", "#buttonRemoveImage", function(e){	
	    var mode="delete";
		   if (confirm("ลบข้อมูล")) {
			   var id  =$(this).val();
			   var mode="delete";
			   	jQuery.ajax({
				        type: "POST",
				        url: 'AA_imageForm.php',
				        data: {id: id,mode:mode}, 
				         success:function(data) {
				        	location.reload();
				         }
				    });
		   	}
		   return false;
	   
	 	});
   
   /*
    * 	Department management section
    */
   
   $(document).on("click", "#createDepartment", function(e){
	   var deptText = $('#insertDeptText').val();
	   var symText = $('#symText').val();
	   var msg= validateForm("departmentForm");
	   $('#load_screen').show();
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
	   jQuery.ajax({
	        type: "POST",
	        url: 'AA_createDepartment.php',
	        data: { deptText:deptText,symText:symText }, 
	         success:function(data) {
	        	// console.log(data);
	        	 $('#load_screen').hide();
	        	 location.reload();
	         }
	    });
 	});
   
   $(document).on("click", "#modifyDepartmentmodal", function(e){
   	e.preventDefault();
   	var formValue = $(this).val();
    var msg= validateForm(formValue);
   	if(msg !=""){
   		alert(msg);
		return false;
	}
   	var id  = $('#'+formValue).attr('value');
   	var deptText = $('#departmenttextModal'+id).val();
   	
	    jQuery.ajax({
	        type: "POST",
	        url: 'AA_updatedepartment.php',
	        data: {id: id, deptText:deptText  }, 
	         success:function(data) {
	        	// alert(data);
	        	 location.reload();
	         }
	    });
 	});
   
   $(document).on("click", "#modifyDepartmentListmodal", function(e){
	   	e.preventDefault();
	   	var formValue = $(this).val();
	    var msg= validateForm(formValue);
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
	   	var id  = $('#'+formValue).attr('value');
    	var text = $('#departmentlisttextModal'+id).val();
    	var dropdown = $('#departmentListlinkmodal'+id).val();
    	var seq = $('#departmentListseqmodal'+id).val();
	   	
		    jQuery.ajax({
		        type: "POST",
		        url: 'AA_updatedepartmentlist.php',
		        data: {id: id, text: text, dropdown: dropdown, seq:seq  }, 
		         success:function(data) {
		        	//alert(data);
		        	 location.reload();
		         }
		    });
	 	});
  
  /* $("#imageAddForm").on('submit',(function(e) {
	   e.preventDefault();
	   var msg= validateForm("imageAddForm");
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
	   $.ajax({
	   url: "AA_imageForm.php", 
	   type: "POST",            
	   data: new FormData(this), 
	   contentType: false,  
	   cache: false,       
	   processData:false, 
	   success: function(data)
		   	{
			// console.log(data);
			 location.reload();
		   	}
	   });
   }));
   */
   $("#dynamicOrg").on('submit',(function(e) {
	   e.preventDefault();
	   var test = new FormData(this);
	  // var msg= validateForm("imageAddForm");
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
	   /*$.ajax({
	   url: "AA_imageForm.php", 
	   type: "POST",            
	   data: new FormData(this), 
	   contentType: false,  
	   cache: false,       
	   processData:false, 
	   success: function(data)
		   	{
			// console.log(data);
			 location.reload();
		   	}
	   });*/
   }));
   
   
   $(document).on("click", "#imageEditBtn", function(e){
	   	e.preventDefault();
	   	var id = $(this).val();
	    var msg= validateForm("imageEditForm"+id);
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
	    var formData = new FormData($("#imageEditForm"+id)[0]);
		   $.ajax({
		   url: "AA_imageForm.php", 
		   type: "POST",            
		   data: formData, 
		   contentType: false,  
		   cache: false,       
		   processData:false, 
		   success: function(data)
			   	{
			//   console.log(data)
				location.reload();
			   	}
		   });
	 	});
	   
   
   
   
   
   
   $("#headerUpload").on('submit',(function(e) {
	   e.preventDefault();
	   $.ajax({
	   url: "AA_upload.php", 
	   type: "POST",            
	   data: new FormData(this), 
	   contentType: false,  
	   cache: false,       
	   processData:false, 
	   success: function(data)
		   	{
			 //console.log(data);
			 location.reload();
		   	}
	   });
   }));
   
   $("#AddNews").on('submit',(function(e) {
	   e.preventDefault();
	   var msg= validateForm("AddNews");
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
	   var id  = $('#id').val();
	   var elm1=tinyMCE.get('elm1Add').getContent();
	   var replaced = elm1.replace('\n', '');
	   formData = new FormData(this);
	   formData.append("elm",replaced);
	   $.ajax({
	   url: "AA_newsForm.php", 
	   type: "POST",            
	   data: formData, 
	   contentType: false,  
	   cache: false,       
	   processData:false, 
	   success: function(data)
		   	{
			// console.log(data);
			 location.reload();
		   	}
	   });
   }));
   
   $(document).on("click", "#EditNewsBtn", function(e){
   	e.preventDefault();
   	var id = $(this).val();
   	var msg= validateForm("newsForm"+id);
   	if(msg !=""){
   		alert(msg);
		return false;
	}
    var formData = new FormData($("#newsForm"+id)[0]);
	   var elm1=tinyMCE.get('elm1'+id).getContent();
	   var replaced = elm1.replace('\n', '');
	   formData.append("elm",replaced);
	   formData.append("mode","edit");
	   $.ajax({
	   url: "AA_newsForm.php", 
	   type: "POST",            
	   data: formData, 
	   contentType: false,  
	   cache: false,       
	   processData:false, 
	   success: function(data)
		   	{
		//   console.log(data)
			location.reload();
		   	}
	   });
 	});
   
   
   
   $(document).on("click", "#imageUploadForm", function(e){
	   	e.preventDefault();
	   	var id = $(this).val();
	    var formData = new FormData($("#imageEditForm"+id)[0]);
	    var msg= validateForm("imageEditForm"+id);
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
		   $.ajax({
		   url: "AA_imageForm.php", 
		   type: "POST",            
		   data: formData, 
		   contentType: false,  
		   cache: false,       
		   processData:false, 
		   success: function(data)
			   	{ 
			 //  console.log(data)
				location.reload();
			   	}
		   });
	 	});
   
 
   $(document).on("click", "#bgEdit", function(e){
	   	e.preventDefault();
	   	var id = $(this).val();
	    var formData = new FormData($("#bgForm"+id)[0]);
		   $.ajax({
		   url: "AA_BgForm.php", 
		   type: "POST",            
		   data: formData, 
		   contentType: false,  
		   cache: false,       
		   processData:false, 
		   success: function(data)
			   	{ 
			   //console.log(data)
				location.reload();
			   	}
		   });
	 	});
   
  
 
		 $('#uploadForm').submit(function(e) {
				e.preventDefault();
				var formData = new FormData($("#uploadForm")[0]);
			    var showFlag = $('#status option:selected').val();
			    formData.append("status",showFlag);
			    var msg= validateForm("uploadForm");
			   	if(msg !=""){
			   		alert(msg);
					return false;
				}
				
			    
			    if($('#userImage').val()) {
			   // $('#loader-icon').show();
				$(this).ajaxSubmit({ 
					beforeSubmit: function() {
					  $("#progress-bar").width('0%');
					},
					uploadProgress: function (event, position, total, percentComplete){	
						$("#progress-bar").width(percentComplete + '%');
						$("#progress-bar").html('<div id="progress-status">' + percentComplete +' %</div>')
					},
					success:function (){
					$.ajax({
					   url: "AA_videoForm.php", 
					   type: "POST",            
					   data: formData, 
					   contentType: false,  
					   cache: false,       
					   processData:false, 
					   success: function(data)
						   	{ 
						 // console.log(data)
						//   $('#loader-icon').hide();
						   location.reload();
						   	}
						});
					}
				}); 
			}
		});
	
   
   
   $(document).on("click", "#submitVideoEdit", function(e){
	   var form = $(this).parents('form:first');
	   	e.preventDefault();
	   	var id= $(this).val();
	    var formData = new FormData($("#uploadForm"+id)[0]);    
	    var msg= validateForm("uploadForm"+id);
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
	    var status = $('#status'+id+' option:selected').val();
	    var videoName = $('#videoName'+id).val();
	    var pageTypeId = $('#PageTypeId').val();
	    formData.append("status",status);
	    formData.append("videoName",videoName);
	    formData.append("pageTypeId",pageTypeId);
	    formData.append("id",id);
	    formData.append("mode","edit");
	    formData.append( 'userImage'+id,  $( '#userImage'+id )[0].files[0] );
	    if($('#userImage'+id).val()) {
			$(form).ajaxSubmit({ 
				beforeSubmit: function() {
				  $("#progress-bar"+id).width('0%');
				},
				uploadProgress: function (event, position, total, percentComplete){	
					$("#progress-bar"+id).width(percentComplete + '%');
					$("#progress-bar"+id).html('<div id="progress-status">' + percentComplete +' %</div>')
				},
				success:function (){
				$.ajax({
				   url: "AA_videoForm.php", 
				   type: "POST",            
				   data: formData, 
				   contentType: false,  
				   cache: false,       
				   processData:false, 
				   success: function(data)
					   	{ 
					  //console.log(data)
					   location.reload();
					   	}
					});
				}
			}); 
		}
	 	});


   $(document).on("click", "#buttonDeleteDepartmnt", function(e){
	   	e.preventDefault();
	   	var id = $(this).val();
	   	
	   	bootbox.confirm("Are you sure?", function(result) {
	   		  if(result){
	   			  jQuery.ajax({
	   			        type: "POST",
	   			        url: 'AA_departmentDelete.php',
	   			        data: {id:id }, 
	   			         success:function(data) {
	   			        	 alert(data);
	   			        	 location.reload();
	   			         }
	   			    });
	   		  }
	   		  else{
	   			  return;
	   		  }
	   	}); 
	 }); 
   
   $(document).on("click", "#buttonRemoveDepartmentList", function(e){
	   	e.preventDefault();
	   	var id = $(this).val();
	   	
	   	bootbox.confirm("Are you sure?", function(result) {
	   		  if(result){
	   			  jQuery.ajax({
	   			        type: "POST",
	   			        url: 'AA_departmentListDelete.php',
	   			        data: {id:id }, 
	   			         success:function(data) {
	   			        	 location.reload();
	   			         },
	   			        error: function(ts) { console.log(ts.responseText) }
	   			    });
	   		  }
	   		  else{
	   			  return;
	   		  }
	   	}); 
	 }); 
   
   $(document).on("click", "#buttonRemoveVideo", function(e){	
	    var mode="delete";
		   if (confirm("ลบข้อมูล")) {
			   var id  =$(this).val();
			   	jQuery.ajax({
				        type: "POST",
				        url: 'AA_videoForm.php',
				        data: {id: id,mode:mode}, 
				         success:function(data) {
				        	//console.log(data)
				        	location.reload();
				         }
				    });
		   	}
		   return false;
	   
	 	});
   
   $("#Adddoc").on('submit',(function(e) {
	   e.preventDefault();
	   var msg= validateForm("Adddoc");
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
	   formData = new FormData(this);
	   $.ajax({
	   url: "AA_docForm.php", 
	   type: "POST",            
	   data: formData, 
	   contentType: false,  
	   cache: false,       
	   processData:false, 
	   success: function(data)
		   	{
			// console.log(data);
			 location.reload();
		   	}
	   });
   }));
   
   
   $(document).on("click", "#EditdocBtn", function(e){
		e.preventDefault();
		var id = $(this).val();
		var msg= validateForm("docForm"+id);
		if(msg !=""){
			alert(msg);
			return false;
		}
	 var formData = new FormData($("#docForm"+id)[0]);
		   formData.append("mode","edit");
		   $.ajax({
		   url: "AA_docForm.php", 
		   type: "POST",            
		   data: formData, 
		   contentType: false,  
		   cache: false,       
		   processData:false, 
		   success: function(data)
			   	{
			//   console.log(data)
				location.reload();
			   	}
		   });
		});
   
   $(document).on("click", "#buttonRemovedoc", function(e){
	   var mode="delete";
	   if (confirm("ลบข็อมูล")) {
		   var id  =$(this).val();
		   	var mode="delete";
		   	jQuery.ajax({
			        type: "POST",
			        url: 'AA_docForm.php',
			        data: {id: id,mode:mode}, 
			         success:function(data) {
			        	// console.log(data);
			        	location.reload();
			         }
			    });
	   	}
	   return false;
	});
   
   
   $("#AddOtherNews").on('submit',(function(e) {
	   e.preventDefault();
	   var msg= validateForm("AddOtherNews");
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
	   var elm1=tinyMCE.get('elm1Add').getContent();
	   var replaced = elm1.replace('\n', '');
	   formData = new FormData(this);
	   formData.append("elm",replaced);
	   $.ajax({
	   url: "AA_othernewsForm.php", 
	   type: "POST",            
	   data: formData, 
	   contentType: false,  
	   cache: false,       
	   processData:false, 
	   success: function(data)
		   	{
			// console.log(data);
			 location.reload();
		   	}
	   });
   }));
   
   $(document).on("click", "#EditOtherNewsBtn", function(e){
		e.preventDefault();
		var id = $(this).val();
		var msg= validateForm("OtherNewsForm"+id);
		if(msg !=""){
			alert(msg);
			return false;
		}
		var formData = new FormData($("#OtherNewsForm"+id)[0]);
		   formData.append("mode","edit");
		   var elm1=tinyMCE.get('elm1'+id).getContent();
		   var replaced = elm1.replace(/\n/g, '');
		   formData.append("elm1",replaced);
		   $.ajax({
		   url: "AA_othernewsForm.php", 
		   type: "POST",            
		   data: formData, 
		   contentType: false,  
		   cache: false,       
		   processData:false, 
		   success: function(data)
			   	{
			 //  console.log(data)
			   location.reload();
			   	}
		   });
		});
  
   $(document).on("click", "#buttonRemoveOtherNews", function(e){
	   var mode="delete";
	   if (confirm("ลบข็อมูล")) {
		   var id  =$(this).val();
		   	var mode="delete";
		   	jQuery.ajax({
			        type: "POST",
			        url: 'AA_othernewsForm.php',
			        data: {id: id,mode:mode}, 
			         success:function(data) {
			        	// console.log(data);
			        	location.reload();
			         }
			    });
	   	}
	   return false;
	});
   
   $("#tabContantAddForm").on('submit',(function(e) {
	   e.preventDefault();
	   var msg= validateForm("tabContantAddForm");
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
	   formData = new FormData(this);
	   $.ajax({
	   url: "AA_tabContentForm.php", 
	   type: "POST",            
	   data: formData, 
	   contentType: false,  
	   cache: false,       
	   processData:false, 
	   success: function(data)
		   	{
			// console.log(data);
			 location.reload();
		   	}
	   });
   }));
   
   $(document).on("click", "#tabContentUploadForm", function(e){
		e.preventDefault();
		var id = $(this).val();
		var msg= validateForm("tabContentEditForm"+id);
		if(msg !=""){
			alert(msg);
			return false;
		}
		var formData = new FormData($("#tabContentEditForm"+id)[0]);
		   $.ajax({
		   url: "AA_tabContentForm.php", 
		   type: "POST",            
		   data: formData, 
		   contentType: false,  
		   cache: false,       
		   processData:false, 
		   success: function(data)
			   	{
			 //  console.log(data)
			   location.reload();
			   	}
		   });
		});
   
   $( "#departmentAdd" ).change(function() {
	  $.ajax({
		   url: "AA_tabContentView.php", 
		   type: "POST",            
		   data: {dropdown: $(this).val()},
		   success: function(data)
			   	{
			 //  console.log(data)
			   $('#output').html(data);
			   	}
		  ,error: function (xhr, ajaxOptions, thrownError) {
               alert(xhr.status);
               alert(thrownError);
             } });

	});
   
   $(document).on("click", "#btnRemoveTabContent", function(e){
	   var mode="delete";
	   if (confirm("ลบข็อมูล")) {
		   var id  =$(this).val();
		   var pageType = $("#pageType"+id).val();
		   	var mode="delete";
		   	jQuery.ajax({
			        type: "POST",
			        url: 'AA_tabContentForm.php',
			        data: {id: id,mode:mode,pageType:pageType}, 
			         success:function(data) {
			        	//console.log(data);
			        	location.reload();
			         }
			    });
	   	}
	   return false;
	});
   
   $("#AddModal").on('submit',(function(e) {
	   e.preventDefault();
	   var msg= validateForm("AddModal");
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
	   formData = new FormData(this);
	   $.ajax({
	   url: "AA_modalForm.php", 
	   type: "POST",            
	   data: formData, 
	   contentType: false,  
	   cache: false,       
	   processData:false, 
	   success: function(data)
		   	{
			// console.log(data);
			 location.reload();
		   	}
	   });
   }));
   
   $(document).on("click", "#EditModalBtn", function(e){
		e.preventDefault();
		var id = $(this).val();
		var msg= validateForm("modalForm"+id);
		if(msg !=""){
			alert(msg);
			return false;
		}
		var formData = new FormData($("#modalForm"+id)[0]);
		   $.ajax({
		   url: "AA_modalForm.php", 
		   type: "POST",            
		   data: formData, 
		   contentType: false,  
		   cache: false,       
		   processData:false, 
		   success: function(data)
			   	{
			 //  console.log(data)
			   location.reload();
			   	}
		   });
		});
   
   $(document).on("click", "#currcommanderUploadForm", function(e){
	   	e.preventDefault();
	   	var id = $(this).val();
	    var formData = new FormData($("#currCommanderEditForm"+id)[0]);
	    var msg= validateForm("currCommanderEditForm"+id);
	   	if(msg !=""){
	   		alert(msg);
			return false;
		}
		   $.ajax({
		   url: "AA_currCommanderForm.php", 
		   type: "POST",            
		   data: formData, 
		   contentType: false,  
		   cache: false,       
		   processData:false, 
		   success: function(data)
			   	{ 
			 //  console.log(data)
				location.reload();
			   	}
		   });
	 	});
   
   $(document).on("click", "#btnRemoveImage", function(e){
	   var mode="delete";
	   if (confirm("ลบข็อมูล")) {
		   var id  =$(this).val();
		   var pageType = $("#pageType"+id).val();
		   	var mode="delete";
		   	jQuery.ajax({
			        type: "POST",
			        url: 'AA_modalForm.php',
			        data: {id: id,mode:mode,pageType:pageType}, 
			         success:function(data) {
			        	//console.log(data);
			        	location.reload();
			         }
			    });
	   	}
	   return false;
	});
   
   
});

function validateForm(formValue){
	var msg ="";
	$('form#'+formValue).find('input').each(function(){
	    if($(this).prop('required')){
	    	var dataInput=	(this).value;
	    	if(dataInput=="" || dataInput==null || dataInput== undefined){
	    		$(this).parent("div").parent(".form-group").addClass("has-error");
	    		if($(this).attr("type")=="text"){
	    			msg="กรุณากรอกข้อมูล \n";
	    		}
	    		else if($(this).attr("type")=="file"){
	    			msg="กรุณาUploadFile \n";
	    		}
	    	
	    	}
	    	else{
	    		$(this).parent("div").parent(".form-group").removeClass("has-error");	
	    		
	    	}
	    } 
	});
	return msg;
}


/*
 * Organization management
 */

$(document).on("click", "#generateOrganizationLayer", function(e){
	 var layernum  = $('#Layernum').val();
	 var pagetype  = $('#pagetype').val();
	 if(layernum == ""){
		 return;
	 }
	 var layernum 
	 jQuery.ajax({ // create an AJAX call...
		 type: "POST", 
		 url: 'AA_commanderlayermanage.php',  
		 data: {layernum: layernum,pagetype:pagetype}, 
		 success: function(response) { 
		    $('#output').html(response); 
			$("#departmentForm").hide();
		 },
		 error: function (xhr, ajaxOptions, thrownError) {
		    alert(xhr.status);
		    alert(thrownError);
		  }
		 });
	});


$(document).on("click", "#createOrganizationlistLayer", function(e){
	 var layernum  = $('#sumoflevel').val();
	 var pagetype  = $('#pagetype').val();
	 
	 var arr = [];
	 
	  for (i = 1; i <= layernum; i++) {
		 var listvalue  = $('#Layer'+i).val();
		 	arr.push(listvalue);
		}
	  $("#inputForm2").hide();
	  console.log(listvalue);
	 jQuery.ajax({ // create an AJAX call...
		 type: "POST", 
		 url: 'AA_createdynamicorganization.php',  
		 data: {arr: arr, pagetype: pagetype}, 
		 success: function(data) { 
		   $('#generatedTable').html(data);
		   $(".layersumForm").hide();
		 },
		 error: function (xhr, ajaxOptions, thrownError) {
		    alert(xhr.status);
		    alert(thrownError);
		  }
		 });
	});


$(document).on("click", "#createOrganization", function(e){
     var formData = new FormData($("#dynamicOrg")[0]);
	 /*var layernum  = $('#sumoflevel').val();
	 var pagetype  = $('#pagetype').val();
	 var rowcount  = $('#rowcount').val();
	 var pagetype  = $('#pagetype').val();
	 var arrPos = [];
	 var arrName = [];
	 var arrImage = [];
	 var arrCurrrow = [];
	 
	 for (i = 1; i <= rowcount; i++) {
		 var posname  = $('#poscomname'+i).val();
		 var comname  = $('#comname'+i).val();
		 var imagecom  = $('#imagecom'+i).val();
		 var currrow  = $('#currrow'+i).val();
		 arrCurrrow.push(currrow);
		 arrPos.push(posname);
		 arrName.push(comname);
		 arrImage.push(imagecom);
	}
	 */
	 jQuery.ajax({ // create an AJAX call...
		 type: "POST", 
		 url: 'AA_createorganization.php',  
		 data: formData,
		 contentType: false,  
		 cache: false,       
		 processData:false, 
		 //data: {/*arrPos: arrPos, arrName:arrName,arrImage:arrImage , arrCurrrow:arrCurrrow,rowcount:rowcount, pagetype: pagetype*/formData:formData }, 
		 success: function(data) { 
			 location.reload();
			 console.log(data);
		   //$('#generatedTable').html(data);
		  // $(".layersumForm").hide();
		 },
		 error: function (xhr, ajaxOptions, thrownError) {
		    alert(xhr.status);
		    alert(thrownError);
		  }
		 });
	});



/*
 * Reset password
 */

$(document).on("click", "#Resetpassword", function(e){
	e.preventDefault();
	var id = $(this).val();
	
	bootbox.confirm("Are you sure?", function(result) {
		  if(result){
			  jQuery.ajax({
			        type: "POST",
			        url: 'AA_resetpasswordapi.php',
			        data: {id:id }, 
			         success:function(data) {
			        	 alert(data);
			        	 location.reload();
			         }
			    });
		  }
		  else{
			  return;
		  }
	}); 
	});
 



