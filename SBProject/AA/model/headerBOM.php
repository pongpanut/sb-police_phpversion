<?php  namespace SB\model;
class headerBOM{
	var $header_id;
	var $header_logo;
	var $header_text;
	var $header_title;
	var $header_page_type_id;

	function setHeaderId($header_id) {
		$this->header_id=$header_id;
	}
	function getHeaderId() {
		return $this->header_id;
	}
	
	function setHeaderLogo($header_logo) {
		$this->header_logo=$header_logo;
	}
	function getHeaderLogo() {
		return $this->header_logo;
	}
	
	function setHeaderText($header_text) {
		$this->header_text=$header_text;
	}
	function getHeaderText() {
		return $this->header_text;
	}
	
	function setHeaderTitle($header_title) {
		$this->header_title=$header_title;
	}
	function getHeaderTitle() {
		return $this->header_title;
	}
	
	function setHeaderPageTypeId($header_page_type_id) {
		$this->header_page_type_id=$header_page_type_id;
	}
	function getHeaderPageTypeId() {
		return $this->header_page_type_id;
	}

}

?>