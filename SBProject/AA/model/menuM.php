<?php  namespace SB\model;
class menuM{
	var $menu_id;
	var $menu_text;
	var $menu_page_type;
	var $menu_link;
	var $has_submenu;
	var $menu_seq;
	var $is_department;
	
	function setMenuId($menu_id) {
		$this->menu_id=$menu_id;
	}
	function getMenuId() {
		return $this->menu_id;
	}
	
	function setMenuText($menu_text) {
		$this->menu_text=$menu_text;
	}
	function getMenuText() {
		return $this->menu_text;
	}
	
	function setMenuPageType($menu_page_type) {
		$this->menu_page_type=$menu_page_type;
	}
	function getMenuPageType() {
		return $this->menu_page_type;
	}

	function setMenuLink($menu_link) {
		$this->menu_link=$menu_link;
	}
	function getMenuLink() {
		return $this->menu_link;
	}

	function setMenuHasSubmenu($has_submenu) {
		$this->has_submenu=$has_submenu;
	}
	function getMenuHasSubmenu() {
		return $this->has_submenu;
	}

	function setMenuSeq($menu_seq) {
		$this->menu_seq=$menu_seq;
	}
	function getMenuSeq() {
		return $this->menu_seq;
	}
	
	function setMenuIsDepartment($is_department) {
		$this->is_department=$is_department;
	}
	function getMenuIsDepartment() {
		return $this->is_department;
	}



	
	
}


?>