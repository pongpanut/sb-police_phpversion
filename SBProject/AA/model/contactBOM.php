<?php  namespace SB\model;
class contactBOM{
	var $id;
	var $name;
	var $email;
	var $phone;
	var $message;
	var $page_type_id;
	var $ans;
	
	function setContactId($id) {
		$this->id=$id;
	}
	function getContactId() {
		return $this->id;
	}
	
	function setName($name) {
		$this->name=$name;
	}
	function getName() {
		return $this->name;
	}
	
	function setEmail($email) {
		$this->email=$email;
	}
	function getEmail() {
		return $this->email;
	}
	function setPhone($phone) {
		$this->phone=$phone;
	}
	function getPhone() {
		return $this->phone;
	}
	function setMessage($message) {
		$this->message=$message;
	}
	function getMessage() {
		return $this->message;
	}
	function setPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getPageTypeId() {
		return $this->page_type_id;
	}
	function setAns($ans) {
		$this->ans=$ans;
	}
	function getAns() {
		return $this->ans;
	}
	
}


?>