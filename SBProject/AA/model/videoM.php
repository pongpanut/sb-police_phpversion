<?php  namespace SB\model;
class videoM {
	var $video_id;
	var $video_path;
	var $page_type_id;
	var $video_name;
	var $status;
	
	function setVideoId($video_id) {
		$this->video_id=$video_id;
	}
	function getVideoId() {
		return $this->video_id;
	}
	function setVideoPath($video_path) {
		$this->video_path=$video_path;
	}
	function getVideoPath() {
		return $this->video_path;
	}
	function setPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getPageTypeId() {
		return $this->page_type_id;
	}
	function setVideoName($video_name) {
		$this->video_name=$video_name;
	}
	function getVideoName() {
		return $this->video_name;
	}
	
	function setStatus($status) {
		$this->status=$status;
	}
	function getStatus() {
		return $this->status;
	}
	
}


?>
