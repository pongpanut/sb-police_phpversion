<?php  namespace SB\model;
class userM{
	var $admin_id;
	var $admin_user;
	var $admin_password;
	var $admin_privillege;
	var $page_type_id;
	function setAdminId($admin_id) {
		$this->admin_id=$admin_id;
	}
	function getAdminId() {
		return $this->admin_id;
	}
	
	function setAdminUser($admin_user) {
		$this->admin_user=$admin_user;
	}
	function getAdminUser() {
		return $this->admin_user;
	}
	
	function setAdminPassword($admin_password) {
		$this->admin_password=$admin_password;
	}
	function getAdminPassword() {
		return $this->admin_password;
	}
	
	function setAdminPrivillege($admin_privillege) {
		$this->admin_privillege=$admin_privillege;
	}
	function getAdminPrivillege() {
		return $this->admin_privillege;
	}
	function setPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getPageTypeId() {
		return $this->page_type_id;
	}

}

?>