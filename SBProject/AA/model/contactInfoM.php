<?php  namespace SB\model;
class contactInfoM{
	
		var $department_contact_id;
		var $location;
		var $contact_info;
		var $tel;
		var $page_type_id;
		var $email;
		
		function setDepartmentContactId($department_contact_id) {
			$this->department_contact_id=$department_contact_id;
		}
		function getDepartmentContactId() {
			return $this->department_contact_id;
		}
		
		function setLocation($location) {
			$this->location=$location;
		}
		function getLocation() {
			return $this->location;
		}
		
		function setContactInfo($contact_info) {
			$this->contact_info=$contact_info;
		}
		function getContactInfo() {
			return $this->contact_info;
		}
		
		function setTel($tel) {
			$this->tel=$tel;
		}
		function getTel() {
			return $this->tel;
		}
		
		function setEmail($email) {
			$this->email=$email;
		}
		function getEmail() {
			return $this->email;
		}
		
		function setPageTypeId($page_type_id) {
			$this->page_type_id=$page_type_id;
		}
		function getPageTypeId() {
			return $this->page_type_id;
		}
	
}


?>