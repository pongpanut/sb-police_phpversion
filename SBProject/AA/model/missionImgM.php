<?php  namespace SB\model;
class missionImgM{
	var	$id;
	var	$image_paht;
	var	$page_type_id;
	var	$ref_id;
	var	$rec_date;
	
	function setId($id) {
		$this->id=$id;
	}
	function getId() {
		return $this->id;
	}

	
	function setImagePaht($image_paht) {
		$this->image_paht=$image_paht;
	}
	function getImagePaht() {
		return $this->image_paht;
	}
	
	function setPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getPageTypeId() {
		return $this->page_type_id;
	}
	
	function setRefId($ref_id) {
		$this->ref_id=$ref_id;
	}
	function getRefId() {
		return $this->ref_id;
	}
	function setRecDate($rec_date) {
		$this->rec_date=$rec_date;
	}
	function getRecDate() {
		return $this->rec_date;
	}
	
}


?>