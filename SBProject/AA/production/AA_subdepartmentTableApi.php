<?php namespace app\page;
use SB\Controller\Controller;
include '../Controller/Controller.php';
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
include_once 'AA_NosessionRedirect.php';
$departmentListId = $_POST['dropdown'];
$pagetype = $_SESSION['privillege'];
//echo $menuText;
/*Init DAO*/
$DepartmentDao =App_DaoFactory::getFactory()->getDepartmentDao();

/*Retrieve data*/
$departmentList =$DepartmentDao->loadSubDepartmentList($departmentListId);
$department =$DepartmentDao->loadDepartment($pagetype);
//$staticPage = $staticPageDao->loadStaticpage(1);
//$statpagenum= count($staticPage);
$num = count($departmentList);
$departmentNum = count($department);
?>
<!--table-->
 <?php if($num >0){?>
       		<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>ตัวหนังสือเมนู</th>
                          <th>ลิงค์</th>
                          <th>ลำดับที่</th>
                          <th>แก้ไข</th>
                          <th>ลบ</th>
                        </tr>
                      </thead>
                      <tbody>
                     
                           <?php for ($j=0;$j<$num;$j++){?>
                        <tr id="<?php echo $departmentList[$j]->getDeptSubId();?>">
                          <td><?php echo $departmentList[$j]->getDeptSubText() ?></td>
                       			
						 		<?php if($departmentList[$j]->getDeptSubText() != null){?>
						 			<td><?php echo $departmentList[$j]->getDepSubLink();?></td><?php	
								}
								else{
									?><td> </td><?php
						 		}?>
   
                          <td><?php echo $departmentList[$j]->getSeq();?></td>
                         
                          <td>
						  	<button type="button" id="buttonId" class="btn btn-default" aria-label="Left Align"  data-toggle="modal" data-target="#myModal<?php echo $departmentList[$j]->getDeptSubId();?>">
							  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
							</button>
						  </td>
                          <td>
                          	<button type="button" id="buttonRemoveDepartmentsub" class="btn btn-default" value="<?php echo $departmentList[$j]->getDeptSubId();?>">
								  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								</button>
								</td>
	                        </tr>
                       	   <?php }?>
                      </tbody>
                    </table>
<?php }?>
 <!-- End table-->
 
 
                 
<!-- Modal -->
 <?php if($num >0){?>
 	<?php for ($j=0;$j<$num;$j++){?>
<div id="myModal<?php echo $departmentList[$j]->getDeptSubId();?>" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ส่วนปรับปรุงหน่วยย่อย "<?php echo $departmentList[$j]->getDeptSubText()?>"</h4>
      </div>
      <div class="modal-body">
        		<form class="form-horizontal form-label-left" id="confirm-submit<?php echo $departmentList[$j]->getDeptSubId();?>" value ="<?php echo $departmentList[$j]->getDeptSubId();?>" method="post" accept-charset="UTF-8"  >
                <input type="hidden" name="pagetype" id="pagetype" value ="<?php echo $pagetype?>">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="first-name">ตัวหนังสือเมนู <span class="required">*</span>
                    </label>
                    <div class="col-md-7">
                      <input type="text" id="departmentsubtextModal<?php echo $departmentList[$j]->getDeptSubId();?>" name ="menutextModal" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $departmentList[$j]->getDeptSubText() ?>">
                    </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3" for="last-name">link ของเมนู</label>
                     <div class="col-md-7">
                          <select id="departmentsubOptionmodal<?php echo $departmentList[$j]->getDeptSubId();?>" class="form-control" required>
                          <?php if($departmentNum >0){?>
                          	<option value="0">Choose..</option>
                           	<?php for ($x=0;$x<$departmentNum;$x++){?>
	                           	 <?php if($departmentList[$j]->getDepSubLink() == $department[$x]->getDeptLink() ){?>
	                            <option selected="selected" value="<?php echo $department[$x]->getDeptLink()?>"><?php echo $department[$x]->getDeptText()?></option>
	                            <?php } else {?>
	                            <option value="<?php echo $department[$x]->getDeptLink()?>"><?php echo $department[$x]->getDeptText()?></option>
	                            <?php } ?>

                            <?php }?>
                          <?php }?>
                          </select>
                          </div>
                   </div>
                    <div class="form-group">
                    <label class="control-label col-md-3" for="last-name">ลำดับที่<span class="required">*</span>
                    </label>
                    <div class="col-md-2">
                      <input type="text" id="departmentsubseqModal<?php echo $departmentList[$j]->getDeptSubId();?>" name="seqModal<?php echo $departmentList[$j]->getDeptSubId();?>" required="required"  value='<?php echo $departmentList[$j]->getSeq();?>' onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
                  	
                </form>
      </div>
      <div class="modal-footer">
		<div class="col-md-7">
							<button type="submit" form="confirm-submit<?php echo $departmentList[$j]->getDeptSubId();?>" data-target="#confirm-submit<?php echo $departmentList[$j]->getDeptSubId();?>" id="modifydepartmentsubmodal" class="btn btn-primary" data-dismiss="modal" value="confirm-submit<?php echo $departmentList[$j]->getDeptSubId();?>" >แก้ไขข้อมูล</button>
					</div>
      </div>
    </div>

  </div>
</div>
   <?php }?>
<?php }?>
<!-- End Modal -->
 