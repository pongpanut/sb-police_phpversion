<?php namespace app\page;
use SB\Controller\Controller;
include '../Controller/Controller.php';
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
include_once 'AA_NosessionRedirect.php';
$contactDao =App_DaoFactory::getFactory()->getContactDao();
$pagetype=$_SESSION['privillege'];
$contactDetail = $contactDao->loadContact($pagetype);
$contactnum= count($contactDetail);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ส่วนการจัดการหลังบ้าน</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    
 
    <script type='text/javascript' src='../src/js/jquery-2.2.3.min.js'></script>
    <script type='text/javascript' src='../src/js/backoffice.js'></script>
    <script type="text/javascript" src="../tinymce/tinymce.min.js" ></script>
    <script type='text/javascript' src='../src/js/upload.js'></script>
      <link href="../src/scss/backoffice.css" rel="stylesheet">
    <script type='text/javascript' src='../src/js/bootstrap-filestyle.min.js'></script>
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <link href="../src/scss/backoffice.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      	<?php
		 include 'AA_sidebar.php';
		?>
<div id="load_screen"><div id="loading" ><img alt="" src="../../images/image_997636.gif"></div></div>
        <!-- top navigation -->
            <?php
		 include 'nav.php';
		?>
        <!-- /top navigation -->
            <!-- /page content -->

        <!-- page content -->
        <div class="right_col" role="main" style="height:1088;min-height: 1088px;">
          <div class="">
            <div class="page-title">
                <h3>จัดการผู้ที่ติดต่อข้อความ </h3>
               
              </div>
            </div>
            <div class="clearfix"></div>  
              <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;">
                <div class="x_panel">
                  <div class="x_title">
                    <div class="clearfix"></div>
                  </div> 
               <h4>จัดการผู้ที่ติดต่อข้อความ</h4>

              
                  <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                        <th>รหัส</th>
                          <th>ชื่อผู้ติดต่อ</th>
                          <th>อีเมล์ผู้ติดต่อ</th>
                          <th>เบอร์โทรศัพท์</th>
                          <th>ดูรายละเอียด</th>
                          <th>ข้อความตอบกลับ</th>
                           <th>แก้ไข</th>
                          <th>ลบ</th>
                        </tr>
                      </thead>
                      <tbody>
                     <?php if($contactnum >0){?>
                           <?php for ($j=0;$j<$contactnum;$j++){?>
	                        <tr id="<?php echo $contactDetail[$j]->getContactId();?>">
	                        <td><?php echo $contactDetail[$j]->getContactId() ?></td>
	                          <td><?php echo $contactDetail[$j]->getName() ?></td>
	                          <td><?php echo $contactDetail[$j]->getEmail();?></td>
	                           <td><?php echo $contactDetail[$j]->getPhone();?></td>  
	                             <td><?php echo $contactDetail[$j]->getMessage();?></td>        
	                           <td><?php echo $contactDetail[$j]->getAns();?></td>           
	                          <td>
							  	<button type="button" id="buttonId" class="btn btn-default" aria-label="Left Align"  data-toggle="modal" data-target="#myModal<?php echo $contactDetail[$j]->getContactId();?>">
								  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</button>
							  </td>
	                          <td>
	                          	<button type="button" id="deleteContactInbox" class="btn btn-default" value="<?php echo $contactDetail[$j]->getContactId();?>">
								  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								</button>
								</td>
	                        </tr>
                       	   <?php }?>
                      <?php }?>
                       
                        
                      </tbody>
                    </table>
                    
   <!-- Modal -->
 <?php if($contactnum >0){?>
 	<?php for ($j=0;$j<$contactnum;$j++){?>
<div id="myModal<?php echo $contactDetail[$j]->getContactId();?>" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">รายละเอียด "<?php echo $contactDetail[$j]->getName()?>"</h4>
      </div>
      <div class="modal-body">
      <form class="form-horizontal form-label-left" id="contactDetail<?php echo $contactDetail[$j]->getContactId();?>" value ="<?php echo $contactDetail[$j]->getContactId();?>"  method="post" accept-charset="UTF-8" " >
  
                  <input type="hidden" name="pageType" id="pageType"  value="<?php echo $pagetype;?>">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="first-name">ชื่อ </label>
                    <div class="col-md-7">
                     <span class="col-md-12" for="first-name"><?php echo $contactDetail[$j]->getName()?> </span>
                    </div>
                  </div>
   
                   <div class="form-group">
                    <label class="control-label col-md-3" for="last-name">อีเมลล์</label>
                   <div class="col-md-7">
                     <span class=" col-md-12" for="first-name"><?php echo $contactDetail[$j]->getEmail()?> </span>
                    </div>
                  </div>
                    <div class="form-group">
                    <label class="control-label col-md-3" for="last-name">โทรศัพท์</label>
                   <div class="col-md-7">
                     <span class="col-md-12" for="first-name"><?php echo $contactDetail[$j]->getPhone()?> </span>
                    </div>
                  </div>
                    <div class="form-group">
                    <label class="control-label col-md-3" for="last-name">ข้อความ</label>
                   <div class="col-md-7">
                     <span class="col-md-12" for="first-name"><?php echo $contactDetail[$j]->getMessage()?> </span>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="control-label col-md-3" for="ans">ข้อความตอบกลับ</label>
                
                     
                     <div class="col-md-7">
                     <textarea rows="3" cols="5" class="form-control col-md-7 col-xs-12" name="ans<?php echo $contactDetail[$j]->getContactId()?>"  id="ans<?php echo $contactDetail[$j]->getContactId()?>"></textarea>
                     
                    </div>
                  <div class="col-md-7 col-md-offset-6 ">
					
						<button type="submit" class="btn btn-primary" id="updateAns" value="<?php echo $contactDetail[$j]->getContactId();?>" >เพิ่มข้อมูล</button>
					</div>
                  </div>
                  	
                </form>
                
                <script>
                $(document).ready(function() {  
                    <?php if($contactDetail[$j]->getAns() !=""){?>
                    	$("#ans<?php echo $contactDetail[$j]->getContactId()?>").text("<?php echo $contactDetail[$j]->getAns()?>");
                    	
                    	
                	<?php }?>
                });
               
                </script>
      </div>

    </div>

  </div>
</div>
   <?php }?>
<?php }?>

<div id="confirm" class="modal hide fade">
  <div class="modal-body">
    Are you sure?
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
  </div>
</div>
<!-- End modal -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->




    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script type='text/javascript' src='../src/js/jquery.dataTables.js'></script>
    <!-- <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
    <script src="../vendors/datatables.net/js/jquery.dataTables.js"></script> -->
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script type='text/javascript' src='../src/js/knockout-3.4.0.js'></script>
	<script type="text/javascript" src='../src/js/ModelbulderForStaticpage.js'></script>
	    <script type='text/javascript' src='../src/js/bootbox.min.js'></script>
    <!-- Datatables -->
    <script>
      $(document).ready(function() {
          
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable({ "order": [[ 0, "desc" ]] , "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]] , "pageLength": 50});

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
  </body>
</html>