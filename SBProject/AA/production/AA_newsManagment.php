<?php namespace app\page;
use SB\Controller\Controller;
include '../Controller/Controller.php';
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
include_once 'AA_NosessionRedirect.php';
//echo sys_get_temp_dir();

include_once 'constant.php';

$page_type_id= $_SESSION['privillege'];
$newsDao =App_DaoFactory::getFactory()->getNewsDao();
$news =$newsDao->getNews($page_type_id);
$num = count($news);
$imageDao =App_DaoFactory::getFactory()->getImageDao();
$imageArr=$imageDao->loadImg($page_type_id,"'activity'");
$numImage = count($imageArr);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ส่วนการจัดการหลังบ้าน</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    
 
    <script type='text/javascript' src='../src/js/jquery-2.2.3.min.js'></script>
    <script type='text/javascript' src='../src/js/backoffice.js'></script>
    <script type="text/javascript" src="../tinymce/tinymce.min.js" ></script>
    <script type='text/javascript' src='../src/js/upload.js'></script>
    <script type='text/javascript' src='../src/js/bootstrap-filestyle.min.js'></script>
    
    
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <link href="../src/scss/backoffice.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      	<?php
		 include 'AA_sidebar.php';
		?>
<div id="load_screen"><div id="loading" ><img alt="" src="../../images/image_997636.gif"></div></div>
         <?php
		 include 'nav.php';
		?>
    
<!--Modal-->

        <!-- page content -->
        <div class="right_col" role="main" style="height:1088;min-height: 1088px;">
          <div class="">
            <div class="page-title">
                <h3>ส่วนการจัดการข่าวประชาสัมพันธ์ </h3>
               
              </div>
            </div>
            <div class="clearfix"></div>  
              <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;">
                <div class="x_panel">
                  <div class="x_title">
                    <div class="clearfix"></div>
                  </div> 
               <h4>เพิ่มข้อมูล</h4>

               <form  novalidate="novalidate" class=" form-horizontal form-label-left" enctype="multipart/form-data" method="POST"  accept-charset="UTF-8" id="AddNews">
                  <input type="hidden" value="add" name="id" id="id">
                   <input type="hidden" value="<?php echo $page_type_id?>" name="pageTypeAdd">
                    <input type="hidden" value="add" name="mode">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="newsHeaderAdd">หัวข้อข่าว :<span class="required">*</span>
                    </label>
                    <div class="col-md-7">
                      <input type="text" id="newsHeaderAdd" name ="newsHeaderAdd" required="required" class="form-control col-md-7 col-xs-12" >
                    </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3" for="newsCodeAdd">ประเภทข่าว :</label>
                     <div class="col-md-7">
                          <select id="newsCodeAdd" class="form-control" required name="newsCodeAdd">
									 <option selected="selected" value="1">ข่าวประชาสัมพันธ์</option>
									 <option  value="2">ประกาศจัดซื้อจัดจ้าง</option>
                          </select>
                          </div>
                   </div>
                 <div class="form-group">
                     <label class="control-label col-md-3" for="elm1Add">รายละเอียดข่าว :</label>
                     <div class="col-md-7">
                          <textarea id="elm1Add" name="elm1Add"></textarea>
                       </div>
                   </div>
                   <div class="form-group">
	                    <label class="control-label col-md-3" for="newsPdfAdd">แนบไฟล์ :<span class="required">*</span>
	                    </label>
	                    <div class="col-md-5">
	                    	<input type="file" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint , application/pdf, image/*" name="file" id="imageAttach">
	                    </div>
                  </div>
                   <div class="form-group">
                     <label class="control-label col-md-3" for="isHighlightAdd">รูปแบบประกาศ :</label>
                     <div class="col-md-2">
                          <select id="isHighlightAdd" class="form-control" required name="isHighlightAdd">
									 <option selected="selected" value="0">ข่าวปกติ</option>
									 <option  value="1">ข่าวใหม่</option>
                          </select>
                          </div>
                   </div>
                  
                    <script>
						    tinymce.init({
						    	 force_br_newlines : false,
						        selector: "textarea#elm1Add",
						        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt 64pt',
						        theme: "modern",
						        width: 600,
						        height: 300,
						        plugins: [
						             "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
						             "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
						             "save table contextmenu directionality emoticons template paste textcolor"
						       ],
						       content_css: "../tinymce/skins/lightgray/content.min.css",
						       toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | fontsizeselect ", 
						       style_formats: [
						            {title: 'Bold text', inline: 'b'},
						            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
						            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
						            {title: 'Example 1', inline: 'span', classes: 'example1'},
						            {title: 'Example 2', inline: 'span', classes: 'example2'},
						            {title: 'Table styles'},
						            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
						        ],relative_urls : false,
						        remove_script_host : false,
						        convert_urls : true,
						        image_prepend_url: "<?php echo $_SERVER['SERVER_NAME']?>",
						        image_list: [
						                 	<?php if($numImage>0){
					                       		for ($z = 0; $z < $numImage; $z++) {
					                       			?>
					                       			{title: '<?php echo $imageArr[$z]->getImgheader()?>', value: '<?php echo $linkPrefix.$imageArr[$z]->getImgpath()?>'},
					                       			<?php
					                       			if($z==$numImage){
					                       				?>
					                       				{title: '<?php echo $imageArr[$z]->getImgheader()?>', value: '<?php echo $linkPrefix.$imageArr[$z]->getImgpath()?>'}
					                       				<?php
					                       			}
					                       		}
					                       	}?>
						                     
						                   ]
        

						    }); 

					</script>  
					<div class="col-md-7 col-md-offset-6 ">
					
						<button type="submit" class="btn btn-primary">เพิ่มข้อมูล</button>
					</div>
                </form>
                  <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>รหัส</th>
                          <th>ประเภทข่าว</th>
                          <th>หัวข้อข่าว</th>
                          <th>แนบไฟล์</th>
                          <th>แก้ไข</th>
                          <th>ลบ</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if($num >0){?>
                           <?php for ($j=0;$j<$num;$j++){?>
	                        <tr id="<?php echo $news[$j]->getNewsId();?>">
	                        <td><?php echo $news[$j]->getNewsId()?></td>
	                          <td><?php echo $news[$j]->getNewsCode()==1?"ข่าวประชาสัมพันธ์":"ประกาศจัดซื้อจัดจ้าง" ?></td>
	                       			
							 		<?php if($news[$j]->getNewsHeader() != null){
								    	?><td ><?php echo $news[$j]->getNewsHeader();?></td><?php	
									}
									else{
										?><td> </td><?php
							 		}?>
	   
	                          <td><?php echo $news[$j]->getNewsPdf();?></td>
	                         
	                          <td>
							  	<button type="button" id="buttonId" class="btn btn-default" aria-label="Left Align"  data-toggle="modal" data-target="#myModal<?php echo $news[$j]->getNewsId();?>">
								  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</button>
							  </td>
	                          <td>
	                          	<button type="button" id="buttonRemoveNews" class="btn btn-default" value="<?php echo $news[$j]->getNewsId();?>">
								  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								</button>
								</td>
	                        </tr>
                       	   <?php }?>
                      <?php }?>
                       
                        
                      </tbody>
                    </table>
                    
                      <!-- Modal -->
 <?php if($num >0){?>
 	<?php for ($j=0;$j<$num;$j++){?>
<div id="myModal<?php echo $news[$j]->getNewsId();?>" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="max-height: 1024px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ส่วนปรับข่าว : "<?php echo $news[$j]->getNewsHeader()?>"</h4>
      </div>
      <div class="modal-body">
				<form class=" form-horizontal form-label-left" enctype="multipart/form-data" method="POST"  id="newsForm<?php echo $news[$j]->getNewsId();?>" name="newsForm<?php echo $news[$j]->getNewsId();?>" accept-charset="UTF-8">
                  <input type="hidden" value="<?php echo $news[$j]->getNewsId();?>" name="id" id="id">
                   <input type="hidden" value="<?php echo $page_type_id?>" name="pageType">
                    <input type="hidden" value="edit" name="mode">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="newsHeader<?php echo $news[$j]->getNewsId();?>">หัวข้อข่าว :<span class="required">*</span>
                    </label>
                    <div class="col-md-7">
                      <input type="text" id="newsHeader<?php echo $news[$j]->getNewsId();?>" name ="newsHeader<?php echo $news[$j]->getNewsId();?>" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $news[$j]->getNewsHeader() ?>">
                    </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3" for="newsCode<?php echo $news[$j]->getNewsId();?>">ประเภทข่าว :</label>
                     <div class="col-md-7">
                          <select id="newsCode<?php echo $news[$j]->getNewsId();?>" class="form-control" required name="newsCode<?php echo $news[$j]->getNewsId();?>">
                         
                          <?php if($news[$j]->getNewsCode() ==1 ){?>
									 <option selected="selected" value="1">ข่าวประชาสัมพันธ์</option>
									 <option  value="2">ประกาศจัดซื้อจัดจ้าง</option>
								<?php } else {?>
									 <option  value="1">ข่าวประชาสัมพันธ์</option>
									 <option selected="selected" value="2">ประกาศจัดซื้อจัดจ้าง</option>
								<?php } ?>
                           	
                          </select>
                          </div>
                   </div>
                 <div class="form-group">
                     <label class="control-label col-md-3" for="elm1<?php echo $news[$j]->getNewsId();?>">รายละเอียดข่าว :</label>
                     <div class="col-md-7">
                          <textarea id="elm1<?php echo $news[$j]->getNewsId();?>" name="area"></textarea>
                       </div>
                   </div>
                   <div class="form-group" id="upload<?php echo $news[$j]->getNewsId();?>">
	                    <label class="control-label col-md-3" for="newsPdf<?php echo $news[$j]->getNewsId();?>">แนบไฟล์ :<span class="required">*</span>
	                    </label>
	                    <div class="col-md-7">
	                    	<input type="file" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint , application/pdf, image/*" name="file" id="imageAttach">
	                    </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3" for="isHighlight<?php echo $news[$j]->getNewsId();?>">รูปแบบประกาศ :</label>
                     <div class="col-md-7">
                          <select id="isHighlight<?php echo $news[$j]->getNewsId();?>" class="form-control" required name="isHighlight<?php echo $news[$j]->getNewsId();?>">
                         
                          <?php if($news[$j]->getIsHighLight() ==0 ){?>
									 <option selected="selected" value="0">ข่าวปกติ</option>
									 <option  value="1">ข่าวใหม่</option>
								<?php } else {?>
									 <option  value="0">ข่าวปกติ</option>
									 <option selected="selected" value="1">ข่าวใหม่</option>
								<?php } ?>
                           	
                          </select>
                          </div>
                   </div>
     
                    <script>
						    tinymce.init({
						    	 force_br_newlines : false,
						        selector: "textarea#elm1"+<?php echo $news[$j]->getNewsId();?>,
						        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt 64pt',
						        theme: "modern",
						        width: 600,
						        height: 300,
						        plugins: [
						             "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
						             "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
						             "save table contextmenu directionality emoticons template paste textcolor"
						       ],
						       content_css: "../tinymce/skins/lightgray/content.min.css",
						       toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | fontsizeselect", 
						       style_formats: [
						            {title: 'Bold text', inline: 'b'},
						            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
						            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
						            {title: 'Example 1', inline: 'span', classes: 'example1'},
						            {title: 'Example 2', inline: 'span', classes: 'example2'},
						            {title: 'Table styles'},
						            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
						        ],relative_urls : false,
						        remove_script_host : false,
						        convert_urls : true,
						        image_prepend_url: "<?php echo $_SERVER['SERVER_NAME']?>",
						        image_list: [
						                 	<?php if($numImage>0){
					                       		for ($z = 0; $z < $numImage; $z++) {
					                       			?>
					                       			{title: '<?php echo $imageArr[$z]->getImgheader()?>', value: '<?php echo $linkPrefix.$imageArr[$z]->getImgpath()?>'},
					                       			<?php
					                       			
					                       			if($z==$numImage){
					                       				?>
					                       				{title: '<?php echo $imageArr[$z]->getImgheader()?>', value: '<?php echo $linkPrefix.$imageArr[$z]->getImgpath()?>'}
					                       				<?php
					                       			}
					                       		}
					                       	}?>
						                     
						                   ]
        

						    }); 
						    $(document).ready(function() {
	 						var replaced='<?php echo trim(preg_replace('/\s\s+/', ' ', $news[$j]->getNewsDetail()));?>'
						    	$("#elm1<?php echo $news[$j]->getNewsId();?>").html(replaced); 
						    	$("#upload<?php echo $news[$j]->getNewsId();?> .bootstrap-filestyle :text").val("<?php echo $news[$j]->getNewsPdf();?>");
						    	
						    });
					</script>  
					<div class="col-md-7 col-md-offset-6 ">
					
						<button type="submit" class="btn btn-primary" id="EditNewsBtn"  value="<?php echo $news[$j]->getNewsId();?>">แก้ไขข้อมูล</button>
					</div>
                </form>
      </div>
      <div class="modal-footer">
		
      </div>
    </div>

  </div>
</div>
   <?php }?>
<?php }?>
                  

<div id="confirm" class="modal hide fade">
  <div class="modal-body">
    Are you sure?
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
  </div>
</div>
<!-- End modal -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->




    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script type='text/javascript' src='../src/js/jquery.dataTables.js'></script>
    <!-- <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
    <script src="../vendors/datatables.net/js/jquery.dataTables.js"></script> -->
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script type='text/javascript' src='../src/js/knockout-3.4.0.js'></script>
	<script type="text/javascript" src='../src/js/ModelbulderForStaticpage.js'></script>
	    <script type='text/javascript' src='../src/js/bootbox.min.js'></script>
    <!-- Datatables -->
    <script>
      $(document).ready(function() {
          
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable({ "order": [[ 0, "desc" ]] , "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]] , "pageLength": 50});

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
  </body>
</html>