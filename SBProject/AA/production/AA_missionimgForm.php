<?php
use  SB\model\missionImgM;
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
include 'constant.php';
		$ds = "/uploads/images/"; 
		
		isset($_POST['mode']) ? $mode = $_POST['mode'] : $mode = '';
		$msg="";
		$missionImgDao = App_DaoFactory::getFactory()->getMissionImgDao();
		$path="";
		$imgSeq=0;
		$missionImgM  = new missionImgM();
		
		if($mode=="delete"){
			$id=$_POST['id'];
			$missionImgM = $missionImgDao->getMissionImgByid($id);
			$imagepath=$missionImgM->getImagePaht();
			$missionImgDao->deleteMissionImg($id);
			$host = str_replace('\\','/',dirname(__FILE__));
			$imgPath= str_replace('AA/production','',$host).$imagepath;
			if(file_exists($imgPath)){
				unlink($imgPath);
			}
		}
		$fileNum = count($_FILES['file']['name']);
	
		if($fileNum >0){
			for ($i = 0; $i < $fileNum; $i++) {
				if (!empty($_FILES['file']['name'][$i])) {
					$tempFile = $_FILES['file']['tmp_name'][$i];
					$size = GetimageSize($tempFile);
					$ratio = $size[0]/$size[1]; // width/height
					if( $ratio > 1) {
						$width = 500;
						$height = 500/$ratio;
					}
					else {
						$width = 500*$ratio;
						$height = 500;
					}
					$src = imagecreatefromstring(file_get_contents($tempFile));
					$dst = imagecreatetruecolor($width,$height);
					imagecopyresampled($dst,$src,0,0,0,0,$width,$height,$size[0],$size[1]);
					imagedestroy($src);
					imagejpeg($dst,$tempFile); // adjust format as needed
					imagedestroy($dst);
				
					$targetPath = dirname( __FILE__ ) . $ds;
					$targetFile =  $targetPath. $_FILES['file']['name'][$i];
					$removeHost = str_replace($_SERVER["DOCUMENT_ROOT"],'', str_replace('\\','/',$targetFile ));
				
					move_uploaded_file($tempFile,$targetFile); //6
					if($tempFile !=""){
						$path = str_replace($linkPrefix,'',$removeHost) ;
						$missionImgM->setImagePaht($path);
					}
					 
				}
				     if($mode=="add"){
				     	$missionId =$_POST['missionId'];
				     	$pageType = $_POST['pageTypeAdd'];	
				     	$missionImgM->setRefId($missionId);
				     	$missionImgM->setPageTypeId($pageType);
				     	$missionImgDao->InsertMissionImg($missionImgM);
				     	
				     }
				     else if($mode=="edit"){
				     	$id=$_POST['id'];
					  	$missionId =$_POST['missionId'];
				     	$pageType = $_POST['pageType'];
				     	
				     	$missionImgM->setId($id);
				     	$missionImgM->setRefId($missionId);
				     	$missionImgM->setPageTypeId($pageType);
				     	$missionImgDao->UpdateMissionImg($missionImgM);
		
				     }
	}	
}
		
		
?>
