<?php
use SB\model\menuM;
use SB\conn\factory\App_DaoFactory;
include '../model/menuM.php';
include_once '../conn/factory/factory.php';


$id = $_POST['id'];
$menuText = $_POST['menuText'];
if(isset($_POST['dropdown'])!= ""){
	$dropdown = $_POST['dropdown'];
}
else{
	$dropdown ="";
}
$seq = $_POST['seq'];
$pageType = $_POST['pageType'];
$hasSubmenu = $_POST['hasSubmenu'];

if($id!=""){
	$menu = new menuM();
	$menu->setMenuId($id);
	$menu->setMenuText($menuText);
	$menu->setMenuPageType($pageType);
	$menu->setMenuLink($dropdown);
	$menu->setMenuHasSubmenu($hasSubmenu);

	$menu->setMenuSeq($seq);
	
	 
	$menuDao = App_DaoFactory::getFactory()->getMenu();
	$msg = $menuDao->updateMenu($menu);	 
}
?>