<?php
use  SB\model\tab_modalM;
use  SB\model\imageM;
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
include 'constant.php';
		$ds = "/uploads/images/"; 
		$mode=$_POST['mode'];
		$msg="";
		$tabModalDao = App_DaoFactory::getFactory()->getTabModalDao();
		$imageDao = App_DaoFactory::getFactory()->getImageDao();
		$path="";
		$imgSeq=0;
		$tab_modalM  = new tab_modalM();
		$imageM = new imageM();
		$imageM->setImgtype("modal");
		if (!empty($_FILES)) {
				
			$tempFile = $_FILES['file']['tmp_name'];
			$size = GetimageSize($tempFile);
			$ratio = $size[0]/$size[1]; // width/height
			if( $ratio > 1) {
				$width = 500;
				$height = 500/$ratio;
			}
			else {
				$width = 500*$ratio;
				$height = 500;
			}
			$src = imagecreatefromstring(file_get_contents($tempFile));
			$dst = imagecreatetruecolor($width,$height);
			imagecopyresampled($dst,$src,0,0,0,0,$width,$height,$size[0],$size[1]);
			imagedestroy($src);
			imagejpeg($dst,$tempFile); // adjust format as needed
			imagedestroy($dst);
		
			$targetPath = dirname( __FILE__ ) . $ds;
			$targetFile =  $targetPath. $_FILES['file']['name'];
			$removeHost = str_replace($_SERVER["DOCUMENT_ROOT"],'', str_replace('\\','/',$targetFile ));
		
			move_uploaded_file($tempFile,$targetFile); //6
			if($tempFile !=""){
				$path = str_replace($linkPrefix,'',$removeHost) ;
				$imageM->setImgpath($path);
			}
			 
		}
	
		     if($mode=="add"){

		     	$modalHeaderAdd =$_POST['modalHeaderAdd'];
		     	$modalDetailAdd=$_POST['modalDetailAdd'];
		     	$pageType = $_POST['pageTypeAdd'];
		     	$tabContentid=$_POST['tabContent'];
		     	$modal=  $tabModalDao->loadTabModal($pageType,$tabContentid);
		     	
		     	$imageM->setImgheader($modalHeaderAdd);
		     	$ref_id =0;
		     	
		     	if($modal->getTabModalId() !=""){
		     		$ref_id=$modal->getTabModalId();
		     	}
		     	$imageM->setImgdesc($modalDetailAdd);
		     	$imageM->setRefId($ref_id);
		     	$imageM->setPageTypeId($pageType);
		     	$imageDao->InsertImage($imageM);
		     	
		     	
		     }
		     else if($mode=="edit"){
		     	$id=$_POST['id'];
			  	$modalHeader= $_POST['modalHeader'.$id];
			   	$modalDetail= $_POST['modalDetail'.$id];
			   	$pageType = $_POST['pageType'];
			   	$tabContentid =$_POST['tabContent'];
			   	$modal=  $tabModalDao->loadTabModal($pageType,$tabContentid);
			   	
			   	$imageM->setPageTypeId($pageType);
			   	$imageM->setRefId($modal->getTabModalId());
			   	$imageM->setImgid($id);
			   	$imageM->setImgdesc($modalDetail);
			   	$imageM->setImgheader($modalHeader);
			   	$imageDao->UpdateImage($imageM);

		     }
		else if($mode=="delete"){
			$id=$_POST['id'];
				$imageM = $imageDao->loadImgById($id);
				$imagepath=$imageM->getImgpath();
				$imageDao->deleteImage($id);
				$host = str_replace('\\','/',dirname(__FILE__));
				$imgPath= str_replace('AA/production','',$host).$imagepath;
				if(file_exists($imgPath)){
					unlink($imgPath);
				}
			
			
		}
		 
		
		
?>
