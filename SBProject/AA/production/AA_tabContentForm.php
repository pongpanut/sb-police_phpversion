<?php
use  SB\model\tab_contentM;
use  SB\model\imageM;
use  SB\model\tab_modalM;
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
include 'constant.php';
		$ds = "/uploads/images/"; 
		$mode=$_POST['mode'];
		$msg="";
		$TabCostentDao = App_DaoFactory::getFactory()->getTabCostentDao();
		$tabModalDao = App_DaoFactory::getFactory()->getTabModalDao();
		$imageDao = App_DaoFactory::getFactory()->getImageDao();
		$path="";
		$imgSeq=0;
		$tab_contentM  = new tab_contentM();
		$tab_modalM  = new tab_modalM();
		
		$imageM = new imageM();
		if (!empty($_FILES)) {
				
			$tempFile = $_FILES['file']['tmp_name'];
			$size = GetimageSize($tempFile);
			$ratio = $size[0]/$size[1]; // width/height
			if( $ratio > 1) {
				$width = 500;
				$height = 500/$ratio;
			}
			else {
				$width = 500*$ratio;
				$height = 500;
			}
			$src = imagecreatefromstring(file_get_contents($tempFile));
			$dst = imagecreatetruecolor($width,$height);
			imagecopyresampled($dst,$src,0,0,0,0,$width,$height,$size[0],$size[1]);
			imagedestroy($src);
			imagejpeg($dst,$tempFile); // adjust format as needed
			imagedestroy($dst);
		
			$targetPath = dirname( __FILE__ ) . $ds;
			$targetFile =  $targetPath. $_FILES['file']['name'];
			$removeHost = str_replace($_SERVER["DOCUMENT_ROOT"],'', str_replace('\\','/',$targetFile ));
		
			move_uploaded_file($tempFile,$targetFile); //6
			if($tempFile !=""){
		    	$path = str_replace($linkPrefix,'',$removeHost) ;
		    	$imgSeq=$imageDao->getSeqImage();
		    	$imageM->setImgid($imgSeq);
		    	$imageM->setImgpath($path);
			}
			 
		}
	
		     if($mode=="add"){
	
		     	$tabConstanctHeaderAdd =$_POST['tabConstanctHeaderAdd'];
		     	$tabConstanctDetailAdd=$_POST['tabConstanctDetailAdd'];
		     	/* $departmentAdd=$_POST['departmentAdd']; */
		     	$pageType = $_POST['pageTypeAdd'];
		     	$tabContentId =$TabCostentDao->getSeqTabContent();
		     	
		     	$imageM->setImgtype("tab_contant");
		     	$imageM->setPageTypeId($pageType);
		     	$imageDao->InsertImage($imageM);
		     	
		     	$tab_contentM->setTabContentHeader($tabConstanctHeaderAdd);
		     	$tab_contentM->setTabContentDetail($tabConstanctDetailAdd);
		     	$tab_contentM->setTabContentImageId($imgSeq);
		     /* 	$tab_contentM->setTabId($departmentAdd); */
		     	$tab_contentM->setPageTypeId($pageType);
		     	$msg=$TabCostentDao->InsertTabContent($tab_contentM);
		     	
		     	
		     	$tab_modalM->setTabModalHeader($tabConstanctHeaderAdd);
		     	$tab_modalM->setTabModalContent($tabConstanctDetailAdd);
		     	$tab_modalM->setTabContentId($tabContentId);
		     	$tab_modalM->setPageTypeId($pageType);
		     	$msg=$tabModalDao->InsertTabModal($tab_modalM);
		     }
		     else if($mode=="edit"){
		     	$id=$_POST['id'];
		     	
		     	$tabConstanctHeader =$_POST['tabConstanctHeader'.$id];
		     	$tabConstanctDetail=$_POST['tabConstanctDetail'.$id];
		     	$pageType = $_POST['pageType'.$id];
		     	/* $department=$_POST['department'.$id]; */
		     	$imgId =$_POST['tabContentImageId'.$id];
		     	
		     	$imageM->setImgid($imgId);
		     	$imageM->setImgtype("tab_contant");
		     	$imageM->setPageTypeId($pageType);
		     	$imageDao->UpdateImage($imageM);
		     	
		     	$tab_contentM->setTabContentId($id);
		     	$tab_contentM->setTabContentHeader($tabConstanctHeader);
		     	$tab_contentM->setTabContentDetail($tabConstanctDetail);
		     	$tab_contentM->setTabContentImageId($imgId);
		     /* 	$tab_contentM->setTabId($department); */
		     	$tab_contentM->setPageTypeId($pageType);
		     	$msg=$TabCostentDao->updateTabContent($tab_contentM);
		     	
		     	$tab_modalM->setTabModalHeader($tabConstanctHeader);
		     	$tab_modalM->setTabModalContent($tabConstanctDetail);
		     	$tab_modalM->setTabContentId($id);
		     	$tab_modalM->setPageTypeId($pageType);
		     	$msg=$tabModalDao->updateTabModal($tab_modalM);
		     	
		     	
		     }
		else if($mode=="delete"){
			$id=$_POST['id'];
			$pageType=$_POST['pageType'];
			
			$tabContent = $TabCostentDao->loadTabContentById($pageType,$id);
			if($tabContent !=""){
				
				$modalDel = $tabModalDao->loadTabModal($pageType,$id);
			
				if($modalDel->getTabModalId() !=""){
					$imageModalArr = $imageDao->loadImgByRefId($modalDel->getTabModalId());
					for ($i = 0; $i < count($imageModalArr); $i++) {
						$img_id_del=$imageModalArr[$i]->getImgid();
						$imageDao->deleteImage($img_id_del);
						
						$img_path_del=$imageModalArr[$i]->getImgpath();
						$host = str_replace('\\','/',dirname(__FILE__));
						$imgPath= str_replace('AA/production','',$host).$img_path_del;
						if(file_exists($imgPath)){
							unlink($imgPath);
						}
					}
					$tabModalDao->deleteTabModal($modalDel->getTabModalId());
				}
				
				
				$imageId= $tabContent->getTabContentImageId();
				$imageM = $imageDao->loadImgById($imageId);	
				$imagepath=$imageM->getImgpath();		
				
				$imageDao->deleteImage($imageId);
				$TabCostentDao->deleteTabContent($id);
				$host = str_replace('\\','/',dirname(__FILE__));
				$imgPath= str_replace('AA/production','',$host).$imagepath;
				if(file_exists($imgPath)){
					unlink($imgPath);
				}  
				
			}
		}
		 
		
		
?>
