<?php


namespace app\page;

use SB\Controller\Controller;

include '../Controller/Controller.php';
use SB\conn\factory\App_DaoFactory;

include_once '../conn/factory/factory.php';
 include_once 'constant.php';
 include_once 'AA_NosessionRedirect.php';
$page_type_id = $_SESSION ['privillege'];
$subsciptDao = App_DaoFactory::getFactory ()->getSubscriptDao ();
$subsciptlist = $subsciptDao->loadAllSubscripter ();
$numsubscript = count ( $subsciptlist );
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>ส่วนการจัดการหลังบ้าน</title>

<!-- Bootstrap -->
<link href="../vendors/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Font Awesome -->
<link href="../vendors/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<!-- iCheck -->
<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<!-- Datatables -->
<link
	href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css"
	rel="stylesheet">
<link
	href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"
	rel="stylesheet">
<link
	href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
	rel="stylesheet">
<link
	href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css"
	rel="stylesheet">
<link
	href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"
	rel="stylesheet">


<script type='text/javascript' src='../src/js/jquery-2.2.3.min.js'></script>
<script type='text/javascript' src='../src/js/backoffice.js'></script>
<script type="text/javascript" src="../tinymce/tinymce.min.js"></script>
<script type='text/javascript' src='../src/js/upload.js'></script>
<script type='text/javascript' src='../src/js/bootstrap-filestyle.min.js'></script>


<!-- Custom Theme Style -->
<link href="../build/css/custom.min.css" rel="stylesheet">
<link href="../src/scss/backoffice.css" rel="stylesheet">
</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
      	<?php
							include 'AA_sidebar.php';
							?>
<div id="load_screen"><div id="loading" ><img alt="" src="../../images/image_997636.gif"></div></div>
        <!-- top navigation -->
		     <?php
		 include 'nav.php';
		?>
			<!-- /top navigation -->
			<!-- /page content -->

			<!--Modal-->

			<!-- page content -->
			<div class="right_col" role="main"
				style="height: 1088; min-height: 1088px;">
				<div class="">
					<div class="page-title">
						<h3>ส่วนการจัดการผู้ติดตาม</h3>

					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;">
					<div class="x_panel">
						<div class="x_title">
							<div class="clearfix"></div>
						</div>
						<h4>เพิ่มข้อมูล</h4>

						<form novalidate="novalidate"
							class=" form-horizontal form-label-left"
							enctype="multipart/form-data" method="POST"
							accept-charset="UTF-8" id="SendEmail">
							<input type="hidden" value="add" name="id"> <input type="hidden"
								value="<?php echo $page_type_id?>" name="pageTypeAdd"> <input
								type="hidden" value="add" name="mode">
							<div class="form-group">
								<label class="control-label col-md-3" for="emailHeaderAdd">หัวข้อ
									:<span class="required">*</span>
								</label>
								<div class="col-md-7">
									<input type="text" id="emailHeaderAdd" name="emailHeaderAdd"
										required="required" class="form-control col-md-7 col-xs-12">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3" for="elm1Add">รายละเอียดข้อความ
									:</label>
								<div class="col-md-7">
									<textarea id="elm1Add" name="elm1Add"></textarea>
								</div>
							</div>

							<script>
						    tinymce.init({
						    	 force_br_newlines : false,
						    	 menubar:false,
						    	    statusbar: false,
						        selector: "textarea#elm1Add",
						        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt 64pt',
						        theme: "modern",
						        width: 600,
						        height: 300,
						        plugins: [
						             "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
						             "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
						             "save table contextmenu directionality emoticons template paste textcolor"
						       ],
						       content_css: "../tinymce/skins/lightgray/content.min.css",
						       toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |forecolor backcolor emoticons | fontsizeselect ", 
						       style_formats: [
						            {title: 'Bold text', inline: 'b'},
						            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
						            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
						            {title: 'Example 1', inline: 'span', classes: 'example1'},
						            {title: 'Example 2', inline: 'span', classes: 'example2'},
						            {title: 'Table styles'},
						            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
						        ],relative_urls : false,
						        remove_script_host : false,
						        convert_urls : true
						    }); 

					</script>
							<div class="col-md-7 col-md-offset-3 ">

								<button type="submit" class="btn btn-primary">ส่งอีเมลล์</button>
							</div>
						</form>
						<div class="x_content">
							<table id="datatable-responsive"
								class="table table-striped table-bordered dt-responsive nowrap"
								cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>รหัส</th>
										<th>รายชื่อผู้ติดตาม</th>
										<th>ลบ</th>
									</tr>
								</thead>
								<tbody>
                      <?php if($numsubscript >0){?>
                           <?php for ($j=0;$j<$numsubscript;$j++){?>
	                        <tr
										id="<?php echo $subsciptlist[$j]->getSubscriptId();?>">
										<td><?php echo $subsciptlist[$j]->getSubscriptId();?></td>
										<td><?php echo $subsciptlist[$j]->getSubscriptEmail();?></td>


										<td>
											<button type="button" id="deleteSubscript"
												class="btn btn-default"
												value="<?php echo $subsciptlist[$j]->getSubscriptId();?>">
												<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
											</button>
										</td>
									</tr>
                       	   <?php }?>
                      <?php }?>
                       
                        
                      </tbody>
							</table>


							<div id="confirm" class="modal hide fade">
								<div class="modal-body">Are you sure?</div>
								<div class="modal-footer">
									<button type="button" data-dismiss="modal"
										class="btn btn-primary" id="delete">Delete</button>
									<button type="button" data-dismiss="modal" class="btn">Cancel</button>
								</div>
							</div>
							<!-- End modal -->

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- footer content -->
	<footer>
		<div class="pull-right"></div>
		<div class="clearfix"></div>
	</footer>
	<!-- /footer content -->




	<!-- jQuery -->
	<script src="../vendors/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="../vendors/fastclick/lib/fastclick.js"></script>
	<!-- NProgress -->
	<script src="../vendors/nprogress/nprogress.js"></script>
	<!-- Datatables -->
	<script type='text/javascript' src='../src/js/jquery.dataTables.js'></script>
	<!-- <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
    <script src="../vendors/datatables.net/js/jquery.dataTables.js"></script> -->
	<script
		src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script
		src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	<script
		src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
	<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
	<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
	<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
	<script
		src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
	<script
		src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
	<script
		src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script
		src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	<script
		src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
	<script src="../vendors/jszip/dist/jszip.min.js"></script>
	<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
	<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

	<!-- Custom Theme Scripts -->
	<script src="../build/js/custom.min.js"></script>
	<script type='text/javascript' src='../src/js/knockout-3.4.0.js'></script>
	<script type="text/javascript"
		src='../src/js/ModelbulderForStaticpage.js'></script>
	<script type='text/javascript' src='../src/js/bootbox.min.js'></script>
	<!-- Datatables -->
	<script>
      $(document).ready(function() {
          
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable({ "order": [[ 0, "desc" ]] , "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]] , "pageLength": 50});

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        TableManageButtons.init();
      });
    </script>
	<!-- /Datatables -->
</body>
</html>