<?php
use SB\conn\factory\App_DaoFactory;
use SB\model\submenulistBOM;
include '../model/submenulistBOM.php';
include_once '../conn/factory/factory.php';


$id = $_POST['id'];
$menuText = $_POST['menuText'];
$dropdown = $_POST['dropdown'];
$seq = $_POST['seq'];
$pageType = $_POST['pageType'];

if($id!=""){
	$submenulist = new submenulistBOM();
	$submenulist->setSubmenuListId($id);
	$submenulist->setSubmenuListText($menuText);
	$submenulist->setPageTypeId($pageType);
	$submenulist->setSubmenuListLink($dropdown);
	$submenulist->setSubmenuListSeq($seq);

	$menuDao = App_DaoFactory::getFactory()->getMenu();
	$msg = $menuDao->updateSubMenuList($submenulist);	 
}
?>