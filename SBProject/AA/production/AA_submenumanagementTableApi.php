<?php namespace app\page;
use SB\Controller\Controller;
include '../Controller/Controller.php';
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
include_once 'AA_NosessionRedirect.php';
$menuText = $_POST['dropdown'];
//echo $menuText;
$menuDao =App_DaoFactory::getFactory()->getMenu();
$staticPageDao =App_DaoFactory::getFactory()->getStaticPage2();

$menu =$menuDao->loadSubMenu($menuText);
$staticPage = $staticPageDao->loadStaticpage($_SESSION['privillege']);
$statpagenum= count($staticPage);
$num = count($menu);
$pagetype = $_SESSION['privillege'];
?>
<!--table-->
 <?php if($num >0){?>
   <div class="x_content">
       		<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                        <th>รหัส</th>
                          <th>ตัวหนังสือเมนู</th>
                          <th>ลิงค์</th>
                          <th>ลำดับที่</th>
                          <th>แก้ไข</th>
                          <th>ลบ</th>
                        </tr>
                      </thead>
                      <tbody>
                     
                           <?php for ($j=0;$j<$num;$j++){?>
                        <tr id="<?php echo $menu[$j]->getSubMenuId();?>">
                        <td><?php echo $menu[$j]->getSubMenuId() ?></td>
                          <td><?php echo $menu[$j]->getSubMenuText() ?></td>
                       			
						 		<?php if($menu[$j]->getSubMenuLink() != null){?>
						 			<td><?php echo $menu[$j]->getSubMenuLink();?></td><?php	
								}
								else{
									?><td> </td><?php
						 		}?>
   
                          <td><?php echo $menu[$j]->getSubmenuSeq();?></td>
                         
                          <td>
						  	<button type="button" id="buttonId" class="btn btn-default" aria-label="Left Align"  data-toggle="modal" data-target="#myModal<?php echo $menu[$j]->getSubMenuId();?>">
							  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
							</button>
						  </td>
                          <td>
                          	<button type="button" id="buttonRemoveSubmenu" class="btn btn-default" value="<?php echo $menu[$j]->getSubMenuId();?>">
								  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								</button>
								</td>
	                        </tr>
                       	   <?php }?>
                      </tbody>
                    </table>
                    </div>
<?php }?>
 <!-- End table-->
 
                 
<!-- Modal -->
 <?php if($num >0){?>
 	<?php for ($j=0;$j<$num;$j++){?>
<div id="myModal<?php echo $menu[$j]->getSubMenuId();?>" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ส่วนปรับปรุงเมนู "<?php echo $menu[$j]->getSubMenuText()?>"</h4>
      </div>
      <div class="modal-body">
        		<form class="form-horizontal form-label-left" id="confirm-submit<?php echo $menu[$j]->getSubMenuId();?>" value ="<?php echo $menu[$j]->getSubMenuId();?>" method="post" accept-charset="UTF-8" action="AA_updateform.php" >
                 <input type="hidden" name="pagetype" id="pagetype" value ="<?php echo $pagetype?>">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="first-name">ตัวหนังสือเมนู <span class="required">*</span>
                    </label>
                    <div class="col-md-7">
                      <input type="text" id="submenutextModal<?php echo $menu[$j]->getSubMenuId();?>" name ="menutextModal" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $menu[$j]->getSubMenuText() ?>">
                    </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3" for="last-name">link ของเมนู</label>
                     <div class="col-md-7">
                          <select id="submenuOptionmodal<?php echo $menu[$j]->getSubMenuId();?>" class="form-control" required>
                          <?php if($statpagenum >0){?>
                          	<option value="0">Choose..</option>
                           	<?php for ($x=0;$x<$statpagenum;$x++){?>
	                           	 <?php if($menu[$j]->getSubMenuLink() == $staticPage[$x]->getStaticpageLink() ){?>
	                            <option selected="selected" value="<?php echo $staticPage[$x]->getStaticpageId()?>"><?php echo $staticPage[$x]->getStaticpageText()?></option>
	                            <?php } else {?>
	                            <option value="<?php echo $staticPage[$x]->getStaticpageId()?>"><?php echo $staticPage[$x]->getStaticpageText()?></option>
	                            <?php } ?>

                            <?php }?>
                          <?php }?>
                          </select>
                          </div>
                   </div>
                    <div class="form-group">
                    <label class="control-label col-md-3" for="last-name">ลำดับที่<span class="required">*</span>
                    </label>
                    <div class="col-md-2">
                      <input type="text" id="submenuseqModal<?php echo $menu[$j]->getSubMenuId();?>" name="seqModal<?php echo $menu[$j]->getSubMenuId();?>" required="required"  value='<?php echo $menu[$j]->getSubmenuSeq();?>' onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
                  	
                </form>
      </div>
      <div class="modal-footer">
		<div class="col-md-7">
							<button type="submit" form="confirm-submit<?php echo $menu[$j]->getSubMenuId();?>" data-target="#confirm-submit<?php echo $menu[$j]->getSubMenuId();?>" id="modifysubmenumodal" class="btn btn-primary" data-dismiss="modal" value="confirm-submit<?php echo $menu[$j]->getSubMenuId();?>" >แก้ไขข้อมูล</button>
					</div>
      </div>
    </div>

  </div>
</div>
   <?php }?>
<?php }?>
<!-- End Modal -->