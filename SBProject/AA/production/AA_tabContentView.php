 				<?php	
 				use SB\Controller\Controller;
 				include '../Controller/Controller.php';
 				use SB\conn\factory\App_DaoFactory;
 				include_once '../conn/factory/factory.php';

 				$page_type_id=$_SESSION['privillege'];

 				$imageDao =App_DaoFactory::getFactory()->getImageDao();
 				/*$tabCostentArr =$imageDao->loadImg($page_type_id,"'tab_content'");
 				 $num = count($tabCostentArr); */
 				
 				$TabCostentDao=App_DaoFactory::getFactory()->getTabCostentDao();
 				$tabCostentArr = $TabCostentDao->loadTabContentByTabId($page_type_id);
 				$numTabContent =count($tabCostentArr);
 				$constantDao= App_DaoFactory::getFactory()->getconstantDao();
 				$cons_type="page_type";
 				
 				
 				?>
 				<div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                           <th>หัวข้อกิจกรรม</th>
                            <th>รายละเอียดกิจกรรม </th>
                            <th>เพิ่มกิจกรรมย่อย</th>
	                         <th>แก้ไข</th>
	                         <th>ลบ</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if($numTabContent >0){?>
                           <?php for ($j=0;$j<$numTabContent;$j++){?>
                        <tr id="<?php echo $tabCostentArr[$j]->getTabContentId();?>">
                           <td><?php echo $tabCostentArr[$j]->getTabContentHeader();?></td>
                           <td><?php echo $tabCostentArr[$j]->getTabContentDetail();?></td>
                           <td>
                           <a href="AA_tabModalManagement.php?id=<?php echo $tabCostentArr[$j]->getTabContentId();?>">
						  	<button type="button" id="buttonlink" class="btn btn-default" aria-label="Left Align" >
							  <span class="glyphicon  glyphicon-th-list" aria-hidden="true"></span>
							</button>
							</a>
						  </td>
                          <td>
						  	<button type="button" id="buttonId" class="btn btn-default" aria-label="Left Align"  data-toggle="modal" data-target="#myModal<?php echo $tabCostentArr[$j]->getTabContentId();?>">
							  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
							</button>
						  </td>
                          <td>
                          	<button type="button" id="btnRemoveTabContent" class="btn btn-default" value="<?php echo $tabCostentArr[$j]->getTabContentId();?>">
								  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								</button>
								</td>
	                        </tr>
                       	   <?php }?>
                      <?php }?>
                       
                        
                      </tbody>
                    </table>
             
                      <!-- Modal -->
 <?php if($numTabContent >0){?>
 	<?php for ($k=0;$k<$numTabContent;$k++){?>
<div id="myModal<?php echo $tabCostentArr[$k]->getTabContentId();?>" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="max-height: 1024px;">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ส่วนแก้ไข : "<?php echo $tabCostentArr[$k]->getTabContentHeader()?>"</h4>
      </div>
      <div class="modal-body">
				<form  novalidate="novalidate" class=" form-horizontal form-label-left" enctype="multipart/form-data" method="POST"  accept-charset="UTF-8" id="tabContentEditForm<?php echo $tabCostentArr[$k]->getTabContentId();?>" >
                  <input type="hidden" value="<?php echo $tabCostentArr[$k]->getTabContentId();?>" name="id">
                   <input type="hidden" value="<?php echo $page_type_id?>" name="pageType<?php echo $tabCostentArr[$k]->getTabContentId();?>" id="pageType<?php echo $tabCostentArr[$k]->getTabContentId();?>">
                    <input type="hidden" value="edit" name="mode">
                   <input type="hidden" value="<?php echo $tabCostentArr[$k]->getTabId();?>" name="department<?php echo $tabCostentArr[$k]->getTabContentId();?>">
                    <input type="hidden" name="tabContentImageId<?php echo $tabCostentArr[$k]->getTabContentId();?>" value="<?php echo $tabCostentArr[$k]->getTabContentImageId() ?>">
                   <div class="form-group">
                    <label class="control-label col-md-3" for="tabConstanctHeader<?php echo $tabCostentArr[$k]->getTabContentId();?>">หัวข้อกิจกรรม  :<span class="required">*</span>
                    </label>
                    <div class="col-md-7">
                      <input type="text" id="tabConstanctHeader<?php echo $tabCostentArr[$k]->getTabContentId();?>" name ="tabConstanctHeader<?php echo $tabCostentArr[$k]->getTabContentId();?>" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $tabCostentArr[$k]->getTabContentHeader();?>" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="tabConstanctDetail<?php echo $tabCostentArr[$k]->getTabContentId();?>">รายละเอียดกิจกรรม :<span class="required">*</span>
                    </label>
                    <div class="col-md-7">
                      <input type="text" id="tabConstanctDetail<?php echo $tabCostentArr[$k]->getTabContentId();?>" name ="tabConstanctDetail<?php echo $tabCostentArr[$k]->getTabContentId();?>" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $tabCostentArr[$k]->getTabContentDetail();?>">
                    </div>
                  </div>
                 <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="header-title">อัพโหลดรูปภาพ : <span class="required">*</span>
                        </label>
                        <div class="col-md-6 ">  
				            <!-- image-preview-filename input [CUT FROM HERE]-->
				            <div class="input-group image-preview1">
				                <input type="text" class="form-control image-preview-filename1" disabled="disabled" id="imagepath<?php echo $tabCostentArr[$k]->getTabContentId();?>"> <!-- don't give a name === doesn't send on POST/GET -->
				                <span class="input-group-btn">
				                    <!-- image-preview-clear button -->
				                    <button type="button" class="btn btn-default image-preview-clear1" style="display:none;">
				                        <span class="glyphicon glyphicon-remove"></span> Clear
				                    </button>
				                    <!-- image-preview-input -->
				                    <div class="btn btn-default image-preview-input1">
				                        <span class="glyphicon glyphicon-folder-open"></span>
				                        <span class="image-preview-input-title1">Browse</span>
				                        <input type="file" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint , application/pdf, image/*" name="file" id="imageAttach"> <!-- rename it -->
				                    </div>
				                </span>
				            </div><!-- /input-group image-preview [TO HERE]--> 
				        </div>
				       
                      </div>
                      <?php 
                      $image="";
                      if($tabCostentArr[$k]->getTabContentImageId()!=""){
                      	$image=$imageDao->loadImgById($tabCostentArr[$k]->getTabContentImageId());
                     
                      
                      ?>
                       <div class="form-group">
	                     <label class="control-label col-md-3" for="imagePreview<?php echo $tabCostentArr[$k]->getTabContentId();?>">current Image :</label>
	                     <div class="col-md-7">
	                      	<img src="../../<?php echo $image->getImgpath()?>" class="img-responsive" id="imagePreview" name="imagePreview<?php echo $tabCostentArr[$k]->getTabContentId();?>">
	                      </div>
                   </div>
                   <script>
                      $(document).ready(function() { 
                          $("#imagepath<?php echo $tabCostentArr[$k]->getTabContentId();?>").val("<?php echo $image->getImgpath()?>");
                      });
                      </script>
                      <?php }?>
					<div class="col-md-7 col-md-offset-6 ">
						<button type="submit" class="btn btn-primary" id="tabContentUploadForm" value="<?php echo $tabCostentArr[$k]->getTabContentId();?>" >เพิ่มข้อมูล</button>
					</div>
                </form>
      </div>
      <div class="modal-footer">		
      </div>
    </div>
  </div>
</div>
   <?php }?>
<?php }?>
                