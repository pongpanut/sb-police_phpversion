<?php
use SB\model\departmentLiBOM;
use SB\conn\factory\App_DaoFactory;
include '../model/departmentLiBOM.php';
include_once '../conn/factory/factory.php';
include_once 'AA_NosessionRedirect.php';

$id = $_POST['id'];
$text = $_POST['text'];
$dropdown = $_POST['dropdown'];
$seq = $_POST['seq'];

if($id!=""){
	$departmentLiBOM = new departmentLiBOM();
	$departmentLiBOM->setDeptListId($id);
	$departmentLiBOM->setDeptListText($text);
	$departmentLiBOM->setDeptListLink($dropdown);
	$departmentLiBOM->setSeq($seq);
	 
	$deptDao = App_DaoFactory::getFactory()->getDepartmentDao();
	$msg = $deptDao->updateDeptList($departmentLiBOM);	 
}
?>