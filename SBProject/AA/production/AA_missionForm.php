<?php 
use SB\model\missionM;
use SB\conn\factory\App_DaoFactory;

include_once '../conn/factory/factory.php';

	$mode=$_POST['mode'];
	
	$missionDao = App_DaoFactory::getFactory()->getMissionDao();
	if($mode=="add"){
		$id = $_POST['id'];
		$missionHeader =$_POST['missionHeader'];
		$missionDesc=$_POST['missionDesc'];
		$missionType=$_POST['missionType'];
		$showFlag=$_POST['showFlag'];
		$elm1=$_POST['elm1'];
		$pageType=$_POST['pageType'];
		$missionImage=$_POST['missionImage'];
		
		$missionM = new missionM();
		$missionM->setMissionId($id);
		$missionM->setMissionText($elm1);
		$missionM->setMissionHeader($missionHeader);
		$missionM->setMissionImage($missionImage);
		$missionM->setMissionDesc($missionDesc);
		$missionM->setMissionType($missionType);
		$missionM->setShowFlag($showFlag);
		$missionM->setPageTypeId($pageType);
		
		
	
		if($id!=""){
			$msg = $missionDao->UpdateMission($missionM);
		}
		else{
			$msg = $missionDao->InsertMission($missionM);
		}
	}
	else if($mode=="delete"){
		$id = $_POST['id'];
		$msg = $missionDao->deleteMission($id);
	}
	
		
	return  $msg;

	
?>