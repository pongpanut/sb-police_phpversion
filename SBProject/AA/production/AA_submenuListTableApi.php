<?php namespace app\page;
use SB\Controller\Controller;
include '../Controller/Controller.php';
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
include_once 'AA_NosessionRedirect.php';
$subMenuId = $_POST['dropdown'];
//echo $menuText;
/*Init DAO*/
$menuDao =App_DaoFactory::getFactory()->getMenu();
$staticPageDao =App_DaoFactory::getFactory()->getStaticPage2();

/*Retrieve data*/
$submenulist =$menuDao->loadSubMenuList($subMenuId);
$staticPage = $staticPageDao->loadStaticpage(1);
$statpagenum= count($staticPage);
$num = count($submenulist);
$pagetype = $_SESSION['privillege'];
?>
<!--table-->
 <?php if($num >0){?>
       		<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>ตัวหนังสือเมนู</th>
                          <th>ลิงค์</th>
                          <th>ลำดับที่</th>
                          <th>แก้ไข</th>
                          <th>ลบ</th>
                        </tr>
                      </thead>
                      <tbody>
                     
                           <?php for ($j=0;$j<$num;$j++){?>
                        <tr id="<?php echo $submenulist[$j]->getSubmenuListId();?>">
                          <td><?php echo $submenulist[$j]->getSubmenuListText() ?></td>
                       			
						 		<?php if($submenulist[$j]->getSubmenuListLink() != null){?>
						 			<td><?php echo $submenulist[$j]->getSubmenuListLink();?></td><?php	
								}
								else{
									?><td> </td><?php
						 		}?>
   
                          <td><?php echo $submenulist[$j]->getSubmenuListSeq();?></td>
                         
                          <td>
						  	<button type="button" id="buttonId" class="btn btn-default" aria-label="Left Align"  data-toggle="modal" data-target="#myModal<?php echo $submenulist[$j]->getSubmenuListId();?>">
							  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
							</button>
						  </td>
                          <td>
                          	<button type="button" id="buttonRemoveSubmenuList" class="btn btn-default" value="<?php echo $submenulist[$j]->getSubmenuListId();?>">
								  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								</button>
								</td>
	                        </tr>
                       	   <?php }?>
                      </tbody>
                    </table>
<?php }?>
 <!-- End table-->
 
                 
<!-- Modal -->
 <?php if($num >0){?>
 	<?php for ($j=0;$j<$num;$j++){?>
<div id="myModal<?php echo $submenulist[$j]->getSubmenuListId();?>" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ส่วนปรับปรุงเมนู "<?php echo $submenulist[$j]->getSubmenuListText()?>"</h4>
      </div>
      <div class="modal-body">
        		<form class="form-horizontal form-label-left" id="confirm-submit<?php echo $submenulist[$j]->getSubmenuListId();?>" value ="<?php echo $submenulist[$j]->getSubmenuListId();?>" method="post" accept-charset="UTF-8"  >
                 <input type="hidden" name="pagetype" id="pagetype" value ="<?php echo $pagetype?>">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="first-name">ตัวหนังสือเมนู <span class="required">*</span>
                    </label>
                    <div class="col-md-7">
                      <input type="text" id="submenulisttextModal<?php echo $submenulist[$j]->getSubmenuListId();?>" name ="menutextModal" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $submenulist[$j]->getSubmenuListText() ?>">
                    </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3" for="last-name">link ของเมนู</label>
                     <div class="col-md-7">
                          <select id="submenulstOptionmodal<?php echo $submenulist[$j]->getSubmenuListId();?>" class="form-control" required>
                          <?php if($statpagenum >0){?>
                          	<option value="0">Choose..</option>
                           	<?php for ($x=0;$x<$statpagenum;$x++){?>
	                           	 <?php if($submenulist[$j]->getSubmenuListLink() == $staticPage[$x]->getStaticpageLink() ){?>
	                            <option selected="selected" value="<?php echo $staticPage[$x]->getStaticpageId()?>"><?php echo $staticPage[$x]->getStaticpageText()?></option>
	                            <?php } else {?>
	                            <option value="<?php echo $staticPage[$x]->getStaticpageId()?>"><?php echo $staticPage[$x]->getStaticpageText()?></option>
	                            <?php } ?>

                            <?php }?>
                          <?php }?>
                          </select>
                          </div>
                   </div>
                    <div class="form-group">
                    <label class="control-label col-md-3" for="last-name">ลำดับที่<span class="required">*</span>
                    </label>
                    <div class="col-md-2">
                      <input type="text" id="submenulistseqModal<?php echo $submenulist[$j]->getSubmenuListId();?>" name="seqModal<?php echo $submenulist[$j]->getSubmenuListId();?>" required="required"  value='<?php echo $submenulist[$j]->getSubmenuListSeq();?>' onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
                  	
                </form>
      </div>
      <div class="modal-footer">
		<div class="col-md-7">
							<button type="submit" form="confirm-submit<?php echo $submenulist[$j]->getSubmenuListId();?>" data-target="#confirm-submit<?php echo $submenulist[$j]->getSubmenuListId();?>" id="modifysubmenulistmodal" class="btn btn-primary" data-dismiss="modal" value="confirm-submit<?php echo $submenulist[$j]->getSubmenuListId();?>" >แก้ไขข้อมูล</button>
					</div>
      </div>
    </div>

  </div>
</div>
   <?php }?>
<?php }?>
<!-- End Modal -->