<?php namespace app\page;
use SB\Controller\Controller;
include '../Controller/Controller.php';
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
include_once 'AA_NosessionRedirect.php';
$departmentDao =App_DaoFactory::getFactory()->getDepartmentDao();

$departmentListArr =$departmentDao->loadDepartmentList($_SESSION['privillege']);
$departmentListNum= count($departmentListArr);

$departmentArr =$departmentDao->loadDepartment($_SESSION['privillege']);
$departmentNum= count($departmentArr);
$pagetype = $_SESSION['privillege'];
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ส่วนการจัดการหลังบ้าน</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    
 
    <script type='text/javascript' src='../src/js/jquery-2.2.3.min.js'></script>
    <script type='text/javascript' src='../src/js/backoffice.js'></script>
    
    
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      	<?php
		 include 'AA_sidebar.php';
		?>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                         <!-- <p>First name: <input data-bind="value: firstName" /></p>
<p>Last name: <input data-bind="value: lastName" /></p>
<h2>Hello, <span data-bind="text: fullName"> </span>!</h2>-->
  
                    <img src="images/user.png" alt=""><?php echo $_SESSION['username']?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                </li>   
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
            <!-- /page content -->
    
<!--Modal-->

        <!-- page content -->
        <div class="right_col" role="main" style="height:1088;min-height: 1088px;">
          <div class="">
            <div class="page-title">
                <h3>ส่วนการจัดการหน่วยย่อยในหน่วยงาน </h3>
               
              </div>
            </div>
            <div class="clearfix"></div>  
              <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;">
                <div class="x_panel">
                  <div class="x_title">
                    <div class="clearfix"></div>
                  </div> 
               <h4>เพิ่มข้อมูล</h4>
               
                <form class="form-horizontal form-label-left" id="departmentmanagementlistForm">
                 <input type="hidden" name="pagetype" id="pagetype" value ="<?php echo $pagetype?>">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="first-name">ตัวหนังสือหน่วยงานย่อย <span class="required">*</span>
                    </label>
                    <div class="col-md-7">
                      <input type="text" id="insertDepartmentListText" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
            
                  	<div class="form-group">
                  	   <label class="control-label col-md-3" for="last-name">link ของเมนู</label>
                     	<div class="col-md-7">
                          <select id="insertDepartmentListLink" class="form-control" required>
             
                          <?php if($departmentNum >0){?>
                          	<option value="">Choose..</option>
                           	<?php for ($j=0;$j<$departmentNum;$j++){?>
                            <option value="<?php echo $departmentListArr[$j]->getDeptListText()?>"><?php echo $departmentListArr[$j]->getDeptListText()?></option>

                            <?php }?>
                          <?php }?>
                          </select>
                          </div>
                   </div>
                    <div class="form-group">
                    <label class="control-label col-md-3" for="last-name">ลำดับที่<span class="required">*</span>
                    </label>
                    <div class="col-md-1">
                      <input type="text" id="insertDepartmentListSeq" name="last-name" required="required" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="last-name"></span>
                    </label>
                    <div class="col-md-7">
                      <button id="insertDepartmentListButton" type="button" class="center-block btn btn-primary" style="margin-left: 0;">เพิ่มเมนู</button>
                    </div>
                  </div>
              
                </form>
                  <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>ตัวหนังสือหน่วยงานย่อย</th>
                          <th>ลิงค์</th>
         					<th>ลำดับที่</th>
                          <th>แก้ไข</th>
                          <th>ลบ</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if($departmentListNum >0){?>
                           <?php for ($j=0;$j<$departmentListNum;$j++){?>
	                        <tr id="<?php echo $departmentListArr[$j]->getDeptListId();?>">
	                          <td><?php echo $departmentListArr[$j]->getDeptListText() ?></td>
	                       			
							 		<?php if($departmentListArr[$j]->getDeptListLink() != null){
								    	?><td><?php echo $departmentListArr[$j]->getDeptListLink();?></td><?php	
									}
									else{
										?><td> </td><?php
							 		}?>
	                           <td><?php echo $departmentListArr[$j]->getSeq();?></td>
	           
	                         
	                          <td>
							  	<button type="button" id="buttonId" class="btn btn-default" aria-label="Left Align"  data-toggle="modal" data-target="#myModal<?php echo $departmentListArr[$j]->getDeptListId();?>">
								  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</button>
							  </td>
	                          <td>
	                          	<button type="button" id="buttonRemoveDepartmentList" class="btn btn-default" value="<?php echo $departmentListArr[$j]->getDeptListId();?>">
								  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								</button>
								</td>
	                        </tr>
                       	   <?php }?>
                      <?php }?>
                       
                        
                      </tbody>
                    </table>
                    <!-- Modal -->
                    
 <?php if($departmentListNum >0){?>
 	<?php for ($j=0;$j<$departmentListNum;$j++){?>
<div id="myModal<?php echo $departmentListArr[$j]->getDeptListId();?>" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ส่วนปรับปรุงหน่วยงาน "<?php echo $departmentListArr[$j]->getDeptListText()?>"</h4>
      </div>
      <div class="modal-body">
        		<form class="form-horizontal form-label-left" id="confirm-submit<?php echo $departmentListArr[$j]->getDeptListId();?>" value ="<?php echo $departmentListArr[$j]->getDeptListId();?>" method="post" accept-charset="UTF-8"  >
                  <div class="form-group">
                    <label class="control-label col-md-3" for="first-name">ตัวหนังสือเมนู <span class="required">*</span>
                    </label>
                    <div class="col-md-7">
                      <input type="text" id="departmentlisttextModal<?php echo $departmentListArr[$j]->getDeptListId();?>" name ="menutextModal" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $departmentListArr[$j]->getDeptListText() ?>">
                    </div>
                          </div> 
                  	<div class="form-group">
                     <label class="control-label col-md-3" for="last-name">link ของเมนู</label>
                     <div class="col-md-7">
                          <select id="departmentListlinkmodal<?php echo $departmentListArr[$j]->getDeptListId();?>" class="form-control" required>
                          <?php if($departmentListNum >0){?>
                          	<option value="0">Choose..</option>
                           	<?php for ($x=0;$x<$departmentNum;$x++){?>
	                           	 <?php if($departmentListArr[$j]->getDeptListLink() == $departmentArr[$x]->getDeptLink() ){?>
	                            <option selected="selected" value="<?php echo $departmentArr[$x]->getDeptText()?>"><?php echo $departmentArr[$x]->getDeptText()?></option>
	                            <?php } else {?>
	                            <option value="<?php echo $departmentArr[$x]->getDeptText()?>"><?php echo $departmentArr[$x]->getDeptText()?></option>
	                            <?php } ?>

                            <?php }?>
                          <?php }?>
                          </select> 
                
                          </div>
                   </div>
                                       
           
	                  <div class="form-group">
	                    <label class="control-label col-md-3" for="last-name">ลำดับที่<span class="required">*</span>
	                    </label>
	                    <div class="col-md-2">
	                      <input type="text" id="departmentListseqmodal<?php echo $departmentListArr[$j]->getDeptListId();?>" name="departmentListseqmodal<?php echo $departmentListArr[$j]->getDeptListId();?>" required="required"  value='<?php echo $departmentListArr[$j]->getSeq();?>' onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control col-md-7 col-xs-12">
	                    </div>
	                  </div>
            
                </form>
      </div>
      <div class="modal-footer">
		<div class="col-md-7">
							<button type="submit" form="confirm-submit<?php echo $departmentListArr[$j]->getDeptListId();?>" data-target="#confirm-submit<?php echo $departmentListArr[$j]->getDeptListId();?>" id="modifyDepartmentListmodal" class="btn btn-primary" data-dismiss="modal" value="confirm-submit<?php echo $departmentListArr[$j]->getDeptListId();?>" >แก้ไขข้อมูล</button>
					</div>
      </div>
    </div>

  </div>
</div>
   <?php }?>
<?php }?>

<div id="confirm" class="modal hide fade">
  <div class="modal-body">
    Are you sure?
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
  </div>
</div>
<!-- End modal -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->




    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script type='text/javascript' src='../src/js/jquery.dataTables.js'></script>
    <!-- <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
    <script src="../vendors/datatables.net/js/jquery.dataTables.js"></script> -->
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script type='text/javascript' src='../src/js/knockout-3.4.0.js'></script>
	<script type="text/javascript" src='../src/js/ModelbulderForStaticpage.js'></script>
	    <script type='text/javascript' src='../src/js/bootbox.min.js'></script>
    <!-- Datatables -->
    <script>
      $(document).ready(function() {
          
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable({ "order": [[ 0, "desc" ]] , "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]] , "pageLength": 50});

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
  </body>
</html>