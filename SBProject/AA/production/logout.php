<?php
include '../Controller/Controller.php';
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
if (! isset ( $_SESSION )) {
	session_start ();
}

$pagetypeDao =App_DaoFactory::getFactory()->getPageTypeDao();
$test = 'sp';
if(isset($_SESSION['privillege'])){
	$pagetypeMapping = $pagetypeDao ->getPageTypeMappging($_SESSION['privillege']);
	$test = $pagetypeMapping[0];
}

if(isset($_GET['action'])  && $_GET['action'] == "logout" ){
	session_unset();
	session_destroy(); 
	echo('../../'.$test.'/index.php');
}?>