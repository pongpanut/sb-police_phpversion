<?php 
use SB\Controller\Controller;
include '../Controller/Controller.php';
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
include_once 'AA_NosessionRedirect.php';
include_once 'constant.php';
$page_type_id=$_SESSION['privillege'];
$staticPageDao =App_DaoFactory::getFactory()->getStaticPage2();
$staticPage = $staticPageDao->loadStaticpage($page_type_id);
$num = count($staticPage);

$imageDao =App_DaoFactory::getFactory()->getImageDao();
$imageArr=$imageDao->loadImg($page_type_id,"'activity'");
$numImage = count($imageArr);

$videoDao =App_DaoFactory::getFactory()->getVideoDao();
$videoArr=$videoDao->loadVideo($page_type_id);
$numvideo = count($videoArr);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ส่วนการจัดการหลังบ้าน</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
        <!-- Select2 -->
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">
    
     <!-- Dropzone.js -->
    <link href="../vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
    <script type='text/javascript' src='../src/js/jquery-2.2.3.min.js'></script>
    <script type='text/javascript' src='../src/js/backoffice.js'></script>
    <script type="text/javascript" src="../tinymce/tinymce.min.js" ></script>
      <link href="../src/scss/backoffice.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
           
            <!-- sidebar menu -->
            	<?php
            	
			 include 'AA_sidebar.php';
			?>
<div id="load_screen"><div id="loading" ><img alt="" src="../../images/image_997636.gif"></div></div>			
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
             <?php
		 include 'nav.php';
		?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>จัดการ Page </h3>
              </div>
            </div>
           <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                	 <div>
            			<form class="form-horizontal form-label-left" id="staticPageFormAdd"   method="post" accept-charset="UTF-8"  >
            			<input type="hidden" id="page_type_id" name="page_type_id" value="<?php echo $page_type_id ;?>">
							                  <div class="form-group">
							                    <label class="control-label col-md-3" for="staticTextAdd">ชื่อ page :<span class="required">*</span>
							                    </label>
							                    <div class="col-md-7">
							                      <input type="text" id="staticTextAdd" name ="staticTextAdd" required="required" class="form-control col-md-7 col-xs-12" value="">
							                    </div>
							                  
							                  </div>
							                   <div class="form-group">
							                     <label class="control-label col-md-3" for="staticLinkAdd">Html :</label>
							                     <div class="col-md-7">
							                          <textarea id="elm1Add" name="area"></textarea>
							                       </div>
							                   </div>
							                   
							                   
							                   <script>
												    tinymce.init({
												        selector: "textarea#elm1Add",
												        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt 64pt',
												        theme: "modern",
												        width: 690,
												        height: 300,
												        plugins: [
												             "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
												             "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
												             "save table contextmenu directionality emoticons template paste textcolor"
												       ],
												       content_css: "../tinymce/skins/lightgray/content.min.css",
												       toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | fontsizeselect", 
												       style_formats: [
												            {title: 'Bold text', inline: 'b'},
												            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
												            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
												            {title: 'Example 1', inline: 'span', classes: 'example1'},
												            {title: 'Example 2', inline: 'span', classes: 'example2'},
												            {title: 'Table styles'},
												            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
												        ],
												        relative_urls : false,
												        remove_script_host : false,
												        convert_urls : true,
												        image_prepend_url: "<?php echo $_SERVER['SERVER_NAME']?>",
												        image_list: [
												                 	<?php if($numImage>0){
											                       		for ($z = 0; $z < $numImage; $z++) {
											                       			?>
											                       			{title: '<?php echo $imageArr[$z]->getImgheader()?>', value: '<?php echo $linkPrefix.$imageArr[$z]->getImgpath()?>'},
											                       			<?php
											                       			if($z==$numImage){
											                       				?>	
											                       				{title: '<?php echo $imageArr[$z]->getImgheader()?>', value: '<?php echo $linkPrefix.$imageArr[$z]->getImgpath()?>'}
											                       				<?php
											                       			}
											                       		}
											                       	}?>
												                    
												                   ]
												        
												    }); 
												    
												</script>  
							                   
							                   <div class="form-group">
							                     <label class="control-label col-md-3" for="staticActiveAdd">สถานะ  :</label>
							                     <div class="col-md-7">
							                       <select id="staticActiveAdd" class="form-control" required>
									                        <option selected="selected" value="1">แสดง</option>
									                        <option  value="2">ไม่แสดง</option>
									                     
								                      </select>
							                       </div>
							                   </div>
							                  
							                  <div class="col-md-12 col-md-offset-5" style="margin-top: 30px;">
							                  		<button type="submit" form="staticPageFormAdd" data-target="#staticPageFormAdd" id="submitStaticAdd" class="btn btn-primary" data-dismiss="modal" value="staticPageFormAdd" >เพิ่มข้อมูล</button>
								
												</div>
							                  	
							                </form>
            
            		</div>
                
                
                
                
                  <div class="x_content">   
                    <br /> 
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>รหัส</th>
                          <th>ชื่อเพจ</th>
                          <th>ลิงค์</th>
                          <th>สถานะ</th>
                          <th>แก้ไข</th>
                          <th>ลบ</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if($num >0){?>
                           <?php for ($j=0;$j<$num;$j++){
                          ?>
	                        <tr id="<?php echo $staticPage[$j]->getStaticpageId();?>">
	                        <td><?php echo $staticPage[$j]->getStaticpageId() ?></td>
	                          <td><?php echo $staticPage[$j]->getStaticpageText() ?></td>
	                       			
							 		<?php if($staticPage[$j]->getStaticpageLink() != null){
								    	?><td><?php echo $staticPage[$j]->getStaticpageLink();?></td><?php	
									}
									else{
										?><td> </td><?php
							 		}?>
	   
	                          <td><?php echo $staticPage[$j]->getStaticpageActive()==1?"แสดง":"ไม่แสดง";?></td>
	                         
	                          <td>
							  	<button type="button" id="buttonId" class="btn btn-default" aria-label="Left Align"  data-toggle="modal" data-target="#myModal<?php echo $staticPage[$j]->getStaticpageId();?>">
								  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</button>
							  </td>
	                          <td>
	                          	<button type="button" id="StaticButtonRemove" class="btn btn-default" aria-label="Left Align" value="<?php echo $staticPage[$j]->getStaticpageId();?>">
								  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								</button>
								</td>
	                        </tr>
	                        
	                        
	                        <div id="myModal<?php echo $staticPage[$j]->getStaticpageId();?>" class="modal fade" role="dialog">
							  <div class="modal-dialog modal-lg" style="  max-height: 1024px;">
							
							    <!-- Modal content-->
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title">ส่วนปรับปรุง WebPage</h4>
							      </div>
							      <div class="modal-body"  style="margin-bottom: 150px">
							        		<form class="form-horizontal form-label-left" id="staticPageForm<?php echo $staticPage[$j]->getStaticpageId();?>" value ="<?php echo $staticPage[$j]->getStaticpageId();?>"  method="post" accept-charset="UTF-8" " >
							                <input type="hidden" id="page_type_id" name="page_type_id" value="<?php echo $page_type_id ;?>">
							                  <div class="form-group">
							                    <label class="control-label col-md-3" for="staticText<?php echo $staticPage[$j]->getStaticpageId();?>">ชื่อ page :<span class="required">*</span>
							                    </label>
							                    <div class="col-md-7">
							                      <input type="text" id="staticText<?php echo $staticPage[$j]->getStaticpageId();?>" name ="staticText<?php echo $staticPage[$j]->getStaticpageId();?>" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $staticPage[$j]->getStaticpageText();?>">
							                    </div>
							                  </div>
							                  <div class="form-group">
							                     <label class="control-label col-md-3" for="staticLink<?php echo $staticPage[$j]->getStaticpageId();?>">link ของเมนู :</label>
							                     <div class="col-md-7">
							                         <input type="text" id="staticLink<?php echo $staticPage[$j]->getStaticpageId();?>" name ="staticLink<?php echo $staticPage[$j]->getStaticpageId();?>" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $staticPage[$j]->getStaticpageLink();?>">  
							                       </div>
							                   </div>
							                   <div class="form-group">
							                     <label class="control-label col-md-3" for="staticLink<?php echo $staticPage[$j]->getStaticpageId();?>">Html :</label>
							                     <div class="col-md-7">
							                          <textarea id="elm1<?php echo $staticPage[$j]->getStaticpageId();?>" name="area"></textarea>
							                       </div>
							                   </div>
							                   
							                   
							                   <script>
												    tinymce.init({
												    	 force_br_newlines : false,
												        selector: "textarea#elm1"+<?php echo $staticPage[$j]->getStaticpageId();?>,
												        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt 64pt',
												        theme: "modern",
												        width: 600,
												        height: 300,
												        plugins: [
												             "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
												             "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
												             "save table contextmenu directionality emoticons template paste textcolor"
												       ],
												       content_css: "../tinymce/skins/lightgray/content.min.css",
												       toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | fontsizeselect", 
												       style_formats: [
												            {title: 'Bold text', inline: 'b'},
												            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
												            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
												            {title: 'Example 1', inline: 'span', classes: 'example1'},
												            {title: 'Example 2', inline: 'span', classes: 'example2'},
												            {title: 'Table styles'},
												            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
												        ]
												        ,relative_urls : false,
												        remove_script_host : false,
												        convert_urls : true,
												        image_prepend_url: "<?php echo $_SERVER['SERVER_NAME']?>",
												        image_list: [
												                 	<?php if($numImage>0){
											                       		for ($z = 0; $z < $numImage; $z++) {
											                       			?>
											                       			{title: '<?php echo $imageArr[$z]->getImgheader()?>', value: '<?php echo $linkPrefix.$imageArr[$z]->getImgpath()?>'},
											                       			<?php
											                       			if($z==$numImage){
											                       				?>
											                       				{title: '<?php echo $imageArr[$z]->getImgheader()?>', value: '<?php echo $linkPrefix.$imageArr[$z]->getImgpath()?>'}
											                       				<?php
											                       			}
											                       		}
											                       	}?>
												                    
												                   ]

												    }); 
												    $(document).ready(function() {
												    	var replaced='<?php echo str_replace("\n","",$staticPage[$j]->getStaticHtml()) ;?>';
												    	$("#elm1<?php echo $staticPage[$j]->getStaticpageId();?>").html(replaced); 
												    });
												</script>  
							                   
							                   <div class="form-group">
							                     <label class="control-label col-md-3" for="staticActive<?php echo $staticPage[$j]->getStaticpageId();?>">สถานะ  :</label>
							                     <div class="col-md-7">
							                       <select id="staticActive<?php echo $staticPage[$j]->getStaticpageId();?>" class="form-control" required>
								                         
								                          	<option value="0">Choose..</option>
								                         
									                           	 <?php if($staticPage[$j]->getStaticpageActive() ==1 ){?>
									                            	<option selected="selected" value="1">แสดง</option>
									                            	<option  value="2">ไม่แสดง</option>
									                            <?php } else {?>
									                           		<option  value="1">แสดง</option>
									                            	<option selected="selected" value="2">ไม่แสดง</option>
									                            <?php } ?>
								                      </select>
							                       </div>
							                   </div>
							                  
							                  <div class="col-md-12 col-md-offset-5" style="margin-top: 30px;">
							                  
							                  		<button type="submit" form="staticPageForm<?php echo $staticPage[$j]->getStaticpageId();?>" data-target="#staticPageForm<?php echo $staticPage[$j]->getStaticpageId();?>" id="submitStatic" class="btn btn-primary" data-dismiss="modal" value="staticPageForm<?php echo $staticPage[$j]->getStaticpageId();?>" >แก้ไขข้อมูล</button>
								
												</div>
							                  	
							                </form>
							      </div>
							      <div class="modal-footer">
									
							      </div>
							    </div>
							
							  </div>
							</div>
	                        
	                        
                       	   <?php }?>
                      <?php }?>
                      </tbody>
                    </table>
                    <br />
                    <br />
                    <br />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- compose -->
    <div class="compose col-md-6 col-xs-12">
      <div class="compose-header">
        New Message
        <button type="button" class="close compose-close">
          <span>×</span>
        </button>
      </div>

      <div class="compose-body">
        <div id="alerts"></div>

        <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor">
          <div class="btn-group">
            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
            <ul class="dropdown-menu">
            </ul>
          </div>

          <div class="btn-group">
            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li>
                <a data-edit="fontSize 5">
                  <p style="font-size:17px">Huge</p>
                </a>
              </li>
              <li>
                <a data-edit="fontSize 3">
                  <p style="font-size:14px">Normal</p>
                </a>
              </li>
              <li>
                <a data-edit="fontSize 1">
                  <p style="font-size:11px">Small</p>
                </a>
              </li>
            </ul>
          </div>

          <div class="btn-group">
            <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
            <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
            <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
            <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
          </div>

          <div class="btn-group">
            <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
            <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
            <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
            <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
          </div>

          <div class="btn-group">
            <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
            <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
            <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
            <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
          </div>

          <div class="btn-group">
            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
            <div class="dropdown-menu input-append">
              <input class="span2" placeholder="URL" type="text" data-edit="createLink" />
              <button class="btn" type="button">Add</button>
            </div>
            <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
          </div>

          <div class="btn-group">
            <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="fa fa-picture-o"></i></a>
            <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
          </div>

          <div class="btn-group">
            <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
            <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
          </div>
        </div>

        <div id="editor" class="editor-wrapper"></div>
      </div>

      <div class="compose-footer">
        <button id="send" class="btn btn-sm btn-success" type="button">Send</button>
      </div>
    </div>
    <!-- /compose -->


    

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script type='text/javascript' src='../src/js/jquery.dataTables.js'></script>
    <!-- <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
    <script src="../vendors/datatables.net/js/jquery.dataTables.js"></script> -->
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script type='text/javascript' src='../src/js/knockout-3.4.0.js'></script>
	<script type="text/javascript" src='../src/js/ModelbulderForStaticpage.js'></script>
	    <script type='text/javascript' src='../src/js/bootbox.min.js'></script>
    <!-- Datatables -->
      
      

    <!-- bootstrap-wysiwyg -->
   <script>
      $(document).ready(function() {
    	  function testFun() {
              quota = $("#quotas").val();
              alert(quota);
          }
          
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable({ "order": [[ 0, "desc" ]] , "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]] , "pageLength": 50});

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        TableManageButtons.init();
      });
    </script>
    
    <!-- /compose -->
  </body>
</html>