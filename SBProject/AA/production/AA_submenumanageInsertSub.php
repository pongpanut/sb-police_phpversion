<?php
use SB\model\submenuBOM;
use SB\conn\factory\App_DaoFactory;
include '../model/submenuBOM.php';
include_once '../conn/factory/factory.php';

$menuText = $_POST['menuText'];
$dropdown = $_POST['dropdown'];
$seq = $_POST['seq'];
$menuId = $_POST['menuId'];
$pageType = $_POST['pageType'];
$hasSubmenu = $_POST['hasSubmenu'];

if($menuText!=""){
	$submenu = new submenuBOM();
	$submenu->setSubMenuText($menuText);
	$submenu->setPageTypeId($pageType);
	$submenu->setSubMenuLink($dropdown);
	$submenu->setMenuId($menuId);
	$submenu->setSubMenuSeq($seq);
	$submenu->setSubmenuHasMenuList($hasSubmenu);	
	 
	$menuDao = App_DaoFactory::getFactory()->getMenu();
	$msg = $menuDao->insertSubMenu($submenu);	 
}
?>