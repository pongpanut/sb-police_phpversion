<?php
use SB\model\menuM;
use SB\conn\factory\App_DaoFactory;
include '../model/menuM.php';
include_once '../conn/factory/factory.php';

$menuText = $_POST['menuText'];
$dropdown = $_POST['dropdown'];
$seq = $_POST['seq'];
$pageType = $_POST['pageType'];
$hasSubmenu = $_POST['hasSubmenu'];

if($menuText!=""){
	$menu = new menuM();
	$menu->setMenuText($menuText);
	$menu->setMenuPageType($pageType);
	$menu->setMenuLink($dropdown);
	$menu->setMenuHasSubmenu($hasSubmenu);
	$menu->setMenuSeq($seq);
	
	 
	$menuDao = App_DaoFactory::getFactory()->getMenu();
	$msg = $menuDao->insertMenu($menu);	 
}
?>