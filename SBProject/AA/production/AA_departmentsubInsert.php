<?php
use SB\model\departmentSubBOM;
use SB\conn\factory\App_DaoFactory;
include '../model/departmentSubBOM.php';
include_once '../conn/factory/factory.php';

$menuText = $_POST['text'];
$dropdown = $_POST['dropdown'];
$seq = $_POST['seq'];
$departmentId = $_POST['departmentId'];
$pageType = $_POST['pageType'];


if($menuText!=""){
	$departmentSubBOM = new departmentSubBOM();
	$departmentSubBOM->setDeptSubText($menuText);
	$departmentSubBOM->setDeptSubLink($dropdown);
	$departmentSubBOM->setSeq($seq);
	$departmentSubBOM->setDeptId($departmentId);
	$departmentSubBOM->setPageTypeId($pageType);
	 
	$deptDao = App_DaoFactory::getFactory()->getDepartmentDao();
	$msg = $deptDao->insertDepartmentSub($departmentSubBOM);	 
}
?>