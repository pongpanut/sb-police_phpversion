<?php namespace app\page;
use SB\Controller\Controller;
include '../Controller/Controller.php';
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
include_once 'AA_NosessionRedirect.php';
@session_start ();
$menuDao =App_DaoFactory::getFactory()->getMenu();
$staticPageDao =App_DaoFactory::getFactory()->getStaticPage2();
$menu =$menuDao->loadMenu(1);
$staticPage = $staticPageDao->loadStaticpage(1);
$statpagenum= count($staticPage);
$num = count($menu);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ส่วนการจัดการหลังบ้าน</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    
 
    <script type='text/javascript' src='../src/js/jquery-2.2.3.min.js'></script>
    <script type='text/javascript' src='../src/js/backoffice.js'></script>
    
    
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <link href="../src/scss/backoffice.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      	<?php
		 include 'AA_sidebar.php';
		?>
<div id="load_screen"><div id="loading" ><img alt="" src="../../images/image_997636.gif"></div></div>
           <?php
		 include 'nav.php';
		?>
            <!-- /page content -->
    
<!--Modal-->

        <!-- page content -->
        <div class="right_col" role="main" style="height:1088;min-height: 1088px;">
          <div class="">
            <div class="page-title">
                <h3>ส่วนการจัดการเปลี่ยน password </h3>
               
              </div>
            </div>
            <div class="clearfix"></div>  
              <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;">
                <div class="x_panel">
                  <div class="x_title">
                    <div class="clearfix"></div>
                  </div> 
               
                <form class="form-horizontal form-label-left">
                <div class="form-group">
                    <label class="control-label col-md-3" for="first-name">ชื่อผู้เข้าใช้ <span class="required">*</span>
                    </label>
                    <div class="col-md-2">
                      <label class="control-label col-md-3" for="first-name" style="float: left;font-size: 20px; margin-top: -8px;"><?php echo $_SESSION['username']?></label>
                      <input type="hidden" id="hiddenuser" value="<?php echo $_SESSION['username']?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" pattern="[a-zA-Z0-9!@#$%^*_|]{6,25}" for="first-name">รหัสผ่านปัจจุบัน <span class="required">*</span>
                    </label>
                    <div class="col-md-7">
                      <input type="text" id="currpass" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
            		<div class="form-group">
                    <label class="control-label col-md-3" pattern="[a-zA-Z0-9!@#$%^*_|]{6,25}" for="first-name">รหัสผ่านใหม่ <span class="required">*</span>
                    </label>
                    <div class="col-md-7">
                      <input type="text" id="newpass" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3"  pattern="[a-zA-Z0-9!@#$%^*_|]{6,25}" for="first-name">ยืนยันรหัสผ่านใหม่ <span class="required">*</span>
                    </label>
                    <div class="col-md-7">
                      <input type="text" id="confirmnewpass" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
                      <div class="form-group">
	                    <label class="control-label col-md-3" for="Changpassbutton">
	                    </label>
	                    <div class="col-md-7">
	                      <button id="Changpassbutton" type="button" class="center-block btn btn-primary" style="margin-left: 0;"> เปลี่ยนรหัสผ่าน</button>
	                    </div>
	                  </div>
                </form>
                </div>
                </div>    
          </div>
        </div>
    

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->




    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script type='text/javascript' src='../src/js/jquery.dataTables.js'></script>
    <!-- <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
    <script src="../vendors/datatables.net/js/jquery.dataTables.js"></script> -->
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script type='text/javascript' src='../src/js/knockout-3.4.0.js'></script>
	<script type="text/javascript" src='../src/js/ModelbulderForStaticpage.js'></script>
	    <script type='text/javascript' src='../src/js/bootbox.min.js'></script>

    <!-- /Datatables -->
  </body>
</html>