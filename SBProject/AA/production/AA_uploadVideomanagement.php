<?php namespace app\page;
use SB\Controller\Controller;
include '../Controller/Controller.php';
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
include_once 'AA_NosessionRedirect.php';
$page_type_id=$_SESSION['privillege'];
$videoDao =App_DaoFactory::getFactory()->getVideoDao();
$videoArr =$videoDao->loadVideo($page_type_id);
$num = count($videoArr);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ส่วนการจัดการหลังบ้าน</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    
 
  <script type='text/javascript' src='../src/js/jquery-2.1.1.min.js'></script>
 	<script type='text/javascript' src='../src/js/jquery.form.js'></script>
 	<script type='text/javascript' src='../src/js/backoffice.js'></script>
	<link href="../src/scss/backoffice.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      	<?php
		 include 'AA_sidebar.php';
		?>
<div id="load_screen"><div id="loading" ><img alt="" src="../../images/image_997636.gif"></div></div>
        <!-- top navigation -->
             <?php
		 include 'nav.php';
		?>
        <!-- /top navigation -->
            <!-- /page content -->
    
<!--Modal-->

        <!-- page content -->
        <div class="right_col" role="main" style="height:1088;min-height: 1088px;">
          <div class="">
            <div class="page-title">
                <h3>การจัดการวิดิโอ </h3>
               
              </div>
            </div>
            <div class="clearfix"></div>  
              <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;">
                <div class="x_panel">
                  <div class="x_title">
                    <div class="clearfix"></div>
                  </div> 
               <h4>เพิ่มข้อมูล</h4>

           <form  novalidate="novalidate" class=" form-horizontal form-label-left"  method="POST" enctype="multipart/form-data"  id="uploadForm" accept-charset="UTF-8">
			 <input type="hidden" name="pageTypeId" id="pageTypeId" value="<?php echo $page_type_id?>">
							<input type="hidden" name="mode" id="mode" value="add">
							<div class="form-group">
			                    <label class="control-label col-md-3" for="videoNameAdd">ชื่อวีดีโอ  :<span class="required">*</span>
			                    </label>
			                    <div class="col-md-7">
			                      <input type="text" id="videoNameAdd" name ="videoNameAdd" required="required" class="form-control col-md-7 col-xs-12">
			                    </div>
			                  </div>
			                  <div class="form-group">
			                    <label class="control-label col-md-3" for="videoPathAdd">Upload Video File:<span class="required">*</span>
			                    </label>
			                    <div class="col-md-7">
			                      <input name="userImage" id="userImage" type="file" class="demoInputBox" accept="video/*"/>
			                      <div id="progress-div"><div id="progress-bar"></div></div>
								  <div id="targetLayer"></div>
			                    </div>
			                  </div>
			                  <div class="form-group">
			                     <label class="control-label col-md-3" for="status">สถานะ :</label>
			                     <div class="col-md-7">
			                          <select id="status" class="form-control" required>
										<option selected="selected" value="Y">แสดง</option>
										<option  value="N">ไม่แสดง</option>
			                          </select>
			                          </div>
			                   </div>
							<div class="col-md-7 col-md-offset-6 ">
								<button type="submit" class="btn btn-primary" id="addVideoBtn">เพิ่มข้อมูล</button>
							</div>
						</form>
				
                  <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                        <th>รหัส</th>
                          <th>ชื่อวีดีโอ</th>
                          <th>path</th>
                           <th>สถานะ</th>
                          <th>แก้ไข</th>
                          <th>ลบ</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if($num >0){?>
                           <?php for ($j=0;$j<$num;$j++){?>
	                        <tr id="<?php echo $videoArr[$j]->getVideoId();?>">
	                         <td><?php echo $videoArr[$j]->getVideoId() ?></td>
	                          <td><?php echo $videoArr[$j]->getVideoName() ?></td>
	                           <td><?php echo $videoArr[$j]->getVideoPath() ?></td>
	                          <td><?php echo $videoArr[$j]->getStatus()=='Y'?"แสดง":"ไม่แสดง";?></td>
	                         
	                          <td>
							  	<button type="button" id="buttonId" class="btn btn-default" aria-label="Left Align"  data-toggle="modal" data-target="#myModal<?php echo $videoArr[$j]->getVideoId();?>">
								  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</button>
							  </td>
	                          <td>
	                          <button type="button" id="buttonRemoveVideo" class="btn btn-default" aria-label="Left Align" value="<?php echo $videoArr[$j]->getVideoId();?>">
								  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								</button>
	                          	
								</td>
	                        </tr>
                       	   <?php }?>
                      <?php }?>
                       
                        
                      </tbody>
                    </table>
                    
                      <!-- Modal -->
 <?php if($num >0){?>
 	<?php for ($j=0;$j<$num;$j++){?>
 	<style>
	#videoForm<?php echo $videoArr[$j]->getVideoId();?> {border-top:#F0F0F0 2px solid;background:#FAF8F8;padding:10px;}
	#videoForm<?php echo $videoArr[$j]->getVideoId();?> label {margin:2px; font-size:1em; font-weight:bold;}
	.demoInputBox<?php echo $videoArr[$j]->getVideoId();?>{padding:5px; border:#F0F0F0 1px solid; border-radius:4px; background-color:#FFF;}
	#progress-bar<?php echo $videoArr[$j]->getVideoId();?> {background-color: #12CC1A;height:20px;color: #FFFFFF;width:0%;-webkit-transition: width .3s;-moz-transition: width .3s;transition: width .3s;}
	.btnSubmit<?php echo $videoArr[$j]->getVideoId();?>{background-color:#09f;border:0;padding:10px 40px;color:#FFF;border:#F0F0F0 1px solid; border-radius:4px;}
	#progress-div<?php echo $videoArr[$j]->getVideoId();?> {border:#0FA015 1px solid;padding: 5px 0px;margin:30px 0px;border-radius:4px;text-align:center;}
	#targetLayer<?php echo $videoArr[$j]->getVideoId();?>{width:100%;text-align:center;}
</style>
<div id="myModal<?php echo $videoArr[$j]->getVideoId();?>" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="max-height: 1024px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">แก้ไขส่วน : "<?php echo $videoArr[$j]->getVideoName()?>"</h4>
      </div>
      <div class="modal-body">
				<form  novalidate="novalidate" class=" form-horizontal form-label-left"  method="POST"  id="videoForm<?php echo $videoArr[$j]->getVideoId();?>" accept-charset="UTF-8" value="<?php echo $videoArr[$j]->getVideoId() ?>">
                  <input type="hidden" value="<?php echo $videoArr[$j]->getPageTypeId()?>" id="PageTypeId">
                  <input type="hidden" value="edit" id="mode">
                   
                  <div class="form-group">
                    <label class="control-label col-md-3" for="videoName<?php echo $videoArr[$j]->getVideoId();?>">ชื่อวีดีโอ  :<span class="required">*</span>
                    </label>
                    <div class="col-md-7">
                      <input type="text" id="videoName<?php echo $videoArr[$j]->getVideoId();?>" name ="videoName<?php echo $videoArr[$j]->getVideoId();?>" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $videoArr[$j]->getVideoName() ?>">
                    </div>
                  </div>
                  <div class="form-group">
			                    <label class="control-label col-md-3" for="videoPath<?php echo $videoArr[$j]->getVideoId();?>">Upload Video File:<span class="required">*</span>
			                    </label>
			                    <div class="col-md-7">
			                      <input name="userImage<?php echo $videoArr[$j]->getVideoId();?>" id="userImage<?php echo $videoArr[$j]->getVideoId();?>" type="file" class="demoInputBox" accept="video/*" value/>
			                      <div id="progress-div<?php echo $videoArr[$j]->getVideoId();?>"><div id="progress-bar<?php echo $videoArr[$j]->getVideoId();?>"></div></div>
								  <div id="targetLayer"></div>
			                    </div>
			                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3" for="status<?php echo $videoArr[$j]->getVideoId();?>">สถานะ :</label>
                     <div class="col-md-7">
                          <select id="status<?php echo $videoArr[$j]->getVideoId();?>" class="form-control" required>
                         
                          	<option value="0">Choose..</option>
                          	 	<?php if($videoArr[$j]->getStatus() =="Y" ){?>
									 <option selected="selected" value="Y">แสดง</option>
									 <option  value="N">ไม่แสดง</option>
								<?php } else {?>
									 <option  value="Y">แสกง</option>
									 <option selected="selected" value="N">ไม่แสดง</option>
								<?php } ?>
                           	
                          </select>
                          </div>
                   </div>
					<div class="col-md-7 col-md-offset-6">
						<button type="button" form="videoForm<?php echo $videoArr[$j]->getVideoId();?>" data-target="#videoForm<?php echo $videoArr[$j]->getVideoId();?>" id="submitVideoEdit" class="btn btn-primary"  value="<?php echo $videoArr[$j]->getVideoId();?>" >แก้ไขข้อมูล</button>
					</div>
                </form>
      </div>
      <div class="modal-footer">
		
      </div>
    </div>

  </div>
</div>
   <?php }?>
<?php }?>
                  

<div id="confirm" class="modal hide fade">
  <div class="modal-body">
    Are you sure?
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
  </div>
</div>
<!-- End modal -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->




    <!-- jQuery -->
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script type='text/javascript' src='../src/js/jquery.dataTables.js'></script>
    <!-- <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
    <script src="../vendors/datatables.net/js/jquery.dataTables.js"></script> -->
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script type='text/javascript' src='../src/js/knockout-3.4.0.js'></script>
	<script type="text/javascript" src='../src/js/ModelbulderForStaticpage.js'></script>
	    <script type='text/javascript' src='../src/js/bootbox.min.js'></script>
    <!-- Datatables -->
    <script>
      $(document).ready(function() {
          
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable({ "order": [[ 0, "desc" ]] , "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]] , "pageLength": 50});

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
  </body>
</html>