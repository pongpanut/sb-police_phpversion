<?php
use SB\conn\factory\App_DaoFactory;
use SB\model\submenulistBOM;
include '../model/submenulistBOM.php';
include_once '../conn/factory/factory.php';

$menuText = $_POST['menuText'];
$dropdown = $_POST['dropdown'];
$seq = $_POST['seq'];
$menuId = $_POST['menuId'];
$pageType = $_POST['pageType'];

if($menuText!=""){
	$submenulist = new submenulistBOM();
	$submenulist->setSubmenuListText($menuText);
	$submenulist->setPageTypeId($pageType);
	$submenulist->setSubmenuListLink($dropdown);
	$submenulist->setSubmenuId($menuId);
	$submenulist->setSubmenuListSeq($seq);
	 
	$menuDao = App_DaoFactory::getFactory()->getMenu();
	$msg = $menuDao->insertSubMenuList($submenulist);	 
}
?>