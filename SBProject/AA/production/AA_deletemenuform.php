<?php
use SB\model\menuM;
use SB\conn\factory\App_DaoFactory;
include '../model/menuM.php';
include_once '../conn/factory/factory.php';

$menuId = $_POST['id']; 

if($menuId!=""){
	$menu = new menuM();
	$menu->setMenuId($menuId);		 
	$menuDao = App_DaoFactory::getFactory()->getMenu(); 
	$listChildSubMenu= $menuDao->getChildSubmenuID($menuId);
	$msg = $menuDao->deleteMenu($menu);
	$countSub = count($listChildSubMenu);

	if($countSub > 0 ){
		for ($x=0;$x<$countSub;$x++){
			$listChildSubMenuList = $menuDao->getChildSubmenuListID($listChildSubMenu[$x]->getSubMenuId());
			$msg = $menuDao->deleteSubMenu($listChildSubMenu[$x]);
			$countSubList = count($listChildSubMenu);
			if($countSubList > 0 ){
				for ($y=0;$y<$countSubList;$y++){
					$msg = $menuDao->deleteSubMenuList($listChildSubMenuList[$y]);
				}
			}		
		}
	}
}
?>