<?php namespace app\page;
use SB\Controller\Controller;
include '../Controller/Controller.php';
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
include_once 'AA_NosessionRedirect.php';
$page_type_id=$_SESSION['privillege'];
$imageDao =App_DaoFactory::getFactory()->getImageDao();
$imageArr =$imageDao->loadImg($page_type_id,"'main'");
$num = count($imageArr);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ส่วนการจัดการหลังบ้าน</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    
 
    <script type='text/javascript' src='../src/js/jquery-2.2.3.min.js'></script>
    <script type='text/javascript' src='../src/js/backoffice.js'></script>
    <script type="text/javascript" src="../tinymce/tinymce.min.js" ></script>
    <script type='text/javascript' src='../src/js/upload.js'></script>
      <link href="../src/scss/backoffice.css" rel="stylesheet">
    <script type='text/javascript' src='../src/js/bootstrap-filestyle.min.js'></script>
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <link href="../src/scss/backoffice.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      	<?php
		 include 'AA_sidebar.php';
		?>
<div id="load_screen"><div id="loading" ><img alt="" src="../../images/image_997636.gif"></div></div>
        <!-- top navigation -->
           <?php
		 include 'nav.php';
		?>
        <!-- /top navigation -->
            <!-- /page content -->

        <!-- page content -->
        <div class="right_col" role="main" style="height:1088;min-height: 1088px;">
          <div class="">
            <div class="page-title">
                <h3>ส่วนการจัดการ  แบนเนอร์</h3>
               
              </div>
            </div>
            <div class="clearfix"></div>  
              <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;">
                <div class="x_panel">
                  <div class="x_title">
                    <div class="clearfix"></div>
                  </div> 
               <h4>เพิ่มข้อมูล</h4>

               <form  id="imageAddForm"  novalidate="novalidate" class=" form-horizontal form-label-left" enctype="multipart/form-data" method="POST"  accept-charset="UTF-8" >
                  <input type="hidden" value="add" name="id">
                   <input type="hidden" value="<?php echo $page_type_id?>" name="pageTypeAdd">
                    <input type="hidden" value="add" name="mode">
                  	<input type="hidden" value="main" name="imageTypeAdd"> 
                   <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="header-title">อัพโหลดรูปภาพ : <span class="required">*</span>
                        </label>
                        <div class="col-md-6 ">  
				            <!-- image-preview-filename input [CUT FROM HERE]-->
				            <div class="input-group image-preview">
				                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
				                <span class="input-group-btn">
				                    <!-- image-preview-clear button -->
				                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
				                        <span class="glyphicon glyphicon-remove"></span> Clear
				                    </button>
				                    <!-- image-preview-input -->
				                    <div class="btn btn-default image-preview-input">
				                        <span class="glyphicon glyphicon-folder-open"></span>
				                        <span class="image-preview-input-title">Browse</span>
				                        <input type="file" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint , application/pdf, image/*" name="file" id="imageAttach"> <!-- rename it -->
				                    </div>
				                </span>
				            </div><!-- /input-group image-preview [TO HERE]--> 
				        </div>
                      </div>

					<div class="col-md-7 col-md-offset-6 ">
					
						<button type="submit" class="btn btn-primary">เพิ่มข้อมูล</button>
					</div>
                </form>
                  <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                        <th>รหัส</th>
                          <th>path</th>
                          <th>แก้ไข</th>
                          <th>ลบ</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if($num >0){?>
                           <?php for ($j=0;$j<$num;$j++){?>
	                        <tr id="<?php echo $imageArr[$j]->getImgid();?>">   
	                         <td><?php echo $imageArr[$j]->getImgid();?></td>
	                           <td><?php echo $imageArr[$j]->getImgpath();?></td>
	                          <td>
							  	<button type="button" id="buttonId" class="btn btn-default" aria-label="Left Align"  data-toggle="modal" data-target="#myModal<?php echo $imageArr[$j]->getImgid();?>">
								  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</button>
							  </td>
	                          <td>
	                          	<button type="button" id="buttonRemoveImage" class="btn btn-default" value="<?php echo $imageArr[$j]->getImgid();?>">
								  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								</button>
								</td>
	                        </tr>
                       	   <?php }?>
                      <?php }?>
                       
                        
                      </tbody>
                    </table>
                    
                      <!-- Modal -->
 <?php if($num >0){?>
 	<?php for ($j=0;$j<$num;$j++){?>
<div id="myModal<?php echo $imageArr[$j]->getImgid();?>" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="max-height: 1024px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ส่วนปรับรูป : "<?php echo $imageArr[$j]->getImgname()?>"</h4>
      </div>
      <div class="modal-body">
				<form  novalidate="novalidate" class=" form-horizontal form-label-left" enctype="multipart/form-data" method="POST"  accept-charset="UTF-8" id="imageEditForm<?php echo $imageArr[$j]->getImgid();?>" >
                  <input type="hidden" value="<?php echo $imageArr[$j]->getImgid();?>" name="id">
                   <input type="hidden" value="<?php echo $page_type_id?>" name="pageType<?php echo $imageArr[$j]->getImgid();?>">
                    <input type="hidden" value="edit" name="mode">     
                    <input type="hidden" value="main" name="imageType<?php echo $imageArr[$j]->getImgid();?>"> 
                   <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="header-title">อัพโหลดรูปภาพ : <span class="required">*</span>
                        </label>
                        <div class="col-md-6 ">  
				            <!-- image-preview-filename input [CUT FROM HERE]-->
				            <div class="input-group image-preview1">
				                <input type="text" class="form-control image-preview-filename1" disabled="disabled" id="imagepath<?php echo $imageArr[$j]->getImgid();?>"> <!-- don't give a name === doesn't send on POST/GET -->
				                <span class="input-group-btn">
				                    <!-- image-preview-clear button -->
				                    <button type="button" class="btn btn-default image-preview-clear1" style="display:none;">
				                        <span class="glyphicon glyphicon-remove"></span> Clear
				                    </button>
				                    <!-- image-preview-input -->
				                    <div class="btn btn-default image-preview-input1">
				                        <span class="glyphicon glyphicon-folder-open"></span>
				                        <span class="image-preview-input-title1">Browse</span>
				                        <input type="file" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint , application/pdf, image/*" name="file" id="imageAttach"> <!-- rename it -->
				                    </div>
				                </span>
				            </div><!-- /input-group image-preview [TO HERE]--> 
				        </div>
                      </div>
                     <div class="form-group">
                     <label class="control-label col-md-3" for="imagePreview<?php echo $imageArr[$j]->getImgid();?>">current Image :</label>
                     <div class="col-md-7">
                      <img src="../../<?php echo $imageArr[$j]->getImgpath();?>" class="img-responsive" id="imagePreview<?php echo $imageArr[$j]->getImgid();?>" name="imagePreview<?php echo $imageArr[$j]->getImgid();?>">
                       </div>
                   </div>
                      
                      <script>
                      $(document).ready(function() { 
                          $("#imagepath<?php echo $imageArr[$j]->getImgid();?>").val("<?php echo $imageArr[$j]->getImgpath();?>");
                      });
                      </script>

					<div class="col-md-7 col-md-offset-6 ">
					
						<button type="submit" class="btn btn-primary" id="imageUploadForm" value="<?php echo $imageArr[$j]->getImgid();?>" >เพิ่มข้อมูล</button>
					</div>
                </form>
      </div>
      <div class="modal-footer">
		
      </div>
    </div>

  </div>
</div>
   <?php }?>
<?php }?>
                  

<div id="confirm" class="modal hide fade">
  <div class="modal-body">
    Are you sure?
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
  </div>
</div>
<!-- End modal -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->




    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script type='text/javascript' src='../src/js/jquery.dataTables.js'></script>
    <!-- <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
    <script src="../vendors/datatables.net/js/jquery.dataTables.js"></script> -->
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script type='text/javascript' src='../src/js/knockout-3.4.0.js'></script>
	<script type="text/javascript" src='../src/js/ModelbulderForStaticpage.js'></script>
	    <script type='text/javascript' src='../src/js/bootbox.min.js'></script>
    <!-- Datatables -->
    <script>
      $(document).ready(function() {
          
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable({ "order": [[ 0, "desc" ]] , "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]] , "pageLength": 50});

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
  </body>
</html>