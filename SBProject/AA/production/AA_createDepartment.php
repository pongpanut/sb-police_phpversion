<?php
use SB\model\departmentBOM;
use SB\model\bgM;
use SB\model\tab_activityM;
use SB\conn\factory\App_DaoFactory;
$ini_array = parse_ini_file("sbpolice.ini");
include '../model/departmentBOM.php';
include_once '../conn/factory/factory.php';

$newDeptname = $_POST['deptText'];
$symText = $_POST['symText'];
$pageType = $_SESSION['privillege'];


//Get latest index department id
$departmentDao =App_DaoFactory::getFactory()->getDepartmentDao();
$curDepartmentId =$departmentDao->loadLatestDepartment();
$newDepartmentId = $curDepartmentId+1;
//echo 'id : '.$newDepartmentId;

//Add url mapping
$sql = "INSERT INTO pagetype_mapping(page_type_id, page_type_text) VALUES ('$newDepartmentId','$symText')";
$test = $departmentDao->insertDepartment($sql);

//new Department link
$newDepltLink = $ini_array['contextRoot'].$symText.'/index.php';


/*
 * 1. Insert statement for department table
 */

$sql = "INSERT INTO department(dept_id, dept_text, dept_link, page_type_id) VALUES ('$newDepartmentId','$newDeptname','$newDepltLink','$pageType')";
$test = $departmentDao->insertDepartment($sql);
$column ='dept_id';
$table= 'department';
$MaxId =  $departmentDao->getMaxId($column,$table);


/*
 * 2. Insert statement for static page
 */
//Generate link

 $history = $ini_array['contextRoot'].$symText.'/history.php';
 $plan =$ini_array['contextRoot'].$symText.'/planstructure.php';
 //$Commander_Old  = 'sb'.$newDepartmentId.'/comandor-old.php';
 $Commander_Current  =$ini_array['contextRoot'].$symText.'/comandor-current.php';
 $vision = $ini_array['contextRoot'].$symText.'/vision.php';
 $calendar = $ini_array['contextRoot'].$symText.'/calendar1.php';
//Finish generate dink

 //insert sql
 $column ='id';
 $table = 'staticpage';
$insertStaticHistoryRec = "INSERT INTO staticpage(id, text, link, is_active, page_type_id, html) VALUES ('', 'ประวัติ','$history','1','$newDepartmentId','')";
$test = $departmentDao->insertDepartment($insertStaticHistoryRec);
$MaxStaticHistoryRec =  $departmentDao->getMaxId($column,$table);
//echo " MaxStaticHistoryRec : ".$MaxStaticHistoryRec;

//$insertStaticPlanRec = "INSERT INTO staticpage(id, text, link, is_active, page_type_id, html) VALUES ('', 'โครงสร้างองค์กร','$plan','1','$newDepartmentId','')";
//$test = $departmentDao->insertDepartment($insertStaticPlanRec);
//$MaxStaticPlanRec =  $departmentDao->getMaxId($column,$table);
//echo " MaxStaticPlanRec : ".$MaxStaticPlanRec;

$insertCalendar = "INSERT INTO staticpage(id, text, link, is_active, page_type_id, html) VALUES ('', 'ปฏิทินกิจกกรม','$calendar','1','$newDepartmentId','')";
$test = $departmentDao->insertDepartment($insertCalendar);
$MaxCarlendarRec =  $departmentDao->getMaxId($column,$table);
//echo " MaxStaticComOldRec : ".$MaxStaticComOldRec;


$insertStaticComCurrRec = "INSERT INTO staticpage(id, text, link, is_active, page_type_id, html) VALUES ('', 'ทำเนียบ ผู้บริหาร','$Commander_Current','1','$newDepartmentId','')";
$test = $departmentDao->insertDepartment($insertStaticComCurrRec);
$MaxStaticComCurrRec =  $departmentDao->getMaxId($column,$table);
//echo " MaxStaticComCurrRec : ".$MaxStaticComCurrRec;

$insertStaticVisionRec = "INSERT INTO staticpage(id, text, link, is_active, page_type_id, html) VALUES ('', 'วิสัยทัศน์ / พันธกิจ','$vision','1','$newDepartmentId','')";
$test = $departmentDao->insertDepartment($insertStaticVisionRec);
$MaxStaticVisionRec =  $departmentDao->getMaxId($column,$table);
//echo " MaxStaticVisionRec : ".$MaxStaticVisionRec;


/*
 * 3. Insert menu table
 */

$column ='menu_id';
$table = 'menu';
$insertMenuAboutRec = "INSERT INTO menu(menu_id, menu_text, menu_page_type, menu_link, has_submenu, is_department, seq) VALUES ('','เกี่ยวกับ','$newDepartmentId', '','1','0','0')";
$test = $departmentDao->insertDepartment($insertMenuAboutRec);
$MaxHistoryRec =  $departmentDao->getMaxId($column,$table);
//echo " MaxHistoryRec : ".$MaxHistoryRec;

$insertDeptAboutRec = "INSERT INTO menu(menu_id, menu_text, menu_page_type, menu_link, has_submenu, is_department, seq) VALUES ('','หน่วยงานในสังกัด','$newDepartmentId', '','1','1','0')";
$test = $departmentDao->insertDepartment($insertDeptAboutRec);
$MaxDeptAboutRec =  $departmentDao->getMaxId($column,$table);
//echo " MaxDeptAboutRec : ".$MaxDeptAboutRec;

$insertManageAboutRec = "INSERT INTO menu(menu_id, menu_text, menu_page_type, menu_link, has_submenu, is_department, seq) VALUES ('','ผู้บริหาร','$newDepartmentId', '','1','0','0')";
$test = $departmentDao->insertDepartment($insertManageAboutRec);
$MaxManageAboutRec =  $departmentDao->getMaxId($column,$table);
//echo " MaxManageAboutRe : ".$MaxManageAboutRec;

$insertMissionAboutRec = "INSERT INTO menu(menu_id, menu_text, menu_page_type, menu_link, has_submenu, is_department, seq) VALUES ('','ยุทธศาสตร์','$newDepartmentId', '','1','0','0')";
$test = $departmentDao->insertDepartment($insertMissionAboutRec);
$MaxMissionAboutRec =  $departmentDao->getMaxId($column,$table);

$insertMissionAboutRec = "INSERT INTO menu(menu_id, menu_text, menu_page_type, menu_link, has_submenu, is_department, seq) VALUES ('','ปฏิทินกิจกรรม','$newDepartmentId', '$MaxCarlendarRec','0','0','0')";
$test = $departmentDao->insertDepartment($insertMissionAboutRec);
$MaxMissionAboutRec =  $departmentDao->getMaxId($column,$table);
//echo " MaxMissionAboutRec : ".$MaxMissionAboutRec;

/*
 * 4. Insert Sub menu table
 */

$insertAboutSubmenu = "INSERT INTO submenu(submenu_id,submenu_text, submenu_link, submenu_has_menulist, menu_id, page_type_id, is_department, seq) VALUES ('','ประวัติ','$MaxStaticHistoryRec','0','$MaxHistoryRec','$newDepartmentId','0','1')";
$test = $departmentDao->insertDepartment($insertAboutSubmenu);

//$insertPlanSubmenu = "INSERT INTO submenu(submenu_id,submenu_text, submenu_link, submenu_has_menulist, menu_id, page_type_id, is_department, seq) VALUES ('','โครงสร้างองค์กร','$MaxStaticPlanRec','0','$MaxHistoryRec','$newDepartmentId','0','1')";
//$test = $departmentDao->insertDepartment($insertPlanSubmenu);

//$insertComOldRecSubmenu = "INSERT INTO submenu(submenu_id,submenu_text, submenu_link, submenu_has_menulist, menu_id, page_type_id, is_department, seq) VALUES ('','อดีตผู้บริหาร','$MaxStaticComOldRec','0','$MaxManageAboutRec','$newDepartmentId','0','1')";
//$test = $departmentDao->insertDepartment($insertComOldRecSubmenu);

$insertComCurrRecSubmenu = "INSERT INTO submenu(submenu_id,submenu_text, submenu_link, submenu_has_menulist, menu_id, page_type_id, is_department, seq) VALUES ('','ผู้บริหารปัจจุบัน','$MaxStaticComCurrRec','0','$MaxManageAboutRec','$newDepartmentId','0','1')";
$test = $departmentDao->insertDepartment($insertComCurrRecSubmenu);

$insertVisionRecSubmenu = "INSERT INTO submenu(submenu_id,submenu_text, submenu_link, submenu_has_menulist, menu_id, page_type_id, is_department, seq) VALUES ('','วิสัยทัศน์ / พันธกิจ','$MaxStaticVisionRec','0','$MaxMissionAboutRec','$newDepartmentId','0','1')";
$test = $departmentDao->insertDepartment($insertVisionRecSubmenu);

/*
 *  5. Insert username and password for admin
 */

$adminuser = 'root'.$newDepartmentId;
$pass = '1234';
$hash = password_hash($pass, PASSWORD_DEFAULT, ['cost' => 12]);
$newpagetype = $newDepartmentId;
$currid = $pageType;

$test = $departmentDao->InsertnewAdmin($adminuser, $hash, $newpagetype, $newpagetype);

/*
 *  6. Update new index value for new department
 */


//$file = '../../template.php';
//$newfile = '../../pt'.$newDepartmentId.'_index.php';

//if (!copy($file, $newfile)) {
//	echo "failed to copy";
//}
//else{
//	echo "Create new department successed"."\r\n";
//}

/*
 * 7. Update index_dept table
 */

$test = $departmentDao->UpdateLatestDepartmentIndex($newpagetype);

/*
 * 8. Write htaccess file 
 */

	$fp = fopen('../../.htaccess','a+');
	echo $fp;
	if($fp)
	{
		fwrite($fp,'RewriteRule ^'.$newDeptname.'/ index.php?pagetype='.$newDeptname.''.PHP_EOL.'');
		fwrite($fp,'RewriteRule ^'.$newDeptname.' index.php?pagetype='.$newDeptname.''.PHP_EOL.'');
		fclose($fp);
	}


/// insert bg image


$bgDao =App_DaoFactory::getFactory()->getBgDao();
$BGM = new  bgM();
$BGM->setBgName("default");
$BGM->setBgLink("AA/production/uploads/bg/default_bg.png");
$BGM->setPageTypeId($newDepartmentId);
$bgDao->insertBg($BGM);

// create Tab

$TabDao =App_DaoFactory::getFactory()->getTabDao(); 
$tabM =   new tab_activityM();
$tabM->setTabId($newDepartmentId);
$tabM->setTabHeader($newDeptname);
$tabM->setTabCode($newDepartmentId);
$tabM->setPageTypeId($newDepartmentId);
$TabDao->insertTab($tabM);
echo 'Link is '.$_SERVER['HTTP_HOST'].'/SBProject/'.$symText.'/index.php'." \r\n";
echo "New admin username is : ".$adminuser." \r\n";
echo "Password is 1234";


?>