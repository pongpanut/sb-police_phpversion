<?php
use SB\model\menuM;
use SB\conn\factory\App_DaoFactory;
use SB\model\submenuBOM;
include '../model/submenuBOM.php';
include_once '../conn/factory/factory.php';


$id = $_POST['id'];
$menuText = $_POST['menuText'];
$dropdown = $_POST['dropdown'];
$seq = $_POST['seq'];
$pageType = $_POST['pageType'];
$hasSubmenu = $_POST['hasSubmenu'];

if($id!=""){
	$menu = new submenuBOM();
	$menu->setSubMenuId($id);
	$menu->setSubMenuText($menuText);
	$menu->setPageTypeId($pageType);
	$menu->setSubMenuLink($dropdown);
	$menu->setSubmenuHasMenuList($hasSubmenu);
	$menu->setSubMenuSeq($seq);

	 $menuDao = App_DaoFactory::getFactory()->getMenu();
	 $msg = $menuDao->updateSubMenu($menu);	 
}
?>