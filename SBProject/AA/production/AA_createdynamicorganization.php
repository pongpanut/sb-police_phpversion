<?php 
$arrValue = $_POST['arr'];
$page_type_id= $_POST['pagetype'];
$result = count($arrValue);
$count = 1 ;
?>
<hr>
<form id="dynamicOrg" enctype="multipart/form-data" method="POST"  accept-charset="UTF-8"  class="form-horizontal form-label-left">
  	 <input id="pagetype" name="pagetype" type="hidden" value="<?php echo $page_type_id?>" >
  	 <input id="rowcount" name="rowcount"  type="hidden" value="<?php echo array_sum($arrValue);?>" >
  	 
<?php for ($j=0;$j < $result;$j++){?>
	<div class="form-group">
		 	 <span class="control-label col-md-5" > </span>
		 	 <div class="col-md-5" style="display:block">
			    <span style="font-size: 20px;font-weight: 400;text-decoration: underline;">ชั้นที่ <?php echo $j+1?></span>
			 </div>
		 </div>

	
	<?php $sumpos = $arrValue[$j];
	for ($i=0;$i < $sumpos;$i++){?>
	 	<div class="form-group">
	 		 <input id="currrow<?php echo $count ?>" name="currrow<?php echo $count ?>" type="hidden" value="<?php echo $j+1 ?>" >
		 	 <span class="control-label col-md-1" >ชื่อตำแหน่ง : </span>
		 	 <div class="col-md-1" style="display:inline">
			 	<input type="text" id="poscomname<?php echo $count ?>" name="poscomname<?php echo $count ?>" required="required" max="5" class=" form-control col-md-12 col-xs-12">
			 </div>
			 <span class="control-label col-md-1" >ชื่อ : </span>
		 	 <div class="col-md-3" style="display:inline">
				 <input type="text" id="comname<?php echo $count ?>" name="comname<?php echo $count ?>" required="required" max="5" class="form-control col-md-12 col-xs-12">
			 </div>
			 <!-- image-preview-filename input [CUT FROM HERE]-->
             <span class="control-label col-md-1" >รูปภาพ : </span>
             <div class="col-md-3 image-preview<?php echo $count ?> " style="display:inline">
             	<input type="text" class="col-md-1 form-control image-preview-filename<?php echo $count ?>" disabled="disabled" id="imagecom<?php echo $count ?>" name="imagecom<?php echo $count ?>"  required="required" class="form-control col-md-12 col-xs-12">               
			 </div>
			 
                <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear<?php echo $count ?>" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input<?php echo $count ?>" style="position: relative;overflow: hidden; margin: 0px; color: #333; background-color: #fff; border-color: #ccc;">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title<?php echo $count ?>">Browse</span>
                        <input type="file" multiple accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/pdf, image/*" name="imageAttach<?php echo $count ?>" id="imageAttach" style="position: absolute;top: 0;right: 0;margin: 0;padding: 0;font-size: 20px;cursor: pointer;opacity: 0;filter: alpha(opacity=0);"/> <!-- rename it -->
                    </div>
                </span>
		</div>
		 <script>
		   // Create the preview image
		    $(".image-preview-input<?php echo $count ?> input:file").change(function (){     
		        var img = $('<img/>', {
		            id: 'dynamic',
		            width:250,
		            height:200
		        });      
		        var file = this.files[0];
		        var reader = new FileReader();
		        // Set preview image into the popover data-content
		        reader.onload = function (e) {
		            $(".image-preview-input-title<?php echo $count ?>").text("Change");
		            $(".image-preview-clear<?php echo $count ?>").show();
		            $(".image-preview-filename<?php echo $count ?>").val(file.name);            
		            img.attr('src', e.target.result);
		            $(".image-preview<?php echo $count ?>").attr("data-content",$(img)[0].outerHTML).popover("show");
		        }        
		        reader.readAsDataURL(file);
		    }); 
		    $('.image-preview-clear<?php echo $count ?>').click(function(){
		        $('.image-preview<?php echo $count ?>').attr("data-content","").popover('hide');
		        $('.image-preview-filename<?php echo $count ?>').val("");
		        $('.image-preview-clear<?php echo $count ?>').hide();
		        $('.image-preview-input<?php echo $count ?> input:file').val("");
		        $(".image-preview-input-title<?php echo $count ?>").text("Browse"); 
		    }); 
		    // Create the close button
		    var closebtn = $('<button/>', {
		        type:"button",
		        text: 'x',
		        id: 'close-preview',
		        style: 'font-size: initial;',
		    });
		    closebtn.attr("class","close pull-right");
		    // Set the popover default content
		    $('.image-preview<?php echo $count ?>').popover({
		        trigger:'manual',
		        html:true,
		        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
		        content: "There's no image",
		        placement:'bottom'
		    });
		    $(document).on('click', '#close-preview', function(){ 
		        $('.image-preview<?php echo $count ?>').popover('hide');
		        // Hover befor close the preview
		        $('.image-preview<?php echo $count ?>').hover(
		            function () {
		               $('.image-preview<?php echo $count ?>').popover('show');
		            }, 
		             function () {
		               $('.image-preview<?php echo $count ?>').popover('hide');
		            }
		        );    
		    });
		 </script>
		 
	<?php $count++; }?>
<?php }?>
	  	<div class="form-group">
	  	<span class="control-label col-md-3" > </span>
		<div class="col-md-2" style="display:inline">
		 <!-- id="createOrganization" --> 
	  		<button id="createOrganization" type="submit" class="center-block btn btn-primary" style="margin-left: 0;">สร้าง</button>
	  	</div>
	  	</div>
</form>
