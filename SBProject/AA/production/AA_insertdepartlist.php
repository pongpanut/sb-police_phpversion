insertDepartmentListButton<?php
use SB\model\departmentLiBOM;
use SB\conn\factory\App_DaoFactory;
include '../model/departmentLiBOM.php';
include_once '../conn/factory/factory.php';

$menuText = $_POST['menuText'];
$dropdown = $_POST['dropdown'];
$seq = $_POST['seq'];
$pageType = $_POST['pagetype'];


if($menuText!=""){
	$departmentLiBOM = new departmentLiBOM();
	$departmentLiBOM->setDeptListText($menuText);
	$departmentLiBOM->setPageTypeId($pageType);
	$departmentLiBOM->setDeptListLink($dropdown);
	$departmentLiBOM->setSeq($seq);
	 
	$departmentDao = App_DaoFactory::getFactory()->getDepartmentDao();
	$msg = $departmentDao->insertDepartmentList($departmentLiBOM);	 
}
?>