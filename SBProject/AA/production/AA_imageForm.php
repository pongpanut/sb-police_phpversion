<?php
use  SB\model\imageM;
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
include 'constant.php';
		$ds = "/uploads/images/"; 
		$mode=$_POST['mode'];
		$msg="";
		$imageDao = App_DaoFactory::getFactory()->getImageDao();
		$path="";
		$image  = new imageM();
		if (!empty($_FILES)) {
			
			$tempFile = $_FILES['file']['tmp_name'];
			$size = GetimageSize($tempFile);
			$ratio = $size[0]/$size[1]; // width/height
			if( $ratio > 1) {
				$width = 500;
				$height = 500/$ratio;
			}
			else {
				$width = 500*$ratio;
				$height = 500;
			}
			$src = imagecreatefromstring(file_get_contents($tempFile));
			$dst = imagecreatetruecolor($width,$height);
			imagecopyresampled($dst,$src,0,0,0,0,$width,$height,$size[0],$size[1]);
			imagedestroy($src);
			imagejpeg($dst,$tempFile); // adjust format as needed
			imagedestroy($dst);
			 
		    $targetPath = dirname( __FILE__ ) . $ds; 
		    $targetFile =  $targetPath. $_FILES['file']['name']; 
		    $removeHost = str_replace($_SERVER["DOCUMENT_ROOT"],'', str_replace('\\','/',$targetFile ));
		
		    move_uploaded_file($tempFile,$targetFile); //6
		    if($tempFile !=""){
		    	$path = str_replace($linkPrefix,'',$removeHost) ;
		    	$image->setImgpath($path);
		    }
		     
		} 
		     if($mode=="add"){
	
		     	isset($_POST['imageTypeAdd']) ? $imageTypeAdd = $_POST['imageTypeAdd'] : $imageTypeAdd = '';
		     	isset($_POST['imageHeaderAdd']) ? $imageHeaderAdd = $_POST['imageHeaderAdd'] : $imageHeaderAdd = '';
		     	isset($_POST['imageDescAdd']) ? $imageDescAdd = $_POST['imageDescAdd'] : $imageDescAdd = '';
		     	isset($_POST['pageTypeAdd']) ? $pageType = $_POST['pageTypeAdd'] : $pageType = '';
		     	isset($_POST['imageNameAdd']) ? $imageName = $_POST['imageNameAdd'] : $imageName = '';
		     	
		     	
		     /* 	$imageTypeAdd =$_POST['imageTypeAdd'];
		     	$imageHeaderAdd=$_POST['imageHeaderAdd'];
		     	$imageDescAdd=$_POST['imageDescAdd'];
		     	$pageType = $_POST['pageTypeAdd'];
		     	$imageName = $_POST['imageNameAdd']; */

		     	$image->setImgdesc($imageDescAdd);
		     	$image->setImgpath($path);
		     	$image->setImgtype($imageTypeAdd);
		     	$image->setPageTypeId($pageType);
		     	$image->setImgheader($imageHeaderAdd);
		     	$image->setImgname($imageDescAdd);
		     	$msg=$imageDao->InsertImage($image);
		     }
		     else if($mode=="edit"){
		     	
		     	isset($_POST['id']) ? $id = $_POST['id'] : $id = '';
		     	isset($_POST['imageType'.$id]) ? $imageType = $_POST['imageType'.$id] : $imageType = '';
		     	isset($_POST['imageHeader'.$id]) ? $imageHeader = $_POST['imageHeader'.$id] : $imageHeader = '';
		     	isset($_POST['imageDesc'.$id]) ? $imageDesc = $_POST['imageDesc'.$id] : $imageDesc = '';
		     	isset($_POST['pageType'.$id]) ? $pageType = $_POST['pageType'.$id] : $pageType = '';
		     	isset($_POST['imageName'.$id]) ? $imageName = $_POST['imageName'.$id] : $imageName = '';
		     	
		     	
		     	
		     	/* $id=$_POST['id'];
		     	$imageType =$_POST['imageType'.$id];
		     	$imageHeader=$_POST['imageHeader'.$id];
		     	$imageDesc=$_POST['imageDesc'.$id];
		     	$pageType = $_POST['pageType'.$id];
		     	$imageName = $_POST['imageName'.$id];  */
		     	$image->setImgid($id);
		     	$image->setImgdesc($imageDesc);
		     	$image->setImgtype($imageType);
		     	$image->setPageTypeId($pageType);
		     	$image->setImgheader($imageHeader);
		     	$image->setImgname($imageDesc); 
		     	$msg=$imageDao->UpdateImage($image);
		     	
		     }
		else if($mode=="delete"){
				 
				$id =$_POST['id'];
			 	$imageM=	$imageDao->loadImgById($id);
			 	if($imageM!=""){
			 		$pathDel =	$imageM->getImgpath();
			 		if($pathDel!=""){
			 			$host = str_replace('\\','/',dirname(__FILE__));
			 			$imagePath= str_replace('AA/production','',$host).$pathDel;
			 			if(file_exists($imagePath)){
			 				unlink($imagePath);
			 			}
			 		}
			 	}
			 	
				$msg=$imageDao->deleteImage($id);
			
			
		}
		 
		
		
?>
