<?php namespace app\page;
use SB\Controller\Controller;
include '../Controller/Controller.php';
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
include_once 'AA_NosessionRedirect.php';
$menuDao =App_DaoFactory::getFactory()->getMenu();
$staticPageDao =App_DaoFactory::getFactory()->getStaticPage2();
$menu =$menuDao->loadMenuNoDept($_SESSION['privillege']);
$staticPage = $staticPageDao->loadStaticpage($_SESSION['privillege']);
$statpagenum= count($staticPage);
$num = count($menu);
$pagetype = $_SESSION['privillege'];

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ส่วนการจัดการหลังบ้าน</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    
 
    <script type='text/javascript' src='../src/js/jquery-2.2.3.min.js'></script>
    <script type='text/javascript' src='../src/js/backoffice.js'></script>
    
    
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <link href="../src/scss/backoffice.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      	<?php
		 include 'AA_sidebar.php';
		?>
<div id="load_screen"><div id="loading" ><img alt="" src="../../images/image_997636.gif"></div></div>
            <?php
		 include 'nav.php';
		?>
            <!-- /page content -->
    
<!--Modal-->

        <!-- page content -->
        <div class="right_col" role="main" style="height:1088;min-height: 1088px;">
          <div class="">
            <div class="page-title">
                <h3>ส่วนการจัดการเมนูรอง </h3>
               
              </div>
            </div>
            <div class="clearfix"></div>  
              <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;">
                <div class="x_panel">
                  <div class="x_title">
                    <div class="clearfix"></div>
                  </div> 
              <!-- <h4>เพิ่มข้อมูล</h4> -->
               <!-- Form1 -->
                <form class="form-horizontal form-label-left">
			      	<div class="form-group">
                  	   <label class="control-label col-md-3" for="last-name">link ของเมนู</label>
                     	<div class="col-md-7">
                          <select id="menuList" class="form-control" required>
             
                          <?php if($num >0){?>
                          	<option value="">Choose..</option>
                           	<?php for ($j=0;$j<$num;$j++){?>
                            <option value="<?php echo $menu[$j]->getMenuId()?>"><?php echo $menu[$j]->getMenuText()?></option>
                            <?php }?>
                          <?php }?>
                          </select>
                     	</div>
                   </div>  
                </form>
                <!-- End Form1 -->
                
                <!-- Form2 -->
                <div id="inputForm2" style ="display:none;">
                <hr>
		                <form class="form-horizontal form-label-left" id="submenuForm">
		                <input type="hidden" name="pagetype" id="pagetype" value ="<?php echo $pagetype?>">
	                  <div class="form-group">
	                    <label class="control-label col-md-3" for="first-name">ตัวหนังสือเมนูรอง <span class="required">*</span>
	                    </label>
	                    <div class="col-md-7">
	                      <input type="text" id="insertSubMenuText" required="required" class="form-control col-md-7 col-xs-12">
	                    </div>
	                  </div>
	            
	                  	<div class="form-group">
	                     <label class="control-label col-md-3" for="last-name">link ของเมนู</label>
	                         	<div class="col-md-7">
		                          <select id="AddlinkSubMenuValue" class="form-control" required>
			                          <?php if($statpagenum >0){?>
			                          	<option value="">Choose..</option>
			                           	<?php for ($j=0;$j<$statpagenum;$j++){?>
			                            <option value="<?php echo $staticPage[$j]->getStaticpageId()?>"><?php echo $staticPage[$j]->getStaticpageText()?></option>
			
			                            <?php }?>
			                          <?php }?>
			                          </select>
		                          </div>
	                   </div>
	                   <div class="form-group">
	                    <label class="control-label col-md-3" for="last-name">ลำดับที่<span class="required">*</span>
	                    </label>
	                    <div class="col-md-7">
	                      <input type="text" id="insertSubMenuSeq" name="last-name" required="required" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control col-md-7 col-xs-12">
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="control-label col-md-3" for="last-name">
	                    </label>
	                    <div class="col-md-7">
	                      <button id="insertSubmenuButton" type="button" class="center-block btn btn-primary" style="margin-left: 0;">เพิ่มเมนู</button>
	                    </div>
	                  </div>
	              
	                </form>
                </div>
                <!-- End Form2 -->
                
                <div id="output"></div>
      
<div id="confirm" class="modal hide fade">
  <div class="modal-body">
    Are you sure?
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
  </div>
</div>
<!-- End modal -->

                  </div>
                </div>
              </div>
            </div>
          </div>


        <!-- footer content -->
        <footer>
          <div class="pull-right">
           
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->





    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script type='text/javascript' src='../src/js/jquery.dataTables.js'></script>
    <!-- <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
    <script src="../vendors/datatables.net/js/jquery.dataTables.js"></script> -->
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script type='text/javascript' src='../src/js/knockout-3.4.0.js'></script>
	<script type="text/javascript" src='../src/js/ModelbulderForStaticpage.js'></script>
	    <script type='text/javascript' src='../src/js/bootbox.min.js'></script>
    <!-- Datatables -->
    <script>
      $(document).ready(function() {
    	  function testFun() {
              quota = $("#quotas").val();
              alert(quota);
          }
          
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable({ "order": [[ 0, "desc" ]] , "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]] , "pageLength": 50});

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
  </body>
</html>