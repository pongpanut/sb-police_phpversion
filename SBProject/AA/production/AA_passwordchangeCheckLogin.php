<?php namespace app\page;
use SB\Controller\Controller;
include '../Controller/Controller.php';
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';

$user = $_POST['user'];
$pass = $_POST['pass'];
$newpass = $_POST['newpass'];

$userDao = App_DaoFactory::getFactory()->getUserDao();
$userArr =$userDao->loadUser($user);
$numuser = count($userArr);

if($numuser >=1){
	$checked = password_verify($_POST['pass'], $userArr['0']->getAdminPassword());
	if($checked){
		$hash = password_hash($newpass, PASSWORD_DEFAULT, ['cost' => 12]);
		$userArr =$userDao->updateAdminpass($user, $hash);
		echo "1";
	}
	else{
		echo "2";
	}
}
?>
