  <?php 
	@session_start();
	use SB\conn\factory\App_DaoFactory;
	
	include_once '../conn/factory/factory.php';
	include_once 'AA_NosessionRedirect.php';
	$pagetype = $_SESSION['privillege'];
	
	
	$pageTypeMappingDao = App_DaoFactory::getFactory()->getPageTypeDao();
	$page_type_text =$pageTypeMappingDao->loadpagetypemappingText($pagetype);
?>
  
  <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="../../<?php echo $page_type_text;?>/index.php" class="site_title"><i class="fa fa-paw"></i> <span>ส่วนปรับแต่ง Website (Website configuration)</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
                <img src="images/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $_SESSION['username']?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu" style="margin-top: 100px;">
              <div class="menu_section">
                <h3></h3>
                <ul class="nav side-menu">
                <?php if($pagetype == '999'){?>

               	    <li><a><i class="fa fa-users"></i> จัดการ user ต่างๆ<span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu">
                     <li><a href="AA_resetpassword.php">รีเซ๊ตรหัสผ่าน</a></li>
                    </ul>
                  </li>
                <?php }else{?>
                	<li><a><i class="fa fa-desktop"></i> ส่วนการจัดการเมนู <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                     <li><a href="AA_memumanagement.php">จัดการเมนูหลัก</a></li>
                     <li><a href="AA_submenumanagement.php">จัดการเมนูรอง</a></li>
                     <li><a href="AA_submenuList.php">จัดการเมนูย่อย</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-header"></i> จัดการ Header <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                     <li><a href="AA_headermanagement.php">จัดการ Header</a></li>
                    </ul>
                  </li>
                   <li><a><i class="fa fa-file-o"></i> จัดการ Page <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                     <li><a href="AA_staticPageManagment.php">จัดการ Page</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-key"></i> จัดการ Password <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                     <li><a href="AA_passwordchangemanagement.php">เปลี่ยน password</a></li>
                    </ul>
                  </li>    
                   <li><a><i class="fa fa-newspaper-o"></i> จัดการข่าวประชาสัมพันธ์<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                     <li><a href="  AA_newsManagment.php">จัดการข่าวประชาสัมพันธ์</a></li>
                    </ul>
                  </li>
                  
                  <li><a><i class="fa fa-bullhorn"></i> จัดการกิจกรรมผู้บริหาร<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                     <li><a href="AA_missionmanagment.php">จัดการกิจกรรมผู้บริหาร</a></li>
                    </ul>
                  </li>
                   <li><a><i class="fa fa-picture-o"></i> จัดการรูปภาพ<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                     <li><a href="AA_imagemanagement.php">จัดการรูปภาพ</a></li>
                    </ul>
                    
                  </li>
                  <li><a><i class="fa fa-television"></i> จัดการภาพพื้นหลัง<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                     <li><a href="AA_bgmanagement.php">จัดการภาพพื้นหลัง</a></li>
                    </ul>
                  </li>
                   <li><a><i class="fa fa-object-ungroup"></i>จัดการหน่วยงานย่อย<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="AA_departmentAdd.php">การจัดการหน่วยงานในสังกัดทั้งหมด</a></li>
                    <li><a href="AA_departmentlist.php">จัดการเมนูหน่วยงาน</a></li>
                    <li><a href="AA_departmentSublist.php">จัดการเมนูหน่วยย่อยในหน่วยงาน</a></li>
                    </ul>
                  </li>
                    <li><a><i class="fa fa-film"></i> จัดการวิดีโอ<span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu">
                     <li><a href="AA_uploadVideomanagement.php">จัดการวิดีโอ</a></li>
                    </ul>
                  </li>
                   <?php if($pagetype == '1'){?>
                  <li><a><i class="fa fa-desktop"></i> ศูนย์บริการออกหนังสือรับรองความประพฤติ <span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu">
                     <li><a href="AA_documentmanagement.php">จัดการเอกสาร</a></li>
                     <li><a href="AA_OtherNewsmanagement.php">จัดการข่าว</a></li>
                    </ul>
                  </li>
                  <?php }?>
                    <li><a><i class="fa fa-users"></i> จัดการผู้ติดตาม<span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu">
                     <li><a href="AA_managesubscript.php">ส่งข้อความกลับไปยังผู้ติดตาม</a></li>
                    </ul>
                  </li>
                    <li><a><i class="fa fa-envelope-o"></i> จัดการผู้ที่ติดต่อข้อความ<span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu">
                     <li><a href="AA_manageInbox.php">ดูรายละเอียดข้อความ</a></li>
                    </ul>
                  </li>
                  <!-- <li><a><i class="fa fa-envelope-o"></i> จัดการ tab กิจกรรม<span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu">
                     <li><a href="AA_tabmanagement.php"> tab กิจกรรม</a></li>
                    </ul>
                  </li> -->
                  <li><a><i class="fa fa-sitemap"></i> จัดการผู้บัญชาการ<span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu">
                     <li><a href="AA_commanderselectlist.php">สร้างแผนภูมิผู้บัญชาการใหม่</a></li>
                     <li><a href="AA_managecommander.php">จัดการผู้บัญชาการปัจจุบัน</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-caret-square-o-up"></i> จัดการ Banner<span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu">
                     <li><a href="AA_banner.php">จัดการ Banner</a></li>
                    </ul>
                  </li>
                  
                   <li><a><i class="fa fa-caret-square-o-up"></i> จัดการ ปฏิทิน<span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu">
                     <li><a href="AA_calendar.php">จัดการ ปฏิทิน</a></li>
                    </ul>
                  </li>
                  
                   <li><a><i class="fa fa-caret-square-o-up"></i>ผลสำรวจความพึงพอใจ<span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu">
                     <li><a href="AA_pollView.php">ผลสำรวจความพึงพอใจ</a></li>
                    </ul>
                  </li>
                   <li><a><i class="fa fa-caret-square-o-up"></i>จัดการส่วนติดต่อเรา<span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu">
                     <li><a href="AA_contactInfoMenagment.php">จัดการส่วนติดต่อเรา</a></li>
                    </ul>
                  </li>
                 
                  <?php }?>
                  
                  
                  
                 
                  
                <!-- 
                  <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="index.php">Dashboard</a></li>
                      <li><a href="index2.html">Dashboard2</a></li>
                      <li><a href="index3.html">Dashboard3</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-edit"></i> Forms <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="form.html">General Form</a></li>
                      <li><a href="form_advanced.html">Advanced Components</a></li>
                      <li><a href="form_validation.html">Form Validation</a></li>
                      <li><a href="form_wizards.html">Form Wizard</a></li>
                      <li><a href="form_upload.html">Form Upload</a></li>
                      <li><a href="form_buttons.html">Form Buttons</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-desktop"></i> UI Elements <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="general_elements.html">General Elements</a></li>
                      <li><a href="media_gallery.html">Media Gallery</a></li>
                      <li><a href="typography.html">Typography</a></li>
                      <li><a href="icons.html">Icons</a></li>
                      <li><a href="glyphicons.html">Glyphicons</a></li>
                      <li><a href="widgets.html">Widgets</a></li>
                      <li><a href="invoice.html">Invoice</a></li>
                      <li><a href="inbox.html">Inbox</a></li>
                      <li><a href="calendar.html">Calendar</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-table"></i> Tables <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="tables.html">Tables</a></li>
                      <li><a href="tables_dynamic.html">Table Dynamic</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-bar-chart-o"></i> Data Presentation <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="chartjs.html">Chart JS</a></li>
                      <li><a href="chartjs2.html">Chart JS2</a></li>
                      <li><a href="morisjs.html">Moris JS</a></li>
                      <li><a href="echarts.html">ECharts</a></li>
                      <li><a href="other_charts.html">Other Charts</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-clone"></i>Layouts <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="fixed_sidebar.html">Fixed Sidebar</a></li>
                      <li><a href="fixed_footer.html">Fixed Footer</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
              <div class="menu_section">
                <h3>Live On</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="e_commerce.html">E-commerce</a></li>
                      <li><a href="projects.html">Projects</a></li>
                      <li><a href="project_detail.html">Project Detail</a></li>
                      <li><a href="contacts.html">Contacts</a></li>
                      <li><a href="profile.html">Profile</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="page_403.html">403 Error</a></li>
                      <li><a href="page_404.html">404 Error</a></li>
                      <li><a href="page_500.html">500 Error</a></li>
                      <li><a href="plain_page.html">Plain Page</a></li>
                      <li><a href="login.html">Login Page</a></li>
                      <li><a href="pricing_tables.html">Pricing Tables</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="#level1_1">Level One</a>
                        <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="level2.html">Level Two</a>
                            </li>
                            <li><a href="#level2_1">Level Two</a>
                            </li>
                            <li><a href="#level2_2">Level Two</a>
                            </li>
                          </ul>
                        </li>
                        <li><a href="#level1_2">Level One</a>
                        </li>
                    </ul>
                  </li>                  
                  <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>
                </ul>
                           -->
              </div>
  
            </div>

            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <!-- 
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div> -->
            <!-- /menu footer buttons -->
          </div>
        </div>