<?php
use SB\conn\factory\App_DaoFactory;
use SB\model\departmentSubBOM;
include '../model/departmentSubBOM.php';
include_once '../conn/factory/factory.php';


$id = $_POST['id'];
$menuText = $_POST['text'];
$dropdown = $_POST['dropdown'];
$seq = $_POST['seq'];


if($id!=""){
	$departmentSubBOM = new departmentSubBOM();
	$departmentSubBOM->setDeptSubId($id);
	$departmentSubBOM->setDeptSubText($menuText);
	$departmentSubBOM->setDeptSubLink($dropdown);
	$departmentSubBOM->setSeq($seq);

	$departmentDao = App_DaoFactory::getFactory()->getDepartmentDao();
	$msg = $departmentDao->updateDeptSub($departmentSubBOM);	 
}
?>