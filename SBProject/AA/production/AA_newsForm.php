<?php
use  SB\model\newsM;
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
include 'constant.php';	
		$ds = "/uploads/pdf/";
		isset($_POST['id']) ? $id = $_POST['id'] : $id = '';
		isset($_POST['mode']) ? $mode = $_POST['mode'] : $mode = '';
		isset($_POST['elm']) ? $elm1 = $_POST['elm'] : $elm1 = '';
		$newsDao = App_DaoFactory::getFactory()->getNewsDao();
		$msg="";
		$path="";
		$news  = new newsM();
		if (!empty($_FILES)) {
				
			$tempFile = $_FILES['file']['tmp_name'];
			$size = GetimageSize($tempFile);
			$ratio = $size[0]/$size[1]; // width/height
			if( $ratio > 1) {
				$width = 500;
				$height = 500/$ratio;
			}
			else {
				$width = 500*$ratio;
				$height = 500;
			}
			$src = imagecreatefromstring(file_get_contents($tempFile));
			$dst = imagecreatetruecolor($width,$height);
			imagecopyresampled($dst,$src,0,0,0,0,$width,$height,$size[0],$size[1]);
			imagedestroy($src);
			imagejpeg($dst,$tempFile); // adjust format as needed
			imagedestroy($dst);
		
			$targetPath = dirname( __FILE__ ) . $ds;
			$targetFile =  $targetPath. $_FILES['file']['name'];
			$removeHost = str_replace($_SERVER["DOCUMENT_ROOT"],'', str_replace('\\','/',$targetFile ));
		
			move_uploaded_file($tempFile,$targetFile); //6
			if($tempFile !=""){
				$path = str_replace($linkPrefix,'',$removeHost) ;
				$news->setNewsPdf($path);
			}
			 
		}
		     if($mode=="add"){
		     	isset($_POST['newsHeaderAdd']) ? $newsHeader = $_POST['newsHeaderAdd'] : $newsHeader = '';
		     	isset($_POST['newsCodeAdd']) ? $newsCode = $_POST['newsCodeAdd'] : $newsCode = '';
		     	isset($_POST['pageTypeAdd']) ? $pageType = $_POST['pageTypeAdd'] : $pageType = '';
		     	isset($_POST['isHighlightAdd']) ? $is_highlight = $_POST['isHighlightAdd'] : $is_highlight = '';
		     	
		     	
		     	$news->setNewsCode($newsCode);
		     	$news->setNewsHeader($newsHeader);
		     	$news->setNewsDetail($elm1);
		     	$news->setNewsPdf($path);
		     	$news->setPageTypeId($pageType);
		     	$news->setIsHighLight($is_highlight);
		     	$msg=$newsDao->InsertNews($news);
		     }
		     else if($mode=="edit"){
	
		     	isset($_POST['newsHeader'.$id]) ? $newsHeader = $_POST['newsHeader'.$id] : $newsHeader = '';
		     	isset($_POST['newsCode'.$id]) ? $newsCode = $_POST['newsCode'.$id] : $newsCode = '';
		     	isset($_POST['pageType']) ? $pageType = $_POST['pageType'] : $pageType = '';
		     	isset($_POST['isHighlight'.$id]) ? $is_highlight = $_POST['isHighlight'.$id] : $is_highlight = '';
		     	
		   
		     	$news->setNewsId($id);
		     	$news->setNewsCode($newsCode);
		     	$news->setNewsHeader($newsHeader);
		     	$news->setNewsDetail($elm1);
		     	$news->setPageTypeId($pageType);
		     	$news->setIsHighLight($is_highlight);
		     	$msg=$newsDao->UpdateNews($news);
		     }
		       
		
		else if($mode=="delete"){
				$newsDel= $newsDao->getNewsById($id);
				if($newsDel!=""){
					$host = str_replace('\\','/',dirname(__FILE__));
				 	$pdfPath= str_replace('AA/production','',$host).$newsDel->getNewsPdf();
					if(file_exists($pdfPath)){
						unlink($pdfPath);
					} 
				}
				$msg=$newsDao->deleteNews($id);
		}
		 
	
	
?>
