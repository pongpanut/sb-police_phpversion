<?php
use  SB\model\videoM;
use SB\conn\factory\App_DaoFactory;
include_once '../conn/factory/factory.php';
include 'constant.php';
$ds = "/uploads/video/"; 
$ds_tmp = "uploads/video/";
$mode=$_POST['mode'];
$path="";
$videoM = new videoM();
$videoDao = App_DaoFactory::getFactory()->getVideoDao();
$id=$_POST['id'];

if(!empty($_FILES)) {
	if($id==""){
		if(is_uploaded_file($_FILES['userImage']['tmp_name'])){
			$tempFile = $_FILES['userImage']['tmp_name'];
			$targetPath = dirname( __FILE__ ) . $ds;
			$targetFile =  $targetPath. $_FILES['userImage']['name'];
			$removeHost = str_replace($_SERVER["DOCUMENT_ROOT"],'', str_replace('\\','/',$targetFile ));
			move_uploaded_file($tempFile,$targetFile); //6
			if($tempFile !=""){
				//$path = str_replace($linkPrefix,'',$removeHost);
				$videoM->setVideoPath($ds_tmp.$_FILES['userImage'.$id]['name']);
			}
		}
	}else{
		
		if(is_uploaded_file($_FILES['userImage'.$id]['tmp_name'])) {
			$tempFile = $_FILES['userImage'.$id]['tmp_name'];
			$targetPath = dirname( __FILE__ ) . $ds;
			$targetFile =  $targetPath. $_FILES['userImage'.$id]['name'];
			$removeHost = str_replace($_SERVER["DOCUMENT_ROOT"],'', str_replace('\\','/',$targetFile ));
			move_uploaded_file($tempFile,$targetFile); //6
			if($tempFile !=""){
				//$path = str_replace($linkPrefix,'',$removeHost) ;
				$videoM->setVideoPath($ds_tmp.$_FILES['userImage'.$id]['name']);
			}
		}
		
	}
	
}

if($mode=="add"){
	$videoNameAdd=$_POST['videoNameAdd'];
	$status=$_POST['status'];
	$pageTypeid =$_POST['pageTypeId'];
	
	$videoM->setVideoName($videoNameAdd);
	$videoM->setStatus($status);
	$videoM->setPageTypeId($pageTypeid);
	$videoDao->InsertVideo($videoM);
	
}
else if($mode=="edit"){
	
	$videoName=$_POST['videoName'];
	$status=$_POST['status'];
	$pageTypeId=$_POST['pageTypeId'];
	
	
	$videoM->setVideoName($videoName);
	$videoM->setStatus($status);
	$videoM->setPageTypeId($pageTypeId);
	$videoM->setVideoId($id);
	$videoDao->UpdateVideo($videoM);

}

else if($mode=="delete"){
	$id=$_POST['id'];
	$video = $videoDao->loadVideoById($id);
	if($video !=""){
		$pathDel =	$video->getVideoPath();
		if($pathDel!=""){
			$host = str_replace('\\','/',dirname(__FILE__));
			$imagePath= str_replace('AA/production','',$host).$pathDel;
			if(file_exists($imagePath)){
				unlink($imagePath);
			}
		}
	}
	$videoDao->deleteVideo($id);

}

?>