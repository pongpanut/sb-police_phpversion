<?php
use  SB\model\headerBOM;
use SB\conn\factory\App_DaoFactory;
include '../model/headerBOM.php';
include_once '../conn/factory/factory.php';

$headerDao = App_DaoFactory::getFactory()->getheaderDao();
$header = $headerDao->loadHeader($page_type_id);
?>
			<?php include 'AA_pageType.php';?>
			
			<form action="AA_upload.php" class=" form-horizontal form-label-left" enctype="multipart/form-data" method="POST"  id="my-awesome-dropzone">
                   <input type="hidden" value="1" name="page_type_id">
                   
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="header-text">Header Text <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="header-text" name="headerText" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="header-title">Header title <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="headerTitle" id="header-title" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="header-logo">Logo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <input name="file" type="file" id="header-logo" />
                        </div>
                      </div>
                      <div class="form-group">
                        
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                         	<button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                     </form>