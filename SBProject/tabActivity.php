
<script type="text/javascript" src="js/lightbox.js"></script>
<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen">

<div class="tab">
				<ul class="nav nav-tabs">
		  			<li class="active"><a data-toggle="tab" href="#home">กิจกรรม บก.อก.</a></li>
		  			<li><a data-toggle="tab" href="#menu1">กิจกรรม บก.ส.1</a></li>
		  			<li><a data-toggle="tab" href="#menu2">กิจกรรม บก.ส.2</a></li>
		  			<li><a data-toggle="tab" href="#menu3">กิจกรรม บก.ส.3</a></li>
		  			<li><a data-toggle="tab" href="#menu4">กิจกรรม บก.ส.4</a></li>
		  			<li><a data-toggle="tab" href="#menu5">บก.อก.บช.ส.</a></li>
		  			<li><a data-toggle="tab" href="#menu6"> ศูนย์พัฒนาด้านการข่าว</a></li>
				</ul>
					<div class="tab-content">
					  <div id="home" class="tab-pane fade in active">
						  <div class="row">
						  		
									
										<div class="col-md-5 box text-center" >
											<h3>ประชุมคณะกรรมการเครื่องมือพิเศษ</h3>
											<a class="example-image-link" href="images/activity/cat1/01/343697.jpg" data-lightbox="data-lightbox1" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img class="img-responsive" src="images/activity/cat1/01/343697.jpg" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat1-1" >อ่านต่อ</a>
										</div>
										
										<div class="col-md-5  box text-center">
										 	<h3>งานกาชาด 2559 </h3>
										 	<a class="example-image-link" href="images/activity/cat1/02/316543.jpg" data-lightbox="data-lightbox2" data-title="งานกาชาด 2559 "><img class="img-responsive" src="images/activity/cat1/02/316543.jpg" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat1-2" >อ่านต่อ</a>
											
										</div>
										<div class="col-md-5 box text-center">
											 	<h3>งานวันเด็กแห่งชาติ ปี2559 </h3>
											 	<a class="example-image-link" href="images/activity/cat1/03/IMG_4926.jpg" data-lightbox="data-lightbox3" data-title="งานวันเด็กแห่งชาติ ปี2559 "><img class="img-responsive" src="images/activity/cat1/03/IMG_4926.jpg" alt=""/></a>
												<a  class="btn btn_" data-toggle="modal" data-target="#cat1-3" >อ่านต่อ</a>
												
											</div>
											<div class="col-md-5 box text-center">
											 	<h3>จเรมาตรวจราชการประจำปีงบประมาณ 2558</h3>
											 	<a class="example-image-link" href="images/activity/cat1/04/IMG_0038.jpg" data-lightbox="data-lightbox4" data-title="จเรมาตรวจราชการประจำปีงบประมาณ 2558"><img class="img-responsive" src="images/activity/cat1/04/IMG_0038.jpg" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat1-4" >อ่านต่อ</a>
											</div>

							</div>
							 <div class="tab-more-info">
								<a href="http://www.sbpolice.go.th/page-all-picactivity-sbperson.php" class="btn btn_">more info</a>
							</div>
					  </div>
					  <div id="menu1" class="tab-pane fade">
					    <div class="row">
						  			<div class="col-md-5 box text-center">
											<h3>เปิดศูนย์เอกภพ</h3>
											<a class="example-image-link" href="images/activity/cat2/01/IMG_1605.JPG" data-lightbox="data-lightboxc21" data-title="เปิดศูนย์เอกภพ"><img class="img-responsive" src="images/activity/cat2/01/IMG_1605.JPG" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat2-1" >อ่านต่อ</a>
										</div>
										
										<div class="col-md-5  box text-center">
										 	<h3>โครงการกิจกรรม 5 ส Big Cleanning Day </h3>
										 	<a class="example-image-link" href="images/activity/cat2/02/IMG_9300.JPG" data-lightbox="data-lightboxc2" data-title="โครงการกิจกรรม 5 ส Big Cleanning Day "><img class="img-responsive" src="images/activity/cat2/02/IMG_9300.JPG" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat2-2" >อ่านต่อ</a>
											
										</div>
										<div class="col-md-5 box text-center">
											 	<h3>โครงการอบรมเชิงปฏิบัติการวิทยากร โครงการจิตสำนึกฯ บก.ส.1 13 - 17 มี.ค.56</h3>									
												<a class="example-image-link" href="images/activity/cat2/03/_DSC1713.JPG" data-lightbox="data-lightboxc3" data-title="โครงการอบรมเชิงปฏิบัติการวิทยากร โครงการจิตสำนึกฯ บก.ส.1 13 - 17 มี.ค.56"><img class="img-responsive" src="images/activity/cat2/03/_DSC1713.JPG" alt=""/></a>	
											<a  class="btn btn_" data-toggle="modal" data-target="#cat2-3" >อ่านต่อ</a>
											</div>
											<div class="col-md-5 box text-center">
											 	<h3>โครงการสร้างจิตสำนึกต่อสถาบันพระมหากษัตริย์   <br> ม.ราชภัฎสกลนคร</h3>
											 	<a class="example-image-link" href="images/activity/cat2/04/sakon1.JPG" data-lightbox="data-lightboxc4" data-title="โครงการสร้างจิตสำนึกต่อสถาบันพระมหากษัตริย์    ม.ราชภัฎสกลนคร"><img class="img-responsive" src="images/activity/cat2/04/sakon1.JPG" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat2-4" >อ่านต่อ</a>
											</div>
							</div>
							 <div class="tab-more-info">
								<a href="http://www.sb1police.go.th/" class="btn btn_">more info</a>
							</div>
					  </div>
					  <div id="menu2" class="tab-pane fade">
					     <div class="row">
						  		<div class="col-md-5 box text-center" >
											<h3>ประชุมคณะกรรมการเครื่องมือพิเศษ</h3>
											<a class="example-image-link" href="images/activity/cat1/01/343697.jpg" data-lightbox="data-lightbox1" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img class="img-responsive" src="images/activity/cat1/01/343697.jpg" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat1-1" >อ่านต่อ</a>
										</div>
										
										<div class="col-md-5  box text-center">
										 	<h3>งานกาชาด 2559 </h3>
										 	<a class="example-image-link" href="images/activity/cat1/02/316543.jpg" data-lightbox="data-lightbox2" data-title="งานกาชาด 2559 "><img class="img-responsive" src="images/activity/cat1/02/316543.jpg" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat1-2" >อ่านต่อ</a>
											
										</div>
										<div class="col-md-5 box text-center">
											 	<h3>งานวันเด็กแห่งชาติ ปี2559 </h3>
											 	<a class="example-image-link" href="images/activity/cat1/03/IMG_4926.jpg" data-lightbox="data-lightbox3" data-title="งานวันเด็กแห่งชาติ ปี2559 "><img class="img-responsive" src="images/activity/cat1/03/IMG_4926.jpg" alt=""/></a>
												<a  class="btn btn_" data-toggle="modal" data-target="#cat1-3" >อ่านต่อ</a>
												
											</div>
											<div class="col-md-5 box text-center">
											 	<h3>จเรมาตรวจราชการประจำปีงบประมาณ 2558</h3>
											 	<a class="example-image-link" href="images/activity/cat1/04/IMG_0038.jpg" data-lightbox="data-lightbox4" data-title="จเรมาตรวจราชการประจำปีงบประมาณ 2558"><img class="img-responsive" src="images/activity/cat1/04/IMG_0038.jpg" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat1-4" >อ่านต่อ</a>
											</div>
							</div>
							 <div class="tab-more-info">
								<a href="more.html" class="btn btn_">more info</a>
							</div>
					  </div>
					  <div id="menu3" class="tab-pane fade">
					     <div class="row">
						  		<div class="col-md-5 box text-center">
											<h3>ทำพิธีถวายพระพร สมเด็จพระเทพรัตราชสุดา ฯ สยามบรมราชกุมารี</h3>
											<a class="example-image-link" href="images/activity/cat3/01/IMG20160401131327.jpg" data-lightbox="data-lightboxc31" data-title="ทำพิธีถวายพระพร สมเด็จพระเทพรัตราชสุดา ฯ สยามบรมราชกุมารี"><img class="img-responsive" src="images/activity/cat3/01/IMG20160401131327.jpg" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat3-1" >อ่านต่อ</a>
										</div>
										
										<div class="col-md-5  box text-center">
										 	<h3>พิธีถวายสัตย์ปฏิญาณ ในโอกาสวันเฉลิมพระชนมพรรษา 88 พรรษา 5 ธันวาคม 2558</h3>
										 	<a class="example-image-link" href="images/activity/cat3/02/68587.jpg" data-lightbox="data-lightboxc32" data-title="พิธีถวายสัตย์ปฏิญาณ ในโอกาสวันเฉลิมพระชนมพรรษา 88 พรรษา 5 ธันวาคม 2558"><img class="img-responsive" src="images/activity/cat3/02/68587.jpg" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat3-2" >อ่านต่อ</a>
											
										</div>
										<div class="col-md-5 box text-center">
											 	<h3>พิธีพระราชทานปริญญาบัตร มหาลัยราชภัฏเชียงใหม่</h3>									
												<a class="example-image-link" href="images/activity/cat3/03/749979.jpg" data-lightbox="data-lightboxc33" data-title="พิธีพระราชทานปริญญาบัตร มหาลัยราชภัฏเชียงใหม่"><img class="img-responsive" src="images/activity/cat3/03/749979.jpg" alt=""/></a>	
											<a  class="btn btn_" data-toggle="modal" data-target="#cat3-3" >อ่านต่อ</a>
											</div>
											<div class="col-md-5 box text-center">
											 	<h3>โงานคอนเสิร์ต 12 ปี บ้านพระพร สร้างคนดีสู่สังคม</h3>
											 	<a class="example-image-link" href="images/activity/cat3/04/8839175.jpg" data-lightbox="data-lightboxc34" data-title="งานคอนเสิร์ต 12 ปี บ้านพระพร สร้างคนดีสู่สังคม"><img class="img-responsive" src="images/activity/cat3/04/8839175.jpg" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat3-4" >อ่านต่อ</a>
											</div>
							</div>
							<div class="tab-more-info">
								<a href="more.html" class="btn btn_">more info</a>
							</div>
							  
					  </div>

				<div id="menu4" class="tab-pane fade">
					    <div class="row">
						  			<div class="col-md-5 box text-center">
											<h3>โครงการตำรวจไทยใจสะอาด(จิตอาสา)</h3>
											<a class="example-image-link" href="images/activity/cat4/01/1.jpg" data-lightbox="data-lightboxc21" data-title="โครงการตำรวจไทยใจสะอาด(จิตอาสา) ณ วัดปทุมวนารามวรมหาวิหาร"><img class="img-responsive" src="images/activity/cat4/01/1.jpg" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat4-1" >อ่านต่อ</a>
										</div>
										
										<div class="col-md-5  box text-center">
										 	<h3>งานวันเด็กแห่งชาติ ปี 2559 ร่วมกับ บช.ส. ที่ทำเนียบรัฐบาล</h3>
										 	<a class="example-image-link" href="images/activity/cat4/02/2.jpg" data-lightbox="data-lightboxc2" data-title="งานวันเด็กแห่งชาติ ปี 2559 ร่วมกับ บช.ส. ที่ทำเนียบรัฐบาล"><img class="img-responsive" src="images/activity/cat4/02/2.jpg" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat4-2" >อ่านต่อ</a>
											
										</div>
										<div class="col-md-5 box text-center">
											 	<h3>ถวายสัตยาบรรณ</h3>									
												<a class="example-image-link" href="images/activity/cat4/03/1.jpg" data-lightbox="data-lightboxc3" data-title="ถวายสัตยาบรรณ"><img class="img-responsive" src="images/activity/cat4/03/1.jpg" alt=""/></a>	
											<a  class="btn btn_" data-toggle="modal" data-target="#cat4-3" >อ่านต่อ</a>
											</div>
											<div class="col-md-5 box text-center">
											 	<h3>Big Cleaning Day</h3>
											 	<a class="example-image-link" href="images/activity/cat4/04/1.jpg" data-lightbox="data-lightboxc4" data-title="Big Cleaning Day"><img class="img-responsive" src="images/activity/cat4/04/1.jpg" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat4-4" >อ่านต่อ</a>
											</div>
							</div>
							 <div class="tab-more-info">
								<a href="http://www.sb1police.go.th/" class="btn btn_">more info</a>
							</div>
					  </div>
					  <div id="menu5" class="tab-pane fade">
					    <div class="row">
						  			<div class="col-md-5 box text-center">
											<h3>เปิดศูนย์เอกภพ</h3>
											<a class="example-image-link" href="images/activity/cat2/01/IMG_1605.JPG" data-lightbox="data-lightboxc21" data-title="เปิดศูนย์เอกภพ"><img class="img-responsive" src="images/activity/cat2/01/IMG_1605.JPG" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat2-1" >อ่านต่อ</a>
										</div>
										
										<div class="col-md-5  box text-center">
										 	<h3>โครงการกิจกรรม 5 ส Big Cleanning Day </h3>
										 	<a class="example-image-link" href="images/activity/cat2/02/IMG_9300.JPG" data-lightbox="data-lightboxc2" data-title="โครงการกิจกรรม 5 ส Big Cleanning Day "><img class="img-responsive" src="images/activity/cat2/02/IMG_9300.JPG" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat2-2" >อ่านต่อ</a>
											
										</div>
										<div class="col-md-5 box text-center">
											 	<h3>โครงการอบรมเชิงปฏิบัติการวิทยากร โครงการจิตสำนึกฯ บก.ส.1 13 - 17 มี.ค.56</h3>									
												<a class="example-image-link" href="images/activity/cat2/03/_DSC1713.JPG" data-lightbox="data-lightboxc3" data-title="โครงการอบรมเชิงปฏิบัติการวิทยากร โครงการจิตสำนึกฯ บก.ส.1 13 - 17 มี.ค.56"><img class="img-responsive" src="images/activity/cat2/03/_DSC1713.JPG" alt=""/></a>	
											<a  class="btn btn_" data-toggle="modal" data-target="#cat2-3" >อ่านต่อ</a>
											</div>
											<div class="col-md-5 box text-center">
											 	<h3>โครงการสร้างจิตสำนึกต่อสถาบันพระมหากษัตริย์   <br> ม.ราชภัฎสกลนคร</h3>
											 	<a class="example-image-link" href="images/activity/cat2/04/sakon1.JPG" data-lightbox="data-lightboxc4" data-title="โครงการสร้างจิตสำนึกต่อสถาบันพระมหากษัตริย์    ม.ราชภัฎสกลนคร"><img class="img-responsive" src="images/activity/cat2/04/sakon1.JPG" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat2-4" >อ่านต่อ</a>
											</div>
							</div>
							 <div class="tab-more-info">
								<a href="http://www.sb1police.go.th/" class="btn btn_">more info</a>
							</div>
					  </div>
					  <div id="menu6" class="tab-pane fade">
					    <div class="row">
						  			<div class="col-md-5 box text-center">
											<h3>ประชุมบริหาร ศพข.บช.ส.</h3>
											<a class="example-image-link" href="images/activity/cat6/01/original-1461038799486.jpg" data-lightbox="data-lightboxc21" data-title="ประชุมบริหาร ศพข.บช.ส."><img class="img-responsive" src="images/activity/cat6/01/original-1461038799486.jpg" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat6-1" >อ่านต่อ</a>
										</div>
										
										<div class="col-md-5  box text-center">
										 	<h3>ประชุมคณะทำงานประชาสัมพันธ์ของหน่วย </h3>
										 	<a class="example-image-link" href="images/activity/cat6/02/original-1461039149185.jpg" data-lightbox="data-lightboxc2" data-title="ประชุมคณะทำงานประชาสัมพันธ์ของหน่วย"><img class="img-responsive" src="images/activity/cat6/02/original-1461039149185.jpg" alt=""/></a>
											<a  class="btn btn_" data-toggle="modal" data-target="#cat6-2" >อ่านต่อ</a>
											
										</div>
										<div class="col-md-5 box text-center">
											 	<h3>กิจกรรมวันสงกรานต์59</h3>									
												<a class="example-image-link" href="images/activity/cat6/03/original-1461039991278.jpg" data-lightbox="data-lightboxc3" data-title="กิจกรรมวันสงกรานต์59"><img class="img-responsive" src="images/activity/cat6/03/original-1461039991278.jpg" alt=""/></a>	
											<a  class="btn btn_" data-toggle="modal" data-target="#cat6-3" >อ่านต่อ</a>
											</div>
											
							</div>
							 <div class="tab-more-info">
								<a href="http://www.sb1police.go.th/" class="btn btn_">more info</a>
							</div>
					  </div>
				</div>
		</div> 
	<!-- modal  -->
	

<div id="cat1-1" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ประชุมคณะกรรมการเครื่องมือพิเศษ ลงวันที่ 28 เมษายน 2559</h4>
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat1/01/343697.jpg" data-lightbox="data-lightbox1-1" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class=" modal-img img-thumbnail " src="images/activity/cat1/01/343697.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	<a class="example-image-link" href="images/activity/cat1/01/54217.jpg" data-lightbox="data-lightbox1-1" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail " src="images/activity/cat1/01/54217.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat1/01/343697.jpg" data-lightbox="data-lightbox1-1" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail " src="images/activity/cat1/01/343697.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	<a class="example-image-link" href="images/activity/cat1/01/54217.jpg" data-lightbox="data-lightbox1-1" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img 	img-thumbnail	" src="images/activity/cat1/01/54217.jpg" alt=""/></a>
      </div>
      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>



<div id="cat1-2" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">งานกาชาด 2559</h4>
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat1/02/316530.jpg" data-lightbox="data-lightbox1-2" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail " src="images/activity/cat1/02/316530.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	<a class="example-image-link" href="images/activity/cat1/02/316543.jpg" data-lightbox="data-lightbox1-2" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail " src="images/activity/cat1/02/316543.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat1/02/4-18-2016 9-56-15 AM.jpg" data-lightbox="data-lightbox1-2" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail " src="images/activity/cat1/02/4-18-2016 9-56-15 AM.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	<a class="example-image-link" href="images/activity/cat1/02/IMG_4401_resize.JPG" data-lightbox="data-lightbox1-2" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail	" src="images/activity/cat1/02/IMG_4401_resize.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	<a class="example-image-link" href="images/activity/cat1/02/IMG_4439_resize.JPG" data-lightbox="data-lightbox1-2" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail	" src="images/activity/cat1/02/IMG_4439_resize.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	<a class="example-image-link" href="images/activity/cat1/02/IMG_4609_resize.JPG" data-lightbox="data-lightbox1-2" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail	" src="images/activity/cat1/02/IMG_4609_resize.JPG" alt=""/></a>
      </div>
      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>

<div id="cat1-3" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> งานวันเด็กแห่งชาติ ปี2559</h4>
         <h5 class="modal-title">  @ทำเนียบรัฐบาล</h5>
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat1/03/IMG_4926.jpg" data-lightbox="data-lightbox1-3" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail " src="images/activity/cat1/03/IMG_4926.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat1/03/IMG_4947.jpg" data-lightbox="data-lightbox1-3" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail " src="images/activity/cat1/03/IMG_4947.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat1/03/IMG_4981.jpg" data-lightbox="data-lightbox1-3" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail " src="images/activity/cat1/03/IMG_4981.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat1/03/IMG_4991.jpg" data-lightbox="data-lightbox1-3" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail " src="images/activity/cat1/03/IMG_4991.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat1/03/IMG_5098.jpg" data-lightbox="data-lightbox1-3" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail " src="images/activity/cat1/03/IMG_5098.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat1/03/IMG_5323.jpg" data-lightbox="data-lightbox1-3" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail " src="images/activity/cat1/03/IMG_5323.jpg" alt=""/></a>
      </div>
  
      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>


<div id="cat1-4" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">  จเรมาตรวจราชการประจำปีงบประมาณ 2558 </h4>
         <h5 class="modal-title">  5 ส.ค.2558 จเรมาตรวจราชการประจำปีงบประมาณ 2558 นำโดย พ.ต.อ.สนั่น วงศ์เพียร รอง ผบก.จต.กต.3 , พ.ต.อ.หญิง พิมมาศ เปลี่ยนดี รอง ผบก.จต.กต.3 เวลา 09.00น. ตรวจ บก.อก.บช.ส. รอรับการตรวจโดย พ.ต.อ.หญิง นิรมล แก้วผลึก รอง ผบก.อก.บช.ส.</h5>
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat1/04/IMG_0038.jpg" data-lightbox="data-lightbox1-4" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail " src="images/activity/cat1/04/IMG_0038.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat1/04/IMG_0055.jpg" data-lightbox="data-lightbox1-4" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail " src="images/activity/cat1/04/IMG_0055.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat1/04/IMG_0113.jpg" data-lightbox="data-lightbox1-4" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail " src="images/activity/cat1/04/IMG_0113.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat1/04/IMG_0130.jpg" data-lightbox="data-lightbox1-4" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail " src="images/activity/cat1/04/IMG_0130.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat1/04/IMG_0157.jpg" data-lightbox="data-lightbox1-4" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail " src="images/activity/cat1/04/IMG_0157.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat1/04/IMG_0161.jpg" data-lightbox="data-lightbox1-4" data-title="ประชุมคณะกรรมการเครื่องมือพิเศษ"><img  class="modal-img img-thumbnail " src="images/activity/cat1/04/IMG_0161.jpg" alt=""/></a>
      </div>
  
      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>

<div id="cat2-1" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">  เปิดศูนย์เอกภพ </h4>
         <h5 class="modal-title">  ศุนย์เอกภพ ได้รับการจัดสรรงบประมาณจาก บช.ส. จำนวน 4,450,000 บาท เพื่อทำการปรับปรุงศูนย์ พร้อมติดตั้งอุปกรณ์เพื่อให้ปฏิบัติงารนอย่างมีรปะสิทธิภาพ รองรับภารกิจของ บช.ส.โดยการปรับปรุงศูนย์เอกภพ แบ่งเป็น</h5>
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/01/IMG_1605.JPG" data-lightbox="data-lightbox2-1" data-title="ข้าราชการตำรวจ บก.ส.1 ตั้งแถวรับผู้บังคับบัญชา"><img  class="modal-img img-thumbnail " src="images/activity/cat2/01/IMG_1605.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/01/IMG_1612.JPG" data-lightbox="data-lightbox2-1" data-title="พล.ต.ท.ชัยวัฒน์ เกตุวรชัย ผบช.ส.ทำพิธีไหว้ศาลพระภูมิประจำวังรัตนาภา"><img  class="modal-img img-thumbnail " src="images/activity/cat2/01/IMG_1612.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/01/IMG_1643.JPG" data-lightbox="data-lightbox2-1" data-title="พล.ต.ท.ชัยวัฒน์ฯ ตรวจเยี่ยมส่วนปฏิบัติการต่างๆ ของศูนย์เอกภพ โดยมี พล.ต.ต.สราวุฒิ การพานิช ผบก.ส.1 บรรยายสรุป"><img  class="modal-img img-thumbnail " src="images/activity/cat2/01/IMG_1643.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/01/IMG_1666.JPG" data-lightbox="data-lightbox2-1" data-title="ผู้บังคับบัญชา บช.ส.ร่วมพิธีทำบุญเลี้ยงเพลพระสงฆ์"><img  class="modal-img img-thumbnail " src="images/activity/cat2/01/IMG_1666.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/01/IMG_1715.JPG" data-lightbox="data-lightbox2-1" data-title="ผู้บังคับบัญชาร่วมเปิดศูนย์เอกภพอย่างเป็นทางการ"><img  class="modal-img img-thumbnail " src="images/activity/cat2/01/IMG_1715.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/01/IMG_1717.JPG" data-lightbox="data-lightbox2-1" data-title="ผบก.ส.1 มอบของที่ระลึกแก่ ผบช.ส."><img  class="modal-img img-thumbnail " src="images/activity/cat2/01/IMG_1717.JPG" alt=""/></a>
      </div>
  
      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>


<div id="cat2-2" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">   โครงการกิจกรรม 5 ส Big Cleanning Day </h4>
         <h5 class="modal-title">  กองบังคับการตำรวจสันติบาล 1 จัดโครงการกิจกรรม 5 ส Big Cleanning Day วันพุธที่ 19 มิถุนายน 2556 โดยมีข้าราชการตำรวจทั้ง ส.1 และ ส.4 เข้าร่วมกิจกรรมในครั้งนี้ ตั้งแต่เวลา 14.00 - 16.00 น.</h5>
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/02/IMG_9300.JPG" data-lightbox="data-lightbox2-2" data-title="กล่าวรายงานต่อประธาน"><img  class="modal-img img-thumbnail " src="images/activity/cat2/02/IMG_9300.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/02/IMG_9307.JPG" data-lightbox="data-lightbox2-2" data-title="ผบก.ส.1 ประธานในพิธีกล่าวเปิดงาน"><img  class="modal-img img-thumbnail " src="images/activity/cat2/02/IMG_9307.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/02/IMG_9310.JPG" data-lightbox="data-lightbox2-2" data-title="ผบก.ส.4 ร่วมกิจกรรมด้วย"><img  class="modal-img img-thumbnail " src="images/activity/cat2/02/IMG_9310.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/02/IMG_9313.JPG" data-lightbox="data-lightbox2-2" data-title="ถ่ายภาพร่วมกันก่อนลงมือปฏิบัติ"><img  class="modal-img img-thumbnail " src="images/activity/cat2/02/IMG_9313.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/02/IMG_9327.JPG" data-lightbox="data-lightbox2-2" data-title="ด้านหลังอาคารรัตนาภา"><img  class="modal-img img-thumbnail " src="images/activity/cat2/02/IMG_9327.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/02/IMG_9336.JPG" data-lightbox="data-lightbox2-2" data-title="ร่วมแรงร่วมใจ"><img  class="modal-img img-thumbnail " src="images/activity/cat2/02/IMG_9336.JPG" alt=""/></a>
      </div>
  
      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>



<div id="cat2-3" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">   โครงการอบรมเชิงปฏิบัติการวิทยากร โครงการจิตสำนึกฯ บก.ส.1 13 - 17 มี.ค.56 </h4>
         <h5 class="modal-title"> ภาพการจัดกิจกรรม โครงการอบรมเชิงปฏิบัติการ ทักษะการเป็นวิทยากร โครงการจิตสำนึกการทำนุบำรุง ชาติ ศาสน์ กษัตริย์ ของกองบังคับการตำรวจสันติบาล 1 ระหว่างวันที่ 13 - 17 มีนาคม 2556 ณ ศูนย์พัฒนาด้านการข่าว</h5>
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/03/_DSC1713.JPG" data-lightbox="data-lightbox2-3" data-title="พล.ต.ต.อัตตชัย ดวงอัมพร รอง ผบช.ส. ประธานเปิดการอมรม"><img  class="modal-img img-thumbnail " src="images/activity/cat2/03/_DSC1713.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/03/_DSC1762.JPG" data-lightbox="data-lightbox2-3" data-title="เจ้าหน้าที่ตำรวจ บก.ส.1ที่เข้าร่วมอมรม"><img  class="modal-img img-thumbnail " src="images/activity/cat2/03/_DSC1762.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/03/_DSC1795.JPG" data-lightbox="data-lightbox2-3" data-title="แบ่งกลุ่มทำกิจกรรม"><img  class="modal-img img-thumbnail " src="images/activity/cat2/03/_DSC1795.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/03/_DSC1832.JPG" data-lightbox="data-lightbox2-3" data-title="ตัวแทนกลุ่มออกมานำเสนอแนวความคิด"><img  class="modal-img img-thumbnail " src="images/activity/cat2/03/_DSC1832.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/03/_DSC2588.JPG" data-lightbox="data-lightbox2-3" data-title="ประธานมอบใบประกาศ"><img  class="modal-img img-thumbnail " src="images/activity/cat2/03/_DSC2588.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/03/_DSC2663.JPG" data-lightbox="data-lightbox2-3" data-title="ถ่ายภาพหมู่ร่วมกัน"><img  class="modal-img img-thumbnail " src="images/activity/cat2/03/_DSC2663.JPG" alt=""/></a>
      </div>
  
      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>

<div id="cat2-4" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">  โครงการสร้างจิตสำนึกต่อสถาบันพระมหากษัตริย์ ม.ราชภัฎสกลนคร	</h4>
         <h5 class="modal-title"> โครงการสร้างจิตสำนึกต่อสถาบันพระมหากษัตริย์”รุ่นที่ ๕ ณ มหาวิทยาลัยราชภัฎสกลนคร อ.เมือง จว.สกลนคร วันศุกร์ที่ ๓๐ มิถุนายน พ.ศ. ๒๕๕๕</h5>
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/04/sakon1.JPG" data-lightbox="data-lightbox2-4" data-title="โครงการสร้างจิตสำนึกต่อสถาบันพระมหากษัตริย์ ม.ราชภัฎสกลนคร"><img  class="modal-img img-thumbnail " src="images/activity/cat2/04/sakon1.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/04/sakon2.JPG" data-lightbox="data-lightbox2-4" data-title="โครงการสร้างจิตสำนึกต่อสถาบันพระมหากษัตริย์ ม.ราชภัฎสกลนคร	"><img  class="modal-img img-thumbnail " src="images/activity/cat2/04/sakon2.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/04/sakon3.JPG" data-lightbox="data-lightbox2-4" data-title="โครงการสร้างจิตสำนึกต่อสถาบันพระมหากษัตริย์ ม.ราชภัฎสกลนคร"><img  class="modal-img img-thumbnail " src="images/activity/cat2/04/sakon3.JPG" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat2/04/sakon4.JPG" data-lightbox="data-lightbox2-4" data-title="โครงการสร้างจิตสำนึกต่อสถาบันพระมหากษัตริย์ ม.ราชภัฎสกลนคร"><img  class="modal-img img-thumbnail " src="images/activity/cat2/04/sakon4.JPG" alt=""/></a>
      </div>
      
  
      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>


<div id="cat3-1" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">  ทำพิธีถวายพระพร สมเด็จพระเทพรัตราชสุดา ฯ สยามบรมราชกุมารี	</h4>
         <h5 class="modal-title">พล.ต.ต.พิสิฐ  ตันประเสริฐ ผบก.ส.3 นำข้าราชการในสังกัดร่วมทำพิธี ถวายพระพร เนื่องในวโรกาสวันคล้ายวันพระราชสมภพ สมเด็จพระเทพรัตราชสุดา ฯ สยามบรมราชกุมารี ในวันที่ 2 เมษายน พ.ศ.2559</h5>
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat3/01/IMG20160401131327.jpg" data-lightbox="data-lightbox3-1" data-title="ทำพิธีถวายพระพร สมเด็จพระเทพรัตราชสุดา ฯ สยามบรมราชกุมารี"><img  class="modal-img img-thumbnail " src="images/activity/cat3/01/IMG20160401131327.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat3/01/IMG20160401131402.jpg" data-lightbox="data-lightbox3-1" data-title="ทำพิธีถวายพระพร สมเด็จพระเทพรัตราชสุดา ฯ สยามบรมราชกุมารี"><img  class="modal-img img-thumbnail " src="images/activity/cat3/01/IMG20160401131402.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat3/01/IMG20160401131426.jpg" data-lightbox="data-lightbox3-1" data-title="ทำพิธีถวายพระพร สมเด็จพระเทพรัตราชสุดา ฯ สยามบรมราชกุมารี"><img  class="modal-img img-thumbnail " src="images/activity/cat3/01/IMG20160401131426.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat3/01/IMG20160401131610.jpg" data-lightbox="data-lightbox3-1" data-title="ทำพิธีถวายพระพร สมเด็จพระเทพรัตราชสุดา ฯ สยามบรมราชกุมารี"><img  class="modal-img img-thumbnail " src="images/activity/cat3/01/IMG20160401131610.jpg" alt=""/></a>
      </div>
      
  
      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>


<div id="cat3-2" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">  พิธีถวายสัตย์ปฏิญาณ ในโอกาสวันเฉลิมพระชนมพรรษา 88 พรรษา 5 ธันวาคม 2558</h4>
         <h5 class="modal-title">วันที่ 3 ธันวาคม 2558 เวลา 07.30 น.  พล.ต.ต.พิสิฐ  ตันประเสริฐ ผบก.ส.3 นำข้าราชการตำรวจในสังกัดกองบังคับการตำรวจสันติบาล 3 เข้าร่วมพิธีถวายสัตย์ปฏิญาณเพื่อเป็นข้าราชการตำรวจที่ดีและพลังของแผ่นดิน  ด้วยคณะรัฐมนตรี มีมติเห็นชอบให้จัดพิธีถวายสัตย์ปฏิญาณเพื่อเป็นข้าราชการที่และพลังของแผ่นดิน ซึ่งเป็นกิจกรรมสำคัญของเหล่าข้าราชการทั่วประเทศที่ได้มีการจัดต่อเนื่องมาโดยตลอด  เนื่องในโอกาสวันเฉลิมพระชนมพรรษา ของพระบาทสมเด็จพระเจ้าอยู่หัว

ปีนี้กำหนดจัดพิธี ในวันพฤหัสบดีที่ 3 ธันวาคม 2558 เวลา 08.00 น. เพื่อแสดงความจงรักภักดี สำนึกในพระมหากรุณาธิคุณอย่างหาที่สุดมิได้ และถวายเป็นราชสักการะแด่พระบาทสมเด็จพระเจ้าอยู่หัว เนื่องในโอกาสวันเฉลิมพระชนมพรรษา 5 ธันวาคม 2558 และเพื่อให้ข้าราชการตำรวจได้แสดงความมุ่งมั่นที่จะเป็นข้าราชการตำรวจที่ดีและพลังของแผ่นดิน โดยปฏิบัติหน้าที่ตามรอยพระยุคลบาทในฐานะข้าของแผ่นดินให้เกิดประโยชน์สุขแก่ประชาชนและประเทศชาติสืบไป</h5>
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat3/02/68587.jpg" data-lightbox="data-lightbox3-2" data-title="พิธีถวายสัตย์ปฏิญาณ ในโอกาสวันเฉลิมพระชนมพรรษา 88 พรรษา 5 ธันวาคม 2558"><img  class="modal-img img-thumbnail " src="images/activity/cat3/02/68587.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat3/02/68613.jpg" data-lightbox="data-lightbox3-2" data-title="พิธีถวายสัตย์ปฏิญาณ ในโอกาสวันเฉลิมพระชนมพรรษา 88 พรรษา 5 ธันวาคม 2558"><img  class="modal-img img-thumbnail " src="images/activity/cat3/02/68613.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat3/02/68616.jpg" data-lightbox="data-lightbox3-2" data-title="พิธีถวายสัตย์ปฏิญาณ ในโอกาสวันเฉลิมพระชนมพรรษา 88 พรรษา 5 ธันวาคม 2558"><img  class="modal-img img-thumbnail " src="images/activity/cat3/02/68616.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat3/02/68617.jpg" data-lightbox="data-lightbox3-2" data-title="พิธีถวายสัตย์ปฏิญาณ ในโอกาสวันเฉลิมพระชนมพรรษา 88 พรรษา 5 ธันวาคม 2558"><img  class="modal-img img-thumbnail " src="images/activity/cat3/02/68617.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat3/02/68621.jpg" data-lightbox="data-lightbox3-2" data-title="พิธีถวายสัตย์ปฏิญาณ ในโอกาสวันเฉลิมพระชนมพรรษา 88 พรรษา 5 ธันวาคม 2558"><img  class="modal-img img-thumbnail " src="images/activity/cat3/02/68621.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat3/02/68629.jpg" data-lightbox="data-lightbox3-2" data-title="พิธีถวายสัตย์ปฏิญาณ ในโอกาสวันเฉลิมพระชนมพรรษา 88 พรรษา 5 ธันวาคม 2558"><img  class="modal-img img-thumbnail " src="images/activity/cat3/02/68629.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat3/02/s__14180407.jpg" data-lightbox="data-lightbox3-2" data-title="พิธีถวายสัตย์ปฏิญาณ ในโอกาสวันเฉลิมพระชนมพรรษา 88 พรรษา 5 ธันวาคม 2558"><img  class="modal-img img-thumbnail " src="images/activity/cat3/02/s__14180407.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat3/02/s__14180469.jpg" data-lightbox="data-lightbox3-2" data-title="พิธีถวายสัตย์ปฏิญาณ ในโอกาสวันเฉลิมพระชนมพรรษา 88 พรรษา 5 ธันวาคม 2558"><img  class="modal-img img-thumbnail " src="images/activity/cat3/02/s__14180469.jpg" alt=""/></a>
      </div>
      
  
      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>

<div id="cat3-3" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">พิธีพระราชทานปริญญาบัตร มหาลัยราชภัฏเชียงใหม่</h4>
         <h5 class="modal-title">วันที่ 19 ตุลาคม 2558 บก.ส.3 โดยมี พ.ต.อ.หญิง วนิดา หาญบุญเศรษฐ รอง ผบก.ส.3  พร้อมข้าราชการตำรวจ กก.1 บก.ส.3 ปฏิบัติหน้าที่ถวายความปลอดภัย สมเด็จพระบรมโอรสาธิราชฯ สยามมกุฏราชกุมาร ในวโรกาสเสด็จพระราชดำเนินแทนพระองค์ เนื่องในพิธีพระราชทานปริญญาบัตร มหาวิทยาลัยราชภัฏเขตภาคเหนือ ประจำปี 2558 ระหว่างวันที่ 19-22 ตุลาคม 2558  ณ หอประชุมทีปังกรรัศมีโชติ ศูนย์แม่ริม อ.แม่ริม จ.เชียงใหม่</h5>
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat3/03/749979.jpg" data-lightbox="data-lightbox3-3" data-title="พิธีพระราชทานปริญญาบัตร มหาลัยราชภัฏเชียงใหม่"><img  class="modal-img img-thumbnail " src="images/activity/cat3/03/749979.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat3/03/749981.jpg" data-lightbox="data-lightbox3-3" data-title="พิธีพระราชทานปริญญาบัตร มหาลัยราชภัฏเชียงใหม่"><img  class="modal-img img-thumbnail " src="images/activity/cat3/03/749981.jpg" alt=""/></a>
      </div>

      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>

<div id="cat3-4" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">งานคอนเสิร์ต 12 ปี บ้านพระพร สร้างคนดีสู่สังคม</h4>
         <h5 class="modal-title">วันที่ 4 ตุลาคม  2558   บก.ส.3 โดยมี พ.ต.ท.ชนัญวิทย์ ศักดิ์เชนทร์  สว กก.1 บก.ส.3  พร้อมข้าราชการตำรวจสันติบาล กก.1 บก.ส.3 รวม 5 นาย ปฏิบัติหน้าที่ ถวายความปลอดภัย พระเจ้าวรวงศ์เธอ พระองค์เจ้าโสมสวลี พระวรราชาทินัดดามาตุ ณ โรงเรียนกรุงเทพคริสเตียน กรุงเทพฯ</h5>
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat3/04/8839175.jpg" data-lightbox="data-lightbox3-3" data-title="งานคอนเสิร์ต 12 ปี บ้านพระพร สร้างคนดีสู่สังคม"><img  class="modal-img img-thumbnail " src="images/activity/cat3/04/8839175.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat3/04/8839176.jpg" data-lightbox="data-lightbox3-3" data-title="งานคอนเสิร์ต 12 ปี บ้านพระพร สร้างคนดีสู่สังคม"><img  class="modal-img img-thumbnail " src="images/activity/cat3/04/8839176.jpg" alt=""/></a>
      </div>

      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>

<div id="cat4-1" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">โครงการตำรวจไทยใจสะอาด(จิตอาสา) ณ วัดปทุมวนารามวรมหาวิหาร</h4>
        <!--  <h5 class="modal-title">วันที่ 4 ตุลาคม  2558   บก.ส.3 โดยมี พ.ต.ท.ชนัญวิทย์ ศักดิ์เชนทร์  สว กก.1 บก.ส.3  พร้อมข้าราชการตำรวจสันติบาล กก.1 บก.ส.3 รวม 5 นาย ปฏิบัติหน้าที่ ถวายความปลอดภัย พระเจ้าวรวงศ์เธอ พระองค์เจ้าโสมสวลี พระวรราชาทินัดดามาตุ ณ โรงเรียนกรุงเทพคริสเตียน กรุงเทพฯ</h5> -->
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat4/01/1.jpg" data-lightbox="data-lightbox4-1" data-title="โครงการตำรวจไทยใจสะอาด(จิตอาสา) ณ วัดปทุมวนารามวรมหาวิหาร"><img  class="modal-img img-thumbnail " src="images/activity/cat4/01/1.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat4/01/2.jpg" data-lightbox="data-lightbox4-1" data-title="โครงการตำรวจไทยใจสะอาด(จิตอาสา) ณ วัดปทุมวนารามวรมหาวิหาร"><img  class="modal-img img-thumbnail " src="images/activity/cat4/01/2.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat4/01/4.jpg" data-lightbox="data-lightbox4-1" data-title="โครงการตำรวจไทยใจสะอาด(จิตอาสา) ณ วัดปทุมวนารามวรมหาวิหาร"><img  class="modal-img img-thumbnail " src="images/activity/cat4/01/4.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat4/01/6.jpg" data-lightbox="data-lightbox4-1" data-title="โครงการตำรวจไทยใจสะอาด(จิตอาสา) ณ วัดปทุมวนารามวรมหาวิหาร"><img  class="modal-img img-thumbnail " src="images/activity/cat4/01/6.jpg" alt=""/></a>
      </div>
     

      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>


<div id="cat4-2" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">งานวันเด็กแห่งชาติ ปี 2559 ร่วมกับ บช.ส. ที่ทำเนียบรัฐบาล</h4>
        <!--  <h5 class="modal-title">วันที่ 4 ตุลาคม  2558   บก.ส.3 โดยมี พ.ต.ท.ชนัญวิทย์ ศักดิ์เชนทร์  สว กก.1 บก.ส.3  พร้อมข้าราชการตำรวจสันติบาล กก.1 บก.ส.3 รวม 5 นาย ปฏิบัติหน้าที่ ถวายความปลอดภัย พระเจ้าวรวงศ์เธอ พระองค์เจ้าโสมสวลี พระวรราชาทินัดดามาตุ ณ โรงเรียนกรุงเทพคริสเตียน กรุงเทพฯ</h5> -->
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat4/02/2.jpg" data-lightbox="data-lightbox4-2" data-title="งานวันเด็กแห่งชาติ ปี 2559 ร่วมกับ บช.ส. ที่ทำเนียบรัฐบาล"><img  class="modal-img img-thumbnail " src="images/activity/cat4/02/2.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat4/02/5.jpg" data-lightbox="data-lightbox4-2" data-title="งานวันเด็กแห่งชาติ ปี 2559 ร่วมกับ บช.ส. ที่ทำเนียบรัฐบาล"><img  class="modal-img img-thumbnail " src="images/activity/cat4/02/5.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat4/02/6.jpg" data-lightbox="data-lightbox4-2" data-title="งานวันเด็กแห่งชาติ ปี 2559 ร่วมกับ บช.ส. ที่ทำเนียบรัฐบาล"><img  class="modal-img img-thumbnail " src="images/activity/cat4/02/6.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat4/02/7.jpg" data-lightbox="data-lightbox4-2" data-title="งานวันเด็กแห่งชาติ ปี 2559 ร่วมกับ บช.ส. ที่ทำเนียบรัฐบาล"><img  class="modal-img img-thumbnail " src="images/activity/cat4/02/7.jpg" alt=""/></a>
      </div>
     

      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>


<div id="cat4-3" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ถวายสัตยาบรรณ</h4>
        <!--  <h5 class="modal-title">วันที่ 4 ตุลาคม  2558   บก.ส.3 โดยมี พ.ต.ท.ชนัญวิทย์ ศักดิ์เชนทร์  สว กก.1 บก.ส.3  พร้อมข้าราชการตำรวจสันติบาล กก.1 บก.ส.3 รวม 5 นาย ปฏิบัติหน้าที่ ถวายความปลอดภัย พระเจ้าวรวงศ์เธอ พระองค์เจ้าโสมสวลี พระวรราชาทินัดดามาตุ ณ โรงเรียนกรุงเทพคริสเตียน กรุงเทพฯ</h5> -->
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat4/03/1.jpg" data-lightbox="data-lightbox4-3" data-title="ถวายสัตยาบรรณ"><img  class="modal-img img-thumbnail " src="images/activity/cat4/03/1.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat4/03/3.jpg" data-lightbox="data-lightbox4-3" data-title="ถวายสัตยาบรรณ"><img  class="modal-img img-thumbnail " src="images/activity/cat4/03/3.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat4/03/4.jpg" data-lightbox="data-lightbox4-3" data-title="ถวายสัตยาบรรณ"><img  class="modal-img img-thumbnail " src="images/activity/cat4/03/4.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat4/03/5.jpg" data-lightbox="data-lightbox4-3" data-title="ถวายสัตยาบรรณ"><img  class="modal-img img-thumbnail " src="images/activity/cat4/03/5.jpg" alt=""/></a>
      </div>
     

      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>

<div id="cat4-4" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Big Cleaning Day</h4>
        <!--  <h5 class="modal-title">วันที่ 4 ตุลาคม  2558   บก.ส.3 โดยมี พ.ต.ท.ชนัญวิทย์ ศักดิ์เชนทร์  สว กก.1 บก.ส.3  พร้อมข้าราชการตำรวจสันติบาล กก.1 บก.ส.3 รวม 5 นาย ปฏิบัติหน้าที่ ถวายความปลอดภัย พระเจ้าวรวงศ์เธอ พระองค์เจ้าโสมสวลี พระวรราชาทินัดดามาตุ ณ โรงเรียนกรุงเทพคริสเตียน กรุงเทพฯ</h5> -->
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat4/04/1.jpg" data-lightbox="data-lightbox4-4" data-title="Big Cleaning Day"><img  class="modal-img img-thumbnail " src="images/activity/cat4/04/1.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat4/04/2.jpg" data-lightbox="data-lightbox4-4" data-title="Big Cleaning Day"><img  class="modal-img img-thumbnail " src="images/activity/cat4/04/2.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat4/04/3.jpg" data-lightbox="data-lightbox4-4" data-title="Big Cleaning Day"><img  class="modal-img img-thumbnail " src="images/activity/cat4/04/3.jpg" alt=""/></a>
      </div>
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat4/04/4.jpg" data-lightbox="data-lightbox4-4" data-title="Big Cleaning Day"><img  class="modal-img img-thumbnail " src="images/activity/cat4/04/4.jpg" alt=""/></a>
      </div>
     

      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>


<div id="cat6-1" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ประชุมบริหาร ศพข.บช.ส.</h4>
         <h5 class="modal-title">5 เม.ย.59 เวลา 10.00 น. พ.ต.อ.นพดล  ศรสำราญ  รอง ผบก.ศพข.บช.ส. เป็นประธานในการประชุมบริหาร ศพข.บช.ส. โดยมีข้าราชการตำรวจในสังกัดเข้าร่วมประชุมโดยพร้อมเพรียงกัน</h5> 
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat6/01/original-1461038799486.jpg" data-lightbox="data-lightbox6-1" data-title="ประชุมบริหาร ศพข.บช.ส."><img  class="modal-img img-thumbnail " src="images/activity/cat6/01/original-1461038799486.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat6/01/original-1461038816684.jpg" data-lightbox="data-lightbox6-1" data-title="ประชุมบริหาร ศพข.บช.ส."><img  class="modal-img img-thumbnail " src="images/activity/cat6/01/original-1461038816684.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat6/01/original-1461038830898.jpg" data-lightbox="data-lightbox6-1" data-title="ประชุมบริหาร ศพข.บช.ส."><img  class="modal-img img-thumbnail " src="images/activity/cat6/01/original-1461038830898.jpg" alt=""/></a>
      </div>
      
     

      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>



<div id="cat6-2" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">ประชุมบริหาร ศพข.บช.ส.</h4>
         <h5 class="modal-title">5 เม.ย.59 เวลา 10.00 น. พ.ต.อ.นพดล  ศรสำราญ  รอง ผบก.ศพข.บช.ส. เป็นประธานในการประชุมบริหาร ศพข.บช.ส. โดยมีข้าราชการตำรวจในสังกัดเข้าร่วมประชุมโดยพร้อมเพรียงกัน</h5> 
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat6/02/original-1461039149185.jpg" data-lightbox="data-lightbox6-2" data-title="ประชุมบริหาร ศพข.บช.ส."><img  class="modal-img img-thumbnail " src="images/activity/cat6/02/original-1461039149185.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat6/02/original-1461039164634.jpg" data-lightbox="data-lightbox6-2" data-title="ประชุมบริหาร ศพข.บช.ส."><img  class="modal-img img-thumbnail " src="images/activity/cat6/02/original-1461039164634.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat6/02/original-1461039175435.jpg" data-lightbox="data-lightbox6-2" data-title="ประชุมบริหาร ศพข.บช.ส."><img  class="modal-img img-thumbnail " src="images/activity/cat6/02/original-1461039175435.jpg" alt=""/></a>
      </div>
      
     

      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>
		
<div id="cat6-3" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">กิจกรรมวันสงกรานต์59</h4>
         <h5 class="modal-title">วันที่ 8 เม.ย. 59  เวลา10.00 น. ข้าราชตำรวจทุกนายร่วมรดน้ำขอพรผู้บังคับบัญชา เนื่องในวันวันสงกรานต์</h5> 
      </div>
      <div class="modal-body">
      <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat6/03/original-1461039991278.jpg" data-lightbox="data-lightbox6-3" data-title="กิจกรรมวันสงกรานต์59"><img  class="modal-img img-thumbnail " src="images/activity/cat6/03/original-1461039991278.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat6/03/original-1461040000339.jpg" data-lightbox="data-lightbox6-3" data-title="กิจกรรมวันสงกรานต์59"><img  class="modal-img img-thumbnail " src="images/activity/cat6/03/original-1461040000339.jpg" alt=""/></a>
      </div>
       <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat6/03/original-1461040023151.jpg" data-lightbox="data-lightbox6-3" data-title="กิจกรรมวันสงกรานต์59"><img  class="modal-img img-thumbnail " src="images/activity/cat6/03/original-1461040023151.jpg" alt=""/></a>
      </div>
        <div class="col-md-6">
      	 <a class="example-image-link" href="images/activity/cat6/03/original-1461040035595.jpg" data-lightbox="data-lightbox6-3" data-title="กิจกรรมวันสงกรานต์59"><img  class="modal-img img-thumbnail " src="images/activity/cat6/03/original-1461040035595.jpg" alt=""/></a>
      </div>
      
     

      </div>
  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
      </div>
    </div>

  </div>
</div>
