<?php namespace SB\index;	
if(!isset($_SESSION)) { 
   session_start(); 
}
include  'basepagetype.php';
use SB\Controller\Controller;
include  'Controller/Controller.php';
$Controller = new Controller();
include_once("captcha/simple-php-captcha.php");
$_SESSION['captcha'] = simple_php_captcha();
$ini_array = parse_ini_file("sbpolice.ini");
use SB\conn\factory\App_DaoFactory;
include_once 'conn/factory/factory.php';
?>
<!DOCTYPE html>
<html>
<head>
<?php include $ini_array['contextRoot'].'constant.php';?>
<?php include $ini_array['contextRoot'].'baseurl.php';?>

<!-- <meta charset="UTF-8"> -->
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SBPolice</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/submenu.css" type="text/css" media="screen">

<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/form.js"></script>


</head>
	<body class="background">
	<?php include $ini_array['contextRoot'].'section_header.php';
		  include $ini_array['contextRoot'].'bg.php';
		  $Controller->getImage($page_type_id);
		  $ContactInfoDao = App_DaoFactory::getFactory()->getContactInfoDao();
	   	  $contactInfoM = $ContactInfoDao->loadContactInfo($page_type_id);
	      ?>

	<!-- 	content Body -->
		<div class="container content">
		
		<div class="advertise">
			<div class="row">
			
				<div class="col-md-12 box">
				<div class="col-md-12 " style="margin-bottom: 30px;">
				<?php  if(!empty($contactInfoM)){ ?>
				<iframe class="col-md-12 " style="height:300px;" src="<?php  echo $contactInfoM->getLocation() ?>"></iframe>
				</div>
				 <hr>
				 
				<div class="col-md-6">
					<h4 style="font-size: 24px;">Contact info</h4>
            <div class="map" style="font-size: 24px;">
            
             </div>
            <address style="font-size: 24px;">
            <strong class="clr"><?php echo $contactInfoM->getContactInfo()?></strong><br>
              <!--   <strong class="clr">กองบัญชาการตำรวจสันติบาล สำนักงานตำรวจแห่งชาติ.<br>
อาคาร 20 สำนักงานตำรวจแห่งชาติ, ถนนพระราม 1<br>
แขวงวังใหม่ เขตปทุมวัน กรุงเทพมหานคร, 10330 </strong> --><br>
                <!-- <span>Telephone:</span>02 205 1823-4<br> -->
                <span>Telephone:</span><?php echo $contactInfoM->getTel()?><br>
                E-mail: <a href="#"><u><?php echo $contactInfoM->getEmail()?></u></a>
            </address>
            <?php }?>
					<div style="display: inline-block;">
						<a href="<?php echo $ini_array['contextRoot']?>pdf/tel/Tel_police.pdf">	<img class="img-responsive" src="<?php echo $ini_array['contextRoot']?>images/pic-phone-sb.jpg" alt=""/></a>
						</div>
						<div style="display: inline-block;">
						<a href="<?php $ini_array['contextRoot']?>pdf/tel/Tel2010.pdf">	<img class="img-responsive" src="<?php echo $ini_array['contextRoot']?>images/pic-phone-police.jpg" alt=""/></a>
						</div>
				</div>
				
				<div class="col-md-4">				<h4 style="font-size: 24px;">รายละเอียดผู้ติดต่อ</h4></div>

            <div class="contact-form col-md-4">
              <form name ="ContactForm" id ="ContactForm" method="POST">
               	   <div class="form-group">
					    <input type="text" class="form-control" id="name_contact" placeholder="ชื่อ : " style=" font-size: 20px;" name="name">
				  </div>
				  <div class="form-group">
				    <input type="email" class="form-control" id="email_contact" placeholder="อีเมล : " style=" font-size: 20px;" name="email">
				  </div>
				  <div class="form-group">
					 <input type="text" class="form-control" name="phone"  id="phone_contact" placeholder="โทรศัพท์ : "  maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" style=" font-size: 20px;">
				  </div>
				  <textarea id ="message_contact" name ="message" class="form-control" rows="3" name="remark" placeholder="ข้อความ : " style=" font-size: 20px;"></textarea>
				  <br>
				  <div class="form-group">
				   <?php echo '<img style="margin-left:0;" src="' . $_SESSION['captcha']['image_src'] . '" alt="CAPTCHA code">';?>
				  </div>
				  <div class="form-group">
				  <input type="text" class="form-control" id="captchaCodeContact"  name="captchaCode"  style=" font-size: 20px;" >
				  
				  </div>
				  <input type="hidden"  id ="pagetype"  name="pagetype" maxlength="200" value="<?php echo $page_type_id?>"> 
				<br>
				  	<button id = "sub_contact" type="submit" class="btn btn-default"  style=" font-size: 20px;">ส่ง</button>
				    <button type="reset" class="btn btn-default"  style=" font-size: 20px;">ล้างข้อมูล</button>
				</form>
           </div>
				</div>
			</div>
			</div>
		<?php include $ini_array['contextRoot'].'section_footer.php'; ?>
		</div>
	</body>
</html>