<?php namespace SB;
if(!isset($_SESSION)) {
	session_start();
}

use SB\conn\factory\App_DaoFactory;
include_once 'conn/factory/factory.php';

isset($_POST['name']) ? $name = $_POST['name'] : $name = '';
isset($_POST['email']) ? $email = $_POST['email'] : $email = '';
isset($_POST['phone']) ? $phone = $_POST['phone'] : $phone = '';
isset($_POST['message']) ? $message = $_POST['message'] : $message = '';
isset($_POST['pagetype']) ? $pagetype = $_POST['pagetype'] : $pagetype = '';
isset($_POST['captchaCodeContact']) ? $inputCode = $_POST['captchaCodeContact'] : $inputCode = '';

$emailErr="";
$code = $_SESSION['captcha']['code'];
if($inputCode !=$code){
	$emailErr = "ERROR";
	echo $emailErr;
	exit();
}


if (empty($email)) {
	$emailErr = "Email is required";
	echo $emailErr;
} else {
	//check if e-mail address is well-formed
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$emailErr = "Invalid email format";
		echo $emailErr;
	}
}

if(empty($emailErr)){
	$contactDao = App_DaoFactory::getFactory()->getContactDao();
	$contactDao =$contactDao->insertContact($name,$email,$phone,$message,$pagetype);
}

?>
