<?php 
if(!isset($_SESSION)) { 
   session_start(); 
}
use SB\Controller\Controller;
include  'Controller/Controller.php';
$ini_array = parse_ini_file("sbpolice.ini");
$Controller = new Controller();
include  'basepagetype.php';
?>

<!DOCTYPE html>
<html>
<head>
<?php include $ini_array['contextRoot'].'constant.php';?>
<?php include $ini_array['contextRoot'].'baseurl.php';?>
<meta charset="UTF-8">
<title>SBPolice</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/submenu.css" type="text/css" media="screen">



<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>




</head>
	<body class="background">
		<?php include $ini_array['contextRoot'].'section_header.php'; 
	     include $ini_array['contextRoot'].'section_slider.php';
	     include $ini_array['contextRoot'].'bg.php';
	     ?>
		
		
	<!-- 	content Body -->
	<div class="container content">
		<div class="advertise">
		
			<div class="row">
			<div class="col-md-offset-10" >
					<select name="year" id="year_commander" style="width: 150px">
						    <option value="1">พ.ศ.2475-2535</option>
						     <option value="2">พ.ศ.2535-ปัจจุบัน</option>
					</select>
				</div>
				<div class="col-md-12 box-comandor">
					<div class="comanderViewOld" style="display: block;">
						<?php echo $Controller->getOldCommander('old',1) ?>
					</div>
					<div class="comanderView2535">
						<?php echo $Controller->getOldCommander('2535',1) ?>
					</div>
					 
					
				</div>
			</div>
		</div>
		<script type="text/javascript">
		$(document).ready(function(){ 
			  $('#year_commander').change(function(){ 
				  var value = $('select#year_commander option:selected').val();
				  if(value ==1){
					  $(".comanderViewOld").css("display","block");
					  $(".comanderView2535").css("display","none");
					  
				}
				  else if(value==2){
					  $(".comanderViewOld").css("display","none");
					  $(".comanderView2535").css("display","block");
				}
					
			  });
			});
		
		

		</script>
			<?php include $ini_array['contextRoot'].'section_footer.php'; ?>
		    </div>

	</body>
</html>