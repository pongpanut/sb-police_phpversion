<?php namespace SB\index;	
if(!isset($_SESSION)) { 
   session_start(); 
}
use SB\Controller\Controller;
use SB\conn\factory\App_DaoFactory;
include_once 'conn/factory/factory.php';
include  'Controller/Controller.php';
include_once  'basepagetype.php';
$ini_array = parse_ini_file("sbpolice.ini");
include_once $ini_array['contextRoot'].'basepagetype.php';
$Controller = new Controller();
$page_type_text=$_GET["pagetype"];
$contactDao =App_DaoFactory::getFactory()->getContactDao();

//$pagetype=$_SESSION['privillege'];
$contactDetail = $contactDao->loadContact($page_type_id);
$contactnum= count($contactDetail);
//echo $page_type_id;
?>
<!DOCTYPE html>
<html>
<head>
<!-- <meta charset="UTF-8"> -->
<?php include $ini_array['contextRoot'].'constant.php';?>
<?php include $ini_array['contextRoot'].'baseurl.php';?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<title>SBPolice</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/submenu.css" type="text/css" media="screen">

<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/form.js"></script>


</head>
	<body class="background">
	<?php include $ini_array['contextRoot'].'section_header.php'; 
	include $ini_array['contextRoot'].'bg.php';
	    ?>

	<!-- 	content Body -->
		<div class="container content">
		
		<div class="advertise">
			<div class="row">
				<div class="col-md-12">
					<div class="box">
					 <div class="h1">คำถาม  - คำตอบ</div>
							<hr>
							<ul class="list-1" >
							<?php	
							for ($i = 0; $i < $contactnum; $i++) {?>
									<li class="unwrapclass"><a href="" style="width:100%" data-toggle="modal" data-target="#myModal<?php echo $contactDetail[$i]->getContactId();?>"><?php echo $contactDetail[$i]->getContactMessage()?></a></li>
							
							<?php } ?>
									</ul>
							<?php
						
						?>
			          </div>
				</div>
			</div>
		</div>
	
		</div>
		<?php include $ini_array['contextRoot'].'section_footer.php'; ?>
		
		<!-- Trigger the modal with a button -->
<?php for ($j = 0; $j < $contactnum; $j++) {?>

<!-- Modal -->
<div id="myModal<?php echo $contactDetail[$j]->getContactId();?>" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">คำถาม :<?php echo $contactDetail[$j]->getContactMessage();?></h4>
      </div>
      <div class="modal-body">
        <p>คำตอบ : <?php echo $contactDetail[$j]->getAns();?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
		
	<?php }?>	
	</body>
</html>