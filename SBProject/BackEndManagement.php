<?php namespace SB\index;	
if(!isset($_SESSION)) { 
   session_start(); 
}
use SB\Controller\Controller;
include  'Controller/Controller.php';
$Controller = new Controller();
$page_type_id=$_SESSION['pagetype'];
?>
<!DOCTYPE html>
<html>
<head>
<!-- <meta charset="UTF-8"> -->
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SBPolice</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/submenu.css" type="text/css" media="screen">

<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/form.js"></script>
<script type="text/javascript" src="js/upload.js"></script>


</head>
	<body>

	<!-- 	content Body -->
		<div class="container content">
		
		<div class="advertise">
			<div class="row">
				<div class="col-md-12">
				<ul class="nav nav-tabs">
					  <li class="active"><a data-toggle="tab" href="#home">จัดการ Header Page</a></li>
					  <li><a data-toggle="tab" href="#menu1">Menu 1</a></li>
					  <li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
					</ul>
					
					<div class="tab-content">
					  <div id="Header" class="tab-pane fade in active">
					    <h3 class="text-center" >จัดการ Header Page</h3>
					   <?php $Controller->getHeaderBE($page_type_id)?>
					  </div>
					  <div id="menu1" class="tab-pane fade">
					    <h3>Menu 1</h3>
					    <p>Some content in menu 1.</p>
					  </div>
					  <div id="menu2" class="tab-pane fade">
					    <h3>Menu 2</h3>
					    <p>Some content in menu 2.</p>
					  </div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</body>
</html>