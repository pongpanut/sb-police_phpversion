<?php 
use SB\conn\factory\App_DaoFactory;
include_once 'conn/factory/factory.php';


$pageTypeMappingDao = App_DaoFactory::getFactory()->getPageTypeMappingDao();
$page_type_text =$pageTypeMappingDao->loadpagetypemappingText($page_type_id);
?>

<div class="container header">
			<div class="row">
				<?php include $ini_array['contextRoot'].'header.php';?>
			</div>
			<div class="row">
				<div class="col-md-12 col-xs-12 navMenu ">
								<div class="col-md-offset-7 col-xs-offset-5">
										<div class=" login-menu">
										<!-- navbar-manual-login-->
												<nav class="navbar navbar-manual-login"  style="/* background-color: 255, 255, 255, 0.6; */border-style: solid;border-width: 2px;border-color: #820707;border-radius: 16px 16px 0px 0px;margin: 0px -15px 0px 0px;    background-image: none;">
												  <div>
												    <div class="navbar-header">
												    
												      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar1">
												        <span class="icon-bar"></span>
												        <span class="icon-bar"></span>
												        <span class="icon-bar"></span>                        
												      </button>
												    </div>
												    <div class="collapse navbar-collapse" id="myNavbar1">
												      <ul class="nav navbar-nav form_login">
												        	<?php include $ini_array['contextRoot'].'form_login.php';?>
												      </ul>
												      <ul class="nav navbar-nav">
												        <li style="border-right: 1px solid #a20f0f; border-left: 1px solid #a20f0f; border-image: linear-gradient(to bottom, white, rgba(000, 000,000, 000)) 3 100%;" ><a style="font-size: 20px;color:white;" href="<?php echo $ini_array['contextRoot'].$page_type_text ?>/contacts.php"><span style="color:white;" class="glyphicon glyphicon-log-in"></span>  ติดต่อเรา</a></li>
												      </ul>
												      <ul class="nav navbar-nav">
												     
												        <li><a style="font-size: 20px;color:white; " href="<?php echo $ini_array['contextRoot'].$page_type_text ?>/webboard.php"><span style="color:white;" class="glyphicon glyphicon-list-alt"></span> <i class="fa fa-bullhorn"></i>คำถาม - คำตอบ</a></li>
												      </ul>
												      <ul class="nav navbar-nav">
												        <li>
												        	
												        <a style="font-size: 20px;color:white;" class="dropdown-toggle" data-toggle="dropdown"><span style="color:white;" class="glyphicon glyphicon-user"></span>ความพึงพอใจ</a>
												        <div class="dropdown-menu poll" style="width: 230px; height: 180px;" >
															<form method="post" accept-charset="UTF-8" id="pollForm">
																<input type="hidden" name="page_type_id" id="page_type_id" value="<?php echo $page_type_id ?>"> 
																  <input type="radio" name="poll" value="3" checked> พึงพอใจมาก<br>
																  <input type="radio" name="poll" value="2"> พึงพอใจปานกลาง<br>
																  <input type="radio" name="poll" value="1"> พึงพอใจน้อย<br>
																<input class="btn btn-block" type="submit" id="pollVote" value="Vote" style="color: black;">
															</form>
												        </div>
												        
												        
												        </li>
												      </ul>
												      
												      <ul class="social-icons">
									                        <li><a href="#"><img src="images/icon-1.png" alt=""></a></li>
									                        <li><a href="#"><img src="images/icon-2.png" alt=""></a></li>
									                        <li><a href="#"><img src="images/icon-3.png" alt=""></a></li>
									                        <li><a href="#"><img src="images/icon-4.png" alt=""></a></li>
								                        </ul>
												    </div>
												  </div>
												</nav>
											</div>
									</div>
						</div>
			
			</div>
			<?php include $ini_array['contextRoot'].'menu.php';?>
		</div><!-- end header -->