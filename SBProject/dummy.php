<?php namespace SB;
use SB\conn\factory\App_DaoFactory;
include_once 'conn/factory/factory.php';

$email = $_POST['email'];

if (empty($email)) {
	$emailErr = "Email is required";
	echo $emailErr;
} else {
	//check if e-mail address is well-formed
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$emailErr = "Invalid email format";
		echo $emailErr;
	}
}

if(empty($emailErr)){
	$newsDao = App_DaoFactory::getFactory()->getSubscript();
	$newsArr =$newsDao->Insertemail($email);
}

?>
