<?php 
if(!isset($_SESSION)) { 
   session_start(); 
}
use SB\Controller\Controller;
include  'Controller/Controller.php';
$Controller = new Controller();
include  'basepagetype.php';
$ini_array = parse_ini_file("sbpolice.ini");
?>
<!DOCTYPE html>
<html>
<head>
<?php include $ini_array['contextRoot'].'constant.php';?>
<?php include $ini_array['contextRoot'].'baseurl.php';?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SBPolice</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/submenu.css" type="text/css" media="screen">



<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>




</head>

	<body class="background">
		<?php include $ini_array['contextRoot'].'section_header.php'; 
		include $ini_array['contextRoot'].'bg.php';
		include $ini_array['contextRoot'].'section_slider.php';?>
		
	<!-- 	content Body -->
		<div class="container content">
			
		
		<div class="vision">
		
			<div class="row ">
				<div class="col-md-4 col-md-offset-4 h1 text-center">วิสัยทัศน์</div>
				<div class="col-md-12 h2 text-center">กองบัญชาการตำรวจสันติบาล</div>
				<div class="col-md-12 h2 text-center">“ เป็นเลิศด้านข่าวกรอง ปกป้องสถาบัน ”</div>
				<div class="col-md-12 h2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;พันธกิจ (Mission)</div>
				<div class="col-md-12 detail"> 
					<ul>
						<ol>1. ถวายความปลอดภัยสำหรับองค์พระมหากษัตริย์ พระราชินี พระรัชทายาท ราชการแทนพระองค์ พระบรมวงศานุวงศ์ ผู้แทนพระองค์ และพระราชอาคันตุกะผู้สำเร็จ</ol>
						<ol>2. ดำเนินการเกี่ยวกับการข่าวกรองบุคคลหรือกลุ่มบุคคลที่มีพฤติการณ์เป็นภัยต่อความมั่นคงของประเทศ</ol>
						<ol>3. ดำเนินการเกี่ยวกับงานดำเนินกรรมวิธีข่าวกรองให้สำนักงานตำรวจแห่งชาติ และเป็นศูนย์กลางในการบูรณาการการปฏิบัติตามยุทธศาสตร์ความมั่นคงแห่งชาติของสำนักงานตำรวจแห่งชาติ</ol>
						<ol>4. ดำเนินการเกี่ยวกับงานด้านการรักษาความปลอดภัยของบุคคลสำคัญและสถานที่ที่เกี่ยวข้องกับความมั่นคงของประเทศ</ol>
						<ol>5. ดำเนินการตามกฎหมายว่าด้วยสัญชาติ และกฎหมายอื่นที่เกี่ยวข้องกับความมั่นคงของประเทศตามอำนาจหน้าที่</ol>
						<ol>6. ดำเนินการเกี่ยวกับงานวิชาการ งานมาตรฐานการข่าว และงานฝึกอบรมเพื่อพัฒนาบุคลากรในด้านการข่าวของสำนักงานตำรวจแห่งชาติ</ol>
						<ol>7. ดำเนินการเกี่ยวกับงานสถานีวิทยุกระจายเสียงของกองบัญชาการตำรวจสันติบาล</ol>
					</ul>
				</div>
			<div class="col-md-12 h2"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เป้าหมายประสงค์หลักขององค์กร </div>
			<div class="col-md-12 detail"> 
				<ul>
					<ol>1. เทิดทูนและพิทักษ์รักษาไว้ซึ่งสถาบันพระมหากษัตริย์ ดำรงรักษาไว้ซึ่งพระบรมเดชานุภาพแห่งพระมหากษัตริย์มิให้ผู้ใดล่วงละเมิดได้ รักษาความปลอดภัยอย่างเป็นระบบและมีมาตรฐานสากล</ol>
					<ol>2. เป็นศูนย์กลางการบูรณาการสร้างความร่วมมือและแนวร่วมกับทุกภาคส่วนด้านความมั่นคงของชาติ เพื่อเพิ่มขีดความสามารถตอบสนองต่อการเปลี่ยนแปลงของภัยคุกคาม</ol>
					<ol>3. เสริมสร้างระบบคุณธรรม ธรรมาภิบาล บริหารจัดการ</ol>
				</ul>
			
			
			</div>
			
			</div>
		</div>
		</div>
		<?php include $ini_array['contextRoot'].'section_footer.php'; ?>

	</body>
</html>