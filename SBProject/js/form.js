$(document).ready(function() {    
    $("#subscribe_but").click(function(e) {
        e.preventDefault();
        submitSubscript();
    });
    $("#sign-in").click(function() {
    	submitLogin();
    });
    
    $("#sign-out").click(function() {
    	submitLogout();
    });
    
    $("#admin_page").click(function() {
    	 window.location = 'AA/production/indexBOM.php';
    });
    
    $("#sub_contact").click(function() {
    	submitContact();
	});
    
    $("#subscribe_delbut").click(function() {
    	delContact();
	});
    $("#pollVote").click(function() {
    	pollVote();
	});
    
    
    
});

function submitSubscript()
{
	var email = $('#email').val();
	if(email == ''){
		return 'กรุณากรอก email';
	}
	
    jQuery.ajax({
        type: "POST",
        url: 'form_subscript.php',
        data: {email: email}, 
         success:function(data) {
        	 alert(data);
        	 location.reload();
         }
    });
}

function delContact()
{
	var email = $('#email').val();
	if(email == ''){
		return 'กรุณากรอก email';
	}
	
    jQuery.ajax({
        type: "POST",
        url: 'form_delsubscript.php',
        data: {email: email}, 
         success:function(data) {
        	 //alert(data);
        	 location.reload();
         }
    });
}

function submitContact()
{
	var name = $('#name_contact').val();
	var email = $('#email_contact').val();
	var phone = $('#phone_contact').val();
	var message = $('#message_contact').val();
	var pagetype = $('#pagetype').val();
	var captchaCodeContact = $('#captchaCodeContact').val();
    jQuery.ajax({
        type: "POST",
        url: 'insert_contact.php',
        data: {name: name, email: email, phone:phone, message:message,pagetype:pagetype,captchaCodeContact:captchaCodeContact}, 
         success:function(data) { 
        	if(data!=""){
        		if(data=="ERROR"){
       			 alert("กรุณาใส่ตัวเลขหรือตัวอักษรภาพให้ถูกต้อง");
       			return false;
       		 	}
        		alert(data);
        	}
        	window.location.reload();
         }
    });
}

function submitLogin()
{
	var user = $('#user').val();
	var pass = $('#pass').val();
	var captchaCode = $('#captchaCode').val();
    jQuery.ajax({
        type: "POST",
        url: 'login.php',
        data: {user: user, pass: pass,captchaCode:captchaCode}, 
         async:    false,
         success:function(data) {
        	 if(data != ""){
        		 if(data=="ERROR"){
        			 alert("กรุณาใส่ตัวเลขหรือตัวอักษรภาพให้ถูกต้อง");
        			 return false;
        		 }
        		 window.location  = data;
        	 }
        	 else{
        		 window.location.reload();
        	 }
         },
         error: function(xhr,textStatus,err)
		{
   			 console.log("readyState: " + xhr.readyState);
   			 console.log("responseText: "+ xhr.responseText);
   			 console.log("status: " + xhr.status);
   			 console.log("text status: " + textStatus);
   			 console.log("error: " + err);
		}
    });
}

function submitLogout()
{
	$.ajax({
        url: 'logout.php',
        type: 'get',
        data:{action:'logout'},
        async:    false,
        success: function(data){
        	location.reload();
        }
    });
}

function pollVote(){
	var poll =$('input[name=poll]:checked').val();
	var page_type_id =$('#page_type_id').val();
	$.ajax({
        url: 'poll.php',
        type: 'POST',
        data:{poll:poll , page_type_id:page_type_id},
        async:    false,
        success: function(data){
        	//console.log(data);
        	location.reload();
        }
    });
	
}

