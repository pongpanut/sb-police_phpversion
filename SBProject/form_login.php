<?php include_once("captcha/simple-php-captcha.php");
$_SESSION['captcha'] = simple_php_captcha();
$ini_array = parse_ini_file("sbpolice.ini");
?>
<li class="dropdown">
	<?php 
	if(isset($_SESSION['logged'])){ 
		if(isset($_SESSION['username'])){?>
			<a class="dropdown-toggle" style="font-size: 20px;color:white" href="<?php echo $ini_array['contextRoot']?>" data-toggle="dropdown"><span style="color:white;"  class="glyphicon glyphicon-user"></span><?php echo $_SESSION["username"];?></a>
			<div class="dropdown-menu login" style="padding: 10px;">
			  	<button id="admin_page" style="font-size: 20px;color:white; width: 100%;" class="btn btn-success" >ไปยังหน้าส่วนผู้ควบตุม </button>
				<button id="sign-out" style="margin-top:15px; font-size: 20px;width: 100%;">ออกจากระบบ</button>
  			</div>		
			<?php 
		}
	}
	else {?>
        <a style="font-size: 20px;color:white;" class="dropdown-toggle" data-toggle="dropdown"><span style="color:white;" class="glyphicon glyphicon-user"></span>  เข้าสู่ระบบ</a>
        <div class="dropdown-menu login" style="width: 230px; height: 300px;" >
			<form  method="post" accept-charset="UTF-8">
				<input style="margin-bottom: 15px; width: 100%; height: 40px; font-size: 24px;" type="text" pattern="[a-zA-Z0-9!@#$%^*_|]{3,25}" placeholder="Username" id="user" name="user" required>
				<input style="margin-bottom: 15px; width: 100%; height: 40px;  font-size: 24px;" type="password" pattern="[a-zA-Z0-9!@#$%^*_|]{3,25}" placeholder="Password" id="pass" name="pass" required>
				<div class="form-group">
				   <?php echo '<img src="' . $_SESSION['captcha']['image_src'] . '" alt="CAPTCHA code" style="height: 40px;">';?>
				  </div>
				 <input style="margin-bottom: 15px; width: 100%; height: 40px;  font-size: 24px;" type="text" id="captchaCode" name="captchaCode" required>
				<!-- <input style="float: left; margin-right: 10px;" type="checkbox" name="remember-me" id="remember-me" value="1">
				<label class="string optional" for="user_remember_me" style="color:white;"> จำการเข้าสู่ระบบ</label>-->
				<input class="btn btn-block" type="submit" id="sign-in" value="Sign In">
			</form>
		</div>
    <?php } ?>
</li>