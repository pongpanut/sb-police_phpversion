<?php namespace SB\index;	
if(!isset($_SESSION)) { 
   session_start(); 
}
use SB\Controller\Controller;
include  'Controller/Controller.php';
include  'basepagetype.php';
$Controller = new Controller();
$ini_array = parse_ini_file("sbpolice.ini");
?>
<!DOCTYPE html>
<html>
<head>
<?php include $ini_array['contextRoot'].'constant.php';?>
<?php include $ini_array['contextRoot'].'baseurl.php';?>
<!-- <meta charset="UTF-8"> -->
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SBPolice</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/submenu.css" type="text/css" media="screen">

<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/form.js"></script>s


</head>
	<body class="background">
	<?php include $ini_array['contextRoot'].'section_header.php'; 
	    include $ini_array['contextRoot'].'section_slider.php';
	    include $ini_array['contextRoot'].'bg.php';?>

	<!-- 	content Body -->
		<div class="container content">
		
		<div class="advertise">
			<div class="row">
				<div class="col-md-12 ">
					<div class="box">
						<div class="h1">ข้อมูลการติดต่อ บช.ส</div>
						<div>
							<span>
							<h1 style="color: white">กองบัญชาการตำรวจสันติบาล</h1><br>
							
							อาคาร 20 สำนักงานตำรวจแห่งชาติ<br>
							ถนนพระราม 1 เขตปทุมวัน กรุงเทพ 10330<br>
							
							โทรศัพท์กลาง : 02-205-1823-4<br>
							เวปไซต์ : www.sbpolice.go.th<br>
							</span>
							<!-- <img class="img-responsive" src="images/pic-contractus.jpg" alt=""/> -->
						</div>
						<div class="row" style="margin-top: 20px; margin-left:35%">
						<div style="display: inline-block;">
						<a href="<?php echo $ini_array['contextRoot']?>pdf/tel/Tel_police.pdf">	<img class="img-responsive" src="<?php echo $ini_array['contextRoot']?>images/pic-phone-sb.jpg" alt=""/></a>
						</div>
						<div style="display: inline-block;">
						<a href="<?php echo $ini_array['contextRoot']?>pdf/tel/Tel2010.pdf">	<img class="img-responsive" src="<?php echo $ini_array['contextRoot']?>images/pic-phone-police.jpg" alt=""/></a>
						</div>
						</div>
			          </div>
				</div>

				
			</div>
		</div>
		<?php include $ini_array['contextRoot'].'section_footer.php'; ?>
		</div>
	</body>
</html>