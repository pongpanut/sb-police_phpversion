<?php namespace SB\view;

use SB\conn\factory\App_DaoFactory;
$ini_array = parse_ini_file("sbpolice.ini");
include_once 'conn/factory/factory.php';
	$menuDao = App_DaoFactory::getFactory()->getMenu();
	$menuArr =$menuDao->loadMenu($page_type);
	
	$deptDao = App_DaoFactory::getFactory()->getDepartmentDao();
	$deptArr =$deptDao->loadDepartmentList($page_type);
	
	$pageTypeMappingDao = App_DaoFactory::getFactory()->getPageTypeMappingDao();
	$page_type_text =$pageTypeMappingDao->loadpagetypemappingText($page_type);
	
	$num=count($menuArr);
	$numdept=count($deptArr);
	if($num >0){
		?>
			<div class="row">
				<nav class="navbar navbar-manual-menu">
					  <div class="container-fluid" style="padding-right: 0px;">
					   <a class="navbar-brand" href="<?php echo $ini_array['contextRoot'].$page_type_text?>/index.php"><span class="glyphicon glyphicon-home" style="color: #ffefef; margin-top: -3px"></span></a>
					   <div class="navbar-header">
					      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>             
					      </button>
					     
					    </div>
					    <div class="collapse navbar-collapse" id="myNavbar"  style=" margin-right: 0; padding:right:0px;">
					      <ul class="nav navbar-nav">
					      
					     <?php for ($j=0;$j<$num;$j++){?>
					         <li class="dropdown" ">
					          	<a style=" display: inline-block;"<?php
							    if($menuArr[$j]->getMenuHasSubmenu()>0){
							    	?>class="dropdown-toggle" data-toggle="dropdown" <?php	
								}
								else{
									if($menuArr[$j]->getMenuLink() != ''){
									?>href="<?php echo $menuArr[$j]->getMenuLink();?>"<?php
									}
								}

					          	?>><?php echo $menuArr[$j]->getMenuText() ?>
								<?php
							    if($menuArr[$j]->getMenuHasSubmenu()>0){
							    	?><span class="caret"><?php	
								}
					          	?>
					          	</span> </a>
					
								 <?php
								 if($menuArr[$j]->getMenuIsDepartment() !=  1){
									  $submenuArr =$menuDao->loadSubMenu($menuArr[$j]->getMenuId());
									  $countrow=count($submenuArr);
									  if($countrow >0){ ?>
									  	<ul class="dropdown-menu">
									     <?php	for ($h=0;$h<$countrow;$h++){?> 
						          			<li>

										<?php if($submenuArr[$h]->getSubmenuHasMenuList()>0){?>
						          			<a ><?php echo $submenuArr[$h]->getSubMenuText() ?> </a>
						          		<?php }else{
						          		if($submenuArr[$h]->getSubMenuLink() != ''){?>
						          			<a href="<?php echo $submenuArr[$h]->getSubMenuLink()?>"><?php echo $submenuArr[$h]->getSubMenuText() ?> </a>
						          		<?php }
						          		else{?><a><?php echo $submenuArr[$h]->getSubMenuText() ?> </a><?php 
						          		}?>
						          		<?php }?>
						          		
						          					<?php if($submenuArr[$h]->getSubmenuHasMenuList()>0){
						          						$submenulistArr =$menuDao->loadSubMenuList($submenuArr[$h]->getSubMenuId());	
						          						$countlistrow=count($submenulistArr); 
						          						?><ul><?php
						          						if($countlistrow >0){ 
						          							for ($x=0;$x<$countlistrow;$x++){	
						          								?>
						          							<li><a href="<?php echo $submenulistArr[$x]->getSubmenuListLink()?>"><?php echo $submenulistArr[$x]->getSubmenuListText()?></a></li>
						          							<?php
						          							}
						          						}		
						          						?></ul><?php
						          					}?>


						          			</li>						          		 
						                 <?php }?>
						                </ul>
						        <?php }}else{
						        	if($numdept > 0){?>
						        		<ul class="dropdown-menu"><?php 
						        		for ($b=0;$b<$numdept;$b++){
						        			if($deptArr[$b]->getDeptListLink() != ''){
						        				?><li><a href="<?php echo $deptArr[$b]->getDeptListLink()?>"><?php echo $deptArr[$b]->getDeptListText()?></a>
													<?php 
													//$deptArr =$deptDao->loadDepartmentList($page_type);
										
															$deptsubArr =$deptDao->loadSubDepartmentList($deptArr[$b]->getDeptListId());
															$countdeptsubArr=count($deptsubArr);
															?><ul><?php
							          						if($countdeptsubArr >0){ 
							          							for ($x=0;$x<$countdeptsubArr;$x++){	
							          								?>
							          							<li><a href="<?php echo $deptsubArr[$x]->getDepSubLink()?>"><?php echo $deptsubArr[$x]->getDeptSubText()?></a></li>
							          							<?php
							          							}
							          						}		
							          						?></ul><?php
															
													?>						        				
						        				
						        				</li><?php 
							        		}else{
							        			?><li><a href="#"><?php echo $deptArr[$b]->getDeptListText()?></a></li><?php
							        		}
						        		}
						        		?></ul><?php					        		
						        	}
						        }?>					         
					        </li>
					       <!--  <li><span style="color: white;display: inline;">|</span></li> -->
					     <?php  }?>
						 </ul>
					    </div>
					  </div>
					</nav>
				</div>
		<?php 

	} 
?>




