<?php namespace SB\view;
use SB\conn\factory\App_DaoFactory;
include_once 'conn/factory/factory.php';
$newsDao = App_DaoFactory::getFactory()->getNews();
$news =$newsDao->loadNewsById($news_id,$page_type_id);

if(count($news) > 0){
	?>
	<div class="advertise">
			<div class="row">
				<div class="col-md-12 box">
								<div class="row">
									<div class="col-md-12 h1"><img class="img-thumbnail" alt="Responsive image" src="images/icon/icon-sb.gif">&nbsp; &nbsp;	<?php echo $news->getNewsHeader()?></div>
								</div>
								<!-- <div class="row ">
										<img class="img-responsive" alt="Responsive image" src="images/non-news.gif">
								</div> -->
								<div>
								<p><?php echo $news->getNewsDetail();?></p>
								
								</div>
								<?php if($news->getNewsPdf()!=""){?>
								<div class="row download text-center">
									<span class="glyphicon glyphicon-download" style="color: #7adc7a;top: 5px;"></span> <a style="color: #e1f0fd;font-size: 30px;transition: all 1.2s ease; "href="<?php echo  $news->getNewsPdf() ?>">ดาวน์โหลดไฟล์แนบ</a>
								</div>
								<?php }?>
								<div>
								<p style="color:white; float: right;font-size: 20px;">เวลาแก้ไข <?php echo $news->getRecDate();?></p>
								</div>
				
				</div>
			</div>
	</div>
	
	<?php
	
}
?>
