<?php namespace SB\view;
use SB\conn\factory\App_DaoFactory;
include_once 'conn/factory/factory.php';
$missionDao = App_DaoFactory::getFactory()->getMission();
$missionArr =$missionDao->loadMissionDetail($missionId,$page_type_id);
$constantDao = App_DaoFactory::getFactory()->getConstantDao();
$default_image =	$constantDao->loadconstant("default_image","1");

if(count($missionArr) > 0){
	
	$imageDao = App_DaoFactory::getFactory()->getImgDao();

	$imageM = $imageDao->loadImgById($missionArr->getMissionImage(),$page_type_id);
	
	?>
	<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen">
				<script type="text/javascript" src="js/lightbox.js"></script>
	<div class="advertise">
			<div class="row">
				<div class="col-md-12 box">
					<div class="row">
						<div class="col-md-12 h1"> <?php echo $missionArr->getMissionHeader() ?></div>
					</div>
					<div class="row">
						<div class="col-md-12 h3"> <?php echo $missionArr->getMissionText() ?></div>
					</div>
					<div class="row">
						<div class="col-md-12 col-md-offset-10 h4" style="color:white;">เวลาแก้ไข   :  <?php echo $missionArr->getRecDate() ?></div>
					</div>
				</div>
				
				
				<?php
				$missionImgDao =App_DaoFactory::getFactory()->getMissionImgDao();
				$missionImgArr = 	$missionImgDao->getMissionImgByRefId($missionArr->getMissionId(),$page_type_id);
				if(!empty($missionImgArr)){
					?>
					<br>
					</div>
				<div class="row">
				<div class="col-md-12 box" style="margin-top: 10px;padding-bottom: 20px">
					<div class="col-md-4 col-md-offset-4 text-center h1"> รูปถ่ายกิจกรรม </div>
					<div class="row" style="display: inline-block">
					<?php
					for ($i = 0; $i < count($missionImgArr); $i++) {
					?>
					<div class="col-md-2 col-xs-12 text-center " style="margin-top: 20px">
					     <a class="example-image-link" href="<?php echo $missionImgArr[$i]->getImagePaht()?>" data-lightbox="data-lightbox" data-title= "แก้ไขเมื่อ :  <?php echo $missionImgArr[$i]->getRecDate() ?>"><img  class="img-thumbnail" style="" src="<?php echo $missionImgArr[$i]->getImagePaht()?>" alt=""/></a>
					 </div> 
					<?php 
					}?>
					</div>
				</div>
				<?php }?>
			</div>		
			
	</div>
	<?php
}
?>
