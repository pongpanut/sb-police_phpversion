<?php namespace SB\view;
use SB\conn\factory\App_DaoFactory;
include_once 'conn/factory/factory.php';
$imageDao = App_DaoFactory::getFactory()->getImgDao();
$imageArr =$imageDao->loadImg($commanderType,$page_type_id);
$num= count($imageArr);
	if($num > 0){
		
		?>
		
				
					<div class="row">
							<div class="col-md-12">
							<div class="row header-title">
							<?php if($commanderType=="old"){
								?><div class="h1 col-md-4 col-md-offset-4 text-center">ผู้บัญชาการ สำนักงานตำรวจสันติบาล</div>
									<div class="h2 col-md-4 col-md-offset-4 text-center" style="color: #fff">(ระหว่าง พ.ศ. ๒๔๗๕ - ๒๕๓๕)</div>
								<?php
							}
							else if($commanderType=="2535"){
								?><div class="h1 col-md-4 col-md-offset-4 text-center">ผู้บัญชาการ สำนักงานตำรวจสันติบาล</div>
									<div class="h2 col-md-4 col-md-offset-4 text-center" style="color: #fff">( ระหว่าง พ.ศ. ๒๕๓๕  - ปัจจุบัน)</div>
								<?php
							}
							?>
							</div>
							<?php  for ($i = 0; $i < $num; $i++){
							    	$box_index =$imageArr[$i]->getImgpath();
							    	$box_name=$imageArr[$i]->getImgname();
							    	$box_desc=$imageArr[$i]->getImgdesc()
							?>
									<div class="col-md-3 ">
										<div class="col-md-12 box-img">
											<div class="col-md-12 text-center"><img id="img-responsive" style="width: 100%;" src="<?php echo $box_index ?>" class="img-responsive" alt="Responsive image"></div>
											<div class="col-md-12 text-center"><?php echo $box_name?></div>
											<div class="col-md-12 text-center"><?php echo $box_desc?></div>
										</div>
									</div>
								<?php }?>
									
							</div>
					</div>
				
			
		<?php	
		}
?>