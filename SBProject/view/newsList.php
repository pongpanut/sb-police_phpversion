<?php namespace SB\view;
use SB\conn\factory\App_DaoFactory;
use SB;
include_once 'conn/factory/factory.php';
include_once  'basepagetype.php';
$ini_array = parse_ini_file("sbpolice.ini");
$news_code=$_GET['index'];
$page_type_text=$_GET["pagetype"];
$newsDao = App_DaoFactory::getFactory()->getNews();
$newsArr =$newsDao->loadNews($news_code,$page_type_id);
if (!function_exists('DateThai')) {
	function DateThai($strDate)
	{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear, $strHour:$strMinute";
	}
}

if(!empty($newsArr)){
	$num =count($newsArr);
	if($news_code ==1){
		?><div class="h1">ข่าวประชาสัมพันธ์</div><?php
	}
	else{
		?><div class="h1">ประกาศการจัดซื้อจัดจ้าง</div><?php
	}
	?>
	
	<hr>
	<ul class="list-1" >
	<?php	
	for ($i = 0; $i < $num; $i++) {
			if($newsArr[$i]->getIsHighLight() == 0){?>
						  <li class="unwrapclass"><a href="<?php echo $ini_array['contextRoot'].$page_type_text ?>/<?php echo $newsArr[$i]->getNewsId()?>/news_detail.php" style="width:100%"><?php echo $newsArr[$i]->getNewsHeader()?>  </a> <span style="font-size: 20px; color: white; "><small><i style="color: #e2dbdb;">[ <?php echo DateThai($newsArr[$i]->getRecDate())?> ]</i></small></span></li>
					<?php } else {?>
					      <li class="unwrapclass"><a href="<?php echo $ini_array['contextRoot'].$page_type_text?>/<?php echo $newsArr[$i]->getNewsId()?>/news_detail.php" style="width:100%;color:#ff4b00;font-weight: bold;"><?php echo $newsArr[$i]->getNewsHeader()?><i class="fa fa-camera-retro"></i>  <span class="glyphicon glyphicon-star" style="font-size: 14px; color:#F71204;" aria-hidden="true"> </span></a><small><i style="color: #e2dbdb;">[ <?php echo DateThai($newsArr[$i]->getRecDate())?> ]</i></small> </li>
					<?php } } ?>
			</ul>
	<?php
}
?>