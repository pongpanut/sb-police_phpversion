<?php
use SB\conn\factory\App_DaoFactory;
include_once 'conn/factory/factory.php';
$ini_array = parse_ini_file("sbpolice.ini");
include $ini_array['contextRoot'].'constant.php';
$missionDao = App_DaoFactory::getFactory()->getMission();
$missionArr1 =$missionDao->loadMissionType(1,$page_type_id);
$missionArr2 =$missionDao->loadMissionType(2,$page_type_id);
$constantDao = App_DaoFactory::getFactory()->getConstantDao();
$pageTypeMappingDao = App_DaoFactory::getFactory()->getPageTypeMappingDao();
$page_type_text =$pageTypeMappingDao->loadpagetypemappingText($page_type_id);





$num = count($missionArr1) ;
$num2 = count($missionArr2) ;
	if($num >0){
		
			?>
	<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen">
				<script type="text/javascript" src="js/lightbox.js"></script>
	<div class="row">
		<div class="col-md-6">
				<div class="col-md-12 "> 
					<div class="headerText h1"><a href="<?php echo $ini_array['contextRoot'].$page_type_text?>/1/mission.php">ภารกิจผู้บริหาร</a></div>
				</div>
			<div class="col-md-12 ">
				<?php 
				if($num > 2){
					$num = 2;
				}
				 for( $i = 0; $i<$num; $i++ ) {
				 	$missionImgDao =App_DaoFactory::getFactory()->getMissionImgDao();
					$missionImgArr = 	$missionImgDao->getMissionImgByRefId($missionArr1[$i]->getMissionId(),$page_type_id);
					if(!empty($missionImgArr)){
					 	?>
					 	<!-- Modal -->
							<div class="modal fade" id="myModal1<?php echo $missionArr1[$i]->getMissionId() ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							        <h4 class="modal-title" id="myModalLabel"><?php echo $missionArr1[$i]->getMissionHeader()?></h4>
							      </div>
							      <div class="modal-body">
							        <?php 
						     
						       for ($l = 0; $l < count($missionImgArr) ; $l++) {
						       	?>
					       		 <div class="col-md-6 col-xs-6">
					      	 				<a class="example-image-link" href="<?php echo $missionImgArr[$l]->getImagePaht()?>" data-lightbox="data-lightbox" data-title= "แก้ไขเมื่อ :  <?php echo $missionImgArr[$l]->getRecDate() ?>"><img  class=" modal-img img-thumbnail" src="<?php echo $missionImgArr[$l]->getImagePaht()?>" alt=""/></a>
					      		</div> 	
						       	<?php
						       }
						       ?>
							      </div>
							      <div class="modal-footer">
							        
							      </div>
							    </div>
							  </div>
							</div>
					 	<?php
					}

					 $imageDao =App_DaoFactory::getFactory()->getImgDao();
					 if($missionArr1[$i]->getMissionImage()!=""){
						 $image =  $imageDao->loadImgById($missionArr1[$i]->getMissionImage(),$page_type_id);
						for ($j = 0; $j< count($image); $j++) {
						?>
						 <div class="col-md-6" style="border-right: 1px solid #d0d2d3;">
							<div>
								<a href="#" data-toggle="modal" data-target="#myModal1<?php echo $missionArr1[$i]->getMissionId() ?>"><img class="img-thumbnail" alt="Responsive image" src="<?php echo $image[$j]->getImgpath()?>" style="height: 150px;width: 220px"></a>
							</div>
							<p class="h1"><?php echo $missionArr1[$i]->getMissionHeader() ?></p>
							<span class="h2"><?php echo $missionArr1[$i]->getMissionDesc() ?><a href="<?php echo $ini_array['contextRoot'].$page_type_text?>/<?php echo $missionArr1[$i]->getMissionId()?>/missionDetail.php">  >></a></span>
						</div>
					<?php 
						}
					}
					else{
						$default_image =	$constantDao->loadconstant("default_image","1");
						?>
						 <div class="col-md-6" style="border-right: 1px solid #d0d2d3;">
							<div>
								<img class="img-thumbnail" alt="Responsive image" src="<?php echo $default_image ->getConsName()?>" style="height: 150px;width: 220px">
							</div>
							<p class="h1"><?php echo $missionArr1[$i]->getMissionHeader() ?></p>
							<span class="h2"><?php echo $missionArr1[$i]->getMissionDesc() ?><a href="<?php echo $ini_array['contextRoot'].$page_type_text?>/<?php echo $missionArr1[$i]->getMissionId()?>/missionDetail.php">  >></a></span>
						</div>
						
						<?php
					}
				}
				?>
					
		</div>
	</div>
			<?php }
			else{
				?>
		<div class="col-md-6">
				<div class="col-md-12 "> 
					<div class="headerText h1"><a href="#">ภารกิจผู้บริหาร</a></div>
				</div>
			<div class="col-md-12 ">
				<div>
					<img src="<?php echo $linkPrefix?>images/imageNotFound.jpg" class="img-responsive" alt="Responsive image" style="height: 150px;width: 220px" >
				</div>
			</div>
		</div>
				
				<?php

			}
		
		if(count($missionArr2) >0){
			?>
		<div class="col-md-6">
			<div class="col-md-12  "> 
				<div class="headerText h1"><a href="<?php echo $ini_array['contextRoot'].$page_type_text?>/2/mission.php">กิจกรรมหน่วย</a></div>
			</div>
		<div class="col-md-12 ">
			<?php 
			if($num2 > 2){
				$num2 = 2;
			}
			
			 for( $i = 0; $i<$num2 ; $i++ ) {
			 	$missionImgDao =App_DaoFactory::getFactory()->getMissionImgDao();
			 	$missionImgArr2 = 	$missionImgDao->getMissionImgByRefId($missionArr2[$i]->getMissionId(),$page_type_id);
			 if(!empty($missionImgArr2)){
			
			 	?>
			 	<!-- Modal -->
						<div class="modal fade" id="myModal2<?php echo $missionArr2[$i]->getMissionId()?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						        <h4 class="modal-title" id="myModalLabel"><?php echo $missionArr1[$i]->getMissionHeader()?></h4>
						      </div>
						      <div class="modal-body">
						       <?php 
						     
						       for ($k = 0; $k < count($missionImgArr2) ; $k++) {
						       	?>
					       		 <div class="col-md-6 col-xs-6">
					      	 				<a class="example-image-link" href="<?php echo $missionImgArr2[$k]->getImagePaht()?>" data-lightbox="data-lightbox" data-title= "แก้ไขเมื่อ :  <?php echo $missionImgArr2[$k]->getRecDate() ?>"><img  class=" modal-img img-thumbnail" src="<?php echo $missionImgArr2[$k]->getImagePaht()?>" alt=""/></a>
					      		</div> 	
						       	<?php
						       }
						       ?>
						      </div>
						      <div class="modal-footer">
						    
						      
						      </div>
						    </div>
						  </div>
						</div>
			 	
			 	<?php
			 }
				 $imageDao =App_DaoFactory::getFactory()->getImgDao();
			if($missionArr2[$i]->getMissionImage() !=""){
				 $image =  $imageDao->loadImgById($missionArr2[$i]->getMissionImage(),$page_type_id);
				for ($j = 0; $j< count($image); $j++) {
				?>
				 <div class="col-md-6" style="border-right: 1px solid #d0d2d3;">
					<div>
						<a href="#" data-toggle="modal" data-target="#myModal2<?php echo $missionArr2[$i]->getMissionId() ?>"><img class="img-thumbnail" alt="Responsive image" src="<?php echo $image[$j]->getImgpath()?>" style="height: 150px;width: 220px"></a>
					</div>
					<p class="h1"><?php echo $missionArr2[$i]->getMissionHeader() ?></p>
					<span class="h2"><?php echo $missionArr2[$i]->getMissionDesc() ?><a href="<?php echo $ini_array['contextRoot'].$page_type_text?>/<?php echo $missionArr2[$i]->getMissionId()?>/missionDetail.php">>></a></span>
				</div>
				<?php
					}
				}
				else{
					$default_image =	$constantDao->loadconstant("default_image","1");
					?>
					 <div class="col-md-6" style="border-right: 1px solid #d0d2d3;">
							<div>
								<img class="img-thumbnail" alt="Responsive image" src="<?php echo $default_image ->getConsName()?>" style="height: 150px;width: 220px">
							</div>
							<p class="h1"><?php echo $missionArr2[$i]->getMissionHeader() ?></p>
							<span class="h2"><?php echo $missionArr2[$i]->getMissionDesc() ?><a href="<?php echo $ini_array['contextRoot'].$page_type_text?>/<?php echo $missionArr2[$i]->getMissionId()?>/missionDetail.php">  >></a></span>
						</div>
					<?php
					
				}
			}
			?>
				
	</div>
	</div>
	<?php }else{
		?>
		
		<div class="col-md-6">
				<div class="col-md-12 "> 
					<div class="headerText h1"><a href="#">กิจกรรมหน่วย</a></div>
				</div>
			<div class="col-md-12 ">
				<div>
					<img src="<?php echo $linkPrefix?>images/imageNotFound.jpg" class="img-responsive" alt="Responsive image" style="height: 150px;width: 220px">
				</div>
			</div>
		</div>
		<?php
		
		
		
	}?>

	
	
	
	</div>
