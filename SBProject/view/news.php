<?php 
use SB\conn\factory\App_DaoFactory;
include_once 'conn/factory/factory.php';
$ini_array = parse_ini_file("sbpolice.ini");
include $ini_array['contextRoot'].'constant.php';
	$newsDao = App_DaoFactory::getFactory()->getNews();
	$newsArr =$newsDao->loadNews($news_code,$page_type_id);
	$pageTypeMappingDao = App_DaoFactory::getFactory()->getPageTypeMappingDao();
	$page_type_text =$pageTypeMappingDao->loadpagetypemappingText($page_type_id);
	$num=count($newsArr);
	
	if (!function_exists('DateThai')) {
		function DateThai($strDate)
		{
			$strYear = date("Y",strtotime($strDate))+543;
			$strMonth= date("n",strtotime($strDate));
			$strDay= date("j",strtotime($strDate));
			$strHour= date("H",strtotime($strDate));
			$strMinute= date("i",strtotime($strDate));
			$strSeconds= date("s",strtotime($strDate));
			$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
			$strMonthThai=$strMonthCut[$strMonth];
			return "$strDay $strMonthThai $strYear, $strHour:$strMinute";
		}
	}
	
	
	
	if($num >0){
		if($news_code == '1'){?>
		<div class="h1">ข่าวประชาสัมพันธ์ </div>
		<?php } else{?>
		<div class="h1">ประกาศจัดซื้อจัดจ้าง </div>
		<?php }?>
			<hr>
			<ul class="list-1" style="padding-left: 0px;">
			<?php
				if($num >6){
					$num = 6;	
				}
						
				for ($i=0;$i<$num;$i++){
					if($newsArr[$i]->getIsHighLight() != 0 || $i < 2){?>
							<li class="unwrapclass"><a  data-toggle="tooltip" title="<?php echo $newsArr[$i]->getNewsHeader()?> "   href="<?php echo $ini_array['contextRoot'].$page_type_text?>/<?php echo $newsArr[$i]->getNewsId()?>/news_detail.php" style="width:100%;color:#0061d4;font-weight: bold; text-decoration: underline;"><?php echo $newsArr[$i]->getNewsHeader()?><i class="fa fa-camera-retro"></i>  <span class="glyphicon glyphicon-star" style="font-size: 15px; color:#F71204;" aria-hidden="true"></span></a><br/><small><i style="color: #e2dbdb;">[ <?php echo DateThai($newsArr[$i]->getRecDate())?> ]</i></small></li>
						  
						<?php } else {?>
								<li class="unwrapclass"><a data-toggle="tooltip" title="<?php echo $newsArr[$i]->getNewsHeader()?> "  href="<?php echo $ini_array['contextRoot'].$page_type_text?>/<?php echo $newsArr[$i]->getNewsId()?>/news_detail.php" style="width:100%"><?php echo $newsArr[$i]->getNewsHeader()?></a><br/><small><i style="color: #e2dbdb;">[ <?php echo DateThai($newsArr[$i]->getRecDate())?> ]</i></small></li>
						     
						<?php }
					}
				?>
			</ul>
			  
			  <a href="<?php echo $ini_array['contextRoot'].$page_type_text?>/<?php echo $news_code?>/newsList.php" class="btn btn_" style="font-size: 26px; padding: 9px;">อ่านต่อ</a>
				<?php
	}else{
		?>
		<div class="h1">ข่าวประชาสัมพันธ์ </div>
			<hr>
		<div>
			<img src="<?php echo $linkPrefix?>images/imageNotFound.jpg" class="img-responsive" alt="Responsive image" >
		</div>
		
		<?php
		
	}
?>
