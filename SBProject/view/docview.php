<?php namespace SB\view;
use SB\conn\factory\App_DaoFactory;
use SB;
include_once 'conn/factory/factory.php';
$ini_array = parse_ini_file("sbpolice.ini");
include $ini_array['contextRoot'].'constant.php';
	$docDao = App_DaoFactory::getFactory()->getDocumentDao();
	$docArr =$docDao->loadDoc($page_type_id);
	$num=count($docArr);
	
	$otherNewsDao = App_DaoFactory::getFactory()->getOtherNewsDao();
	$otherNewsArr = $otherNewsDao->loadOtherNews($page_type_id);
	$numOthernews=count($otherNewsArr);
	if($num >0){
		?>
		<div class="row">
		<div class="col-md-6">
			<div class="h1">ข่าวประชาสัมพันธ์ </div>
				<hr>
				<ul class="list-1" >
				<?php  for($j=0;$j<$numOthernews;$j++){?>
					<li class="unwrapclass"><a  style="width:100%" data-toggle="modal" data-target="#myModal<?php echo $otherNewsArr[$j]->getOtherNewsId() ?>"><?php echo $otherNewsArr[$j]->getOtherNewsHeader()?></a></li>
				
				<?php }?>
				</ul>
		</div>
		<div class="col-md-6">
			<div class="h1">ดาวน์โหลดเอกสาร / Download Document </div>
				<hr>
				<ul class="list-1" >
				<?php
					for ($i=0;$i<$num;$i++){
							?>
								  <li class="unwrapclass"><a href="<?php echo $docArr[$i]->getDocUpload()?>" style="width:100%"><?php echo $docArr[$i]->getDocHeader()?></a></li>
							<?php 
						}?>
				</ul>
			</div>
		</div>
 <?php
	}else{
		?>
		<div class="h1">ดาวน์โหลดเอกสาร / Download Document</div>
			<hr>
		<div>
			<img src="<?php echo $linkPrefix?>images/imageNotFound.jpg" class="img-responsive" alt="Responsive image" >
		</div>
		
		<?php
		
	}
	
	
  for($k=0;$k<$numOthernews;$k++){
  	?>
  	<div id="myModal<?php echo $otherNewsArr[$k]->getOtherNewsId() ?>" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
	
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title"><?php echo $otherNewsArr[$k]->getOtherNewsHeader()?></h4>
	      </div>
	      <div class="modal-body">
	       <?php echo $otherNewsArr[$k]->getOtherNewsDetail()?>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	
	  </div>
	</div>
  	<?php
  }?>
