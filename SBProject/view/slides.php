<?php namespace SB\view;
use SB\conn\factory\App_DaoFactory;
include_once 'conn/factory/factory.php';
	
		$imgDao = App_DaoFactory::getFactory()->getImgDao();
	    $images =$imgDao->loadImg('main',$page_type_id);
	    $num=count($images);
	    if($num >0){
	    	?>	
				<!-- imageSlidsShow -->
						<div class="container content-slide"> 
							<div class="row">
								<div id="myCarousel" class="carousel slide" data-ride="carousel">
								  <!-- Indicators -->
								  <ol class="carousel-indicators">
								  <?php 
								
								  for( $i = 0; $i<$num; $i++ ) {
								 
								  		if($i==0){
								  			?>
								  				 <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
								  			<?php
								  		}
								  		else{
								  			?>
								  				<li data-target="#myCarousel" data-slide-to="<?php echo  $i?>"></li>
								  			<?php
								  		}
								  	}
								  ?>

								  </ol>
				
				<!-- Wrapper for slides -->
								  <div class="carousel-inner img-slide" role="listbox">
								  <?php 
								  for( $j = 0; $j<$num; $j++ ) {
					
								  	if($j==0){
								  		?>
								  		 <div class="item active">
								      		<img style="width: 100%;margin: 0px 0px 0px 0px; max-height: 280px;" src="<?php echo $images[$j]->getImgpath()?>" class="img-responsive" alt="Responsive image" alt="Chania">
								    	</div>
								  		<?php
								  	}
								  	else{
								  		?>
								  		 <div class="item">
								      		<img style="width: 100%;margin: 0px 0px 0px 0px; max-height: 280px;"  src="<?php echo $images[$j]->getImgpath()?>" class="img-responsive" alt="Responsive image"  alt="Flower">
								    	</div>
								  		<?php
								  	}
								  }
								  ?>
								  </div>
								
								  <!-- Left and right controls -->
								  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
								    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
								    <span class="sr-only">Previous</span>
								  </a>
								  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
								    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
								    <span class="sr-only">Next</span>
								  </a>
								</div>
							</div>
						</div><!-- END imageSlidsShow -->
	    	<?php 
	    }
	    else{
	    	//echo "not found";
	    }
?>
