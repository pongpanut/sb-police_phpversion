<?php namespace SB\view;
use SB\conn\factory\App_DaoFactory;
include_once 'conn/factory/factory.php';
			$tabDao =App_DaoFactory::getFactory()->getTabs();
			$tabs =$tabDao->loadTab($page_type_id);
			$num = count($tabs);
			if($num>0){
				?>
				<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen">
				<script type="text/javascript" src="js/lightbox.js"></script>
					<div class="tab">
						<ul class="nav nav-tabs">
						<?php 
							for($i = 0; $i<$num; $i++){
								if($i==0){
									?>
									<li class="active"><a data-toggle="tab" href="#menu<?php echo  $i?>"><?php echo $tabs[$i]->getTabHeader() ?></a></li>
									<?php
								}
								else{
									?>
									<li><a data-toggle="tab" href="#menu<?php echo $i?>"><?php echo  $tabs[$i]->getTabHeader() ?></a></li>
									<?php
								}
							}
						?>
						</ul>
						<div class="tab-content">
						<?php 
						$num = count($tabs);
						if($num>0){
							for ($j = 0; $j < $num; $j++) {

									$contentArr =$tabs[$j]->getTabConentArr();
									$numContent=	count($contentArr);
								?>
								<div id="menu<?php echo$j?>" class="tab-pane fade <?php if($j==0){echo "in active"; }?>">
						  				<div class="row">
								<?php
									for ($k=0;$k<$numContent;$k++){
										?>
											<div class="col-md-6 ">
												<div class="col-md-12 box">
												<div class="col-md-12"><h3><?php echo $contentArr[$k]->getTabContentHeader()?></h3></div>
												
												<?php 
												$imageDao =App_DaoFactory::getFactory()->getImgDao();
												$image =$imageDao->loadImgById($contentArr[$k]->getTabContentImageId(),$page_type_id);
												for ($q = 0; $q < count($image); $q++) {
													?>
													<div class="col-md-12"><a class="example-image-link" href="<?php echo $image[$q]->getImgpath()?>" data-lightbox="data-lightboxc<?php echo $j.'-'.$k  ?>" data-title="<?php echo $contentArr[$k]->getTabContentHeader()?>"><img class="img-responsive  margin: 0 auto;"  src="<?php echo $image[$q]->getImgpath()?>" alt=""/></a></div>
													<div class="col-md-10 col-md-offset-1"><a class="btn btn_" data-toggle="modal" data-target="#cat<?php echo $j.'-'.$k  ?>" >อ่านต่อ</a></div>
													<?php
												}
												
												?>
												</div>
											</div>
										<?php
									}?></div>
									</div><?php
								}
								?>
								</div>
												
								<?php
						}
						?>
						</div>

				<?php
			}
	?>
	<!-- maodal body --> 
	<?php 
	for ($m = 0; $m < $num; $m++) {
		$contentArray = $tabs[$m]->getTabConentArr();
		for ($n = 0; $n < count($contentArray); $n++) {
			
			$modalArray = $contentArray[$n]->getTabModalArr();
			$rec_date =$modalArray[$m]->getRecDate();
			for ($o = 0; $o < count($modalArray); $o++) {
				?><div id="cat<?php echo $m.'-'.$n?>" class="modal fade" role="dialog">
						<div class="modal-dialog modal-lg">
						    <!-- Modal content-->
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal">&times;</button>
						        <h4 class="modal-title"><?php echo  $modalArray[$o]->getTabModalHeader() ?></h4>
						      </div>
						      <div class="modal-body">
						      <?php $imageDao =App_DaoFactory::getFactory()->getImgDao();
						      		$ref_id = $modalArray[$o]->getTabModalId();
						      		$imageArray =	$imageDao->loadImgModal('modal',$ref_id,$page_type_id);
						      		$numImages = count($imageArray);
						      		
						      		if($numImages>0){
						      			for ($p = 0; $p < $numImages; $p++) {
						      				?>
						      				  <div class="col-md-6 col-xs-6">
						      	 				<a class="example-image-link" href="<?php echo $imageArray[$p]->getImgpath()?>" data-lightbox="data-lightbox<?php echo $m.'_'.$n?>" data-title="<?php echo $imageArray[$p]->getImgheader()." แก้ไขเมื่อ : ".$rec_date ?>"><img  class=" modal-img img-thumbnail center-block" src="<?php echo $imageArray[$p]->getImgpath()?>" alt=""/></a>
						      				  </div>
						      				<?php
						      			}
						      		}
						      ?>
						      </div>
						  	<div class="modal-footer" style="margin-top: 10px;margin-bottom: 20px">
						        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->   
						      </div>
						    </div>
						</div>
					</div>
				<?php
			}
		}
	}
?>
	
	
	