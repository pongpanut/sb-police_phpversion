<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SBPolice</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/submenu.css" type="text/css" media="screen">



<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>




</head>
	<body class="background">
		<div class="container header">
			<div class="row">
				<div class="col-md-12">
						<div class="col-md-2"><img src="images/SBlogo.png" class="img-responsive" alt="Responsive image"></div>
						<div class="col-md-10">
							<div class="sbText1">à¸�à¸­à¸‡à¸šà¸±à¸�à¸Šà¸²à¸�à¸²à¸£à¸•à¹�à¸²à¸£à¸§à¸ˆà¸ªà¸±à¸™à¸•à¸´à¸šà¸²à¸¥</div>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 navMenu ">
								<div class="col-md-offset-7">
										<div class=" login-menu">
												<nav class="navbar navbar-manual-login">
												  <div class="container-fluid">
												    <div class="navbar-header">
												      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
												        <span class="icon-bar"></span>
												        <span class="icon-bar"></span>
												        <span class="icon-bar"></span>                        
												      </button>
												    </div>
												    <div class="collapse navbar-collapse" id="myNavbar">
												      <ul class="nav navbar-nav">
												        <li class="dropdown">
															<a class="dropdown-toggle" href="#" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>  à¹€à¸‚à¹‰à¸²à¸ªà¸¹à¹ˆà¸£à¸°à¸šà¸š</a>
															<div class="dropdown-menu login" >
																<form method="post" action="login" accept-charset="UTF-8">
																	<input style="margin-bottom: 15px;" type="text" placeholder="Username" id="username" name="username">
																	<input style="margin-bottom: 15px;" type="password" placeholder="Password" id="password" name="password">
																	<input style="float: left; margin-right: 10px;" type="checkbox" name="remember-me" id="remember-me" value="1">
																	<label class="string optional" for="user_remember_me" style="color:white;"> à¸ˆà¸³à¸�à¸²à¸£à¹€à¸‚à¹‰à¸²à¸ªà¸¹à¹ˆà¸£à¸°à¸šà¸š</label>
																	<input class="btn btn-block" type="submit" id="sign-in" value="Sign In">
					
																</form>
															</div>
														</li>
												      </ul>
												      <ul class="nav navbar-nav">
												        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span>  à¸•à¸´à¸”à¸•à¹ˆà¸­à¹€à¸£à¸²</a></li>
												      </ul>
												      <ul class="social-icons">
									                        <li><a href="#"><img src="images/icon-1.png" alt=""></a></li>
									                        <li><a href="#"><img src="images/icon-2.png" alt=""></a></li>
									                        <li><a href="#"><img src="images/icon-3.png" alt=""></a></li>
									                        <li><a href="#"><img src="images/icon-4.png" alt=""></a></li>
								                        </ul>
												    </div>
												  </div>
												</nav>
											</div>
									</div>
						</div>
			
			</div>
			<?php include getenv('contextRoot').'menu.php';?>
		</div><!-- end header -->
		
		<div class="container content-slide"> <!-- imageSlidsShow -->
			<div class="row">
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				    <li data-target="#myCarousel" data-slide-to="1"></li>
				    <li data-target="#myCarousel" data-slide-to="2"></li>
				    <li data-target="#myCarousel" data-slide-to="3"></li>
				  </ol>
				
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner img-slide" role="listbox">
				    <div class="item active">
				      <img src="images/slideshow/bike.png" class="img-responsive" alt="Responsive image" alt="Chania">
				    </div>
				
				    <div class="item">
				      <img src="images/slideshow/police_commander_06_10_2558.png" class="img-responsive" alt="Responsive image" alt="Chania">
				    </div>
				
				    <div class="item">
				      <img src="images/slideshow/sb_commander_06_10_2558.png" class="img-responsive" alt="Responsive image"  alt="Flower">
				    </div>
				
				    <div class="item">
				      <img src="images/slideshow/SB-17-12-57.png" class="img-responsive" alt="Responsive image" alt="Flower">
				    </div>
				  </div>
				
				  <!-- Left and right controls -->
				  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
		</div><!-- END imageSlidsShow -->
	<!-- 	content Body -->
		<div class="container content">
		<!-- 	<div class="row "> 
				<div class="col-md-6 headerText">à¸ à¸²à¸£à¸�à¸´à¸ˆà¸œà¸¹à¹‰à¸šà¸£à¸´à¸«à¸²à¸£</div>
				<div class="col-md-6 headerText">à¸�à¸´à¸ˆà¸�à¸£à¸£à¸¡à¸«à¸™à¹ˆà¸§à¸¢</div>
			</div>
			<div class="row">
				<div class="col-md-3" style="border-right: 1px solid #d0d2d3;">
					<div>
						<img class="img-thumbnail" alt="Responsive image" src="images/thumbnail/3.png">
					</div>
				<p class="h1">à¸«à¸±à¸§à¸‚à¹‰à¸­ 1 à¹ƒà¸™à¸ªà¹ˆà¸§à¸™à¸‚à¸­à¸‡ à¸�à¸´à¸ˆà¸�à¸£à¸£à¸¡à¸«à¸™à¹ˆà¸§à¸¢</p>
				<span class="h2">à¸£à¸²à¸¢à¸¥à¸°à¹€à¸­à¸µà¸¢à¸” à¹ƒà¸™à¸ªà¹ˆà¸§à¸™à¸‚à¸­à¸‡à¸«à¸±à¸§à¸‚à¹‰à¸­ 1 à¸žà¸£à¹‰à¸­à¸¡à¸›à¸¸à¹ˆà¸¡à¹€à¸žà¸·à¹ˆà¸­à¹„à¸›à¸­à¹ˆà¸²à¸™à¸£à¸²à¸¢à¸¥à¸°à¹€à¸­à¸µà¸¢à¸” <a href="#">>></a></span>
			</div>
			<div class="col-md-3">
				<div>
					<img class="img-thumbnail" alt="Responsive image" src="images/thumbnail/4.png">
				</div>
				<p class="h1">à¸«à¸±à¸§à¸‚à¹‰à¸­ 2 à¹ƒà¸™à¸ªà¹ˆà¸§à¸™à¸‚à¸­à¸‡ à¸ à¸²à¸£à¸�à¸´à¸ˆà¸œà¸¹à¹‰à¸šà¸£à¸´à¸«à¸²à¸£</p>
				<span class="h2">à¸£à¸²à¸¢à¸¥à¸°à¹€à¸­à¸µà¸¢à¸” à¹ƒà¸™à¸ªà¹ˆà¸§à¸™à¸‚à¸­à¸‡à¸«à¸±à¸§à¸‚à¹‰à¸­ 2 à¸žà¸£à¹‰à¸­à¸¡à¸›à¸¸à¹ˆà¸¡à¹€à¸žà¸·à¹ˆà¸­à¹„à¸›à¸­à¹ˆà¸²à¸™à¸£à¸²à¸¢à¸¥à¸°à¹€à¸­à¸µà¸¢à¸” >> <a href="#">>></a></span>
			
			</div>
			<div class="col-md-3" style="border-right: 1px solid #d0d2d3;">
				<div>
					<img class="img-thumbnail" alt="Responsive image" src="images/thumbnail/3.png">
				</div>
				<p class="h1">à¸«à¸±à¸§à¸‚à¹‰à¸­ 1 à¹ƒà¸™à¸ªà¹ˆà¸§à¸™à¸‚à¸­à¸‡ à¸�à¸´à¸ˆà¸�à¸£à¸£à¸¡à¸«à¸™à¹ˆà¸§à¸¢</p>
				<span class="h2">à¸£à¸²à¸¢à¸¥à¸°à¹€à¸­à¸µà¸¢à¸” à¹ƒà¸™à¸ªà¹ˆà¸§à¸™à¸‚à¸­à¸‡à¸«à¸±à¸§à¸‚à¹‰à¸­ 1 à¸žà¸£à¹‰à¸­à¸¡à¸›à¸¸à¹ˆà¸¡à¹€à¸žà¸·à¹ˆà¸­à¹„à¸›à¸­à¹ˆà¸²à¸™à¸£à¸²à¸¢à¸¥à¸°à¹€à¸­à¸µà¸¢à¸” <a href="#">>></a></span>
			</div>
			<div class="col-md-3">
				<div>
					<img class="img-thumbnail" alt="Responsive image" src="images/thumbnail/4.png">
				</div>
				<p class="h1">à¸«à¸±à¸§à¸‚à¹‰à¸­ 2 à¹ƒà¸™à¸ªà¹ˆà¸§à¸™à¸‚à¸­à¸‡ à¸ à¸²à¸£à¸�à¸´à¸ˆà¸œà¸¹à¹‰à¸šà¸£à¸´à¸«à¸²à¸£</p>
				<span class="h2">à¸£à¸²à¸¢à¸¥à¸°à¹€à¸­à¸µà¸¢à¸” à¹ƒà¸™à¸ªà¹ˆà¸§à¸™à¸‚à¸­à¸‡à¸«à¸±à¸§à¸‚à¹‰à¸­ 2 à¸žà¸£à¹‰à¸­à¸¡à¸›à¸¸à¹ˆà¸¡à¹€à¸žà¸·à¹ˆà¸­à¹„à¸›à¸­à¹ˆà¸²à¸™à¸£à¸²à¸¢à¸¥à¸°à¹€à¸­à¸µà¸¢à¸” >> <a href="#">>></a></span>
			</div>
		</div> -->
		
		<div class="advertise">
			<div class="row">
				<div class="col-md-12 box">
				<?php  
				$index = $_GET['index'];
					switch ($index) {
						case 1:
							?>
								<div class="row">
									<div class="col-md-12 h1"><img class="img-thumbnail" alt="Responsive image" src="images/icon/icon-sb.gif">&nbsp; &nbsp;	à¸›à¸£à¸°à¸�à¸²à¸¨à¸‚à¸²à¸¢à¸—à¸­à¸”à¸•à¸¥à¸²à¸”à¸žà¸±à¸ªà¸”à¸¸à¸—à¸µà¹ˆà¸Šà¸³à¸£à¸¸à¸”.. </div>
								</div>
								<div class="row ">
										<img class="img-responsive" alt="Responsive image" src="images/non-news.gif">
								</div>
								<div class="row download text-center">
									<span class="glyphicon glyphicon-download" style="color:green"></span> <a href="pdf/news/img-429155237.pdf">à¸”à¸²à¸§à¸™à¹Œà¹‚à¸«à¸¥à¸”à¹„à¸Ÿà¸¥à¹Œà¹�à¸™à¸š</a>
								</div>
							<?php 
							break;
						case 2:
							?>
							
								<div class="row">
									<div class="col-md-12 h1"><img class="img-thumbnail" alt="Responsive image" src="images/icon/icon-sb.gif">&nbsp; &nbsp;	 à¸„à¸³à¸ªà¸±à¹ˆà¸‡ à¸šà¸Š.à¸ª.à¸—à¸µà¹ˆ 43-45 2559 à¹€à¸¥à¸·à¹ˆà¸­à¸™à¸‚à¸±à¹‰à¸™à¹€à¸‡à¸´à¸™à¹€à¸”à¸·à¸­à¸™ à¸„à¸£à¸¶à¹ˆà¸‡à¸›à¸µà¹�à¸£à¸� ( à¹€à¸¡.à¸¢.59).. </div>
								</div>
								<div class="row ">
										<img class="img-responsive" alt="Responsive image" src="images/non-news.gif">
								</div>
								<div class="row download text-center">
									<span class="glyphicon glyphicon-download" style="color:green"></span> <a href="pdf/news/img-405153643-2.pdf">à¸”à¸²à¸§à¸™à¹Œà¹‚à¸«à¸¥à¸”à¹„à¸Ÿà¸¥à¹Œà¹�à¸™à¸š</a>
								</div>
								<?php 
								break;
						case 3:?>
								<div class="row">
									<div class="col-md-12 h1"><img class="img-thumbnail" alt="Responsive image" src="images/icon/icon-sb.gif">&nbsp; &nbsp;	à¸„à¸³à¸ªà¸±à¹ˆà¸‡ à¸šà¸�.à¸­à¸�.à¸šà¸Š.à¸ª.à¸—à¸µà¹ˆ 15-16 2559 à¹€à¸¥à¸·à¹ˆà¸­à¸™à¸‚à¸±à¹‰à¸™à¹€à¸‡à¸´à¸™à¹€à¸”à¸·à¸­à¸™ à¸„à¸£à¸¶à¹ˆà¸‡à¸›à¸µà¹�à¸£à¸� (à¹€à¸¡.à¸¢.59).. </div>
								</div>
								<div class="row ">
										<img class="img-responsive" alt="Responsive image" src="images/non-news.gif">
								</div>
								<div class="row download text-center">
									<span class="glyphicon glyphicon-download" style="color:green"></span> <a href="pdf/news/img-405153729.pdf">à¸”à¸²à¸§à¸™à¹Œà¹‚à¸«à¸¥à¸”à¹„à¸Ÿà¸¥à¹Œà¹�à¸™à¸š</a>
								</div>

							<?php 
							break;
						case 4:
							?>
								
								<div class="row">
									<div class="col-md-12 h1"><img class="img-thumbnail" alt="Responsive image" src="images/icon/icon-sb.gif">&nbsp; &nbsp;	  à¸„à¸³à¸ªà¸±à¹ˆà¸‡ à¸šà¸Š.à¸ª.à¸—à¸µà¹ˆ 41/2559 à¹�à¸•à¹ˆà¸‡à¸•à¸±à¹‰à¸‡à¸§à¹ˆà¸²à¸—à¸µà¹ˆà¸¢à¸¨à¹�à¸¥à¸°à¹�à¸•à¹ˆà¸‡à¸•à¸±à¹‰à¸‡à¸‚à¹‰à¸²à¸£à¸²à¸Šà¸�à¸²à¸£à¸•à¸³à¸£à¸§à¸ˆ.. </div>
								</div>
								<div class="row ">
										<img class="img-responsive" alt="Responsive image" src="images/non-news.gif">
								</div>
								<div class="row download text-center">
									<span class="glyphicon glyphicon-download" style="color:green"></span> <a href="pdf/news/img-401160510.pdf">à¸”à¸²à¸§à¸™à¹Œà¹‚à¸«à¸¥à¸”à¹„à¸Ÿà¸¥à¹Œà¹�à¸™à¸š</a>
								</div>
							
							<?php 
							break;
						case 5:
							?>
								
								<div class="row">
									<div class="col-md-12 h1"><img class="img-thumbnail" alt="Responsive image" src="images/icon/icon-sb.gif">&nbsp; &nbsp;	 à¸¥à¸³à¸”à¸±à¸šà¸­à¸²à¸§à¸¸à¹‚à¸ªà¸‚à¹‰à¸²à¸£à¸²à¸Šà¸�à¸²à¸£à¸•à¸³à¸£à¸§à¸ˆà¸Šà¸±à¹‰à¸™à¸ªà¸±à¸�à¸�à¸²à¸šà¸±à¸•à¸£ à¸£à¸°à¸”à¸±à¸š à¸£à¸­à¸‡à¸ªà¸²à¸£à¸§à¸±à¸•à¸£ à¸–à¸¶à¸‡ à¸œà¸¹à¹‰à¸�à¸³à¸�à¸±à¸šà¸�à¸²à¸£.. </div>
								</div>
								<div class="row ">
										<img class="img-responsive" alt="Responsive image" src="images/non-news.gif">
								</div>
								<div class="row download text-center">
									<span class="glyphicon glyphicon-download" style="color:green"></span> <a href="pdf/news/img-316162851.pdf">à¸”à¸²à¸§à¸™à¹Œà¹‚à¸«à¸¥à¸”à¹„à¸Ÿà¸¥à¹Œà¹�à¸™à¸š</a>
								</div>
							
							<?php 
							break;
						case 6:
							?>
								
								<div class="row">
									<div class="col-md-12 h1"><img class="img-thumbnail" alt="Responsive image" src="images/icon/icon-sb.gif">&nbsp; &nbsp;	à¸„à¸³à¸ªà¸±à¹ˆà¸‡ à¸šà¸Š.à¸ª. à¸—à¸µà¹ˆ 239-240/2558 à¹€à¸£à¸·à¹ˆà¸­à¸‡ à¹�à¸•à¹ˆà¸‡à¸•à¸±à¹‰à¸‡à¸¢à¸¨à¸•à¸³à¸£à¸§à¸ˆà¸Šà¸±à¹‰à¸™à¸ªà¸±à¸�à¸�à¸²à¸šà¸±à¸•à¸£à¹�à¸¥à¸°à¸•à¸³à¸£à¸§à¸ˆà¸Šà¸±à¹‰à¸™à¸›à¸£à¸°à¸—à¸§à¸™.. </div>
								</div>
								<div class="row ">
										<img class="img-responsive" alt="Responsive image" src="images/non-news.gif">
								</div>
								<div class="row download text-center">
									<span class="glyphicon glyphicon-download" style="color:green"></span> <a href="pdf/news/img-104110548.pdf">à¸”à¸²à¸§à¸™à¹Œà¹‚à¸«à¸¥à¸”à¹„à¸Ÿà¸¥à¹Œà¹�à¸™à¸š</a>
								</div>	
							<?php 
							break;
						default:
							echo "not found data";
					}
				?>
			
				
				
				</div>
				

			
			</div>
		</div>
		
		
		
		
		
		
		
		
		
		
		<!-- footer -->
		<footer>
  			<div class="col-md-offset-8 box" >
			        <form id="newsletter" method="post" >
			            <label>à¸�à¸£à¸­à¸�à¸‚à¹‰à¸­à¸¡à¸¹à¸¥à¹€à¸žà¸·à¹ˆà¸­à¸£à¸±à¸šà¸‚à¹ˆà¸²à¸§à¸ªà¸²à¸£</label>
			            <div class="clearfix">
			                <input type="text" onFocus="if(this.value =='Enter e-mail here' ) this.value=''" onBlur="if(this.value=='') this.value='Enter e-mail here'" value="à¸­à¸µà¹€à¸¡à¸¥à¸¥à¹Œà¸‚à¸­à¸‡à¸„à¸¸à¸“" >
			                <a href="#" style="margin-right:50px;" onClick="document.getElementById('newsletter').submit()" class="btn btn_">subscribe</a>
			            </div>
			        </form>
			    </div>
		</footer>
		 <div class="span8 float" style="margin-left:50px;" >
		      	<ul class="footer-menu">
		        	<li><a href="index.html">à¸«à¸™à¹‰à¸²à¸«à¸¥à¸±à¸�</a>|</li>
		            <!--<li><a href="index-1.html" class="current">about</a>|</li>
		            <li><a href="index-2.html">Services</a>|</li>
		            <li><a href="index-3.html">collections</a>|</li>
		            <li><a href="index-4.html">styles</a>|</li>
		            <li><a href="index-5.html">Contacts</a></li>-->
		        </ul>
		      	à¸�à¸­à¸‡à¸šà¸±à¸�à¸Šà¸²à¸�à¸²à¸£à¸•à¸³à¸£à¸§à¸ˆà¸ªà¸±à¸™à¸•à¸´à¸šà¸²à¸¥ à¸ªà¸³à¸™à¸±à¸�à¸‡à¸²à¸™à¸•à¸³à¸£à¸§à¸ˆà¹�à¸«à¹ˆà¸‡à¸Šà¸²à¸•à¸´.
				à¸­à¸²à¸„à¸²à¸£ 20 à¸ªà¸³à¸™à¸±à¸�à¸‡à¸²à¸™à¸•à¸³à¸£à¸§à¸ˆà¹�à¸«à¹ˆà¸‡à¸Šà¸²à¸•à¸´, à¸–à¸™à¸™à¸žà¸£à¸°à¸£à¸²à¸¡ 1
				à¹�à¸‚à¸§à¸‡à¸§à¸±à¸‡à¹ƒà¸«à¸¡à¹ˆ à¹€à¸‚à¸•à¸›à¸—à¸¸à¸¡à¸§à¸±à¸™ à¸�à¸£à¸¸à¸‡à¹€à¸—à¸žà¸¡à¸«à¸²à¸™à¸„à¸£, 10330 
		      </div>
		    </div>

	</body>
</html>