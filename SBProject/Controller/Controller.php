<?php 	namespace SB\Controller;
use SB\view;
class Controller{

	public function getImage($page_type_id){
		$page_type_id=$page_type_id;
		include 'view/slides.php';
	}
	
	public function getNews($type,$page_type_id){
		$news_code=$type;
		$page_type_id=$page_type_id;
		include 'view/news.php';
	}
	public function getActivityMain($type,$page_type_id){
		$activity_type=$type;
		$page_type_id=$page_type_id;
		include 'view/activityMain.php';
	}
	public function getTabActivity($page_type_id){
		$page_type_id=$page_type_id;
		include 'view/tab.php';
	}

	public function getMenu($page_type){
		$page_type = $page_type;
		include 'view/menu2.php';
	}
	public function getNewsDetail($newsId,$page_type_id){
		$page_type_id=$page_type_id;
		$news_id=$newsId;
		include 'view/newsDetail.php';
	}
	public function getMission($page_type_id){
		$page_type_id=$page_type_id;
		include 'view/mission.php';
	}
	public function getMissionList($page_type_id){
		$page_type_id=$page_type_id;
		include 'view/missionList.php';
	}
	public function getMissionDetail($mission_id,$page_type_id){
		$missionId=$mission_id;
		$page_type_id=$page_type_id;
		include 'view/missionDetail.php';
	}
	public function getNewsList($type,$page_type_id){
		$page_type_id=$page_type_id;
		$news_code=$type;
		include 'view/newsList.php';
	}
	public function getHistory($page_type_id){
		$page_type_id=$page_type_id;
		include 'view/historyView.php';
	}
	
	public function getCurrentCommander($commanderType,$page_type_id){
		$commanderType=$commanderType;
		$page_type_id=$page_type_id;
		include 'view/commanderView.php';
	}
	public function getOldCommander($commanderType,$page_type_id){
		$commanderType=$commanderType;
		$page_type_id=$page_type_id;
		include 'view/commanderOldView.php';
	}
	public function getHeaderBE($page_type_id){
		$page_type_id=$page_type_id;
		include 'back_end_view/header.php';
	}
	
	public function getStaticPage($id,$page_type_id){
		$page_type_id=$page_type_id;
		$id=$id;
		include 'view/pageview.php';
	}
	public function getDownloadDoc($page_type_id){
		$pagetype=$page_type_id;
		include 'view/docview.php';
	}
	
	
	
	
}


?>