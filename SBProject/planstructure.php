<?php namespace SB\index;	
if(!isset($_SESSION)) { 
   session_start(); 
}

use SB\Controller\Controller;
include  'Controller/Controller.php';
include  'basepagetype.php';
$Controller = new Controller();
$ini_array = parse_ini_file("sbpolice.ini");
?>
<!DOCTYPE html>
<html>
<head>
<?php include $ini_array['contextRoot'].'constant.php';?>
<?php include $ini_array['contextRoot'].'baseurl.php';?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SBPolice</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/submenu.css" type="text/css" media="screen">



<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>




</head>
	<body class="background">
		<?php include $ini_array['contextRoot'].'section_header.php';
		include $ini_array['contextRoot'].'section_slider.php';
		include $ini_array['contextRoot'].'bg.php';?>
		
		
	<!-- 	content Body -->
		<div class="container content">
			<div class="row "> 
			
		</div>
		
		<div class="advertise">
			<div class="row">
				<div class="col-md-12 box">
					<img src="images/sb-planstructure.jpg" class="img-responsive" alt="Responsive image">
				</div>
				

			
			</div>
		</div>
	</div>
		<?php include $ini_array['contextRoot'].'section_footer.php'; ?>
	</body>
</html>