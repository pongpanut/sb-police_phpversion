<?php 
if(!isset($_SESSION)) { 
   session_start(); 
}
use SB\Controller\Controller;
include  'Controller/Controller.php';
$Controller = new Controller();
$page_type_id =$_SESSION['pagetype'];
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SBPolice</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/submenu.css" type="text/css" media="screen">



<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>




</head>
	<body class="background">
		<?php include '../SBProject/section_header.php'; 
		include '../SBProject/bg.php';?>
		
		<div class="container content-slide"> <!-- imageSlidsShow -->
			<div class="row">
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				    <li data-target="#myCarousel" data-slide-to="1"></li>
				    <li data-target="#myCarousel" data-slide-to="2"></li>
				    <li data-target="#myCarousel" data-slide-to="3"></li>
				  </ol>
				
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner img-slide" role="listbox">
				    <div class="item active">
				      <img src="images/slideshow/bike.png" class="img-responsive" alt="Responsive image" alt="Chania">
				    </div>
				
				    <div class="item">
				      <img src="images/slideshow/police_commander_06_10_2558.png" class="img-responsive" alt="Responsive image" alt="Chania">
				    </div>
				
				    <div class="item">
				      <img src="images/slideshow/sb_commander_06_10_2558.png" class="img-responsive" alt="Responsive image"  alt="Flower">
				    </div>
				
				    <div class="item">
				      <img src="images/slideshow/SB-17-12-57.png" class="img-responsive" alt="Responsive image" alt="Flower">
				    </div>
				  </div>
				
				  <!-- Left and right controls -->
				  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
		</div><!-- END imageSlidsShow -->
	<!-- 	content Body -->
		<div class="container content">
			
		
		<div class="vision">
		
			<div class="row ">
				<div class="col-md-5 col-md-offset-4 h1 text-center">การขอถือสัญชาติไทยของหญิงต่างด้าว โดยการสมรส</div>
				<div class="col-md-12 h2 text-center">หน่วยงานที่รับผิดชอบ ฝ่ายกฎหมายและวินัย กองบังคับการอำนวยการ กองบัญชาการตำรวจสันติบาล</div>
				<div class="col-md-12 h2" style="color: red;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; การขอถือสัญชาติไทยตามสามี</div>
				<div class="col-md-12 detail"> 
					<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <i>หญิงต่างชาติซึ่งได้จดทะเบียนสมรสกับสามีซึ่งเป็นคนไทยอย่างถูกต้องตามกฎหมาย(กฎหมายไทยหรือกฎหมายต่างประเทศก็ได้) หากต้องการมีสัญชาติไทยตามสามี มีเงื่อนไข ดังนี้</i></p>
					<ul>
						<ol>1. จดทะเบียนสมรสกับสามีอย่างถูกต้องตามกฎหมาย(หากจดทะเบียนสมรสกับสามีตามกฎหมายต่างประเทศ จะต้องไปจดทะเบียนรับรองฐานะแห่งครอบครัว ณ สำนักทะเบียนเขตหรืออำเภอก่อน)</ol>
						<ol>2. หากยื่นคำขอเข้าถือสัญชาติไทยตามสามีในประเทศไทย ต้องเดินทางเข้ามาในประเทศไทยอย่างถูกต้องตามกฎหมาย(มีหนังสือเดินทาง,ใบต่างด้าว หรือบัตรชนกลุ่มน้อย)</ol>
						<ol>3. มีชื่อในทะเบียนบ้าน(ทร. 13 หรือ ทร. 14 )ิ</ol>
						<ol>4 .สามีต้องประกอบอาชีพมีรายได้ไม่ต่ำกว่า 30,000 บาทต่อเดือน หากเป็นข้าราชการสามีต้องมีรายได้ไม่ต่ำกว่า 15,000 บาทต่อเดือน</ol>
					</ul>
				</div>
			<div class="col-md-12 h2" style="color: red;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;หลักฐานเอกสารที่เกี่ยวข้องและนำมาแสดง </div>
			<div class="col-md-12 detail"> 
			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <i>หลักฐานสำเนาใบสำคัญการสมรส ( แบบ คร .3)</i></p>
				<ul>
					<ol>1. สำเนาทะเบียนการสมรส ( แบบ คร .2 หรือ คร .4) (หากจดทะเบียนสมรสตามกฎหมายต่างประเทศต้องจดทะเบียนฐานะแห่งครอบครัว แล้วนำหลักฐานทะเบียนสมรสต่างประเทศและทะเบียนฐานะแห่งครอบครัวมาแสดง)</ol>
					<ol>2. กรณีผู้ยื่นคำขอเป็นหญิงต่างด้าวได้รับอนุญาตให้อยู่ในราชอาณาจักรไทยชั่วคราวให้ถ่ายภาพ หนังสือเดินทางหรือเอกสารที่ใช้แทนหนังสือเดินทางทุกหน้าที่มีอักษร หรือตราประทับไว้ ( ให้ผู้ขอ เอาชื่อเข้าทะเบียนบ้านชั่วคราว และนำสำเนาทะเบียนบ้านชั่วคราว(ทร. 13 ) ดังกล่าวมาแสดง )	</ol>
					<ol>3. กรณีผู้ยื่นคำขอเป็นหญิงต่างด้าวมีภูมิลำเนาอยู่ในราชอาณาจักรไทยแล้ว ให้ถ่ายภาพใบสำคัญประจำตัวคนต่างด้าวหน้า 1 ถึงหน้า 6 และช่องรายการต่ออายุใบสำคัญฯ ครั้งสุดท้าย และสำเนาทะเบียนบ้าน (ทร. 14 )</ol>
					<ol>4 .รูปถ่ายของผู้ขอฯ กับสามี ขนาด 2 นิ้ว จำนวนคนละ 12 ภาพ</ol>
					<ol>5 .เอกสารสำเนาสูติบัตรการเกิดของบุตรทุกคน พร้อมรูปถ่ายขนาด 2 นิ้ว คนละ 2 ภาพ</ol>
					<ol>6 .ถ้าสามีรับราชการ ให้นำหนังสือรับรองของผู้บังคับบัญชา ยืนยันว่าปัจจุบันยังคงรับราชการในตำแหน่ง สังกัดมาแสดง</ol>
					<ol>7 .หนังสือรับรองการทำงาน ระบุตำแหน่ง หน้าที่การงาน และอัตราเงินเดือน</ol>
					<ol>8 .ใบเสร็จรับเงินเสียภาษีเงินได้ส่วนบุคคลปีที่ผ่านมา(แบบ เสียภาษีเงินได้บุคคลธรรมดา แบบ ภ.ง.ด. 90 หรือ 91 )</ol>
					<ol>9 .ให้นำเอกสารหลักฐานที่แสดงว่าสามีเป็นบุคคลสัญชาติไทยมาแสดงดังนี้</ol>
					<ol>10 .สำเนาสูติบัตรการเกิด หรือทะเบียนคนเกิด หรือหนังสือรับรองสถานที่เกิด</ol>
					<ol>11 .ภาพถ่ายบัตรประจำตัวประชาชน หรือบัตรประจำตัวข้าราชการ องค์การรัฐวิสาหกิจแล้วแต่กรณี ให้ถ่ายทั้งสองด้านอย่างชัดเจน</ol>
					<ol>12 .สำเนาใบสำคัญทหารกองหนุน หรือกองเกิน ( แบบ สด .1, สด .8, สด .9 หรือ สด .43</ol>
					<ol>13 .สำเนาทะเบียนบ้านปัจจุบัน ( ให้ถ่ายอย่างชัดเจนทุกหน้า )</ol>
					<ol>14 .ภาพถ่ายหนังสือเดินทางของสามี ( ถ่ายทุกหน้าที่มีตราประทับ )</ol>
					<ol>15 .สำเนาหนังสือสำคัญการเปลี่ยนชื่อตัว , ชื่อสกุล ( หากมี )</ol>
					<ol>16 .ในกรณีที่บิดามารดาของสามีเป็นคนต่างด้าว ให้ถ่ายภาพใบสำคัญประจำตัวคนต่างด้าว ของบิดามารดา หน้า 1 ถึงหน้า 6 และหน้าต่ออายุครั้งสุดท้ายมาแสดง</ol>
					<ol>17 .บิดามารดาของสามีผู้ขอเป็นบุคคลสัญชาติไทย ให้ถ่ายบัตรประจำตัวประชาชนหน้า - หลัง บิดามารดาสามีผู้ขอถึงแก่กรรม ให้ถ่ายใบมรณบัตร</ol>
				</ul>
			</div>		
		  <div class="col-md-12 h2" style="color: red;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วิธีการยื่นคำขอเข้าถือสัญชาติไทยตามสามี </div>
			<div class="col-md-12 detail"> 
				<ul>
					<ol>1. ผู้มีชื่อในทะเบียนบ้านซึ่งอยู่ในกรุงเทพมหานคร ให้ยื่นคำขอต่อผู้บังคับการ กองบังคับการอำนวยการ กองบัญชาการตำรวจสันติบาล (งาน 2 ฝ่ายกฎหมายและวินัย กองบังคับการ อำนวยการ กองบัญชาการตำรวจสันติบาล เป็นผู้ดำเนินการ</ol>
					<ol>2 .ผู้มีชื่อในทะเบียนบ้านซึ่งอยู่ในต่างจังหวัด ให้ยื่นคำขอต่อผู้บังคับการตำรวจภูธรจังหวัด(หัวหน้าตำรวจภูธรจังหวัด) ซึ่งผู้ร้อง ฯ มีชื่ออยู่ในทะเบียนบ้าน	</ol>
					<ol>3 .ผู้อยู่ในต่างประเทศให้ยื่นคำขอเข้าถือสัญชาติไทยต่อพนักงานทูต ในประเทศนั้น</ol>
				</ul>	
			</div>
			</div>
		</div>
		<?php include '../SBProject/section_footer.php'; ?>

	</body>
</html>