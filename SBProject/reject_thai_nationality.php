<?php namespace SB\index;	
if(!isset($_SESSION)) { 
   session_start(); 
}
use SB\Controller\Controller;
include  'Controller/Controller.php';
$Controller = new Controller();
$page_type_id =$_SESSION['pagetype'];
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <meta charset="UTF-8"> -->
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<title>SBPolice</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/submenu.css" type="text/css" media="screen">

<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/form.js"></script>


</head>
	<body class="background">
	<?php include '../SBProject/section_header.php'; 
	include '../SBProject/bg.php';
	      $Controller->getImage($page_type_id);?>

	<!-- 	content Body -->
		<div class="container content">
		
		<div class="advertise">
			<div class="row">
				<div class="col-md-12 ">
					<div class="box" style="background-color: rgba(0, 0, 0, 0.16)">
						<div class="h1">&nbsp;<b>คำแนะนำระเบียบว่าด้วยการยื่นคำขอสละสัญชาติไทย</b></div>
						<table width="100%" border="0" cellpadding="1" cellspacing="0" style="margin-left: 50px;font-size: 20px;">
							   <tbody>
							   <tr><td height="50">อาศัยอำนาจตามนัยกฎกระทรวง (พ.ศ.2510) ระเบียบปฏิบัติตามกฎกระทรวง (พ.ศ.2511) และตามระเบียบกรมตำรวจไม่เกี่ยวกับคดี ลักษณะที่ 48 ซึ่งออกตามความพระราชบัญญัติสัญชาติ พ.ศ.2508 การยื่นคำขอสละสัญชาติไทย ให้ผู้ยื่นคำร้องฯ ปฏิบัติดังต่อไปนี้ </td></tr>
					
					           <tr><td height="30">ผู้ยื่นคำขอมีภูมิลำเนาอยู่ในกรุงเทพมหานคร ให้ยื่นคำขอต่อผู้บังคับการกองบังคับการ กองบัญชาการตำรวจสันติบาล</td></tr>
					
					
					 <tr><td height="30">ผู้ยื่นคำขอซึ่งมีภูมิลำเนาอยู่ในจังหวัดอื่น ให้ยื่นคำขอต่อผู้บังคับการตำรวจภูธรจังหวัดนั้น</td></tr>
					
					 <tr><td height="30">ผู้ยื่นคำขออยู่ในต่างประเทศ ให้ยื่นคำขอต่อพนักงานทูตหรือกงสุล ณ ที่ทำการสถานทูตไทย หรือสถานกงสุลในประเทศนั้น</td></tr>
					
					<tr><td height="30">1. การยื่นคำขอสละสัญชาติไทยไปถือสัญชาติตามสามี ตามความในมาตรา 13 ให้ยื่นคำขอตาม แบบ สช.1 พร้อมด้วยหลักฐานประกอบคำขอ ดังนี้</td></tr>
					
					<tr><td height="30"><dd>1.1 ทะเบียนสมรส หรือใบสำคัญแสดงการจดทะเบียนสมรส</dd></td></tr>
					
					<tr><td height="30"><dd>1.2 หลักฐานเอกสารที่แสดงว่ากฎหมายด้วยสัญชาติของสามีมีบทบัญญัติหรือยินยอมให้เข้าถือสัญชาติของสามีได้</dd></td></tr>
					
					<tr><td height="30"><dd>1.3 รูปถ่ายขนาด 1 นิ้ว ของผู้ร้องและสามี คนละ 6 รูป (ผู้ชายสวมชุดสูทสากล ผู้หญิงแต่งกายสุภาพเรียบร้อย)</dd></td></tr>
					
					<tr><td height="30"><dd>1.4 สำเนาทะเบียนบ้าน</dd></td></tr>
					
					<tr><td height="30"><dd>1.5 หลักฐานเอกสารที่แสดงว่าหญิงผู้ขอเกิดในราชอาณาจักรไทยหรือเป็นบุคคลสัญชาติไทยมาก่อน</dd></td></tr>
					
					<tr><td height="30"><dd>&nbsp;&nbsp;&nbsp;&nbsp;- สำเนาสูติบัตร/หนังสือรับรองการเกิด</dd></td></tr>
					
					<tr><td height="30"><dd>&nbsp;&nbsp;&nbsp;&nbsp;- ภาพถ่ายบัตรประจำตัวประชาชน หรือบัตรประจำตัวข้าราชการ หรือองค์กรรัฐวิสาหกิจ แล้วแต่กรณีให้ถ่ายภาพทั้งสองด้านอย่างชัดเจน</dd></td></tr>
					
					<tr><td height="30">2. การยื่นคำร้องขอสละสัญชาติไทย ตามความในมาตรา 14 หรือ มาตรา 15 ให้ยื่นคำขอตามแบบ ส.ช.2 หรือด้วยหลักฐานประกอบคำขอ ดังนี้</td></tr>
					
					<tr><td height="30"><dd>2.1 รูปถ่ายขนาด 2 นิ้ว ของผู้ขอ บิดา มารดา คนละ 6 รูป</dd></td></tr>
					
					<tr><td height="30"><dd>2.2 หลักฐานที่แสดงว่าอาจถือสัญชาติของบิดาได้ตามกฎหมายว่าด้วยสัญชาติของบิดา หรือ หลักฐานการได้สัญชาติไทย ตามมาตรา 12 วรรค 2 หรือหลักฐานการได้สัญชาติอื่นแล้ว แล้วแต่กรณี</dd></td></tr>
					
					<tr><td height="30"><dd>2.3 สำเนาใบสำคัญประจำตัวคนต่างด้าวของบิดาหรือหลักฐานการขอแปลงสัญชาติเป็นไทยของบิดา</dd></td></tr>
					
					<tr><td height="30"><dd>2.4 หลักฐานที่แสดงว่าเกิดในราชอาณาจักรไทย หรือเคยมีสัญชาติไทยมาก่อน</dd></td></tr>
					
					<tr><td height="30"><dd>&nbsp;&nbsp;&nbsp;&nbsp;- สำเนาสูติบัตร</dd></td></tr>
					
					<tr><td height="30"><dd>&nbsp;&nbsp;&nbsp;&nbsp;- ภาพถ่ายบัตรประจำตัวประชาชน หรือบัตรข้าราชการ หรือองค์การรัฐวิสาหกิจ แล้วแต่กรณีให้ถ่ายภาพทั้งสองด้านอย่างชัดเจน
					
					</dd></td></tr><tr><td height="30"><dd>&nbsp;&nbsp;&nbsp;&nbsp;- สำเนาใบสำคัญทหารกองหนุน หรือกองเกิน (แบบ สด.1 หรือ สด.8 หรือ สด.9)</dd></td></tr>
					
					<tr><td height="30"><dd>&nbsp;&nbsp;&nbsp;&nbsp;- สำเนาทะเบียนบ้านปัจจุบัน</dd></td></tr>
					
					<tr><td height="30">3. หลังจากที่ผู้ร้องได้ยื่นคำร้องแล้วเจ้าหน้าที่ได้ตรวจสอบคำร้องพร้อมเอกสารประกอบเรื่องครบถ้วนแล้วจะดำเนินการสอบสวนปากคำผู้ร้อง และผู้ร้องจะต้องนำพยานบุคคลซึ่งยืนยันว่า ผู้ขอเกิดในประเทศไทยหรือเคยมีสัญชาติไทยมาก่อนมาสอบยืนยัน 2 ปาก</td></tr>
					
					<tr><td height="30"><u>หมายเหตุ&nbsp;ในการยื่นคำร้องฯ ให้นำหลักฐานดังกล่าวข้างต้นมาแสดงให้ครบถ้วนทุกฉบับ จึงจะมายื่นคำร้องฯ ถ้าหากหลักฐานเอกสารฉบับใดมีครบจะไม่พิจารณารับคำร้องฯ</u></td></tr>
					</tbody></table>
					 <div><img  src="../SBProject/images/pic-phone.jpg"></div>
			          </div>
			         
				</div>
				
			</div>
		</div>
		
		
		
		<?php include '../SBProject/section_footer.php'; ?>
		</div>
	</body>
</html>