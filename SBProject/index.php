<?php
namespace SB\index;
//phpinfo();
//error_reporting(E_ALL | E_WARNING | E_NOTICE);
//ini_set('display_errors', TRUE);
$ini_array = parse_ini_file("sbpolice.ini");
include $ini_array['contextRoot'].'pagetypemapping.php';
if (! isset ( $_SESSION )) {
	session_start ();
}
use SB\Controller\Controller;
include 'Controller/Controller.php';
$Controller = new Controller ();
?>
<!DOCTYPE html>
<html>
<head>
<?php include $ini_array['contextRoot'].'constant.php';?>
<?php include $ini_array['contextRoot'].'baseurl.php';?>
<?php include $ini_array['contextRoot'].'basepagetype.php';?>

<!-- <meta charset="UTF-8"> -->
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SBPolice</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"
	media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css"
	media="screen">
<link rel="stylesheet" href="css/bootstrap-theme.min.css"
	type="text/css" media="screen">
<link rel="stylesheet" href="css/submenu.css" type="text/css"
	media="screen">
<link rel="stylesheet" href="css/sb-website.css" type="text/css"
	media="screen">

<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/form.js"></script>


</head>
<body class="background" >
<!-- <body class="background" style="url('images/background.png') center top no-repeat;"> -->
	<?php
	
	include $ini_array['contextRoot'].'section_header.php';
	include $ini_array['contextRoot'].'section_slider.php';
	include $ini_array['contextRoot'].'bg.php';
	
	
	?>

	<!-- 	content Body -->
	<div class="container content">

		<div class="advertise">
			<div class="row">
				<div class="col-md-6 ">
					<div class="box">
					<?php 
					$Controller->getNews('1',$page_type_id); ?>
			          </div>
				</div>

				<div class="col-md-6 ">
					<div class="box">
							<?php $Controller->getNews('2',$page_type_id); ?>
						</div>

				</div>
			</div>
		</div>
		
		<?php $Controller->getMission($page_type_id); ?>
		<?php include $ini_array['contextRoot'].'calendar1.php'; ?>
		<?php include $ini_array['contextRoot'].'section_footer.php'; ?>
		</div>
		
		
</body>
</html>
