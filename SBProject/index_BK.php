<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SBPolice</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/submenu.css" type="text/css" media="screen">



<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>




</head>
	<body class="background">
		<div class="container header">
			<div class="row">
				<div class="col-md-12">
						<div class="col-md-2"><img src="images/SBlogo.png" class="img-responsive" alt="Responsive image"></div>
						<div class="col-md-10">
							<div class="sbText1">กองบัญชาการตํารวจสันติบาล</div>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 navMenu ">
								<div class="col-md-offset-7">
										<div class=" login-menu">
												<nav class="navbar navbar-manual-login">
												  <div class="container-fluid">
												    <div class="navbar-header">
												      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
												        <span class="icon-bar"></span>
												        <span class="icon-bar"></span>
												        <span class="icon-bar"></span>                        
												      </button>
												    </div>
												    <div class="collapse navbar-collapse" id="myNavbar">
												      <ul class="nav navbar-nav">
												        <li class="dropdown">
															<a class="dropdown-toggle" href="#" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>  เข้าสู่ระบบ</a>
															<div class="dropdown-menu login" >
																<form method="post" action="login" accept-charset="UTF-8">
																	<input style="margin-bottom: 15px;" type="text" placeholder="Username" id="username" name="username">
																	<input style="margin-bottom: 15px;" type="password" placeholder="Password" id="password" name="password">
																	<input style="float: left; margin-right: 10px;" type="checkbox" name="remember-me" id="remember-me" value="1">
																	<label class="string optional" for="user_remember_me" style="color:white;"> จำการเข้าสู่ระบบ</label>
																	<input class="btn btn-block" type="submit" id="sign-in" value="Sign In">
					
																</form>
															</div>
														</li>
												      </ul>
												      <ul class="nav navbar-nav">
												        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span>  ติดต่อเรา</a></li>
												      </ul>
												      <ul class="social-icons">
									                        <li><a href="#"><img src="images/icon-1.png" alt=""></a></li>
									                        <li><a href="#"><img src="images/icon-2.png" alt=""></a></li>
									                        <li><a href="#"><img src="images/icon-3.png" alt=""></a></li>
									                        <li><a href="#"><img src="images/icon-4.png" alt=""></a></li>
								                        </ul>
												    </div>
												  </div>
												</nav>
											</div>
									</div>
						</div>
			
			</div>
			<div class="row">
				<nav class="navbar navbar-manual-menu">
					  <div class="container-fluid">
					    <div class="navbar-header">
					      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>                        
					      </button>
					      <a class="navbar-brand" href="#"><span class="glyphicon glyphicon-home" style="color:red"></span></a>
					    </div>
					    <div class="collapse navbar-collapse" id="myNavbar">
					      <ul class="nav navbar-nav">
					         <li class="dropdown">
					          	<a class="dropdown-toggle" data-toggle="dropdown" href="#">เกี่ยวกับ<span class="caret"></span></a>
						          <ul class="dropdown-menu">
						          		<li><a href="#">ประวัติ</a></li>
		                                <li><a href="#">ที่ตั้ง</a></li>
		                                <li><a href="#">โครงสร้างองค์กร</a></li>
		                               	<li class="sub-menu"><a href="#">หน่วยงานในสังกัด</a>
		                                        <ul>
		                                          <li><a href="#">บก.อก.บช.ส</a></li>
		                                          <li><a href="#">บก.ส.1</a></li>
		                                          <li><a href="#">บก.ส.2</a></li>
		                                          <li><a href="#">บก.ส.3</a></li>
		                                          <li><a href="#">บก.ส.4</a></li>
		                                          <li><a href="#">ศูนย์พัฒนาด้านการข่าว</a></li>
		                                        </ul>
		                                      </li>
						          </ul>
					        </li>
					         <li class="dropdown">
					          	<a class="dropdown-toggle" data-toggle="dropdown" href="#">ผู้บริหาร<span class="caret"></span></a>
						          <ul class="dropdown-menu">
						          		<li class="sub-menu"><a href="#">อดีตผู้บริหาร</a>
		                                        <ul>
		                                          <li><a href="#">อดีต ผบช.ส</a></li>
		                                        </ul>
		                                      </li>        
		                               	<li class="sub-menu"><a href="#">ผู้บริหารปัจจุบัน</a>
		                                        <ul>
		                                          <li><a href="#">ผบช.ส-รอง ผบช.ส</a></li>
		                                        <li><a href="#">ผบก.อก</a></li>
		                                        <li><a href="#">ผบก.ประจำฯ</a></li>
		                                        <li><a href="#">ผบก.ส. 1-4</a></li>
		                                        <li><a href="#">รอง ผบก.ศพข.</a></li>
		                                        <li><a href="#">รอง ผบก.กชข.</a></li>
		                                        </ul>
		                                 </li>
						          </ul>
					        </li>      
					        <li class="dropdown">
					          	<a class="dropdown-toggle" data-toggle="dropdown" href="#">ยุทธศาสตร์<span class="caret"></span></a>
						          <ul class="dropdown-menu">
						         	<li><a href="#">วิสัยทัศน์</a></li>
		                                  <li><a href="#">พันธกิจ</a></li>
		                                  <li><a href="#">แผนยุทธศาสตร์</a></li>
						          		   <li class="sub-menu"><a href="#">นโยบาย</a>
		                                        <ul>
		                                         	<li><a href="#">นโยบายรัฐบาล</a></li>
			                                        <li><a href="#">นโยบาย ผบ.ตร.</a></li>
			                                        <li><a href="#">นโยบาย ผบช.ส.</a></li>
		                                        </ul>
		                                    </li>
						          </ul>
					        </li>        
					        <li class="dropdown">
					          	<a class="dropdown-toggle" data-toggle="dropdown" href="#">ศูนย์ความรู้<span class="caret"></span></a>
						          <ul class="dropdown-menu">
						         			<li><a href="#">กฎหมายที่เกี่ยวข้อง</a></li>
	                                        <li><a href="#">องค์ความรู้ด้านรักษาความปลอดภัย</a></li>
	                                        <li><a href="#">องค์ความรู้ด้านการข่าว</a></li>
	                                        <li><a href="#">องค์ความรู้ด้านงานอำนวยการ</a></li>
	                                        <li><a href="#">องค์ความรู้ด้านเทคโนโลยี</a></li>
	                                        <li><a href="#">องค์ความรู้ด้านภาษาต่างประเทศ</a></li>
	                                        <li><a href="#">ดาวน์โหลด</a></li>
						          </ul>
					        </li>   
					      <li><a href="index-1.html">ร้องเรียน/แจ้งข่าว</a></li>
                          <li><a href="index-1.html">งานสัญชาติ</a></li>
                          <li><a href="index-1.html">ศูนย์บริการออกหนังสือรับรองความประพฤติ</a></li>
                          <li><a href="calendar.html">ปฎิทินกิจกรรม</a></li>  
                          </ul>
					    </div>
					  </div>
					</nav>
				</div>
		</div><!-- end header -->
		
		<div class="container content-slide"> <!-- imageSlidsShow -->
			<div class="row">
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				    <li data-target="#myCarousel" data-slide-to="1"></li>
				    <li data-target="#myCarousel" data-slide-to="2"></li>
				    <li data-target="#myCarousel" data-slide-to="3"></li>
				  </ol>
				
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner img-slide" role="listbox">
				    <div class="item active">
				      <img src="images/slideshow/bike.png" class="img-responsive" alt="Responsive image" alt="Chania">
				    </div>
				
				    <div class="item">
				      <img src="images/slideshow/police_commander_06_10_2558.png" class="img-responsive" alt="Responsive image" alt="Chania">
				    </div>
				
				    <div class="item">
				      <img src="images/slideshow/sb_commander_06_10_2558.png" class="img-responsive" alt="Responsive image"  alt="Flower">
				    </div>
				
				    <div class="item">
				      <img src="images/slideshow/SB-17-12-57.png" class="img-responsive" alt="Responsive image" alt="Flower">
				    </div>
				  </div>
				
				  <!-- Left and right controls -->
				  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
		</div><!-- END imageSlidsShow -->
	<!-- 	content Body -->
		<div class="container content">
			<div class="row "> 
				<div class="col-md-6 headerText">ภารกิจผู้บริหาร</div>
				<div class="col-md-6 headerText">กิจกรรมหน่วย</div>
			</div>
			<div class="row">
				<div class="col-md-3" style="border-right: 1px solid #d0d2d3;">
					<div>
						<img class="img-thumbnail" alt="Responsive image" src="images/thumbnail/3.png">
					</div>
				<p class="h1">หัวข้อ 1 ในส่วนของ กิจกรรมหน่วย</p>
				<span class="h2">รายละเอียด ในส่วนของหัวข้อ 1 พร้อมปุ่มเพื่อไปอ่านรายละเอียด <a href="#">>></a></span>
			</div>
			<div class="col-md-3">
				<div>
					<img class="img-thumbnail" alt="Responsive image" src="images/thumbnail/4.png">
				</div>
				<p class="h1">หัวข้อ 2 ในส่วนของ ภารกิจผู้บริหาร</p>
				<span class="h2">รายละเอียด ในส่วนของหัวข้อ 2 พร้อมปุ่มเพื่อไปอ่านรายละเอียด >> <a href="#">>></a></span>
			
			</div>
			<div class="col-md-3" style="border-right: 1px solid #d0d2d3;">
				<div>
					<img class="img-thumbnail" alt="Responsive image" src="images/thumbnail/3.png">
				</div>
				<p class="h1">หัวข้อ 1 ในส่วนของ กิจกรรมหน่วย</p>
				<span class="h2">รายละเอียด ในส่วนของหัวข้อ 1 พร้อมปุ่มเพื่อไปอ่านรายละเอียด <a href="#">>></a></span>
			</div>
			<div class="col-md-3">
				<div>
					<img class="img-thumbnail" alt="Responsive image" src="images/thumbnail/4.png">
				</div>
				<p class="h1">หัวข้อ 2 ในส่วนของ ภารกิจผู้บริหาร</p>
				<span class="h2">รายละเอียด ในส่วนของหัวข้อ 2 พร้อมปุ่มเพื่อไปอ่านรายละเอียด >> <a href="#">>></a></span>
			</div>
		</div>
		
		<div class="advertise">
			<div class="row">
				<div class="col-md-6 ">
					<div class="box">
						<div class="h1">ข่าวประชาสัมพันธ์</div>
							<hr>
						 		<ul class="list-1" >
				                     <li class="unwrapclass"><a href="#" style="width:100%">คำสั่ง บช.ส. ที่ 239-240/2558 เรื่อง แต่งตั้งยศตำรวจชั้นสัญญาบัตรและตำรวจชั้นประทวน [วันที่ 4 มกราคม 2559]</a></li>
				                     <li class="unwrapclass"><a href="#" style="width:100%">คำสั่ง บช.ส. ที่ 267/2557 เรื่องให้ข้าราชการตำรวจปฏิบัติหน้าที่ราชการ [30 ธันวาคม 2558]</a></li>
				                     <li class="unwrapclass"><a href="#" style="width:100%">ส่งสำเนาคำสั่งกองอำนวยการคุมสอบข้อเขียน [23 พฤศจิกายน 2558]</a></li>
				                     <li class="unwrapclass"><a href="#" style="width:100%">ประกาศลำดับอาวุโสข้าราชการตำรวจระดับ รอง สว. ถึง ผกก. วาระประจำปี พ.ศ.2558 [12 พฤศจิกายน 2558]</a></li>
									 <li class="unwrapclass"><a href="#" style="width:100%">ประกาศ บก.ส.2 เรื่อง ขายทอดตลาดพัสดุ [11 พฤศจิกายน 2558]</a></li>
									 <li class="unwrapclass"><a href="#" style="width:100%">ข้าราชการตำรวจและลูกจ้างประจำดีเด่น ประจำปี 2558 ครั้งที่ 2 ของ บช.ส. [4 พฤศจิกายน 2558]</a></li>
				          		</ul>
				          		<a href="more.html" class="btn btn_">อ่านต่อ</a>
			          </div>
				</div>

				<div class="col-md-6 ">
						<div class="box">
							<div class="h1">ประกาศการจัดซื้อจัดหา</div>
								<hr>
							  		<ul class="list-1"   >
					                    <li class="unwrapclass"><a href="#" style="width:100%">โครงการเช่ายานพาหนะสำหรับภารกิจสืบสวนหาข่าว 52 คัน</a>   <i class=" icon-asterisk icon-large icon-red"></i></li>
					                    <li class="unwrapclass"><a href="#" style="width:100%">ขายทอดตลาด รถเก๋ง ยี่ห้อนิสสัน ทะเบียนโล่ 06083 2 คัน</a></li>
					                    <li class="unwrapclass"><a href="#" style="width:100%">ขายทอดตลาด ครุภัณฑ์ยานพาหนะ รถยนต์เก๋ง ยี่ห้อ วอลโว่ รุ่น ๙๖๐</a></li>
					                    <li class="unwrapclass"><a href="#" style="width:100%">ประกาศ บช.ส. เรื่อง ประกวดราคาเช่ายานพาหนะสำหรับภารกิจสืบสวนหาข่าว จำนวน ๕๒ คัน ด้วย...</a></li>
										<li class="unwrapclass"><a href="#" style="width:100%">โครงการเช่ายานพาหนะสำหรับภารกิจสืบสวนหาข่าว จำนวน 1 ชุด</a></li>
										<li class="unwrapclass"><a href="#" style="width:100%">โครงการจัดหาครุภัณฑ์คอมพิวเตอร์สำหรับงานด้านการข่าว</a></li>
					                </ul>
					                <a href="more.html" class="btn btn_">อ่านต่อ</a>
						</div>
				
				</div>
			</div>
		</div>
		
		
		
		
		
		
		
		
		
		<!-- teb session -->
			<div class="tab">
				<ul class="nav nav-tabs">
		  			<li class="active"><a data-toggle="tab" href="#home">กิจกรรม บก.อก.</a></li>
		  			<li><a data-toggle="tab" href="#menu1">กิจกรรม บก.ส.1</a></li>
		  			<li><a data-toggle="tab" href="#menu2">กิจกรรม บก.ส.2</a></li>
		  			<li><a data-toggle="tab" href="#menu3">กิจกรรม บก.ส.3</a></li>
				</ul>
					<div class="tab-content">
					  <div id="home" class="tab-pane fade in active">
						  <div class="row">
						  		<div class="col-md-3">
						  			<img class="img-thumbnail" alt="Responsive image" src="images/slideshow/tab1.jpg">
							  	</div>
								<div class="col-md-9">
								  	 	<h3>เนื้อหาบริวณนี้สำหรับ กิจกรรม บก.อก</h3>
								    	<p>Some content.</p>
								</div>
							</div>
							 <div class="tab-more-info">
								<a href="more.html" class="btn btn_">more info</a>
							</div>
					  </div>
					  <div id="menu1" class="tab-pane fade">
					    <div class="row">
						  		<div class="col-md-3">
						  			<img class="img-thumbnail" alt="Responsive image" src="images/slideshow/tab2.jpg">
							  	</div>
								<div class="col-md-9">
								  	 	<h3>เนื้อหาบริวณนี้สำหรับ กิจกรรม บก.ส.1</h3>
								    	<p>Some content.</p>
								</div>
							</div>
							 <div class="tab-more-info">
								<a href="more.html" class="btn btn_">more info</a>
							</div>
					  </div>
					  <div id="menu2" class="tab-pane fade">
					     <div class="row">
						  		<div class="col-md-3">
						  			<img class="img-thumbnail" alt="Responsive image" src="images/slideshow/tab3.jpg">
							  	</div>
								<div class="col-md-9">
								  	 	<h3>เนื้อหาบริวณนี้สำหรับ กิจกรรม บก.ส.2.</h3>
								    	<p>Some content.</p>
								</div>
							</div>
							 <div class="tab-more-info">
								<a href="more.html" class="btn btn_">more info</a>
							</div>
					  </div>
					  <div id="menu3" class="tab-pane fade">
					     <div class="row">
						  		<div class="col-md-3">
						  			<img class="img-thumbnail" alt="Responsive image" src="images/slideshow/tab4.jpg">
							  	</div>
								<div class="col-md-9">
								  	 	<h3>เนื้อหาบริวณนี้สำหรับ กิจกรรม บก.ส.3.</h3>
								    	<p>Some content.</p>
								</div>
							</div>
							<div class="tab-more-info">
								<a href="more.html" class="btn btn_">more info</a>
							</div>
							  
					  </div>
					</div>
				</div> 
		<!-- end tab -->
		<!-- footer -->
		<footer>
  			<div class="col-md-offset-8 box" >
			        <form id="newsletter" method="post" >
			            <label>กรอกข้อมูลเพื่อรับข่าวสาร</label>
			            <div class="clearfix">
			                <input type="text" onFocus="if(this.value =='Enter e-mail here' ) this.value=''" onBlur="if(this.value=='') this.value='Enter e-mail here'" value="อีเมลล์ของคุณ" >
			                <a href="#" style="margin-right:50px;" onClick="document.getElementById('newsletter').submit()" class="btn btn_">subscribe</a>
			            </div>
			        </form>
			    </div>
		</footer>
		 <div class="span8 float" style="margin-left:50px;" >
		      	<ul class="footer-menu">
		        	<li><a href="index.html">หน้าหลัก</a>|</li>
		            <!--<li><a href="index-1.html" class="current">about</a>|</li>
		            <li><a href="index-2.html">Services</a>|</li>
		            <li><a href="index-3.html">collections</a>|</li>
		            <li><a href="index-4.html">styles</a>|</li>
		            <li><a href="index-5.html">Contacts</a></li>-->
		        </ul>
		      	กองบัญชาการตำรวจสันติบาล สำนักงานตำรวจแห่งชาติ.
				อาคาร 20 สำนักงานตำรวจแห่งชาติ, ถนนพระราม 1
				แขวงวังใหม่ เขตปทุมวัน กรุงเทพมหานคร, 10330 
		      </div>
		    </div>	
	</body>
</html>