<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SBPolice</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/submenu.css" type="text/css" media="screen">


<script type="text/javascript" src="js/responsive-calendar.js"></script>
<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>




</head>
	<body class="background">
		<div class="container header">
			<div class="row">
				<div class="col-md-12">
						<div class="col-md-2"><img src="images/SBlogo.png" class="img-responsive" alt="Responsive image"></div>
						<div class="col-md-10">
							<div class="sbText1">กองบัญชาการตํารวจสันติบาล</div>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 navMenu ">
								<div class="col-md-offset-7">
										<div class=" login-menu">
												<nav class="navbar navbar-manual-login">
												  <div class="container-fluid">
												    <div class="navbar-header">
												      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
												        <span class="icon-bar"></span>
												        <span class="icon-bar"></span>
												        <span class="icon-bar"></span>                        
												      </button>
												    </div>
												    <div class="collapse navbar-collapse" id="myNavbar">
												      <ul class="nav navbar-nav">
												        <li class="dropdown">
															<a class="dropdown-toggle" href="#" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>  เข้าสู่ระบบ</a>
															<div class="dropdown-menu login" >
																<form method="post" action="login" accept-charset="UTF-8">
																	<input style="margin-bottom: 15px;" type="text" placeholder="Username" id="username" name="username">
																	<input style="margin-bottom: 15px;" type="password" placeholder="Password" id="password" name="password">
																	<input style="float: left; margin-right: 10px;" type="checkbox" name="remember-me" id="remember-me" value="1">
																	<label class="string optional" for="user_remember_me" style="color:white;"> จำการเข้าสู่ระบบ</label>
																	<input class="btn btn-block" type="submit" id="sign-in" value="Sign In">
					
																</form>
															</div>
														</li>
												      </ul>
												      <ul class="nav navbar-nav">
												        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span>  ติดต่อเรา</a></li>
												      </ul>
												      <ul class="social-icons">
									                        <li><a href="#"><img src="images/icon-1.png" alt=""></a></li>
									                        <li><a href="#"><img src="images/icon-2.png" alt=""></a></li>
									                        <li><a href="#"><img src="images/icon-3.png" alt=""></a></li>
									                        <li><a href="#"><img src="images/icon-4.png" alt=""></a></li>
								                        </ul>
												    </div>
												  </div>
												</nav>
											</div>
									</div>
						</div>
			
			</div>
			<?php
			$ini_array = parse_ini_file("sbpolice.ini");
			include $ini_array['contextRoot'].'menu.php';?>
		</div><!-- end header -->
		
		<div class="container content-slide"> <!-- imageSlidsShow -->
			<div class="row">
				     <!-- Responsive calendar - START -->
    	<div class="responsive-calendar calendar">
        <div class="controls">
            <a class="pull-left" data-go="prev"><div class="btn btn-primary" style="color: grey; ">Prev</div></a>
            <h4><span data-head-year></span> <span data-head-month></span></h4>
            <a class="pull-right" data-go="next"><div class="btn btn-primary" style="color: grey; ">Next</div></a>
        </div><hr/>
        <div class="day-headers">
          <div class="day header">Mon</div>
          <div class="day header">Tue</div>
          <div class="day header">Wed</div>
          <div class="day header">Thu</div>
          <div class="day header">Fri</div>
          <div class="day header">Sat</div>
          <div class="day header">Sun</div>
        </div>
        <div class="days" data-group="days">
          
        </div>
      </div>
      <!-- Responsive calendar - END -->

</div>
		
		<!-- footer -->
		<footer>
  			<div class="col-md-offset-8 box" >
			        <form id="newsletter" method="post" >
			            <label>กรอกข้อมูลเพื่อรับข่าวสาร</label>
			            <div class="clearfix">
			                <input type="text" onFocus="if(this.value =='Enter e-mail here' ) this.value=''" onBlur="if(this.value=='') this.value='Enter e-mail here'" value="อีเมลล์ของคุณ" >
			                <a href="#" style="margin-right:50px;" onClick="document.getElementById('newsletter').submit()" class="btn btn_">subscribe</a>
			            </div>
			        </form>
			    </div>
		</footer>
		 <div class="span8 float" style="margin-left:50px;" >
		      	<ul class="footer-menu">
		        	<li><a href="index.html">หน้าหลัก</a>|</li>
		            <!--<li><a href="index-1.html" class="current">about</a>|</li>
		            <li><a href="index-2.html">Services</a>|</li>
		            <li><a href="index-3.html">collections</a>|</li>
		            <li><a href="index-4.html">styles</a>|</li>
		            <li><a href="index-5.html">Contacts</a></li>-->
		        </ul>
		      	กองบัญชาการตำรวจสันติบาล สำนักงานตำรวจแห่งชาติ.
				อาคาร 20 สำนักงานตำรวจแห่งชาติ, ถนนพระราม 1
				แขวงวังใหม่ เขตปทุมวัน กรุงเทพมหานคร, 10330 
		      </div>
		    </div>

	</body>
</html>