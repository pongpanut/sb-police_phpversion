<?php namespace app\page;
use SB\conn\factory\App_DaoFactory;
include_once 'conn/factory/factory.php';

	$headerDao = App_DaoFactory::getFactory()->getHeader();
	$header =$headerDao->loadHeader($page_type_id);
	?>


<div class="col-md-12">
	<form action="./" method="post" accept-charset="UTF-8"   role="form" class="header-form" enctype="multipart/form-data">
		<div class="form-group col-md-6 preview-img">
		 	<img src="<?php echo $header[0]->getHeaderLogo();?>" class="img-responsive" alt="Responsive image">
		 	
		 	<div class="text-center"> Current Logo</div>
	  	</div>
		
		<div class="form-group col-xs-12 col-md-6 col-md-offset-1 col-sm-8 col-sm-offset-2 ">
		   <div class="input-group image-preview">
                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint , application/pdf, image/*" name="file" id="imageAttach"> <!-- rename it -->
                    </div>
                   
                </span>
            </div><!-- /input-group image-preview [TO HERE]--> 
	  	</div>
	  	<div class="col-md-12 formStyle">
	  		<label for="HeaderMain">Header Main</label>
    		<input type="text" class="form-control" id="HeaderMain" value="<?php echo $header[0]->getHeaderText()?>">
	  	</div>
	  	<div class="col-md-12 formStyle">
	  		<label for="HeaderTitle">Header Title</label>
    		<input type="text" class="form-control" id="HeaderTitle" value="<?php echo $header[0]->getHeaderTitle()?>">
	  	</div>
	  
	  	<div class="col-md-12 btn-session">
		  	<div class="col-md-2 col-md-offset-3"><input class="btn  btnSize" type="submit" id="uploadHeader" value="Save"></div>
		  	<div class="col-md-1 col-md-offset-1"><input class="btn  btnSize" type="reset" id="reset" value="Reset"></div>
		</div>
			
	  	
		
	</form>
</div>

