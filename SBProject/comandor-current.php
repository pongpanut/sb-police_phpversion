<?php 
if(!isset($_SESSION)) { 
   session_start(); 
}
use SB\Controller\Controller;
use SB\conn\factory\App_DaoFactory;
include  'Controller/Controller.php';
include_once 'conn/factory/factory.php';
include  'basepagetype.php';
$ini_array = parse_ini_file("sbpolice.ini");
$Controller = new Controller();
$orgDao = App_DaoFactory::getFactory()->getOrgDao();
$maxRow =$orgDao->loadRowcount($page_type_id);
?>

<!DOCTYPE html>
<html>
<head>
<?php include $ini_array['contextRoot'].'constant.php';?>
<?php include $ini_array['contextRoot'].'baseurl.php';?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SBPolice</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/submenu.css" type="text/css" media="screen">



<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/form.js"></script>




</head>
	<body class="background">
		<?php include $ini_array['contextRoot'].'section_header.php'; 
		include $ini_array['contextRoot'].'section_slider.php';
		include $ini_array['contextRoot'].'bg.php';
	    ?>
		
	<!-- 	content Body -->
		<div class="container content">
			
		
		<div class="advertise">
		
			<div class="row">
			
				
				<div class="col-md-12 box-comandor col-xs-12 text-center">
					<h1 style="color: white;">สายการบังคับบัญชา</h1>
									<?php  for( $i = 1; $i<=$maxRow; $i++ ) {?>
									<div class="row" style="margin-top: 20px;">
										<div class="col-md-12">
										<?php $orgLevarr =$orgDao->loadOrgarnizationByLevel($i ,$page_type_id);
											$commander =  count($orgLevarr);
											$offset =5- $commander;
										
											for( $j = 0; $j<$commander; $j++ ) {
												if($j==0 && $commander<4){
													switch ($commander) {
														case 1:
														?><div class="col-md-3 com1"><?php
														break;
														case 2:
														?><div class="col-md-3 com2"><?php
														break;
														case 3:
														?><div class="col-md-3 com3"><?php
														break;
														case 4:
														?><div class="col-md-3 com4"><?php
														break;
														default:
															;
														break;
													}
												}
												else if($j==0 && $commander==5){
													?><div class="col-md-2 col-md-offset-1"><?php
												}
												else if($commander>4){
													?>
													<div class="col-md-2">
													<?php
												}
												else{?>	
													<div class="col-md-3">
												<?php } ?>
												<?php if($commander!=5){?>
												<div class="col-md-12 " style="background-color: white;margin-top: 15;border: solid 1px white;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;margin-top: 20px;">
													<div class="col-md-12 text-center"><img id="img-responsive" src="<?php echo $orgLevarr[$j]->getOrgImg() ?>" class="img-responsive" alt="Responsive image" style=" margin-top: 10px;  height:200px;"></div>
													<div class="col-md-12 text-center" style="font-size: 26px; padding-left: 0px;padding-right: 0px; font-weight: bold; "><?php echo $orgLevarr[$j]->getOrgName();?></div>
													<div class="col-md-12 text-center" style="font-size: 20px; padding-left: 0px;padding-right: 0px;font-weight: bold; "><?php echo $orgLevarr[$j]->getOrgPos();?></div>
												</div>
												<?php }else{?>
												<div class="col-md-12 " style="background-color: white;margin-top: 15;border: solid 1px white;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; padding: 1; height: 180px;margin-top: 20px;">
													<div class="col-md-12 text-center"><img id="img-responsive" src="<?php echo $orgLevarr[$j]->getOrgImg() ?>" class="img-responsive" alt="Responsive image" style=" margin-top: 10px;"></div>
													<div class="col-md-12 text-center" style="font-size: 26px; padding-left: 0px;padding-right: 0px; font-weight: bold;"><?php echo $orgLevarr[$j]->getOrgName();?></div>
													<div class="col-md-12 text-center" style="font-size: 20px; padding-left: 0px;padding-right: 0px; font-weight: bold;"><?php echo $orgLevarr[$j]->getOrgPos();?></div>
												</div>
												<?php }?>
											</div>
											<?php }?>
										</div>
									</div>
									<?php }?>
									
					</div>
					</div>
					</div>
					</div>
					<?php //$Controller->getCurrentCommander("current",1); ?>
				</div>
				

			
			</div>
		</div>
		
		<?php include $ini_array['contextRoot'].'section_footer.php'; ?>
		</div>
</div>
</div>
	</body>
</html>