<?php  namespace SB\model;
class bgM {
	var $bg_id;
	var $bg_link;
	var $page_type_id;
	var $bg_name;
	
	function setBgId($bg_id) {
		$this->bg_id=$bg_id;
	}
	function getBgId() {
		return $this->bg_id;
	}
	function setBgLink($bg_link) {
		$this->bg_link=$bg_link;
	}
	function getBgLink() {
		return $this->bg_link;
	}
	function setPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getPageTypeId() {
		return $this->page_type_id;
	}
	function setBgName($bg_name) {
		$this->bg_name=$bg_name;
	}
	function getBgName() {
		return $this->bg_name;
	}
	
}


?>
