<?php   namespace SB\model;
class submenuM{
	var $submenu_id;
	var $submenu_text;
	var $submenu_link;
	var $submenu_has_menulist;
	var $submenu_submenu_id;
	var $page_type_id;
	
	function setSubMenuId($submenu_id) {
		$this->submenu_id=$submenu_id;
	}
	function getSubMenuId() {
		return $this->submenu_id;
	}
	
	function setSubMenuText($submenu_text) {
		$this->submenu_text=$submenu_text;
	}
	function getSubMenuText() {
		return $this->submenu_text;
	}
	
	function setSubMenuLink($submenu_link) {
		$this->submenu_link=$submenu_link;
	}
	function getSubMenuLink() {
		return $this->submenu_link;
	}
	
	function setSubmenuHasMenuList($submenu_has_menulist ) {
		$this->submenu_has_menulist =$submenu_has_menulist ;
	}
	function getSubmenuHasMenuList() {
		return $this->submenu_has_menulist;
	}

	function setMenuId($menu_id) {
		$this->menu_id=$menu_id;
	}
	function getMenuId() {
		return $this->menu_id;
	}
	function setPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getPageTypeId() {
		return $this->page_type_id;
	}
}


?>