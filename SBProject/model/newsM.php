<?php  namespace SB\model;
class newsM{
	var $news_id;
	var $news_code;
	var $news_header;
	var $news_detail;
	var $news_pdf;
	var $page_type_id;
	var $rec_date;
	var $is_highlight;
	
	function setNewsId($news_id) {
		$this->news_id=$news_id;
	}
	function getNewsId() {
		return $this->news_id;
	}
	
	function setNewsCode($news_code) {
		$this->news_code=$news_code;
	}
	function getNewsCode() {
		return $this->news_code;
	}
	
	function setNewsHeader($news_header) {
		$this->news_header=$news_header;
	}
	function getNewsHeader() {
		return $this->news_header;
	}
	
	function setNewsDetail($news_detail) {
		$this->news_detail=$news_detail;
	}
	function getNewsDetail() {
		return $this->news_detail;
	}
	
	function setNewsPdf($news_pdf) {
		$this->news_pdf=$news_pdf;
	}
	function getNewsPdf() {
		return $this->news_pdf;
	}
	function setPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getPageTypeId() {
		return $this->page_type_id;
	}
	function setRecDate($rec_date) {
		$this->rec_date=$rec_date;
	}
	function getRecDate() {
		return $this->rec_date;
	}
	function setIsHighLight($is_highlight) {
		$this->is_highlight=$is_highlight;
	}
	function getIsHighLight() {
		return $this->is_highlight;
	}
}

?>