<?php  namespace SB\model;
class submenulistM{
	var $submenu_list_id;
	var $submenu_list_text;
	var $submenu_list_link;
	var $submenu_id;
	var $page_type_id;
	function setSubmenuListId($submenu_list_id) {
		$this->submenu_list_id=$submenu_list_id;
	}
	function getSubmenuListId() {
		return $this->submenu_list_id;
	}

	function setSubmenuListText($submenu_list_text) {
		$this->submenu_list_text=$submenu_list_text;
	}
	function getSubmenuListText() {
		return $this->submenu_list_text;
	}

	function setSubmenuListLink($submenu_list_link) {
		$this->submenu_list_link=$submenu_list_link;
	}
	function getSubmenuListLink() {
		return $this->submenu_list_link;
	}

	function setSubmenuId($submenu_id) {
		$this->submenu_id=$submenu_id;
	}
	function getSubmenuId() {
		return $this->submenu_id;
	}
	function setPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getPageTypeId() {
		return $this->page_type_id;
	}
}


?>