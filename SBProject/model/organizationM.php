<?php  namespace SB\model;
class organizationM{
	var $org_id;
	var $org_pos;
	var $org_name;
	var $org_img;
	var $org_currow;
	var $org_pagetype;
		
	function setOrgId($org_id) {
		$this->org_id=$org_id;
	}
	function getOrgId() {
		return $this->org_id;
	}	
	function setOrgPos($org_pos) {
		$this->org_pos=$org_pos;
	}
	function getOrgPos() {
		return $this->org_pos;
	}	
	function setOrgName($org_name) {
		$this->org_name=$org_name;
	}
	function getOrgName() {
		return $this->org_name;
	}
	function setOrgImg($org_img) {
		$this->org_img=$org_img;
	}
	function getOrgImg() {
		return $this->org_img;
	}
	function setOrgCurrow($org_currow) {
		$this->org_currow=$org_currow;
	}
	function getOrgCurrow() {
		return $this->org_currow;
	}
	function setpagetype($org_pagetype) {
		$this->org_pagetype=$org_pagetype;
	}
	function getpagetype() {
		return $this->org_pagetype;
	}
	
}


?>