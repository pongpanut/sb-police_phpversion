<?php namespace SB\model;
class missionM {
	var $mission_id;
	var $mission_text;
	var $mission_header;
	var $mission_image;
	var $mission_desc;
	var $mission_type;
	var $show_flag;
	var $page_type_id;
	var $rec_date;
	
	function setMissionId($mission_id) {
		$this->mission_id=$mission_id;
	}
	function getMissionId() {
		return $this->mission_id;
	}
	
	function setMissionText($mission_text) {
		$this->mission_text=$mission_text;
	}
	function getMissionText() {
		return $this->mission_text;
	}
	
	function setMissionHeader($mission_header) {
		$this->mission_header=$mission_header;
	}
	function getMissionHeader() {
		return $this->mission_header;
	}
	
	function setMissionImage($mission_image) {
		$this->mission_image=$mission_image;
	}
	function getMissionImage() {
		return $this->mission_image;
	}
	
	function setMissionDesc($mission_desc) {
		$this->mission_desc=$mission_desc;
	}
	function getMissionDesc() {
		return $this->mission_desc;
	}
	
	function setMissionType($mission_type) {
		$this->mission_type=$mission_type;
	}
	function getMissionType() {
		return $this->mission_type;
	}
	function setShowFlag($show_flag) {
		$this->show_flag=$show_flag;
	}
	function getShowFlag() {
		return $this->show_flag;
	}
	function setPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getPageTypeId() {
		return $this->page_type_id;
	}
	function setRecDate($rec_date) {
		$this->rec_date=$rec_date;
	}
	function getRecDate() {
		return $this->rec_date;
	}
	
	
}