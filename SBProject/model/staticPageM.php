<?php  namespace SB\model;
class staticPageM{
	var $id;
	var $text;
	var $link;
	var $is_active;
	var $page_type_id;
	var $html;
	
	function setStaticpageId($id) {
		$this->id=$id;
	}
	function getStaticpageId() {
		return $this->id;
	}
	
	function setStaticpageText($text) {
		$this->text=$text;
	}
	function getStaticpageText() {
		return $this->text;
	}
	
	function setStaticpageLink($link) {
		$this->link=$link;
	}
	function getStaticpageLink() {
		return $this->link;
	}

	function setStaticpageActive($is_active) {
		$this->is_active=$is_active;
	}
	function getStaticpageActive() {
		return $this->is_active;
	}
	
	function setStaticPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getStaticPageTypeId() {
		return $this->page_type_id;
	}
	function setStaticHtml($html) {
		$this->html=$html;
	}
	function getStaticHtml() {
		return $this->html;
	}
}


?>