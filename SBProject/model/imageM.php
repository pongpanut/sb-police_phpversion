<?php  namespace SB\model;
class imageM {
	var $imgid;
	var $imgdesc;
	var $imgname;
	var $imgpath;
	var $imgtype;
	var $imgheader;
	var $page_type_id;
	var $seq;
	
	function setImgid($imgid) {
		$this->imgid=$imgid;
	}
	function getImgid() {
		 return $this->imgid;
	}
	function setImgdesc($imgdesc) {
		$this->imgdesc=$imgdesc;
	}
	function getImgdesc() {
		return $this->imgdesc;
	}
	function setImgname($imgname) {
		$this->imgname=$imgname;
	}
	function getImgname() {
		return $this->imgname;
	}
	function setImgpath($imgpath) {
		$this->imgpath=$imgpath;
	}
	function getImgpath() {
		return $this->imgpath;
	}
	function setImgtype($imgtype) {
		$this->imgtype=$imgtype;
	}
	function getImgtype() {
		return $this->imgtype;
	}
	function setImgheader($imgheader) {
		$this->imgheader=$imgheader;
	}
	function getImgheader() {
		return $this->imgheader;
	}
	function setPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getPageTypeId() {
		return $this->page_type_id;
	}
	function setSeq($seq) {
		$this->seq=$seq;
	}
	function getSeq() {
		return $this->seq;
	}
}


?>
