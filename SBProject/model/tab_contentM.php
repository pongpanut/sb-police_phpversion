<?php namespace SB\model;
	class tab_contentM{
		var $tab_content_id;
		var	$tab_content_header;
		var	$tab_content_detail;
		var	$tab_content_image_id;
		var	$tab_id;
		var $tab_modalMArr;
		var $page_type_id;
		
		function setTabContentId($tab_content_id) {
			$this->tab_content_id=$tab_content_id;
		}
		function getTabContentId() {
			return $this->tab_content_id;
		}
		function setTabContentHeader($tab_content_header) {
			$this->tab_content_header=$tab_content_header;
		}
		function getTabContentHeader() {
			return $this->tab_content_header;
		}
		function setTabContentDetail($tab_content_detail) {
			$this->tab_content_detail=$tab_content_detail;
		}
		function getTabContentDetail() {
			return $this->tab_content_detail;
		}
		function setTabContentImageId($tab_content_image_id) {
			$this->tab_content_image_id=$tab_content_image_id;
		}
		function getTabContentImageId() {
			return $this->tab_content_image_id;
		}
		function setTabId($tabid) {
			$this->tabid=$tabid;
		}
		function getTabId() {
			return $this->tabid;
		}
		
		function setTabModalArr($tab_modalMArr) {
			$this->tab_modalM=$tab_modalMArr;
		}
		function getTabModalArr() {
			return $this->tab_modalM;
		}
		function setPageTypeId($page_type_id) {
			$this->page_type_id=$page_type_id;
		}
		function getPageTypeId() {
			return $this->page_type_id;
		}
		
	}

?>