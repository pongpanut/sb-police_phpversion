<?php  namespace SB\model;
class other_newsM {

	var $page_type_id;
	var $other_news_id;
	var $other_news_header;
	var $other_news_detail;
	var $other_news_date;

	
	function setPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getPageTypeId() {
		return $this->page_type_id;
	}
	
	function setOtherNewsId($other_news_id) {
		$this->other_news_id=$other_news_id;
	}
	function getOtherNewsId() {
		return $this->other_news_id;
	}
	
	function setOtherNewsHeader($other_news_header) {
		$this->other_news_header=$other_news_header;
	}
	function getOtherNewsHeader() {
		return $this->other_news_header;
	}
	
	function setOtherNewsDetail($other_news_detail) {
		$this->other_news_detail=$other_news_detail;
	}
	function getOtherNewsDetail() {
		return $this->other_news_detail;
	}
	
	function setOtherNewsDate($other_news_date) {
		$this->other_news_date=$other_news_date;
	}
	function getOtherNewsDate() {
		return $this->other_news_date;
	}
	
}
?>
