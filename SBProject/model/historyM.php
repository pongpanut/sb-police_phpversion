<?php namespace SB\model;
class historyM{
	var $history_id;
	var $history_body;
	var $page_type_id;
	
	function setHistoryId($history_id) {
		$this->history_id=$history_id;
	}
	function getHistoryId() {
		return $this->history_id;
	}
	
	function setHistoryBody($history_body) {
		$this->history_body=$history_body;
	}
	function getHistoryBody() {
		return $this->history_body;
	}
	function setPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getPageTypeId() {
		return $this->page_type_id;
	}
	
}
?>
