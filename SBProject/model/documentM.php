<?php  namespace SB\model;
class documentM {

	var $page_type_id;
	var $doc_id;
	var $doc_header;
	var $doc_header_en;
	var $doc_detail;
	var $doc_upload;
	
	function setPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getPageTypeId() {
		return $this->page_type_id;
	}
	
	function setDocId($doc_id) {
		$this->doc_id=$doc_id;
	}
	function getDocId() {
		return $this->doc_id;
	}
	
	function setDocHeader($doc_header) {
		$this->doc_header=$doc_header;
	}
	function getDocHeader() {
		return $this->doc_header;
	}
	function setDocHeaderEN($doc_header_en) {
		$this->doc_header_en=$doc_header_en;
	}
	function getDocHeaderEN() {
		return $this->doc_header_en;
	}
	
	function setDocDetail($doc_detail) {
		$this->doc_detail=$doc_detail;
	}
	function getDocDetail() {
		return $this->doc_detail;
	}
	
	function setDocUpload($doc_upload) {
		$this->doc_upload=$doc_upload;
	}
	function getDocUpload() {
		return $this->doc_upload;
	}
	
	
	
}


?>
