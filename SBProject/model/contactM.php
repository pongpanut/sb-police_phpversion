<?php  namespace SB\model;
class contactM{
	var $contact_id;
	var $contact_name;
	var $contact_email;
	var $contact_phone;
	var $contact_message;
	var $contact_page_type_id;
	var $answer;

	
	function setContactId($contact_id) {
		$this->contact_id=$contact_id;
	}
	function getContactId() {
		return $this->contact_id;
	}
	
	function setContactText($contact_name) {
		$this->contact_name=$contact_name;
	}
	function getContactText() {
		return $this->contact_name;
	}
	
	function setContactEmail($contact_email) {
		$this->contact_email=$contact_email;
	}
	function getContactEmail() {
		return $this->contact_email;
	}
	
	function setContactPhone($contact_phone) {
		$this->contact_phone=$contact_phone;
	}
	function getContactPhone() {
		return $this->contact_phone;
	}
	function setContactMessage($contact_message) {
		$this->contact_message=$contact_message;
	}
	function getContactMessage() {
		return $this->contact_message;
	}

	function setContactPageType($contact_page_type_id) {
		$this->contact_page_type_id=$contact_page_type_id;
	}
	function getContactPageType() {
		return $this->contact_page_type_id;
	}
	function setAns($answer) {
		$this->answer=$answer;
	}
	function getAns() {
		return $this->answer;
	}
}


?>