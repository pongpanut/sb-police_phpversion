<?php  namespace SB\model;
class departmentM{
	var $dept_id;
	var $dept_text;
	var $dept_link;
	var $page_type_id;
	var $seq;
	
	function setDeptId($dept_id) {
		$this->dept_id=$dept_id;
	}
	function getDeptId() {
		return $this->dept_id;
	}
	
	function setDeptText($dept_text) {
		$this->dept_text=$dept_text;
	}
	function getDeptText() {
		return $this->dept_text;
	}
	
	function setDeptLink($dept_link) {
		$this->dept_link=$dept_link;
	}
	function getDeptLink() {
		return $this->dept_link;
	}
	function setPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getPageTypeId() {
		return $this->page_type_id;
	}
	function setSeq($seq) {
		$this->seq=$seq;
	}
	function getSeq() {
		return $this->seq;
	}
}


?>