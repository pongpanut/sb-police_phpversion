<?php  namespace SB\model;
class departmentSubM{
	var $dept_sub_id;
	var $dept_sub_text;
	var $dept_sub_link;
	var $dept_li_id;
	var $page_type_id;
	var $seq;
	
	function setDeptSubId($dept_sub_id) {
		$this->dept_sub_id=$dept_sub_id;
	}
	function getDeptSubId() {
		return $this->dept_sub_id;
	}
	
	function setDeptSubText($dept_sub_text) {
		$this->dept_sub_text=$dept_sub_text;
	}
	function getDeptSubText() {
		return $this->dept_sub_text;
	}
	
	function setDeptSubLink($dept_sub_link) {
		$this->dept_sub_link=$dept_sub_link;
	}
	function getDepSubLink() {
		return $this->dept_sub_link;
	}
	function setDeptId($dept_li_id) {
		$this->dept_li_id=$dept_li_id;
	}
	function getDeptId() {
		return $this->dept_li_id;
	}
	function setPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getPageTypeId() {
		return $this->page_type_id;
	}
	function setSeq($seq) {
		$this->seq=$seq;
	}
	function getSeq() {
		return $this->seq;
	}
	
}


?>