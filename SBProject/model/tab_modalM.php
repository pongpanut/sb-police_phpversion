<?php namespace SB\model;
	class tab_modalM{
	var	$modal_id;
	var	$modal_header;
	var	$modal_content;
	var	$tab_content_id;
	var $page_type_id;
	var $rec_date;
	
	function setTabModalId($modal_id) {
		$this->modal_id=$modal_id;
	}
	function getTabModalId() {
		return $this->modal_id;
	}
	function setTabModalHeader($modal_header) {
		$this->modal_header=$modal_header;
	}
	function getTabModalHeader() {
		return $this->modal_header;
	}
	function setTabModalContent($modal_content) {
		$this->modal_content=$modal_content;
	}
	function getTabModalContent() {
		return $this->modal_content;
	}
	function setTabContentId($tab_content_id) {
		$this->tab_content_id=$tab_content_id;
	}
	function getTabContentId() {
		return $this->tab_content_id;
	}
	function setPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getPageTypeId() {
		return $this->page_type_id;
	}
	function setRecDate($rec_date) {
		$this->rec_date=$rec_date;
	}
	function getRecDate() {
		return $this->rec_date;
	}
	
	}
?>