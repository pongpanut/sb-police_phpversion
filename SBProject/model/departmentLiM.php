<?php  namespace SB\model;
class departmentLiM{
	var $dept_li_id;
	var $dept_li_text;
	var $dept_li_link;
	var $page_type_id;
	var $seq;
	
	function setDeptListId($dept_li_id) {
		$this->dept_li_id=$dept_li_id;
	}
	function getDeptListId() {
		return $this->dept_li_id;
	}
	
	function setDeptListText($dept_li_text) {
		$this->dept_li_text=$dept_li_text;
	}
	function getDeptListText() {
		return $this->dept_li_text;
	}
	
	function setDeptListLink($dept_li_link) {
		$this->dept_li_link=$dept_li_link;
	}
	function getDeptListLink() {
		return $this->dept_li_link;
	}
	function setPageTypeId($page_type_id) {
		$this->page_type_id=$page_type_id;
	}
	function getPageTypeId() {
		return $this->page_type_id;
	}
	function setSeq($seq) {
		$this->seq=$seq;
	}
	function getSeq() {
		return $this->seq;
	}
}


?>