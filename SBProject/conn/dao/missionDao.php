<?php namespace SB\conn\dao;
interface missionDao{
	public function loadMission($page_type_id);
	public function loadMissionDetail($mission_id,$page_type_id);
	public function loadMissionAll($page_type_id);
	public function loadMissionType($missionType,$page_type_id);
	
}
?>