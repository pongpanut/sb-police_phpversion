<?php namespace SB\conn\dao;
interface imgDao{
	public function loadImg($type,$page_type_id);
	public function loadImgModal($type,$ref_id,$page_type_id);
	public function loadImgById($id,$page_type_id);
}
?>