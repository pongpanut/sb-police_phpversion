<?php  namespace SB\conn\factory;
	use SB\conn\dao\imgDao;
	use SB\conn\dao\newsDao;
	use SB\conn\dao\tabDao;
	use SB\conn\dao\menuDao;
	use SB\conn\dao\missionDao;
	use SB\conn\dao\subscriptDao;
	use SB\conn\dao\userDao;
	use SB\conn\dao\headerDao;
	use SB\conn\dao\staticpageDao;
	use SB\conn\dao\contactDao;
	
	
	use SB\conn\daoImpl\imgDaoImpl;
	use SB\conn\daoImpl\newsDaoImpl;
	use SB\conn\daoImpl\tabDaoImpl;
	use SB\conn\daoImpl\menuDaoImpl;
	use SB\conn\daoImpl\missionDaoImpl;
	use SB\conn\daoImpl\subscriptDaoImpl;
	use SB\conn\daoImpl\userDaoImpl;
	use SB\conn\daoImpl\headerDaoImpl;
	use SB\conn\daoImpl\historyDaoImpl;
	use SB\conn\daoImpl\staticpageDaoImpl;
	use SB\conn\daoImpl\contactDaoImpl;
	use SB\conn\daoImpl\departmentDaoImpl;
	use SB\conn\daoImpl\documentDaoImpl;
	use SB\conn\daoImpl\othernewsDaoImpl;
	use SB\conn\daoImpl\constantDaoImpl;
	use SB\conn\daoImpl\bgDaoImpl;
	use SB\conn\daoImpl\organizationDaoImpl;
	use SB\conn\daoImpl\pagetypemappingDaoImpl;
	use SB\conn\daoImpl\calendarDaoImpl;
	use SB\conn\daoImpl\pollDaoImpl;
	use SB\conn\daoImpl\missionImgDaoImpl;
	use SB\conn\daoImpl\contactInfoDaoImpl;
		
	include 'conn/dao/imgDao.php';
	include 'conn/dao/newsDao.php';	
	include 'conn/dao/tabDao.php';
	include 'conn/dao/menuDao.php';
	include 'conn/dao/missionDao.php';
	include 'conn/dao/subscriptDao.php';
	include 'conn/dao/userDao.php';
	include 'conn/dao/headerDao.php';
	include 'conn/dao/staticpageDao.php';
	include 'conn/dao/contactDao.php';
	
	include 'conn/daoImpl/imgDaoImpl.php';
	include 'conn/daoImpl/newsDaoImpl.php';
	include 'conn/daoImpl/tabDaoImpl.php';
	include 'conn/daoImpl/menuDaoImpl.php';
	include 'conn/daoImpl/missionDaoImpl.php';
	include 'conn/daoImpl/subscriptDaoImpl.php';
	include 'conn/daoImpl/userDaoImpl.php';
	include 'conn/daoImpl/headerDaoImpl.php';
	include 'conn/daoImpl/historyDaoImpl.php';
	include 'conn/daoImpl/staticpageDaoImpl.php';
	include 'conn/daoImpl/contactDaoImpl.php';
	include 'conn/daoImpl/departmentDaoImpl.php';
	include 'conn/daoImpl/documentDaoImpl.php';
	include 'conn/daoImpl/othernewsDaoImpl.php';
	include 'conn/daoImpl/constantDaoImpl.php';
	include 'conn/daoImpl/bgDaoImpl.php';
	include 'conn/daoImpl/organizationDaoImpl.php';
	include 'conn/daoImpl/pagetypemappingDaoImpl.php';
	include 'conn/daoImpl/calendarDaoImpl.php';
	include 'conn/daoImpl/pollDaoImpl.php';
	include 'conn/daoImpl/missionImgDaoImpl.php';
	include 'conn/daoImpl/contactInfoDaoImpl.php';
	
	class App_DaoFactory{
		private static $_instance;

		public function __construct()
		{
		}
		
		/**
		 * Set the factory instance
		 * @param App_DaoFactory $f
		 */
		public static function setFactory(App_DaoFactory $f)
		{
			self::$_instance = $f;
		}
		
		/**
		 * Get a factory instance.
		 * @return App_DaoFactory
		 */
		public static function getFactory()
		{
			if(!self::$_instance)
				self::$_instance = new self;
		
				return self::$_instance;
		}
		public function getImgDao()
		{
			return new imgDaoImpl();
		}
		
		public  function  getNews(){
			return new newsDaoImpl();
		}
		
		public  function  getTabs(){
			return new tabDaoImpl();
		}
		public  function  getMenu(){
			return new menuDaoImpl();
		}
		
		public  function  getSubscript(){
			return new subscriptDaoImpl();
		}
		public  function  getMission(){
			return new missionDaoImpl();
		}
		
		public  function  getUser(){
			return new userDaoImpl();
		}
		
		public  function  getHeader(){
			return new headerDaoImpl();
		}
		public  function  getHistory(){
			return new historyDaoImpl();
		}
		public  function  getStaticPage(){
			return new staticpageDaoImpl();
		}
		
		public  function  getContactDao(){
			return new contactDaoImpl();
		}
		
		public  function  getDepartmentDao(){
			return new departmentDaoImpl();
		}
		public  function  getDocumentDao(){
			return new documentDaoImpl();
		}
		public  function  getOtherNewsDao(){
			return new othernewsDaoImpl();
		}
		public  function  getTabCostentDao(){
			return new tabContentDaoImpl();
		}
		public  function  getConstantDao(){
			return new constantDaoImpl();
		}
		public  function  getBgDao(){
			return new bgDaoImpl();
		}
		public  function  getOrgDao(){
			return new organizationDaoImpl();
		}
		
		public  function  getPageTypeMappingDao(){
			return new pagetypemappingDaoImpl();
		}
		
		public  function  getCalendarDao(){
			return new calendarDaoImpl();
		}
		public  function  getPollDao(){
			return new pollDaoImpl();
		}
		
		public  function  getMissionImgDao(){
			return new missionImgDaoImpl();
		}
		public  function  getContactInfoDao(){
			return new contactInfoDaoImpl();
		}
		
		
		
		
	}
?>