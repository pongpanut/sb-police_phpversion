<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\organizationM;

include_once 'conn/connection.php';
include_once 'model/organizationM.php';


class organizationDaoImpl{
	

	public function loadOrgarnization($page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT o.org_id, o.org_pos, o.org_name, o.org_img, o.org_currow, o.org_pagetype FROM organization as o where o.org_pagetype = '$page_type_id' order by o.org_id desc";
	
		$result = mysqli_query($db, $sql);
		
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$organizationM = new organizationM();	
				$organizationM->setOrgId($row['org_id']);
				$organizationM->setOrgPos($row['org_pos']);
				$organizationM->setOrgName($row['org_name']);
				$organizationM->setOrgImg($row['org_img']);
				$organizationM->setOrgCurrow($row['org_currow']);
				$organizationM->setpagetype($row['org_pagetype']);
				array_push($data,$organizationM);
			}

		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	
	public function loadOrgarnizationByLevel( $curr_row, $page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT o.org_id, o.org_pos, o.org_name, o.org_img, o.org_currow, o.org_pagetype FROM organization as o where o.org_pagetype = '$page_type_id' AND o.org_currow='$curr_row' order by o.org_id desc";
	
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$organizationM = new organizationM();
				$organizationM->setOrgId($row['org_id']);
				$organizationM->setOrgPos($row['org_pos']);
				$organizationM->setOrgName($row['org_name']);
				$organizationM->setOrgImg($row['org_img']);
				$organizationM->setOrgCurrow($row['org_currow']);
				$organizationM->setpagetype($row['org_pagetype']);
				array_push($data,$organizationM);
			}
	
		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	
	public function loadRowcount($page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  0;
		$sql= "SELECT max(org_currow) as currow FROM organization where org_pagetype = '$page_type_id'";
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$data = $row['currow'];
			}
		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}

	
}

?>
