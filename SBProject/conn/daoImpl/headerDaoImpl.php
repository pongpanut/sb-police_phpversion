<?php   namespace SB\conn\daoImpl;
//session_start();
@session_start();
use  SB\conn\connection;
use  SB\model\headerM;
use  SB\conn\dao\headerDao;
include_once 'conn/connection.php';
include 'model/headerM.php';

class headerDaoImpl implements headerDao{
	
	public function loadHeader($pageType){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql=" SELECT m.* FROM header as m  WHERE m.header_page_type_id ='$pageType' order by header_id desc";
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$headerM = new headerM();
				$headerM->setHeaderId($row['header_id']);
				$headerM->setHeaderLogo($row['header_logo']);
				$headerM->setHeaderText($row['header_text']);
				$headerM->setHeaderTitle($row['header_title']);
				$headerM->setHeaderPageTypeId($row['header_page_type_id']);
				array_push($data,$headerM);
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $data;
	}
	

}

?>
