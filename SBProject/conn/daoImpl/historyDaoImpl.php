<?php namespace SB\conn\daoImpl; 
use  SB\conn\connection;
use  SB\model\historyM;
use  SB\conn\dao\historyDAO;

include_once 'conn/connection.php';
include 'model/historyM.php';
class historyDaoImpl{
	public function loadHistoryById($id,$page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$sql="SELECT history_id, history_body FROM history where history_id =$id  and page_type_id=$page_type_id order by history_id desc";
		$result = mysqli_query($db, $sql);
		$historyM = new historyM();
		
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$historyM->setHistoryId($row['history_id']);
				$historyM->setHistoryBody($row['history_body']);
				
			}
	
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $historyM;
	}
	
	
}
?>
