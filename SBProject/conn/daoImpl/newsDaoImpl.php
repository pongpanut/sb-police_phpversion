<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\newsM;
use  SB\conn\dao\newsDao;

	  include_once 'conn/connection.php';
      include 'model/newsM.php';

class newsDaoImpl implements newsDao{
	
	public function loadNews($news_code,$page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT news_id, news_code, news_header, news_detail , pdf_path, rec_date, is_highlight FROM news where news_code='$news_code' and page_type_id=$page_type_id   order by is_highlight desc, news_id desc ";
		$result = mysqli_query($db, $sql);
		
		if (!empty($result)  && mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$newsM = new newsM();
				$newsM->setNewsId($row['news_id']);
				$newsM->setNewsCode($row['news_code']);
				$newsM->setNewsHeader($row['news_header']);
				$newsM->setNewsDetail($row['news_detail']);
				$newsM->setNewsPdf($row['pdf_path']);
				$newsM->setRecDate($row['rec_date']);
				$newsM->setIsHighLight($row['is_highlight']);
				array_push($data,$newsM);
			}

		} else {
			//echo "0 results";
		}
		
		mysqli_close($db);
		return $data;
	}
	
	
	public function loadNewsById($news_id,$page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$newsM = new newsM();
		$sql="SELECT news_id, news_code, news_header, news_detail , pdf_path, rec_date, is_highlight FROM news where news_id ='$news_id' and page_type_id=$page_type_id  order by news_id desc";
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				
				$newsM->setNewsId($row['news_id']);
				$newsM->setNewsCode($row['news_code']);
				$newsM->setNewsHeader($row['news_header']);
				$newsM->setNewsDetail($row['news_detail']);
				$newsM->setNewsPdf($row['pdf_path']);
				$newsM->setRecDate($row['rec_date']);
				$newsM->setIsHighLight($row['is_highlight']);
			}
	
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $newsM;
	}
}

?>
