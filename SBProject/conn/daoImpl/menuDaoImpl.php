<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\menuM;
use  SB\model\submenuM;
use  SB\model\submenulistM;
use  SB\conn\dao\menuDao;
	  include_once 'conn/connection.php';
      include 'model/menuM.php';
      include 'model/submenuM.php';
      include 'model/submenulistM.php';


class menuDaoImpl implements menuDao{
	
	public function loadMenu($page_type){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT DISTINCT m.menu_id, m.menu_text,m.menu_page_type,s.link,m.has_submenu, m.is_department  FROM menu as m LEFT join staticpage as s  ON  m.menu_link = s.id WHERE m.menu_page_type = '$page_type' order by seq asc";
		$result = mysqli_query($db, $sql);
		
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$menuM = new menuM();	
				$menuM->setMenuId($row['menu_id']);
				$menuM->setMenuText($row['menu_text']);
				$menuM->setMenuPageType($row['menu_page_type']);
				$menuM->setMenuLink($row['link']);
				$menuM->setMenuHasSubmenu($row['has_submenu']);
				$menuM->setMenuIsDepartment($row['is_department']);
				array_push($data,$menuM);
			}

		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}

	public function loadSubMenu($id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$newquery = "SELECT DISTINCT m.submenu_id,m.submenu_text,s.link,m.submenu_has_menulist,m.menu_id,m.page_type_id FROM submenu as m  LEFT join staticpage as s  ON  m.submenu_link = s.id  where m.menu_id= '$id' ORDER BY  m.seq ASC";


		$resultSub = mysqli_query($db, $newquery);
		if (mysqli_num_rows($resultSub) > 0) {
			while($row = mysqli_fetch_assoc($resultSub)) {
				$submenuM = new submenuM();	
				$submenuM->setSubMenuId($row['submenu_id']);
				$submenuM->setSubMenuText($row['submenu_text']);				
				$submenuM->setSubMenuLink($row['link']);
				$submenuM->setSubmenuHasMenuList($row['submenu_has_menulist']);
				$submenuM->setMenuId($row['menu_id']);
				array_push($data,$submenuM);
			}

		} else {
			//echo "0 results";
		} 
		mysqli_close($db);
		return $data;
	}

	public function loadSubMenuList($id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$newquery = "SELECT DISTINCT m.submenu_list_id,m.submenu_list_text,s.link,m.submenu_id,m.page_type_id FROM submenu_list as m LEFT join staticpage as s ON m.submenu_list_link = s.id where m.submenu_id= '$id' ORDER BY  m.seq ASC";
		$resultSub = mysqli_query($db, $newquery);
		if (mysqli_num_rows($resultSub) > 0) {
			while($row = mysqli_fetch_assoc($resultSub)) {
				$submenuListM = new submenulistM();	
				$submenuListM->setSubmenuListId($row['submenu_list_id']);
				$submenuListM->setSubmenuListText($row['submenu_list_text']);				
				$submenuListM->setSubmenuListLink($row['link']);
				$submenuListM->setSubmenuId($row['submenu_id']);
				array_push($data,$submenuListM);
			}

		} else {
			//echo "0 results";
		} 
		mysqli_close($db);
		return $data;
	}
}

?>
