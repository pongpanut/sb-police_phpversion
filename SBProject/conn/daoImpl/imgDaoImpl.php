<?php   namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\imageM;
use  SB\conn\dao\imgDao;
include_once 'conn/connection.php';
include 'model/imageM.php';

class imgDaoImpl implements imgDao{
	
	public function loadImg($type,$page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		
		$data =  array();
		$sql="SELECT img_id, img_desc, img_name, img_path ,img_type ,img_header ,seq FROM image where img_type = '$type' AND page_type_id =' $page_type_id' order by img_id desc";
		$result = mysqli_query($db, $sql);
		
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$imageM = new imageM();
				$imageM->setImgid($row['img_id']);
				$imageM->setImgdesc($row['img_desc']);
				$imageM->setImgname($row['img_name']);
				$imageM->setImgpath($row['img_path']);
				$imageM->setImgtype($row['img_type']);
				$imageM->setImgheader($row['img_header']);
				$imageM->setSeq($row['seq']);
				array_push($data,$imageM);
			}
			
		
		} else {
			//echo "0 results";
		}
		
		mysqli_close($db);
		return $data;
	}
	
	public function loadImgModal($type,$ref_id,$page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT img_id, img_desc, img_name, img_path ,img_type ,img_header FROM image where img_type = '$type' and ref_id= $ref_id order by img_id desc";
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$imageM = new imageM();
				$imageM->setImgid($row['img_id']);
				$imageM->setImgdesc($row['img_desc']);
				$imageM->setImgname($row['img_name']);
				$imageM->setImgpath($row['img_path']);
				$imageM->setImgtype($row['img_type']);
				$imageM->setImgheader($row['img_header']);
				array_push($data,$imageM);
			}
				
	
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $data;
	}
	public function loadImgById($id,$page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
	
		$data =  array();
		$sql="SELECT img_id, img_desc, img_name, img_path ,img_type ,img_header FROM image where img_id = $id and page_type_id=$page_type_id  order by img_id desc	";
		$result = mysqli_query($db, $sql);
		if ($result != null && mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$imageM = new imageM();
				$imageM->setImgid($row['img_id']);
				$imageM->setImgdesc($row['img_desc']);
				$imageM->setImgname($row['img_name']);
				$imageM->setImgpath($row['img_path']);
				$imageM->setImgtype($row['img_type']);
				$imageM->setImgheader($row['img_header']);
				array_push($data,$imageM);
			}
				
	
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $data;
	}
}

?>
