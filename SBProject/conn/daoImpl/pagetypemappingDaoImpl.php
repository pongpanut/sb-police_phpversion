<?php   namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\constantM;
include_once 'conn/connection.php';
include_once 'model/constantM.php';

class pagetypemappingDaoImpl{
	
	public function loadpagetypemappingId($cons_type){
		$conn =new  connection();
		$db = $conn->getConnection();
		$sql=" SELECT page_type_id as pagetypeid FROM  pagetype_mapping WHERE page_type_text='$cons_type' ";
		$result = mysqli_query($db, $sql);
		$data = '';
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$data= $row['pagetypeid'];
			
			}
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function loadpagetypemappingText($Id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$sql=" SELECT page_type_text as pagetypetext FROM  pagetype_mapping WHERE page_type_id ='$Id' ";
		//echo $sql;
		$result = mysqli_query($db, $sql);
		$data = '';
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$data= $row['pagetypetext'];
					
			}
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $data;
	}

}

?>
