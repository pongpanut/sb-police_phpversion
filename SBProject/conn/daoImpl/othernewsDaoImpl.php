<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use SB\model\other_newsM;
	  include_once 'conn/connection.php';
	  include 'model/other_newsM.php';
class othernewsDaoImpl{
	
	
	public function InsertOtherNews($other_newsM){
		
		$otherNews_header =$other_newsM->getOtherNewsHeader();
		$otherNews_detail=$other_newsM->getOtherNewsDetail();
		$page_type = $other_newsM->getPageTypeId();		
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="INSERT INTO other_news ( other_news_header, other_news_detail, other_news_date, pagetype) VALUES ('$otherNews_header', '$otherNews_detail',current_date,$page_type );";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function UpdateOtherNews($other_newsM){
		
		$id =$other_newsM->getOtherNewsId();
		$otherNews_header =$other_newsM->getOtherNewsHeader();
		$otherNews_detail=$other_newsM->getOtherNewsDetail();
		$page_type = $other_newsM->getPageTypeId();		

	
		if($other_newsM->getDocUpload()!=""){
			$doc_upload =$other_newsM->getDocUpload();
		}
	
		
		
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="UPDATE other_news SET  other_news_header = '$otherNews_header', other_news_detail = '$otherNews_detail', other_news_date = current_data, pagetype = $page_type WHERE other_news_id = $id ";
		

		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
public function loadOtherNews($page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		
		$data =  array();
		$sql=" SELECT other_news_id, other_news_header, other_news_detail, other_news_date, pagetype FROM other_news  where pagetype=$page_type_id order by other_news_id desc";
		$result = mysqli_query($db, $sql);
		
		if($result != null){
			if (mysqli_num_rows($result) > 0) {
				while($row = mysqli_fetch_assoc($result)) {
					$other_newsM = new other_newsM();
					$other_newsM->setOtherNewsId($row['other_news_id']);
					$other_newsM->setOtherNewsHeader($row['other_news_header']);
					$other_newsM->setOtherNewsDetail($row['other_news_detail']);
					$other_newsM->setOtherNewsDate($row['other_news_date']);
					$other_newsM->setPageTypeId($row['pagetype']);
					array_push($data,$other_newsM);
				}
				
			
			} else {
				//echo "0 results";
			}
		}
		
		mysqli_close($db);
		return $data;
	}
	
	public function deleteOtherNews($id){
		$idDel=$id;
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql="DELETE FROM other_news WHERE other_news_id = $idDel";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	public function getOtherNewsById($id){
		$conn =new  connection();
		$db = $conn->getConnection();
	
		
		$sql=" SELECT other_news_id, other_news_header, other_news_detail, other_news_date, pagetype FROM other_news  where other_news_id =$id order by other_news_id desc";
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$other_newsM = new other_newsM();
				$other_newsM->setOtherNewsId($row['other_news_id']);
				$other_newsM->setOtherNewsHeader($row['other_news_header']);
				$other_newsM->setOtherNewsDetail($row['other_news_detail']);
				$other_newsM->setOtherNewsDate($row['other_news_date']);
				$other_newsM->setPageTypeId($row['pagetype']);
				array_push($data,$other_newsM);
			
			}
				
	
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $other_newsM;
	}
	

	
}

?>
