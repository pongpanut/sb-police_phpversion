<?php  namespace SB\conn\daoImpl;
		use  SB\conn\connection;
		use  SB\model\missionM;
		use  SB\conn\dao\missionDao;

	  include_once 'conn/connection.php';
      include 'model/missionM.php';
     
      
      class missionDaoImpl implements missionDao{
      	public function loadMission($page_type_id){
      		$conn =new  connection();
      		$db = $conn->getConnection();
      		$data =  array();
      		$sql="SELECT mission_id, mission_text, mission_header, mission_image, mission_desc ,mission_type ,show_flag ,rec_date FROM mission where  page_type_id=$page_type_id  and show_flag ='Y' order by mission_id desc";
      		$result = mysqli_query($db, $sql);
      		
      		if (mysqli_num_rows($result) > 0) {
      			while($row = mysqli_fetch_assoc($result)) {
      				$missionM = new missionM();
      				$missionM->setMissionId($row['mission_id']);
      				$missionM->setMissionText($row['mission_text']);
      				$missionM->setMissionHeader($row['mission_header']);
      				$missionM->setMissionImage($row['mission_image']);
      				$missionM->setMissionDesc($row['mission_desc']);
      				$missionM->setMissionType($row['mission_type']);
      				$missionM->setShowFlag($row['show_flag']);
      				$missionM-setRecDate($row['rec_date']);
      				array_push($data,$missionM);
      			}
      		
      		} else {
      			//echo "0 results";
      		}
      		
      		mysqli_close($db);
      		return $data;
      	}
      	public function loadMissionDetail($missionId,$page_type_id){
      		$conn =new  connection();
      		$db = $conn->getConnection();
      		
      		$sql="SELECT mission_id, mission_text, mission_header, mission_image, mission_desc ,mission_type ,show_flag,rec_date FROM mission where mission_id =$missionId and page_type_id=$page_type_id and show_flag ='Y' order by mission_id desc";
      		$result = mysqli_query($db, $sql);
      		
      		if (mysqli_num_rows($result) > 0) {
      			while($row = mysqli_fetch_assoc($result)) {
      				$missionM = new missionM();
      				$missionM->setMissionId($row['mission_id']);
      				$missionM->setMissionText($row['mission_text']);
      				$missionM->setMissionHeader($row['mission_header']);
      				$missionM->setMissionImage($row['mission_image']);
      				$missionM->setMissionDesc($row['mission_desc']);
      				$missionM->setMissionType($row['mission_type']);
      				$missionM->setRecDate($row['rec_date']);
      				$missionM->setShowFlag($row['show_flag']);
      				
      			}
      		
      		} else {
      			//echo "0 results";
      		}
      		
      		mysqli_close($db);
      		return $missionM;
      	}
      	
      	
      	public function loadMissionAll($page_type_id){
      		$conn =new  connection();
      		$db = $conn->getConnection();
      		$data =  array();
      		$sql="SELECT mission_id, mission_text, mission_header, mission_image, mission_desc ,mission_type ,show_flag,rec_date FROM mission where  page_type_id=$page_type_id and show_flag ='Y' order by mission_id desc";
      		$result = mysqli_query($db, $sql);
      	
      		if (mysqli_num_rows($result) > 0) {
      			while($row = mysqli_fetch_assoc($result)) {
      				$missionM = new missionM();
      				$missionM->setMissionId($row['mission_id']);
      				$missionM->setMissionText($row['mission_text']);
      				$missionM->setMissionHeader($row['mission_header']);
      				$missionM->setMissionImage($row['mission_image']);
      				$missionM->setMissionDesc($row['mission_desc']);
      				$missionM->setMissionType($row['mission_type']);
      				$missionM->setShowFlag($row['show_flag']);
      				$missionM->setRecDate($row['rec_date']);
      				array_push($data,$missionM);
      			}
      	
      		} else {
      			//echo "0 results";
      		}
      	
      		mysqli_close($db);
      		return $data;
      	}
      	
      	public function loadMissionType($missionType,$page_type_id){
      		$conn =new  connection();
      		$db = $conn->getConnection();
      		$data =  array();
      		$sql="SELECT mission_id, mission_text, mission_header, mission_image, mission_desc ,mission_type ,show_flag,rec_date FROM mission where mission_type = '$missionType' and page_type_id=$page_type_id and show_flag ='Y' order by mission_id desc";
      		$result = mysqli_query($db, $sql);
      		 
      		if (mysqli_num_rows($result) > 0) {
      			while($row = mysqli_fetch_assoc($result)) {
      				$missionM = new missionM();
      				$missionM->setMissionId($row['mission_id']);
      				$missionM->setMissionText($row['mission_text']);
      				$missionM->setMissionHeader($row['mission_header']);
      				$missionM->setMissionImage($row['mission_image']);
      				$missionM->setMissionDesc($row['mission_desc']);
      				$missionM->setMissionType($row['mission_type']);
      				$missionM->setShowFlag($row['show_flag']);
      				$missionM->setRecDate($row['rec_date']);
      				array_push($data,$missionM);
      			}
      			 
      		} else {
      			//echo "0 results";
      		}
      		 
      		mysqli_close($db);
      		return $data;
      	}
      }