<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\documentM;


include_once 'conn/connection.php';
include 'model/documentM.php';


class documentDaoImpl{
	
	public function loadDoc($page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT doc_id, doc_header,doc_header_en, doc_detail, doc_upload, pagetype FROM document where  pagetype = $page_type_id order by doc_id desc";
		$result = mysqli_query($db, $sql);
	
		if($result != null){
			if (mysqli_num_rows($result) > 0) {
				while($row = mysqli_fetch_assoc($result)) {
					$documentM = new documentM();
					$documentM->setDocId($row['doc_id']);
					$documentM->setDocHeader($row['doc_header']);
					$documentM->setDocHeaderEN($row['doc_header_en']);
					$documentM->setDocDetail($row['doc_detail']);
					$documentM->setDocUpload($row['doc_upload']);
					$documentM->setPageTypeId($row['pagetype']);
					array_push($data,$documentM);
				}
					
		
			} else {
				//echo "0 results";
			}
		}
	
		mysqli_close($db);
		return $data;
	}

	
}

?>
