<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\staticPageM;
use  SB\conn\dao\staticpageDao;

	  include_once 'conn/connection.php';
      include 'model/staticPageM.php';

class staticpageDaoImpl implements staticpageDao{
	
	public function getPage($id,$page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$staticpageM = new staticPageM();
		$sql="SELECT id, text, link, is_active, page_type_id, html FROM staticpage where id=$id and page_type_id =$page_type_id order by id desc";
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				
				$staticpageM->setStaticpageId($row['id']);
				$staticpageM->setStaticpageText($row['text']);
				$staticpageM->setStaticpageLink($row['link']);
				$staticpageM->setStaticpageActive($row['is_active']);
				$staticpageM->setStaticPageTypeId($row['page_type_id']);
				$staticpageM->setStaticHtml($row['html']);
				
			}

		} else {
			//echo "0 results";
		}
		
		mysqli_close($db);
		return $staticpageM;
	}
}

?>
