<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\tab_activityM;
use  SB\model\tab_contentM;
use  SB\model\tab_modalM;
use  SB\conn\dao\tabDao;
	include_once 'conn/connection.php';
	include 'model/tab_activityM.php';
	include 'model/tab_contentM.php';
	include 'model/tab_modalM.php';
	
	class tabDaoImpl implements tabDao{
		public function loadTab($page_type_id){
		
			$conn =new  connection();
			$db = $conn->getConnection();
			$data =  array();
			$sql="SELECT
					    tab_activity.tabid,
					    tab_activity.tabheader,
					    tab_activity.tabdetail,
					    tab_activity.tabcode
					FROM
					    tab_activity where  page_type_id=$page_type_id";
			$result = mysqli_query($db, $sql);
			
			if (mysqli_num_rows($result) > 0) {
				while($row = mysqli_fetch_assoc($result)) {
					
					$tab_activityM = new tab_activityM();
					$tab_activityM->setTabId($row['tabid']);
					$tab_activityM->setTabHeader($row['tabheader']);
					$tab_activityM->setTabDetail($row['tabdetail']);
					$tab_activityM->setTabCode($row['tabcode']);
					$tab_activityM->setTabConentArr($this->loadTab_content($page_type_id));
					array_push($data,$tab_activityM);
				}
			
			} else {
				//echo "0 results";
			}
			
			mysqli_close($db);
			return $data;
			
		}
		private function loadTab_content($page_type_id){
			$conn =new  connection();
			$db = $conn->getConnection();
			$dataContent =  array();
			$sql="SELECT
				    tab_content.tab_content_id,
				    tab_content.tab_content_header,
				    tab_content.tab_content_detail,
				    tab_content.tab_content_image_id,
				    tab_content.tab_id
				FROM
				    tab_content
				    where  page_type_id=$page_type_id ";
			$result = mysqli_query($db, $sql);
				
			if (mysqli_num_rows($result) > 0) {
				while($row = mysqli_fetch_assoc($result)) {
					$tab_contentM = new tab_contentM();
					$tab_contentM->setTabContentId($row['tab_content_id']);
					$tab_contentM->setTabContentHeader($row['tab_content_header']);
					$tab_contentM->setTabContentDetail($row['tab_content_detail']);
					$tab_contentM->setTabContentImageId($row['tab_content_image_id']);
					$tab_contentM->setTabId($row['tab_id']);
					$tab_contentM->setTabModalArr($this->loadTab_Modal($row['tab_content_id'],$page_type_id));
					array_push($dataContent,$tab_contentM);
				}
			
		}
		return $dataContent;
		
	}
	
	private function loadTab_Modal($tab_content_id,$page_type_id){

		$conn =new  connection();
		$db = $conn->getConnection();
		$dataModal =  array();
		$sql="SELECT modal_id,modal_header, modal_content, tab_content_id,rec_date  FROM tab_modal where tab_content_id =$tab_content_id and page_type_id=$page_type_id";
		$result = mysqli_query($db, $sql);
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$tab_modalM = new tab_modalM();
				$tab_modalM->setTabModalId($row['modal_id']);
				$tab_modalM->setTabModalHeader($row['modal_header']);
				$tab_modalM->setTabModalContent($row['modal_content']);
				$tab_modalM->setTabContentId($row['tab_content_id']);
				$tab_modalM->setRecDate($row['rec_date']);
				array_push($dataModal,$tab_modalM);
			}
				
		}
		return $dataModal;
	}
	
}
?>