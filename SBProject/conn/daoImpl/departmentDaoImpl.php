<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\departmentM;
use  SB\model\departmentLiM;
use  SB\model\departmentSubM;

include_once 'conn/connection.php';
include 'model/departmentM.php';
include 'model/departmentLiM.php';
include 'model/departmentSubM.php';

class departmentDaoImpl{
	
	public function loadDepartment($page_type){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT d.dept_id, d.dept_text, d.dept_link, d.page_type_id FROM department as d WHERE d.page_type_id = '$page_type' order by d.dept_id desc";
		$result = mysqli_query($db, $sql);
		
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$departmentM = new departmentM();	
				$departmentM->setDeptId($row['dept_id']);
				$departmentM->setDeptText($row['dept_text']);
				$departmentM->setDeptLink($row['dept_link']);
				$departmentM->setPageTypeId($row['page_type_id']);
				array_push($data,$departmentM);
			}

		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	
	public function loadSubDepartmentList($id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT dept_sub_id, dept_sub_text, dept_sub_link, dept_li_id, page_type_id, seq FROM department_sub WHERE dept_li_id = '$id' order by dept_sub_id desc";
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$departmentSubM = new departmentSubM();
				$departmentSubM->setDeptSubId($row['dept_sub_id']);
				$departmentSubM->setDeptSubText($row['dept_sub_text']);
				$departmentSubM->setDeptSubLink($row['dept_sub_link']);
				$departmentSubM->setDeptId($row['dept_li_id']);
				$departmentSubM->setPageTypeId($row['page_type_id']);
				$departmentSubM->setSeq($row['seq']);
				array_push($data,$departmentSubM);
			}
	
		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}
	
	public function loadDepartmentList($page_type){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql=" SELECT  d.dept_li_id, d.dept_li_text, d.dept_li_link, d.page_type_id, d.seq FROM department_list as d WHERE d.page_type_id = '$page_type' order by dept_sub_id desc";
		$result = mysqli_query($db, $sql);
		if (!empty($result)&& mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$departmentLiM = new departmentLiM();
				$departmentLiM->setDeptListId($row['dept_li_id']);
				$departmentLiM->setDeptListText($row['dept_li_text']);
				$departmentLiM->setDeptListLink($row['dept_li_link']);
				$departmentLiM->setPageTypeId($row['page_type_id']);
				$departmentLiM->setSeq($row['seq']);
				array_push($data,$departmentLiM);
			}
	
		} else {
			//echo "0 results";
		}
		//echo 'data num :'.count($data);
		mysqli_close($db);
		return $data;
	}

	
}

?>
