<?php   namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\contactM;
use  SB\conn\dao\contactDao;
include_once 'conn/connection.php';
include 'model/contactM.php';

class contactDaoImpl implements contactDao{
	
	public function insertContact($contact_name,$contact_email,$contact_phone, $contact_message, $contact_page_type_id){
		$conn =new  connection();
		$conn =new  connection();
		$db = $conn->getConnection();
		$sql="	INSERT INTO contact(id, name, email, phone, message, page_type_id) VALUES ('','$contact_name','$contact_email','$contact_phone','$contact_message', '$contact_page_type_id')";
		$data = false;
		if (mysqli_query($db, $sql)) {
			$data = true;
			echo "เพิ่ม ".$contact_name." ลงในติดต่อสอบถามเรียบร้อยแล้ว";
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
		
		mysqli_close($db);
		return $data;
	}
	
	public function loadContact($contact_page_type_id){
		$conn =new  connection();
		$conn =new  connection();
		$db = $conn->getConnection();
		$data =  array();
		$sql="SELECT id, name, email, phone, message, page_type_id , answer FROM contact where page_type_id ='$contact_page_type_id' and answer is not null order by id desc";
		$result = mysqli_query($db, $sql);
	
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$contactM = new contactM();
				$contactM->setContactId($row['id']);
				$contactM->setContactText($row['name']);
				$contactM->setContactEmail($row['email']);
				$contactM->setContactPhone($row['phone']);
				$contactM->setContactMessage($row['message']);
				$contactM->setContactPageType($row['page_type_id']);
				$contactM->setAns($row['answer']);
				array_push($data,$contactM);
			}
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $data;
	}
	

}

?>
