<?php   namespace SB\conn\daoImpl;
use  SB\conn\connection;
use  SB\model\contactInfoM;
include_once 'conn/connection.php';
include_once 'model/contactInfoM.php';

class contactInfoDaoImpl{
	
	public function loadContactInfo($page_type_id){
		$conn =new  connection();
		$conn =new  connection();
		$db = $conn->getConnection();
		$sql="SELECT department_contact_id, location, contact_info, tel, page_type_id , email FROM department_contact where page_type_id  = $page_type_id";
		$result = mysqli_query($db, $sql);
		$contactInfoM = new contactInfoM();
		if (!empty($result) && mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$contactInfoM->setDepartmentContactId($row['department_contact_id']);
				$contactInfoM->setLocation($row['location']);
				$contactInfoM->setContactInfo($row['contact_info']);
				$contactInfoM->setTel($row['tel']);
				$contactInfoM->setPageTypeId($row['page_type_id']);
				$contactInfoM->setEmail($row['email']);
			}
		} else {
			//echo "0 results";
		}
	
		mysqli_close($db);
		return $contactInfoM;
	}
	
	public function deleteContactInfo($id){
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		
		$sql="DELETE FROM department_contact WHERE department_contact_id = $id ";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
		
		mysqli_close($db);
		return $data;
	}
	
	public function UpdateContactInfo($contactInfoM){
			$id =	$contactInfoM->getDepartmentContactId();
			$pageTypeId =$contactInfoM->getPageTypeId();
			$location = $contactInfoM->getLocation();
			$contactInfo =$contactInfoM->getContactInfo();
			$tel = $contactInfoM->getTel();
			$email = $contactInfoM->getEmail();
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql=" UPDATE department_contact SET department_contact_id = $id, location = '$location', contact_info = '$contactInfo', tel = '$tel', page_type_id = $pageTypeId , email ='$email'  WHERE department_contact_id = $id  ";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	public function InsertContactInfo($contactInfoM){
	
			$pageTypeId =$contactInfoM->getPageTypeId();
			$location = $contactInfoM->getLocation();
			$contactInfo =$contactInfoM->getContactInfo();
			$tel = $contactInfoM->getTel();
			$email =$contactInfoM->getEmail();
	
		$conn =new  connection();
		$db = $conn->getConnection();
		$data = false;
		$sql=" INSERT INTO department_contact ( location, contact_info, tel, page_type_id , email ) VALUES ('$location', '$contactInfo', '$tel', $pageTypeId, '$email') ";
		if (mysqli_query($db, $sql)) {
			$data=true;
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	
		mysqli_close($db);
		return $data;
	}
	
	

}

?>
