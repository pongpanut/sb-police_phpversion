<?php  namespace SB\conn\daoImpl;
use  SB\conn\connection;
use SB\model\calendarM;
	  include_once 'conn/connection.php';
	  include_once 'model/calendarM.php';
class calendarDaoImpl{
	
	public function loadCalendar($page_type_id){
		$conn =new  connection();
		$db = $conn->getConnection();
		
		$sql=" SELECT calendar_id, calendar_name, calendar_body, page_type_id FROM calendar  where page_type_id =$page_type_id order by calendar_id desc";
		$result = mysqli_query($db, $sql);
		$calendarM = new calendarM();
		if ($result!=null && mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				$calendarM->setCalendarId($row['calendar_id']);
				$calendarM->setCalendarName($row['calendar_name']);
				$calendarM->setCalendarBody($row['calendar_body']);
				$calendarM->setPageTypeId($row['page_type_id']);
			
			}
	
		} else {
			//echo "0 results";
		}
		mysqli_close($db);
		return $calendarM;
	}
		
}

?>
