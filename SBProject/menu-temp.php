<div class="row">
				<nav class="navbar navbar-manual-menu">
					  <div class="container-fluid">
					    <div class="navbar-header">
					      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>                        
					      </button>
					      <a class="navbar-brand" href="../SBProject/"><span class="glyphicon glyphicon-home" style="color:red"></span></a>
					    </div>
					    <div class="collapse navbar-collapse" id="myNavbar">
					      <ul class="nav navbar-nav">
					         <li class="dropdown">
					          	<a class="dropdown-toggle" data-toggle="dropdown" href="#">เกี่ยวกับ<span class="caret"></span></a>
						          <ul class="dropdown-menu">
						          		<li><a href="../SBProject/history.php">ประวัติ</a></li>
		                                <li><a href="#">ที่ตั้ง</a></li>
		                                <li><a href="../SBProject/planstructure.php">โครงสร้างองค์กร</a></li>
		                               	<li class="sub-menu"><a href="#">หน่วยงานในสังกัด</a>
		                                        <ul>
		                                         <li><a href="#">บก.อก.บช.ส</a></li>
		                                          <li><a href="#">บก.ส.1</a></li>
		                                          <li><a href="#">บก.ส.2</a></li>
		                                          <li><a href="#">บก.ส.3</a></li>
		                                          <li><a href="#">บก.ส.4</a></li>
		                                          <li><a href="#">ศูนย์พัฒนาด้านการข่าว</a></li>
		                                        </ul>
		                                      </li>
						          </ul>
					        </li>
						         <li class="dropdown">
					          	<a class="dropdown-toggle" data-toggle="dropdown" href="#">ผู้บริหาร<span class="caret"></span></a>
						          <ul class="dropdown-menu">
						          		<li class="sub-menu"><a href="../SBProject/comandor-old.php">อดีตผู้บริหาร</a>
		                                        <ul>
		                                          <li><a href="../SBProject/comandor-old.php">อดีต ผบช.ส</a></li>
		                                        </ul>
		                                      </li>        
		                               	<li class="sub-menu"><a href="../SBProject/comandor-current.php">ผู้บริหารปัจจุบัน</a>
		                                        <ul>
		                                          <li><a href="../SBProject/comandor-current.php">ผบช.ส-รอง ผบช.ส</a></li>
		                                        <li><a href="../SBProject/comandor-current.php">ผบก.อก</a></li>
		                                        <li><a href="../SBProject/comandor-current.php">ผบก.ประจำฯ</a></li>
		                                        <li><a href="../SBProject/comandor-current.php">ผบก.ส. 1-4</a></li>
		                                        <li><a href="../SBProject/comandor-current.php">รอง ผบก.ศพข.</a></li>
		                                        <li><a href="../SBProject/comandor-current.php">รอง ผบก.กชข.</a></li>
		                                        </ul>
		                                 </li>
						          </ul>
					        </li>      
					        <li class="dropdown">
					          	<a class="dropdown-toggle" data-toggle="dropdown" href="#">ยุทธศาสตร์<span class="caret"></span></a>
						          <ul class="dropdown-menu">
						         	<li><a href="../SBProject/vision.php">วิสัยทัศน์</a></li>
		                                  <li><a href="../SBProject/vision.php">พันธกิจ</a></li>
		                                  <li><a href="#">แผนยุทธศาสตร์</a></li>
						          		   <li class="sub-menu"><a href="#">นโยบาย</a>
		                                        <ul>
		                                         	<li><a href="../SBProject/pdf/goverment/goverment2557.pdf">นโยบายรัฐบาล</a></li>
			                                        <li><a href="../SBProject/pdf/goverment/police2557.pdf">นโยบาย ผบ.ตร.</a></li>
			                                        <li><a href="../SBProject/pdf/goverment/sbpolice2558.pdf">นโยบาย ผบช.ส.</a></li>
		                                        </ul>
		                                    </li>
						          </ul>
					        </li>        
					        <li class="dropdown">
					          	<a class="dropdown-toggle" data-toggle="dropdown" href="#">ศูนย์ความรู้<span class="caret"></span></a>
						          <ul class="dropdown-menu">
						         			<li><a href="#">กฎหมายที่เกี่ยวข้อง</a></li>
	                                        <li><a href="#">องค์ความรู้ด้านรักษาความปลอดภัย</a></li>
	                                        <li><a href="#">องค์ความรู้ด้านการข่าว</a></li>
	                                        <li><a href="#">องค์ความรู้ด้านงานอำนวยการ</a></li>
	                                        <li><a href="#">องค์ความรู้ด้านเทคโนโลยี</a></li>
	                                        <li><a href="#">องค์ความรู้ด้านภาษาต่างประเทศ</a></li>
	                                        <li><a href="#">ดาวน์โหลด</a></li>
						          </ul>
					        </li>   
					      <li><a href="index-1.html">ร้องเรียน/แจ้งข่าว</a></li>
                          <li><a href="index-1.html">งานสัญชาติ</a></li>
                          <li><a href="index-1.html">ศูนย์บริการออกหนังสือรับรองความประพฤติ</a></li>
                          <li><a href="calendar.html">ปฎิทินกิจกรรม</a></li>  
                          
                            <li class="dropdown">
					          	<a class="dropdown-toggle" data-toggle="dropdown" href="#">เมนูใช้บ่อย<span class="caret"></span></a>
						          <ul class="dropdown-menu">
						         	<li><a href="http://speexx.edupol.org/index.php?st=1&v=12,0,0">	speexx	</a></li>
										 <li><a href="index-1.html">	ระบบสมาชิกแจ้งข่าวสันติบาล	</a></li>
										 <li><a href="http://webmail.sbpolice.go.th/">	E-mail สันติบาล	</a></li>
										 <li><a href="http://www.sbpolice.go.th/page-register-email.php">	แบบฟอร์มขอใช้ E-mail สันติบาล	</a></li>
										 <li><a href="http://www.gsd.sbpolice.go.th/webboard/webboard.php">	หนังสือเวียนสันติบาล	</a></li>
										 <li><a href="http://www.gsd.sbpolice.go.th/assignment/logintoassignment.php">	ระบบตอบโต้หนังสือ สันติบาล	</a></li>
										 <li><a href="http://www.saranitet.police.go.th/phonebook/index.php">	ระบบฐานข้อมูลโทรศัพท์ สำนักงานตำรวจแห่งชาติ Police PhoneBook	</a></li>
										 <li><a href="http://sbpolice.go.th/phonebook/p_search.php?id=1">	สมุดโทรศัพท์ สันติบาล	</a></li>
										 <li><a href="http://115.31.176.124/sboc4">	ระบบ SBOC ศปก.บช.ส.	</a></li>
										 <li><a href="http://saraban-test.egov.go.th/">	E-Doc	</a></li>
										 <li><a href="https://web2.ginconference.com/login/">	Conference	</a></li>
										 <li><a href="http://sbpolice.go.th/page-manual-conference.php">	คู่มือการใช้งาน Conference	</a></li>
										 <li><a href="https://accounts.gchat.apps.go.th">	G-Chat	</a></li>
										 <li><a href="http://sbpolice.go.th/file/คู่มือการใช้งาน G-Chat.pdf">	G-Chat(คู่มือการใช้งาน)	</a></li>
						         	</ul>
						         </li>
                          
                          
                          
                          
                          
                          </ul>
					    </div>
					  </div>
					</nav>
				</div>