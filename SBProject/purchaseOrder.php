<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SBPolice</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/submenu.css" type="text/css" media="screen">



<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>




</head>
	<body class="background">
		<div class="container header">
			<div class="row">
				<div class="col-md-12">
						<div class="col-md-2"><img src="images/SBlogo.png" class="img-responsive" alt="Responsive image"></div>
						<div class="col-md-10">
							<div class="sbText1">กองบัญชาการตํารวจสันติบาล</div>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 navMenu ">
								<div class="col-md-offset-7">
										<div class=" login-menu">
												<nav class="navbar navbar-manual-login">
												  <div class="container-fluid">
												    <div class="navbar-header">
												      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
												        <span class="icon-bar"></span>
												        <span class="icon-bar"></span>
												        <span class="icon-bar"></span>                        
												      </button>
												    </div>
												    <div class="collapse navbar-collapse" id="myNavbar">
												      <ul class="nav navbar-nav">
												        <li class="dropdown">
															<a class="dropdown-toggle" href="#" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>  เข้าสู่ระบบ</a>
															<div class="dropdown-menu login" >
																<form method="post" action="login" accept-charset="UTF-8">
																	<input style="margin-bottom: 15px;" type="text" placeholder="Username" id="username" name="username">
																	<input style="margin-bottom: 15px;" type="password" placeholder="Password" id="password" name="password">
																	<input style="float: left; margin-right: 10px;" type="checkbox" name="remember-me" id="remember-me" value="1">
																	<label class="string optional" for="user_remember_me" style="color:white;"> จำการเข้าสู่ระบบ</label>
																	<input class="btn btn-block" type="submit" id="sign-in" value="Sign In">
					
																</form>
															</div>
														</li>
												      </ul>
												      <ul class="nav navbar-nav">
												        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span>  ติดต่อเรา</a></li>
												      </ul>
												      <ul class="social-icons">
									                        <li><a href="#"><img src="images/icon-1.png" alt=""></a></li>
									                        <li><a href="#"><img src="images/icon-2.png" alt=""></a></li>
									                        <li><a href="#"><img src="images/icon-3.png" alt=""></a></li>
									                        <li><a href="#"><img src="images/icon-4.png" alt=""></a></li>
								                        </ul>
												    </div>
												  </div>
												</nav>
											</div>
									</div>
						</div>
			
			</div>
			<?php include '../SBProject/menu.php';?>
		</div><!-- end header -->
		
		<div class="container content-slide"> <!-- imageSlidsShow -->
			<div class="row">
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				    <li data-target="#myCarousel" data-slide-to="1"></li>
				    <li data-target="#myCarousel" data-slide-to="2"></li>
				    <li data-target="#myCarousel" data-slide-to="3"></li>
				  </ol>
				
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner img-slide" role="listbox">
				    <div class="item active">
				      <img src="images/slideshow/bike.png" class="img-responsive" alt="Responsive image" alt="Chania">
				    </div>
				
				    <div class="item">
				      <img src="images/slideshow/police_commander_06_10_2558.png" class="img-responsive" alt="Responsive image" alt="Chania">
				    </div>
				
				    <div class="item">
				      <img src="images/slideshow/sb_commander_06_10_2558.png" class="img-responsive" alt="Responsive image"  alt="Flower">
				    </div>
				
				    <div class="item">
				      <img src="images/slideshow/SB-17-12-57.png" class="img-responsive" alt="Responsive image" alt="Flower">
				    </div>
				  </div>
				
				  <!-- Left and right controls -->
				  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
		</div><!-- END imageSlidsShow -->
	<!-- 	content Body -->
		<div class="container content">
			<div class="row "> 
				<div class="col-md-6 headerText">ภารกิจผู้บริหาร</div>
				<div class="col-md-6 headerText">กิจกรรมหน่วย</div>
			</div>
			<div class="row">
				<div class="col-md-3" style="border-right: 1px solid #d0d2d3;">
					<div>
						<img class="img-thumbnail" alt="Responsive image" src="images/thumbnail/3.png">
					</div>
				<p class="h1">หัวข้อ 1 ในส่วนของ กิจกรรมหน่วย</p>
				<span class="h2">รายละเอียด ในส่วนของหัวข้อ 1 พร้อมปุ่มเพื่อไปอ่านรายละเอียด <a href="#">>></a></span>
			</div>
			<div class="col-md-3">
				<div>
					<img class="img-thumbnail" alt="Responsive image" src="images/thumbnail/4.png">
				</div>
				<p class="h1">หัวข้อ 2 ในส่วนของ ภารกิจผู้บริหาร</p>
				<span class="h2">รายละเอียด ในส่วนของหัวข้อ 2 พร้อมปุ่มเพื่อไปอ่านรายละเอียด >> <a href="#">>></a></span>
			
			</div>
			<div class="col-md-3" style="border-right: 1px solid #d0d2d3;">
				<div>
					<img class="img-thumbnail" alt="Responsive image" src="images/thumbnail/3.png">
				</div>
				<p class="h1">หัวข้อ 1 ในส่วนของ กิจกรรมหน่วย</p>
				<span class="h2">รายละเอียด ในส่วนของหัวข้อ 1 พร้อมปุ่มเพื่อไปอ่านรายละเอียด <a href="#">>></a></span>
			</div>
			<div class="col-md-3">
				<div>
					<img class="img-thumbnail" alt="Responsive image" src="images/thumbnail/4.png">
				</div>
				<p class="h1">หัวข้อ 2 ในส่วนของ ภารกิจผู้บริหาร</p>
				<span class="h2">รายละเอียด ในส่วนของหัวข้อ 2 พร้อมปุ่มเพื่อไปอ่านรายละเอียด >> <a href="#">>></a></span>
			</div>
		</div>
		
		<div class="advertise">
			<div class="row">
				<div class="col-md-12 box">
				<?php  
				$index = $_GET['index'];
					switch ($index) {
						case 1:
							?>
								<div class="row">
									<div class="col-md-12 h1"><img class="img-thumbnail" alt="Responsive image" src="images/icon/icon-sb.gif">&nbsp; &nbsp;	 โครงการเช่ายานพาหนะสำหรับภารกิจสืบสวนหาข่าว 52 คัน [วันที่ 28 สิงหาคม 2558] </div>
								</div>
								<div class="row ">
										<img class="img-responsive" alt="Responsive image" src="images/non-news.gif">
								</div>
								<div class="row download text-center">
									<span class="glyphicon glyphicon-download" style="color:green"></span> <a href="pdf/purchaseOrder/img-303135725-web.pdf">ดาวน์โหลดไฟล์แนบ</a>
								</div>
							<?php 
							break;
						case 2:
							?>
							
								<div class="row">
									<div class="col-md-12 h1"><img class="img-thumbnail" alt="Responsive image" src="images/icon/icon-sb.gif">&nbsp; &nbsp;	 ขายทอดตลาด รถเก๋ง ยี่ห้อนิสสัน ทะเบียนโล่ 06083 2 คัน  [วันที่ 15 กุมภาพันธ์ 2559] </div>
								</div>
								<div class="row ">
										<img class="img-responsive" alt="Responsive image" src="images/non-news.gif">
								</div>
								<div class="row download text-center">
									<span class="glyphicon glyphicon-download" style="color:green"></span> <a href="pdf/purchaseOrder/PDF.pdf">ดาวน์โหลดไฟล์แนบ</a>
								</div>
								<?php 
								break;
						case 3:?>
								<div class="row">
									<div class="col-md-12 h1"><img class="img-thumbnail" alt="Responsive image" src="images/icon/icon-sb.gif">&nbsp; &nbsp;	ขายทอดตลาด ครุภัณฑ์ยำนพำหนะ รถยนต์เก๋ง ยี่ห้อ วอลโว่ รุ่น ๙๖๐  [วันที่ 5 กุมภาพันธ์ 2559] </div>
								</div>
								<div class="row ">
										<img class="img-responsive" alt="Responsive image" src="images/non-news.gif">
								</div>
								<div class="row download text-center">
									<span class="glyphicon glyphicon-download" style="color:green"></span> <a href="pdf/purchaseOrder/volvo 960.pdf">ดาวน์โหลดไฟล์แนบ</a>
								</div>

							<?php 
							break;
						case 4:
							?>
								
								<div class="row">
									<div class="col-md-12 h1"><img class="img-thumbnail" alt="Responsive image" src="images/icon/icon-sb.gif">&nbsp; &nbsp;	  ประกาศ บช.ส. เรื่อง ประกวดราคาเช่ายานพาหนะสำหรับภารกิจสืบสวนหาข่าว จำนวน ๕๒ คัน ด้วยวิธีประกวดราคาอิเล็กทรอนิกส์ (e-bidding)  [วันที่ 2 ธันวาคม 2558] </div>
								</div>
								<div class="row ">
										<img class="img-responsive" alt="Responsive image" src="images/non-news.gif">
								</div>
								<div class="row download text-center">
									<span class="glyphicon glyphicon-download" style="color:green"></span> <a href="pdf/purchaseOrder/img-Z02142202.pdf">ดาวน์โหลดไฟล์แนบ</a>
								</div>
							
							<?php 
							break;
						case 5:
							?>
								
								<div class="row">
									<div class="col-md-12 h1"><img class="img-thumbnail" alt="Responsive image" src="images/icon/icon-sb.gif">&nbsp; &nbsp;	 โครงการเช่ายานพาหนะสำหรับภารกิจสืบสวนหาข่าว จำนวน 1 ชุด  [วันที่ 11 กันยายน 2558] </div>
								</div>
								<div class="row ">
										<img class="img-responsive" alt="Responsive image" src="images/non-news.gif">
								</div>
								<div class="row download text-center">
									<span class="glyphicon glyphicon-download" style="color:green"></span> <a href="pdf/purchaseOrder/lentcar.pdf">ดาวน์โหลดไฟล์แนบ</a>
								</div>
							
							<?php 
							break;
						case 6:
							?>
								
								<div class="row">
									<div class="col-md-12 h1"><img class="img-thumbnail" alt="Responsive image" src="images/icon/icon-sb.gif">&nbsp; &nbsp;	โครงการจัดหาครุภัณฑ์คอมพิวเตอร์สำหรับงานด้านการข่าว  [วันที่ 28 สิงหาคม 2558] </div>
								</div>
								<div class="row ">
										<img class="img-responsive" alt="Responsive image" src="images/non-news.gif">
								</div>
								<div class="row download text-center">
									<span class="glyphicon glyphicon-download" style="color:green"></span> <a href="pdf/purchaseOrder/img-828141921.pdf">ดาวน์โหลดไฟล์แนบ</a>
								</div>	
							<?php 
							break;
						default:
							echo "not found data";
					}
				?>
			
				
				
				</div>
				

			
			</div>
		</div>
		<!-- footer -->
		<footer>
  			<div class="col-md-offset-8 box" >
			        <form id="newsletter" method="post" >
			            <label>กรอกข้อมูลเพื่อรับข่าวสาร</label>
			            <div class="clearfix">
			                <input type="text" onFocus="if(this.value =='Enter e-mail here' ) this.value=''" onBlur="if(this.value=='') this.value='Enter e-mail here'" value="อีเมลล์ของคุณ" >
			                <a href="#" style="margin-right:50px;" onClick="document.getElementById('newsletter').submit()" class="btn btn_">subscribe</a>
			            </div>
			        </form>
			    </div>
		</footer>
		 <div class="span8 float" style="margin-left:50px;" >
		      	<ul class="footer-menu">
		        	<li><a href="index.html">หน้าหลัก</a>|</li>
		            <!--<li><a href="index-1.html" class="current">about</a>|</li>
		            <li><a href="index-2.html">Services</a>|</li>
		            <li><a href="index-3.html">collections</a>|</li>
		            <li><a href="index-4.html">styles</a>|</li>
		            <li><a href="index-5.html">Contacts</a></li>-->
		        </ul>
		      	กองบัญชาการตำรวจสันติบาล สำนักงานตำรวจแห่งชาติ.
				อาคาร 20 สำนักงานตำรวจแห่งชาติ, ถนนพระราม 1
				แขวงวังใหม่ เขตปทุมวัน กรุงเทพมหานคร, 10330 
		      </div>
		    </div>

	</body>
</html>