<?php namespace SB\view;

use SB\conn\factory\App_DaoFactory;
include_once 'conn/factory/factory.php';
$ini_array = parse_ini_file("sbpolice.ini");
include $ini_array['contextRoot'].'constant.php';
	include  'basepagetype.php';
	$headerDao = App_DaoFactory::getFactory()->getHeader();
	$headerArr =$headerDao->loadHeader($page_type_id);
	$num=count($headerArr);
	if($num >0){
?>

<div class="col-md-12 col-xs-12" style=" padding-left: 0px; padding-right: 0px;">
						<div class="col-md-2 col-xs-2 col-md-offset-0 " > 
						<?php if($headerArr[$num-1]->getHeaderLogo()!=""){?>
						<img src="<?php echo $headerArr[$num-1]->getHeaderLogo();?>" class="" alt="Responsive image" id="logo">
						<?php }else{?>
						<img src="<? echo $linkPrefix ?>images/imageNotFound.jpg" class=" alt="Responsive image" id="logo">
						<?php }?>
						</div>
						<div class="col-md-10 col-xs-10 col-md-offset-0">
							<div style="text-shadow: -1px 0 white, 0 1px white, 1px 0 white, 0 -1px white;" class="sbText1"><?php echo $headerArr[$num-1]->getHeaderText();?></div>
							<div style="text-shadow: -1px 0 white, 0 1px white, 1px 0 white, 0 -1px white;" class="sbText2"><?php echo $headerArr[$num-1]->getHeaderTitle();?></div>	
						</div>
</div>
<?php } else{?>
<div class="col-md-12 col-xs-12">
						<div class="col-md-2 col-xs-2  col-md-offset-0" style="><img src="<?php echo $linkPrefix ?>images/SBlogo.png" class="img-responsive" alt="Responsive image" style="height:150px;"></div>
						<div class="col-md-10 col-xs-10  col-md-offset-0">
							<div style="text-shadow: -1px 0 #DEA7A7, 0 2px black, -2px 0 black, 0 2px black;" class="sbText1">กรุณาระบุ Header</div>
							<br>
							<div style="text-shadow: -1px 0 #767EA2, 0 2px black, -2px 0 black, 0 2px black;" class="sbText2">กรุณาระบุรายละเอียด Tagline</div>	
						</div>
</div>
<?php }?>