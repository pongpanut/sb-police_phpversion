<?php namespace SB;
use SB\conn\factory\App_DaoFactory;
include_once 'conn/factory/factory.php';

$email = $_POST['email'];
$newsDao = App_DaoFactory::getFactory()->getSubscript();

if (empty($email)) {
	$emailErr = "Email is required";
	echo $emailErr;
} else {
	//check if e-mail address is well-formed
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$emailErr = "Invalid email format";
		echo $emailErr;
	}
}
 $existed  =$newsDao->CheckEmailExist($email);
if($existed == true){
	$emailErr = "Email นี้มีอยู่ในระบบแล้ว";
	echo $emailErr;
}

if(empty($emailErr)){
	$newsArr =$newsDao->Insertemail($email);
}

?>
